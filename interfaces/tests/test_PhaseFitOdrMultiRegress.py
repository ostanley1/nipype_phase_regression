from nipype_phase_regression.interfaces import PhaseFitOdrMultiRegress as phaseregress
import numpy as np
import pytest
import tempfile


def test_initialize():
    pr = phaseregress.PhaseFitOdrMultiRegress()
    assert('pr' in locals())

def test_setup():
    pr = phaseregress.PhaseFitOdrMultiRegress()
    # cannot commit nii.gz so must create one on the fly
    with tempfile.NamedTemporaryFile(suffix='.nii.gz') as temp:
        pr.inputs.mag = temp.name
        assert(pr.inputs.mag == temp.name)
        pr.inputs.phase = temp.name
        assert(pr.inputs.phase == temp.name)
    pr.inputs.TR = 2.5
    assert(pr.inputs.TR == 2.5)
    pr.inputs.sig_ub = -1
    assert (pr.inputs.sig_ub == -1)
    pr.inputs.sig_lb = -1
    assert (pr.inputs.sig_lb == -1)
    pr.inputs.noise_lb = 0.5
    assert(pr.inputs.noise_lb == 0.5)

def test_linear():
    pr = phaseregress.PhaseFitOdrMultiRegress()
    pr.inputs.TR = 2.5
    pr.inputs.sig_ub = -1
    pr.inputs.sig_lb = -1
    pr.inputs.noise_lb = 0.15

    np.random.seed(1234)
    beta = np.random.rand(2)
    ph = (np.random.randint(0, 1001, (5, 5, 5, 200)) + np.random.randint(50,100)).astype(float)
    mag = ph*beta[0]+beta[1]

    sim, filt, r2, stdm, stdp, betameas =pr._run_regression(mag, ph)

    assert(np.allclose(beta, betameas[1,1,1,:]))
    assert(np.allclose(sim,mag))

def test_linear_withsigfilt():
    pr = phaseregress.PhaseFitOdrMultiRegress()
    pr.inputs.TR = 2.5
    pr.inputs.sig_ub = 0.1
    pr.inputs.sig_lb = 0.01
    pr.inputs.noise_lb = 0.15

    np.random.seed(1234)
    beta = np.random.rand(2)
    ph = (np.random.randint(0, 1001, (5, 5, 5, 200)) + np.random.randint(50,100)).astype(float)
    mag = ph*beta[0]+beta[1]

    sim, filt, r2, stdm, stdp, betameas =pr._run_regression(mag, ph)

    assert(np.allclose(beta, betameas[1,1,1,:]))
    assert(np.allclose(sim,mag))