from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec, isdefined
import nibabel as nb
import numpy as np
import os
import matplotlib.pyplot as plt
import scipy.stats as stat
from scipy import odr as odr
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
from ..core import calc_pool_size
from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec, Info

class PlotFitsInputSpec(BaseInterfaceInputSpec):
    mag = File(exists=True, desc='input magnitufe to fit (bp)')
    phase = File(exists=True, desc='input phase to fit (bp)')
    sim = File(exists=True, desc='output from fit')
    mask = File(exist=True, desc='voxels of interest')
    delta = File(exist=True, desc='error in x from phase regression')
    stdm = File(desc='std error of mag')
    stdp = File(desc='std error in phase')


class PlotFitsOutputSpec(TraitedSpec):
    plots = traits.List(exists=True, desc="list of files")


class PlotFits(BaseInterface):
    input_spec = PlotFitsInputSpec
    output_spec = PlotFitsOutputSpec

    def mahalanobis_dist(self, x, y):
        covariance_xy = np.cov(x, y, rowvar=0)
        inv_covariance_xy = np.linalg.inv(covariance_xy)
        xy_mean = np.mean(x), np.mean(y)
        x_diff = np.array([x_i - xy_mean[0] for x_i in x])
        y_diff = np.array([y_i - xy_mean[1] for y_i in y])
        diff_xy = np.transpose([x_diff, y_diff])

        md = []
        for i in range(len(diff_xy)):
            md.append(np.sqrt(np.dot(np.dot(np.transpose(diff_xy[i]), inv_covariance_xy), diff_xy[i])))
        return md

    def _run_interface(self, runtime):
        # read in data
        img = nb.load(self.inputs.mag)
        magimg = img.get_data()
        img = nb.load(self.inputs.phase)
        phaseimg = img.get_data()
        img = nb.load(self.inputs.sim)
        simimg = img.get_data()
        img = nb.load(self.inputs.mask)
        maskimg = img.get_data()
        img = nb.load(self.inputs.delta)
        deltaimg = img.get_data()
        img = nb.load(self.inputs.stdm)
        stdmimg = img.get_data()
        img = nb.load(self.inputs.stdp)
        stdpimg = img.get_data()

        plotlist = []
        for x in xrange(magimg.shape[0]):
            for y in xrange(magimg.shape[1]):
                for z in xrange(magimg.shape[2]):
                    if maskimg[x, y, z] != 0:
                        md = self.mahalanobis_dist(phaseimg[x, y, z, :], magimg[x, y, z, :])
                        fig, ax = plt.subplots(1, 1)
                        colours = ['b'] * magimg.shape[3]
                        magdata = magimg[x, y, z, :]
                        phasedata = phaseimg[x, y, z, :]
                        for i in xrange(len(md)):
                            if md[i]**2 > stat.chi2.ppf(0.975, 2):
                                colours[i] = 'g'
                                magdata = np.delete(magdata, i)
                                phasedata = np.delete(phasedata, i)
                        stat.probplot(md, dist=stat.chi2, plot=ax, sparams=[2])
                        plt.title("Voxel: " + str(x) + " " + str(y) + " " + str(z) + "vs Chi^2")
                        plt.savefig("Voxel" + str(x) + "_" + str(y) + "_" + str(z) + "chi2.png")
                        plt.clf()
                        plotlist.append(os.path.abspath("Voxel" + str(x) + "_" + str(y) + "_" + str(z) + "chi2.png"))

                        linear = odr.unilinear
                        ests = [stdmimg[x, y, z] / stdpimg[x, y, z], np.mean(magdata) / np.mean(phasedata)]
                        mydata = odr.RealData(phasedata, magdata, sx=stdpimg[x, y, z], sy=stdmimg[x, y, z])
                        odr_obj = odr.ODR(mydata, linear, beta0=ests, maxit=200)
                        res = odr_obj.run()
                        est = res.y
                        rsq = 1.0 - sum((magdata - est) ** 2) / sum((magdata - np.mean(magdata)) ** 2)
                        print rsq

                        plt.scatter(phaseimg[x, y, z, :], magimg[x, y, z, :], c=colours)
                        plt.plot(phaseimg[x, y, z, :] + deltaimg[x, y, z, :], simimg[x, y, z, :])
                        plt.plot(res.xplus, res.y, 'k+')
                        plt.plot([np.sum(phaseimg[x, y, z, :]) / magimg.shape[3]],
                                 [np.sum(magimg[x, y, z, :]) / magimg.shape[3]], 'ro')
                        plt.xlabel("Magnitude")
                        plt.ylabel("Phase")
                        plt.title("Voxel: " + str(x) + " " + str(y) + " " + str(z))
                        plt.savefig("Voxel" + str(x) + "_" + str(y) + "_" + str(z) + ".png")
                        plt.clf()
                        plotlist.append(os.path.abspath("Voxel" + str(x) + "_" + str(y) + "_" + str(z) + ".png"))
        self._plotlist = plotlist
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["plots"] = self._plotlist
        return outputs


class PlotResidualsInputSpec(BaseInterfaceInputSpec):
    xres = File(exists=True, desc='x residuals from the fit')
    yres = File(exists=True, desc='y residuals from the fit')
    mask = File(exist=True, desc='voxels of interest')


class PlotResidualsOutputSpec(TraitedSpec):
    plots = traits.List(exists=True, desc="list of files")


class PlotResiduals(BaseInterface):
    input_spec = PlotResidualsInputSpec
    output_spec = PlotResidualsOutputSpec

    def _run_interface(self, runtime):
        # read in data
        img = nb.load(self.inputs.xres)
        xresimg = img.get_data()
        img = nb.load(self.inputs.yres)
        yresimg = img.get_data()
        img = nb.load(self.inputs.mask)
        maskimg = img.get_data()

        plotlist = []
        for x in xrange(xresimg.shape[0]):
            for y in xrange(xresimg.shape[1]):
                for z in xrange(xresimg.shape[2]):
                    if maskimg[x, y, z] != 0:
                        plt.scatter(xresimg[x, y, z, :], yresimg[x, y, z, :])
                        plt.xlabel("X residuals")
                        plt.ylabel("Y residuals")
                        plt.title("Voxel: " + str(x) + " " + str(y) + " " + str(z))
                        plt.savefig("Voxel" + str(x) + "_" + str(y) + "_" + str(z) + ".png")
                        plt.clf()
                        plotlist.append(os.path.abspath("Voxel" + str(x) + "_" + str(y) + "_" + str(z) + ".png"))
        self._plotlist = plotlist
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["plots"] = self._plotlist
        return outputs


class PlotHistogramInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='images containing timecourses to plot', mandatory=True)
    mask_file = File(exists=True, desc='voxels of interest')


class PlotHistogramOutputSpec(TraitedSpec):
    result_filelist = traits.List(desc="list of output PlotHistograms")


class PlotHistogram(BaseInterface):
    input_spec = PlotHistogramInputSpec
    output_spec = PlotHistogramOutputSpec

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        img = nb.load(fname).get_data()

        fname = self.inputs.mask_file
        mask = nb.load(fname).get_data()

        self.histolist = []

        for x in xrange(mask.shape[0]):
            for y in xrange(mask.shape[1]):
                for z in xrange(mask.shape[2]):
                    if mask[x, y, z] != 0:
                        fig, ax = plt.subplots(1, 1)
                        ax.hist(img[x, y, z, :], bins=20)
                        ax.set_title(str(x) + " " + str(y) + " " + str(z))
                        plt.savefig(base + str(x) + "_" + str(y) + "_" + str(z) + '_hist.png')
                        self.histolist.append(os.path.abspath(base + str(x) + "_" + str(y) + "_" + str(z) +
                                                              '_hist.png'))
                        plt.close()
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["result_filelist"] = self.histolist
        return outputs


class PlotDistributionInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='images containing timecourses to plot', mandatory=True)
    mask_file = File(exists=True, desc='voxels of interest')


class PlotDistributionOutputSpec(TraitedSpec):
    result_filelist = traits.List(desc="list of output probability plot")


class PlotDistribution(BaseInterface):
    input_spec = PlotDistributionInputSpec
    output_spec = PlotDistributionOutputSpec

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        img = nb.load(fname).get_data()

        fname = self.inputs.mask_file
        mask = nb.load(fname).get_data()

        self.plotlist = []

        for x in xrange(mask.shape[0]):
            for y in xrange(mask.shape[1]):
                for z in xrange(mask.shape[2]):
                    if mask[x, y, z] != 0:
                        fig, ax = plt.subplots(1, 1)
                        stat.proplot(img[x, y, z, :], dist="norm",plot=ax)
                        ax.set_title(str(x) + " " + str(y) + " " + str(z))
                        plt.savefig(base + str(x) + "_" + str(y) + "_" + str(z) + '_PlotDistribution.png')
                        self.plotlist.append(os.path.abspath(base + str(x) + "_" + str(y) + "_" + str(z) +
                                                             '_PlotDistribution.png'))
                        plt.close()
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["result_filelist"] = self.plotlist
        return outputs


class PlotCorrelationVsPercentChangeInputSpec(BaseInterfaceInputSpec):
    in_dict = traits.Dict(desc="dict of files: mag, sim, filt", mandatory=True)


class PlotCorrelationVsPercentChangeOutputSpec(TraitedSpec):
    plot = File(exists=True, desc="plot")


class PlotCorrelationVsPercentChange(BaseInterface):
    input_spec = PlotCorrelationVsPercentChangeInputSpec
    output_spec = PlotCorrelationVsPercentChangeOutputSpec

    def r_value(self, (magtc, simtc)):
        if np.count_nonzero(magtc) == 0 or np.count_nonzero(simtc) == 0:
            return 0
        slope, intercept, r_value, p_value, std_err = stat.linregress(magtc, simtc)
        return r_value

    def _run_interface(self, runtime):
        # read in data
        in_dict = self.inputs.in_dict

        img = nb.load(in_dict['mag'])
        magimg = np.array(img.get_data()).astype(float)
        magimg = magimg.reshape(-1, magimg.shape[-1])

        img = nb.load(in_dict['sim'])
        simimg = np.array(img.get_data()).astype(float)
        simimg = simimg.reshape(-1, simimg.shape[-1])

        img = nb.load(in_dict['pc_sim'])
        pcimg = np.array(img.get_data()).astype(float)
        pcimg = pcimg.reshape(pcimg.size)

        parapool = Pool(calc_pool_size())

        try:
            result = parapool.map(self.r_value, zip(magimg[xrange(len(pcimg)), :], simimg[xrange(len(pcimg)), :]))
            parapool.close()
        except Exception as e:
            print e
            parapool.close()
            return runtime

        plt.scatter(np.square(result), pcimg)
        plt.xlabel('R^2')
        plt.ylabel('Percent Change')
        _, base, _ = split_filename(self.inputs.in_dict['pc_sim'])
        plt.savefig(base+'_R2vPC.png')
        plt.clf()
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.in_dict['pc_sim'])
        outputs["plot"] = os.path.abspath(base + '_R2vPC.png')
        return outputs


class PlotMeanTimeCourseInputSpec(FSLCommandInputSpec):
    in_file = traits.File(exists=True, mandatory=True, argstr="-i %s", position=1,
                          desc="file to take mean timecourse of")
    out_file = traits.File(argstr="-o %s", position=2, desc="output file", genfile=True)
    mask = traits.File(exists=True, argstr="-m %s", position=3, desc="mask for input", xor=['coords'])
    coords = traits.List(position=3, desc="coordinate to report timecourse of", xor=['mask'])


class PlotMeanTimeCourseOutputSpec(TraitedSpec):
    out_file = traits.File(desc='out_file')


class PlotMeanTimeCourse(FSLCommand):
    """
    Use fslmeants to output averge timecourse to console or a file.

    Examples
    --------

    >>> import nipype_phase_regression.interfaces as pr
    >>> avetc = pr..PlotMeanTimeCourse()
    >>> avetc.inputs.in_file = 'functional.nii.gz'
    >>> avetc.inputs.mask = 'mask.nii.gz'
    >>> avetc.run() #doctest: +SKIP

    """
    _cmd = "fslmeants"
    input_spec = PlotMeanTimeCourseInputSpec
    output_spec = PlotMeanTimeCourseOutputSpec

    def _format_arg(self, name, spec, value):
        if name == "coords":
            if isinstance(value, list):
                args = " ".join(value)
                return "-c %s" % args
        return super(PlotMeanTimeCourse, self)._format_arg(name, spec, value)

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_tc.txt')
        return outputs

    def _gen_filename(self, name):
        if name == 'out_file':
            return self._gen_outfilename()
        return None

    def _gen_outfilename(self):
        out_file = self.inputs.out_file
        if isdefined(out_file):
            out_file = os.path.realpath(out_file)
        if not isdefined(out_file) and isdefined(self.inputs.in_file):
            _, base, _ = split_filename(self.inputs.in_file)
            out_file = base + '_tc.txt'
        return os.path.abspath(out_file)
