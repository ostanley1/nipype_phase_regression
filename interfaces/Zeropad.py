from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
import nibabel as nb
import numpy as np
from numpy.fft import irfft, rfft, rfftfreq
import os

class ZeropadInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='image to blur', mandatory=True)
    padding_factor = traits.Float(desc='how many times to zeropad by', mandatory=True)


class ZeropadOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc="blurred image")


class Zeropad(BaseInterface):
    input_spec = ZeropadInputSpec
    output_spec = ZeropadOutputSpec

    def _run_interface(self, runtime):
        in_file = nb.load(self.inputs.in_file)
        image = in_file.get_data()

        for dim in [0,1,2]:
            image = irfft(rfft(image, axis=int(dim)),
                          n=int(self.inputs.padding_factor*image.shape[dim]),
                          axis=int(dim))

        affine = in_file.affine
        affine[0:3, 0:3] = affine[0:3,0:3]/int(self.inputs.padding_factor)
        new_img = nb.Nifti1Image(image, affine, in_file.header)
        _, base, _ = split_filename(self.inputs.in_file)
        nb.save(new_img, base + '_pad.nii.gz')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_pad.nii.gz')
        return outputs

