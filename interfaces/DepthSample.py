# coding=utf-8
from nipype.interfaces.matlab import MatlabCommand
from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, File
import os
from string import Template

class DepthSampleInputSpec(BaseInterfaceInputSpec):
    mask_file = File(exists=True, mandatory=True)
    pial_surf = File(exists=True, mandatory=True)
    white_surf = File(exists=True, mandatory=True)
    out_file = File('depths.nii', usedefault=True)

class DepthSampleOutputSpec(TraitedSpec):
    out_file = File(exists=True)

class DepthSample(BaseInterface):
    input_spec = DepthSampleInputSpec
    output_spec = DepthSampleOutputSpec

    def _run_interface(self, runtime):
        d = dict(mask_file=self.inputs.mask_file,
                 pial_surf=self.inputs.pial_surf,
                 white_surf=self.inputs.white_surf,
                 out_file=self.inputs.out_file)
        #this is your MATLAB code template
        print(d)
        script = Template("""v1mask = '$mask_file';
        pial = '$pial_surf'
        white = '$white_surf'
        outfile = '$out_file'
        samplesurffunc
        exit;
        """).substitute(d)
        print(script)
        # mfile = True  will create an .m file with your script and executed.
        # Alternatively
        # mfile can be set to False which will cause the matlab code to be
        # passed
        # as a commandline argument to the matlab executable
        # (without creating any files).
        # This, however, is less reliable and harder to debug
        # (code will be reduced to
        # a single line and stripped of any comments).

        mlab = MatlabCommand(script=script, mfile=True)
        result = mlab.run()
        print(result.__dict__)
        return result.runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['out_file'] = os.path.abspath(self.inputs.out_file)
        return outputs