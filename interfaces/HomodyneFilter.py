from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, traits, File
import os
import nibabel as nb
from math import pi as pi
from numpy.fft import ifftn, fftn, fftshift
from numpy import angle, exp, cos, zeros, ones, outer, asarray
from nipype.utils.filemanip import split_filename

class HomodyneFilterInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='phase file in rad')
    mask_file = File(exists=True, desc='mask file')
    no_mask = traits.Bool(desc='whether to use all ones for mask')
    filter_size = traits.Float(desc='filter size of homodyne filter')

class HomodyneFilterOutputSpec(TraitedSpec):
    out_file = File(exists=True)

class HomodyneFilter(BaseInterface):
    input_spec = HomodyneFilterInputSpec
    output_spec = HomodyneFilterOutputSpec

    def _run_interface(self, runtime):
        if int(self.inputs.filter_size) % 2:
            filter_size=self.inputs.filter_size-1
        else:
            filter_size=self.inputs.filter_size

        phase = nb.load(self.inputs.in_file)
        if self.inputs.no_mask:
            mag=ones(phase.shape)
        else:
            img = nb.load(self.inputs.mask_file)
            mag = img.get_data()
        hpima=zeros(phase.shape)
        t1 = asarray(range(1,int(filter_size)+1))
        f1 = 0.5 * (1 + cos(2 * pi * (t1 - filter_size / 2 - 1) / filter_size))
        maskf = outer(f1, f1)
        mask = zeros((phase.shape[0], phase.shape[1]))
        mask[int(phase.shape[0] / 2 + 1 - filter_size / 2):int(phase.shape[0] / 2 + filter_size / 2 + 1),
            int(phase.shape[1] / 2 + 1 - filter_size / 2):int(phase.shape[1] / 2 + filter_size / 2 + 1)]=maskf
        if len(phase.shape) == 2:
            signal = mag * exp(1j * phase.get_data())
            ks = fftshift(fftn(signal))
            ks = ks * mask;
            signaln = ifftn(fftshift(ks))
            hpima = angle(signal / signaln)
        elif len(phase.shape) == 3:
            for i in range(phase.shape[2]):
                tempmag = mag[:,:, i]
                tempph = phase.dataobj[:,:, i]
                signal = tempmag * exp(1j * tempph)
                ks = fftshift(fftn(signal))
                ks = ks * mask;
                signaln = ifftn(fftshift(ks))
                hpima[:,:, i]=angle(signal / signaln)
        else:
            for i in range(phase.shape[2]):
                for j in range(phase.shape[3]):
                    tempmag = mag[:,:, i, j]
                    tempph = phase.dataobj[:,:, i, j]
                    signal = tempmag * exp(1j * tempph)
                    ks = fftshift(fftn(signal))
                    ks = ks * mask;
                    signaln = ifftn(fftshift(ks))
                    hpima[:,:, i, j]=angle(signal / signaln)

        _, outname, _ = split_filename(self.inputs.in_file)
        outnii = nb.Nifti1Image(mask, None, header=phase.get_header())
        outnii.to_filename(outname + '_mask.nii.gz')
        outnii = nb.Nifti1Image(hpima, None, header=phase.get_header())
        outnii.to_filename(outname + '_hpfilt.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.in_file)
        outputs["out_file"] = os.path.abspath(base + '_hpfilt.nii.gz')
        return outputs