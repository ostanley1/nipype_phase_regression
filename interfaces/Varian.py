import os
import os.path
import re
import numpy as n
import matplotlib.pyplot as plt
import struct

from matplotlib.mlab import prctile
from matplotlib.widgets import Slider
from numpy.fft import fft, ifft, fft2, fftn, fftshift, ifft2, ifftshift, ifftn

try:
    import fdf_io.fdflib as fl
except ImportError:
    print('FDF IO c library not available')

#from matplotlib.pyplot import imshow, clim, gray, colorbar, axis, gca, axes

#some utility functions to make running FFTs a little nicer.
#note that this uses fftpack, not FFTW..  in order to get fftw functionality,
# you have to compile numpy/scipy yourself _after_ installing ATLAS/BLAS and FFTW. 
def ift(x,axis=[-1]):
    return ifftshift( ifft( ifftshift(x, axes=axis), axis=axis[0] ), axes=axis)

def ft(x,axis=[-1]):
    return fftshift( fft( fftshift(x, axes=axis), axis=axis[0] ), axes=axis)

def ift2(x):
    return ifftshift( ifft2( ifftshift(x, axes=[-2,-1]) , axes=[-2, -1]), axes=[-2,-1])

def ft2(x):
    return fftshift( fft2( fftshift(x, axes=[-2,-1]), axes=[-2, -1] ), axes=[-2,-1])

def ift3(x):
    return ifftshift( ifftn( ifftshift(x, axes=[-3, -2, -1]), axes=[-3, -2, -1] ), axes=[-3, -2,-1])

def ft3(x):
    return fftshift( fftn( fftshift(x, axes=[-3, -2, -1]), axes=[-3, -2, -1] ), axes=[-3, -2,-1])



#square root Sum of squares over axis
def sumsq(x, axis=0):
    return n.sqrt(n.mean(abs(x) ** 2, axis=axis))

#normalize
def norml(x):
    return x/n.max(n.abs(x))

def da(map, clim=[None, None], cmap=plt.cm.jet, showbar=1, orient='e',interp='nearest'):
#display angle()
    figmri(n.angle(map), clim, cmap, showbar, orient, interp)
    
def dm(map, clim=[None, None], cmap=plt.cm.jet, showbar=1, orient='e', interp='nearest'):
#display magnitude
    figmri(n.abs(map), clim, cmap, showbar, orient, interp)
    
def figmri(data, clim=[None, None], cmap=plt.cm.jet, showbar=1, orient='e',interp='bilinear'):
#figmri from rri matlab function of the same name
    #plt.figure()

    if(n.iscomplexobj(data)):
        data = n.abs(data)
    
    data = data.squeeze()
    ndim = data.ndim
    orient = str.lower(orient)
    N = 1
    
    if ndim > 3:
        data = n.transpose(data, range(0,ndim-3) + [-2, -3, -1] )
        c = data.shape
        print(c)
        print([n.prod(c[:2]), n.prod(c[2:])])
        data = data.reshape([n.prod(c[:2]), n.prod(c[2:])])
    elif ndim == 3:
        c = list(data.shape)
        N=1
        if orient == 'v':
            N = 1
        elif orient == 'h':
            N = c[0]
        else:
            for k in n.arange(int(n.floor(n.sqrt(c[0]))),0,-1):
                if n.remainder(c[0],k) == 0:
                    N=k
                    break
                    
     
        
        data = data.reshape([c[0]/N, N] + c[1:] )
        data = n.transpose(data, [0, 2, 1, 3])
        c = list(data.shape) 
        data = data.reshape([n.prod(c[:2]), n.prod(c[2:])])
        
    if orient == 't':
        data = n.rot90(data)
    
    plt.imshow(data,cmap=cmap,interpolation=interp)
    plt.clim(clim)
    
    
    
    if showbar:
        plt.colorbar()
    
    
def vimage(d):
    """ Very slow at this point """
    
    mimage(d[0,:,:])
    main_ax = plt.gca()
    
    axslice  = plt.axes([0.1, 0.1, 0.8, 0.05])
    plt.axis('off')
    slice_slider = Slider(axslice, 'Slice', 0, d.shape[0], valinit=0)
    
    def update(val):
        plt.axes(main_ax)
        mimage(d[int(slice_slider.val),:,:])
        
    slice_slider.on_changed(update)



def mimage(d):
    plt.imshow( d )
    plt.gray()
    plt.axis('image') # needed so that ginput doesn't resize the image
    plt.clim([ 0, prctile(d, 99) ])


def mimagecb(d):
    mimage(d)
    plt.colorbar()



def stripquotes(data):
    #strip whitespace and quotes from vnmrj string params
    return data.replace("\"", "").strip()


def readImages( filename ):
    if filename.endswith('.fdf'):
        data = readFDF( filename )
    elif filename.endswith('.img'):
        data = readIMG( filename )
    else:
        print("Unknown filename %s " % (filename))

    return data


def writeStim ( filename, par, data, rev=False ):
    sprFile = filename + '.spr'
    sdtFile = filename + '.sdt'

    fp = open( sprFile, 'w')

    # Write header
    
    str1 = 'numDim: {}\n'.format(data.ndim)
    fp.write(str1)

    str1 = 'dim: '
    
    if rev:
        for j in reversed(list(data.shape)):
            str1 = str1 + '{} '.format(j)
    else:
        for j in list(data.shape):
            str1 = str1 + '{} '.format(j)
        
    str1 = str1 + '\n'
    fp.write(str1)

    #str1 = 'origin {}\n'.format(data.ndim)
    #fp.write(str1)

    str1 = 'dataType: REAL\n'.format(data.ndim)
    fp.write(str1)
    
    fp.close()

    dtemp = data.astype('float32')

    dtemp.tofile(sdtFile)

def readFDF_py( filename ):

    fp = open( filename, 'rb' )

    xsize = -1
    ysize = -1
    zsize = 1
    bigendian = -1
    done = False

    while not done :

        line = fp.readline()

        if( len( line ) >= 1 and line[0] == chr(12) ):
            break

        if ( len( line) == 0 ):
            break

        if( len( line ) >= 1 and line[0] != chr(12) ):

            if( line.find('bigendian') > 0 ):
                endian = line.split('=')[-1].rstrip('\n; ').strip(' ')

            if( line.find('echos') > 0 ):
                nechoes = line.split('=')[-1].rstrip('\n; ').strip(' ')

            if( line.find('echo_no') > 0 ):
                echo_no = line.split('=')[-1].rstrip('\n; ').strip(' ')

            if( line.find('nslices') > 0 ):
                nslices = line.split('=')[-1].rstrip('\n; ').strip(' ')

            if( line.find('slice_no') > 0 ):
                sl = line.split('=')[-1].rstrip('\n; ').strip(' ')

            if( line.find('matrix') > 0 ):
                m = re.findall('(\d+)', line.rstrip())

                if len(m) == 2:
                    xsize, ysize = int(m[0]), int(m[1])
                elif len(m) == 3:
                    xsize, ysize, zsize = int(m[0]), int(m[1]), int(m[2])

    fp.seek(-xsize*ysize*zsize*4,2)

    if bigendian == 1:
        fmt = ">%df" % (xsize*ysize*zsize)
    else:
        fmt = "<%df" % (xsize*ysize*zsize)

    data = struct.unpack(fmt, fp.read(xsize*ysize*zsize*4))
    data = n.array( data ).reshape( [zsize, ysize, xsize ] ).squeeze()

    fp.close()

    return data


def readFDFHeader( filename ):
    header = fl.readHeader_c(filename)
    return header

def readFDF( filename ):

    try:
        header = fl.readHeader_c(filename)
        data = fl.readFDF_c(filename, header)
    except:
        print("Something went wrong with c based fdf reader... reverting to python version.")
        data = readFDF_py(filename)

    return data


def readAllIMGs(directory, coil=0, slice=0, image=0):
    (head, tail) = os.path.split(directory)
    dirs = os.listdir(head)

    dirs = [dir for dir in dirs if dir.startswith(tail)]

    if coil > 0:
        coilstr = '{:03}.img'.format(coil)
        dirs = [ dir for dir in dirs if dir.endswith(coilstr) ]

    dirs = [dir for dir in dirs if dir.endswith('.img')]

    count = 0
    data = n.array([0])
    for dir in sorted(dirs):
        print("Reading coil %d/%d..." % (count+1, len(dirs)))
        if count == 0:
            tempdat = readIMG( os.path.join(head, dir), slice, image ) 
            sz = list(tempdat.shape)
            newsize = [len(dirs)] + sz 
            data = n.zeros(newsize, dtype = tempdat.dtype)
            data[0,...] = tempdat
            count = count + 1
            continue

        print("%s" % dir)
        data[count,...] = readIMG( os.path.join(head, dir), slice, image ) 
        count = count + 1
       
    #squash the list into an array.

    return data



def readIMG(directory, slice=0, image=0, returnNames=False) :
    # Get a list of all the FDF files in the directory
    try:
        files = os.listdir(directory)
    except:  
        print("Could not find the directory %s" % directory)
        return
        
    files = [ file for file in files if file.endswith('.fdf') ]
    print("Found %d files in %s." % (len(files), directory))

    if slice > 0:
        slicestr = 'slice{:03}'.format(slice)
        files = [ file for file in files if (file.find(slicestr)>-1) ]

    if image > 0:
        imgstr = 'image{:03}'.format(image)
        files = [ file for file in files if (file.find(imgstr)>-1) ]
        

    count = 0
    #read first file to find size req's
    
    data = n.array([0])
    for file in sorted(files):
        if count == 0:
            tempdat =  readFDF( directory+'/'+file ) 
            sz = list(tempdat.shape)
            newsize = [len(files)] + sz
            data = n.zeros(newsize, dtype = tempdat.dtype)
        #print "Reading file %d/%d..." % (count, len(files))
        #print "%s" % file
        data[count,...] = readFDF( directory+'/'+file ) 
        count = count + 1
       
    if returnNames:
        return data,files
    else:
        return data



def parseProcpar(inlist):
    """parseProcpar - directly parse vnmrj procpar file
    inputs:
        inlist -- list of strings from file.readlines()
        
    returns:
        paramDict -- dicitonary of key value pairs
                        { 'param name' : 'value' }
                        
    usage:
        pd = parseProcpar(lines)
        
    String parameters are returned as lists of strings.
    Numeric parameters are returned as numpy arrays, which means this works:
        pd['np']/2.0
        
    """
    # Procpar parameter definitions
    # Names and types of paramater info line
    paramLineOne = ['name', 'subtype', 'basictype', 'maxvalue', 'minvalue', 'stepsize', 'dgroup', 'ggroup', 'protection', 'active', 'intptr']
    
    paramTypes = [str, int, int, float, float, float, int, int, int, int, int]
    
    """
    from vnmrj reference, valid subtypes are:
    0 (undefined), 1 (real), 2 (string),
    3 (delay), 4 (flag), 5 (frequency), 6 (pulse), 7 (integer).
    """
    
    #Scalings for param subtypes (not currently used)
    paramScalings = [0, 1, 0, 1, 1, 1, 1e-3, 1]
    paramSubtypeConversions = dict(zip(range(0,len(paramScalings)),paramScalings))
    
    paramDict = {}
    
    #state parameters
    parsedInfo = 0
    parsedData = 0
    parsedDataRem = 0
    
    for item in inlist:
        
        if parsedInfo == 0:
            #print "parsing infos"
            #parse the first line of the parameter entry
            paramInfo = {}
            
            for name, oper, data in zip(paramLineOne, paramTypes, item.strip().split(" ") ):
                paramInfo[name] = oper(data)
                
            parsedInfo = 1
            continue
            
        else:
            #have already parsed info line
            
            if parsedData == 0:
                #print "parsing data first"
                #parse the first data line
                
                splitValues = item.split(" ")
                nvals = int(splitValues[0])
                
                #switch depending on what kind of value is it (stored differently)
                if paramInfo['basictype'] == 1:
                    #convert param values to a numpy array and insert in param dictionary
                    splitValues = stripquotes(item).split(" ")
                    floatData = n.array(map(float, splitValues[1:]))
                    
                    #'pulse' parameters are specified in ms, convert to value in seconds
                    if paramInfo['subtype'] == 6:
                        floatData *= 1e-3
                        
                    paramDict[paramInfo['name']] = floatData
                    parsedDataRem = 0
                    
                elif paramInfo['basictype'] == 2:
                    parsedDataRem = nvals - 1
                    if parsedDataRem>0:
                        #convert param values to a list and insert in param dictionary
                        paramDict[paramInfo['name']] = [stripquotes(splitValues[1]),]
                    else:
                        paramDict[paramInfo['name']] = stripquotes(splitValues[1])
                        
                        
                parsedData = 1
                
                continue
                
            else:
                
                if parsedDataRem > 0:
                    #print "parsing data remaining"
                    
                    #we've started parsing data but haven't finished the multiline string
                    paramDict[paramInfo['name']].append(item.strip())
                    parsedDataRem -= 1
                    continue
                    
                #fallthrough... all other things have been parsed, so we're on the enum line
                # could read it in, but I'm just throwing it out
                
                #last line on entry, reset flags and skip
                parsedInfo = 0
                parsedData = 0
                parsedDataRem = 0
                paramInfo = {}
                
                
    paramDict['nrcvrs'] = paramDict['rcvrs'].count('y')
    return paramDict


def readProcpar(fname = ''):
    """docstring for readProcpar"""
    
    if fname.endswith('/'):
        fname = fname[:-1]
    
    if os.path.isdir(fname):
        fname = fname + '/procpar'
        
    pfile = file(fname,'r')
    parlines = pfile.readlines()
    pfile.close()
    
    return parseProcpar(parlines)




# def readFID(filename, verbose=0):
#
#     dataFileHeaderMap = ["nblocks", "ntraces", "np", "ebytes",
#                             "tbytes", "bbytes", "vers_id", "status", "nbheaders" ]
#
#     dataBlockHeaderMap = ["scale", "status", "index", "mode",
#                             "ctcount", "lpval", "rpval", "lvl", "tlt" ]
#
#     dataFileHeadString = "6l2hl"
#     dataFileHeadString = "6i2hi"
#
#     dataFileHeadSize = struct.calcsize(dataFileHeadString)
#
#     dataBlockHeadString = "4hl4f"
#     dataBlockHeadString = "4hi4f"
#
#     dataBlockHeadSize = struct.calcsize(dataBlockHeadString)
#
#     j = complex(0,1)
#
#     fp = open( filename, 'rb' )
#
#     line = fp.read(dataFileHeadSize)
#
#     endian = ">"
#
#     header = struct.unpack(endian+dataFileHeadString, line)
#     fileHeader = dict(zip(dataFileHeaderMap, header))
#
#     if verbose:
#         print(fileHeader)
#
#     if fileHeader['status']<0:
#         endian = "<"
#         header = struct.unpack(endian+dataFileHeadString, line)
#         fileHeader = dict(zip(dataFileHeaderMap, header))
#         if verbose:
#             print(fileHeader)
#
#     elementString = "h"
#     dtypeString = 'complex'
#
#     if fileHeader['ebytes'] == 2:
#         elementString = "h"
#         dtypeString = 'complex64'
#     elif fileHeader['ebytes'] == 4:
#         elementString = "f"
#         dtypeString = 'complex64'
#     elif fileHeader['ebytes'] == 8:
#         elementString = "d"
#         dtypeString = 'complex128'
#
#
#
#     traceSize = fileHeader['np']*fileHeader['ebytes']
#     blockSize = fileHeader['ntraces']*traceSize
#
#     if verbose:
#         print("traceSize: %d  blockSize: %d" % (traceSize, blockSize))
#
#     data = n.zeros( (fileHeader['nblocks'],fileHeader['np']/2.0*fileHeader['ntraces']), dtype=dtypeString )
#
#     for block in xrange(fileHeader['nblocks']):
#         if verbose > 1:
#             print("Block %d" % block
#         line = fp.read(dataBlockHeadSize))
#         blockheader = struct.unpack(endian+dataBlockHeadString, line)
#
#         #print dict(zip(dataBlockHeaderMap, blockheader))
#         #Ignore the block header for now
#
#         #Read all bytes in block
#         line = fp.read(blockSize)
#
#         thisBlock = struct.unpack("%s%d%s" % (endian, fileHeader['np']*fileHeader['ntraces'], elementString), line)
#
#         temp = n.array(thisBlock)
#         data[block,:] = temp[::2] + temp[1::2]*j
#
#
#     fp.close()
#     return data



def readFidDir(fname):
    """docstring for readFidDir"""
    #check if file exists
    #if not, check if file.fid exists
    if not (fname.endswith('.fid') or fname.endswith('.FID')):
        fname = fname + '.fid'

    if (os.path.exists(fname+'/fid') and os.path.exists(fname+'/procpar')):
        procpar = readProcpar(fname+'/procpar')
        fiddata = readFID(fname+'/fid')
    else:
        return -1

    return (fiddata, procpar)



#Seqcon goes like:
# ne, ns, npe, npe2, npe3 (unused)

def readPETable(fname, shot=0):
    fp = open( fname, 'r' )
    
    line = fp.readlines()
    
    fp.close()
    
    temp = n.empty(0,'int32')
    for l in line[:]:
        temp = n.r_[temp, n.array(map(int,l.strip().split()))]

    permuteOrder = n.argsort(temp)
    
    return permuteOrder, temp
    

def makePETable(nv, etl, k0=1):
    tabval = n.zeros(nv,dtype='int32')
    
    count = n.arange(nv/2)+1
    tabval[count-1]= -((count*nv/(2*etl)-1) % (nv/2))
    tabval[count-1]= tabval[count-1] + n.floor((count-1)/etl)
    
    count = n.arange(nv/2,nv)+1
    tabval[count-1] = (count-nv/2-1)*nv/(2*etl) % (nv/2)
    tabval[count-1] = tabval[count-1] + n.floor((count-nv/2-1)/etl) +1
    
    #plot(tabval)
    tabval = tabval.reshape((nv/etl,etl))
    
    #cycle columns of table to align k0 traversal
    inds2 = n.r_[n.arange(etl-1,etl-k0-1,-1),n.arange(etl-k0)]
    t = tabval[:, inds2]
    return t


#todo: finish this
def determineLoopStructure(par):
    
    arraydim = par['arraydim']

    if (par['seqcon'][1] == 's') and (par['ns']>0) and (par['nD']==2):
        arraydim = arraydim/par['ns']
    if par['seqcon'][2] == 's':
        arraydim = arraydim/par['nv']
    if (par['seqcon'][3] == 's') and (par['nv2']>0) and (par['nD']==3):
        arraydim = arraydim/par['nv2']

    extradims = arraydim/par['nrcvrs']

    #acq method code
    method = par['seqfil']

    if method=='flash':
        #phase slice vol
        reshapeOrder = ['np','ns','nv']

    elif method=='gems' or method=='mxmt_gems' :
        reshapeOrder = ['np','nv','ns']

    elif method=='mems' or method=='mxmt_mems' :
        reshapeOrder = ['ne','np','nv','ns']

    else:
        pass

    return reshapeSize, permuteOrder



def simpleReconFSE(fname, tabName):
    
    
    dat,par = readFidDir(fname)

    #test reshape
    #permuteOrder,test = readPETable('../data/fse256_8_4')
    permuteOrder,test = readPETable('/Users/andrewcurtis/temp/fse250_5_3')

    shots = par['nv'][0]/par['etl'][0]
    etl = par['etl'][0]
    np = par['np'][0]/2.0
    ns =  par['ns'][0]
    nv = par['nv'][0]
    nrcvrs = par['nrcvrs']*1.0


    sizes = (nrcvrs, shots, ns, etl, np)


    #tt = dat.reshape( sizes ).copy()
    tt = dat.reshape( sizes )
    dat=None

    tt = n.transpose(tt,(0,2,1,3,4))

    tt = tt.reshape((nrcvrs,ns,nv,np))

    #reorder for echoes
    tt =n.take(tt,permuteOrder,axis=2)


    tun = tt[:,10,:,:].squeeze()
    tun[:,1::2,:] = 0
    ft = ft2(tun)

    t2 = ft2(tt)



    sos = sumsq(tt)
    tt=None



def ernst(tr,t1):
    return 180.0/n.pi*(n.arccos(n.exp(-tr/t1)))



#tests
if __name__ == '__main__':
    pass



