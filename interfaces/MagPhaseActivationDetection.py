from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os
from numpy.fft import irfft, rfft
from ..core import calc_pool_size
import scipy.linalg as linalg


class ConvolveMagPhaseInputSpec(BaseInterfaceInputSpec):
    sim = File(exists=True, desc='sim image', mandatory=True)
    mag = File(exists=True, desc='mag image', mandatory=True)


class ConvolveMagPhaseOutputSpec(TraitedSpec):
    convolve_tc = File(exists=True, desc="filtered image")


class ConvolveMagPhase(BaseInterface):
    input_spec = ConvolveMagPhaseInputSpec
    output_spec = ConvolveMagPhaseOutputSpec

    def convolve_tc(self, (mag_tc, sim_tc)):
        if np.count_nonzero(mag_tc - np.mean(mag_tc)) != 0 and np.count_nonzero(sim_tc - np.mean(sim_tc)) != 0:
            return irfft(rfft(mag_tc)*rfft(sim_tc))
        else:
            return np.zeros_like(mag_tc)

    def _run_interface(self, runtime):
        # read in data
        fnamemag = self.inputs.mag
        _, basemag, _ = split_filename(fnamemag)
        fnamesim = self.inputs.sim
        _, basesim, _ = split_filename(fnamesim)
        #
        # if basemag[:5] != basesim[:5]:
        #     raise TypeError('NotSameImageSet')

        img = nb.load(fnamemag)
        magimg = np.array(img.get_data()).astype(float)

        img = nb.load(fnamesim)
        simimg = np.array(img.get_data()).astype(float)

        mag_arr = magimg.reshape(-1, magimg.shape[-1])
        sim_arr = simimg.reshape(-1, simimg.shape[-1])
        parapool = Pool(calc_pool_size())
        try:
            result = parapool.map(self.convolve_tc, zip(mag_arr[xrange(mag_arr.shape[0]), :],
                                                        sim_arr[xrange(sim_arr.shape[0]), :]))
            parapool.close()
        except Exception as e:
            print e
            parapool.close()
            return runtime

        # save figure
        new_img = nb.Nifti1Image(np.asarray(result).reshape(img.shape), img.affine, img.header)

        nb.save(new_img, base + '_convolve.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.sim
        _, base, _ = split_filename(fname)
        outputs["convolve_tc"] = os.path.abspath(base + '_convolve.nii.gz')
        return outputs


class CrosscorrMagPhaseInputSpec(BaseInterfaceInputSpec):
    sim = File(exists=True, desc='sim image', mandatory=True)
    mag = File(exists=True, desc='mag image', mandatory=True)


class CrosscorrMagPhaseOutputSpec(TraitedSpec):
    crosscorr_tc = File(exists=True, desc="cross correlation tc")


class CrosscorrMagPhase(BaseInterface):
    input_spec = CrosscorrMagPhaseInputSpec
    output_spec = CrosscorrMagPhaseOutputSpec

    def crosscorr_tc(self, (mag_tc, sim_tc)):
        if np.count_nonzero(mag_tc - np.mean(mag_tc)) != 0 and np.count_nonzero(sim_tc - np.mean(sim_tc)) != 0:
            p = np.correlate(mag_tc, sim_tc)/np.sqrt(np.correlate(mag_tc, mag_tc)*np.correlate(sim_tc, sim_tc))
            return p[0]
        else:
            return 0

    def _run_interface(self, runtime):
        # read in data
        fnamemag = self.inputs.mag
        _, basemag, _ = split_filename(fnamemag)
        fnamesim = self.inputs.sim
        _, basesim, _ = split_filename(fnamesim)

        # if basemag[:5] != basesim[:5]:
        #     raise TypeError('NotSameImageSet')

        img = nb.load(fnamemag)
        magimg = np.array(img.get_data()).astype(float)

        img = nb.load(fnamesim)
        simimg = np.array(img.get_data()).astype(float)

        mag_arr = magimg.reshape(-1, magimg.shape[-1])
        sim_arr = simimg.reshape(-1, simimg.shape[-1])

        parapool = Pool(calc_pool_size())
        try:
            result = parapool.map(self.crosscorr_tc, zip(mag_arr[xrange(mag_arr.shape[0]), :],
                                                         sim_arr[xrange(sim_arr.shape[0]), :]))
            parapool.close()
        except Exception as e:
            print e
            parapool.close()
            return runtime

        # save figure
        new_img = nb.Nifti1Image(np.asarray(result).reshape(img.shape[:-1]), img.affine, img.header)
        nb.save(new_img, basesim + '_crosscorr.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.sim
        _, base, _ = split_filename(fname)
        outputs["crosscorr_tc"] = os.path.abspath(base + '_crosscorr.nii.gz')
        return outputs


class MagandPhasePCAInputSpec(BaseInterfaceInputSpec):
    mag_file = File(exists=True, desc='mag tc', mandatory=True)
    phase_file = File(exists=True, desc='phase tc', mandatory=True)


class MagandPhasePCAOutputSpec(TraitedSpec):
    s_values = File(exists=True, desc="singular values")
    c_values = File(exists=True, desc="components")
    tc1 = File(exists=True, desc="primary tc")
    tc2 = File(exists=True, desc="secondary tc")


class MagandPhasePCA(BaseInterface):
    input_spec = MagandPhasePCAInputSpec
    output_spec = MagandPhasePCAOutputSpec

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.mag_file
        mag = nb.load(fname)

        fname = self.inputs.phase_file
        phase = nb.load(fname)

        magimg = np.array(mag.get_data()).astype(float)
        mag_array = magimg.reshape(-1, magimg.shape[-1])

        phase_array = np.array(phase.get_data()).astype(float)
        phase_array = phase_array.reshape(-1, phase_array.shape[-1])

        tc1 = np.zeros_like(phase_array)
        tc2 = np.zeros_like(phase_array)

        c = np.zeros([np.prod(mag_array.shape[:-1]), 2])
        s = np.zeros([np.prod(mag_array.shape[:-1]), 2])

        for n in xrange(mag_array.shape[0]):
            # results=self.PCAvoxel((mag_array[n,:], phase_array[n,:]))
            # print results.__dict__
            timecourse = np.asarray([mag_array[n, :], phase_array[n, :]])
            u, s, v = linalg.svd(timecourse.transpose())
            tc1[n, :] = u[:, 0].transpose()
            tc2[n, :] = u[:, 1].transpose()
            c[n, :] = v[1, :]
            s[n, :] = s

        # poolsize = calcPoolSize()
        # print "POOL SIZE: " + str(poolsize)
        # parapool = Pool(poolsize)
        # try:
        #     result = parapool.map(self.PCAvoxel, zip(mag_array[xrange(mag_array.shape[0]), :],
        #                                                  phase_array[xrange(phase_array.shape[0]), :]))
        #     s, c, tc1, tc2= zip(*result)
        #     parapool.close()
        # except Exception as e:
        #     print e
        #     parapool.close()
        #     return runtime

        # save figure
        _, base, _ = split_filename(self.inputs.mag_file)
        new_img = nb.Nifti1Image(np.asarray(tc1).reshape(magimg.shape), mag.affine, mag.header)
        nb.save(new_img, base + '_tc1.nii.gz')

        new_img = nb.Nifti1Image(np.asarray(tc2).reshape(magimg.shape), mag.affine, mag.header)
        nb.save(new_img, base + '_tc2.nii.gz')

        magsize = list(magimg.shape)
        magsize[3] = 2
        new_img = nb.Nifti1Image(np.asarray(s).reshape(magsize), mag.affine, mag.header)
        nb.save(new_img, base + '_singularvalues.nii.gz')

        new_img = nb.Nifti1Image(np.asarray(c).reshape(magsize), mag.affine, mag.header)
        nb.save(new_img, base + '_components.nii.gz')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.mag_file
        _, base, _ = split_filename(fname)
        outputs["s_values"] = os.path.abspath(base + '_singularvalues.nii.gz')
        outputs["c_values"] = os.path.abspath(base + '_components.nii.gz')
        outputs["tc1"] = os.path.abspath(base + '_tc1.nii.gz')
        outputs["tc2"] = os.path.abspath(base + '_tc2.nii.gz')
        return outputs
