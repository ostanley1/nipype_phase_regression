from nipype.interfaces.base import (
    TraitedSpec,
    BaseInterface,
    File,
    traits
)
import os
import nibabel as nb
import numpy as np
from scipy.interpolate import griddata
from nipype.utils.filemanip import split_filename


class SurfacetoNiftiInputSpec(TraitedSpec):
    in_files = traits.List(File(exists=True), mandatory=True, desc="list of surface at various depths from the same patch, list of lists will average runs together")
    depths = traits.ListFloat(mandatory=True, desc="List of depths to include in the nifti (must be equally spaced)")


class SurfacetoNiftiOutputSpec(TraitedSpec):
    out_file = File(desc = "surface patch in nifti format", exists = True)


class SurfacetoNifti(BaseInterface):
    input_spec = SurfacetoNiftiInputSpec
    output_spec = SurfacetoNiftiOutputSpec

    def gridimage(self, x, y, z, npts=None):
        if npts == None:
            npts = int(np.sqrt(len(z)))

        if np.min(x) < np.min(y):
            true_min = np.min(x)
        else:
            true_min = np.min(y)

        if np.max(x) < np.max(y):
            true_max = np.max(x)
        else:
            true_max = np.max(y)

        xi = np.linspace(true_min, true_max, npts)
        yi = np.linspace(true_min, true_max, npts)
        zi = griddata((x, y), z, (xi[None, :], yi[:, None]), method='nearest')

        return xi, yi, zi

    def _run_interface(self, runtime):

        files = self.inputs.in_files
        depths = self.inputs.depths
        assert(len(files)==len(depths))

        if len(depths)!=1:
            idx = np.argsort(depths)
            depths=depths[int(idx)]
            files=files[int(idx)]
            delta=depths[1]-depths[0]
        else:
            delta=1

        for d in range(len(depths)):
            if isinstance(files[d], list):
                if d==0:
                    _, outname, _ = split_filename(files[0][0])
                for f in range(len(files[d])):
                    if f == 0:
                        funcs = np.loadtxt(files[d][f])
                        x = funcs[:, 1]
                        y = funcs[:, 2]
                        z = funcs[:, -1]
                    else:
                        funcs = np.loadtxt(files[d][f])
                        z = z + funcs[:, -1]
                z = z / len(files[d])
            else:
                if d==0:
                    _, outname, _ = split_filename(files[0])
                funcs = np.loadtxt(files[d])
                x = funcs[:, 1]
                y = funcs[:, 2]
                z = funcs[:, -1]
            if d == 0:
                npts = int(np.sqrt(len(z)))
                dx = np.zeros((npts, len(depths)))
                dy = np.zeros((npts, len(depths)))
                surface_vol = np.zeros((npts, npts, len(depths)))

            dx[:, d], dy[:, d], surface_vol[:, :, d] = self.gridimage(x, y, z, npts=int(np.sqrt(len(z))))

            affine = np.array([[dx[1, 0] - dx[0, 0], 0, 0, xi[0]],
                               [0, dy[1, 0] - dy[0, 0], 0, yi[0]],
                               [0, 0, delta, min(depths)],
                               [0, 0, 0, 1]
                               ])
            img = nb.Nifti1Image(surface_vol, affine)
            img.to_filename(outname+'_sampledtonifti.nii.gz')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        if isinstance(self.inputs.in_files[0], list):
            _, base, _ = split_filename(self.inputs.in_files[0][0])
        else:
            _, base, _ = split_filename(self.inputs.in_files[0])
        outputs["out_file"] = os.path.abspath(base + '_sampledtonifti.nii.gz')
        return outputs


# class NiftiMasktoSurfaceInputSpec(TraitedSpec):
#     in_file = traits.File(exists=True, mandatory=True, desc="surface nifti from fsleyes to be converted into asc mask")
#     surf_file = traits.File(exists=True, mandatory=True, desc="surface asc nifti was generated from")
#     slice = traits.Int(desc="slice to put into asc format", usedefault=True, default_value=0)
#
# class NiftiMasktoSurfaceOutputSpec(TraitedSpec):
#     out_file = File(desc = "nifti surface mask in asc format", exists = True)
#
# class NiftiMasktoSurface(BaseInterface):
#     input_spec = NiftiMasktoSurfaceInputSpec
#     output_spec = NiftiMasktoSurfaceOutputSpec
#
#     def _run_interface(self, runtime):
#         in_file = nb.load(self.inputs.in_file).get_data()
#         in_affine = nb.load(self.inputs.in_file).get_affine()
#         surf_file = np.loadtxt(self.inputs.surf_file)
#
#         x = np.linspace(in_affine[0,3], in_affine[0,3]+in_file.shape*in_affine[0,0], in_file.shape)
#         y = np.linspace(in_affine[0,3], in_affine[0,3]+in_file.shape*in_affine[0,0], in_file.shape)
#
#         for row in surf_file.shape[0]:
#
#
#         return runtime
#
#     def _list_outputs(self):
#         outputs = self._outputs().get()
#         if isinstance(self.inputs.in_files[0], list):
#             _, base, _ = split_filename(self.inputs.in_files[0][0])
#         else:
#             _, base, _ = split_filename(self.inputs.in_files[0])
#         outputs["out_file"] = os.path.abspath(base + '_sampledtonifti.nii.gz')
#         return outputs
