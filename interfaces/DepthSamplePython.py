# coding=utf-8
from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, File
import os
from string import Template
import nibabel as nib
import numpy as np
from nipype.utils.filemanip import split_filename
import time
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class DepthSamplePythonInputSpec(BaseInterfaceInputSpec):
    mask_file = File(exists=True, mandatory=True)
    pial_surf = File(exists=True, mandatory=True)
    white_surf = File(exists=True, mandatory=True)

class DepthSamplePythonOutputSpec(TraitedSpec):
    depths = File(exists=True)
    counts = File(exists=True)

class DepthSamplePython(BaseInterface):
    input_spec = DepthSamplePythonInputSpec
    output_spec = DepthSamplePythonOutputSpec

    def read_freesurfer_ascii_surf(self, filename):
        f = open(filename, 'r')
        f.readline()
        [nverts, nfaces]=map(int,f.readline().split())
        data=np.loadtxt(f, float)
        verts=data[0:nverts,:]
        faces=data[nverts:-1,:]
        return verts, faces

    def incell(self, vox, verts):
        from scipy.spatial import ConvexHull, Delaunay
        from scipy.optimize import fminbound
        # edge effect in freesurfer means certain faces can't be counted
        if np.unique(verts, axis=0).shape[0]<6:
            return 0, 0, 0
        # try:
            # hull = ConvexHull(verts)
            # nhull = ConvexHull(np.concatenate((verts,vox[np.newaxis,:])))
            # if not isinstance(hull, Delaunay):
        hull = Delaunay(verts)
        # except Exception as inst:
        #     print(verts)
        #     print(np.concatenate((verts,vox[np.newaxis,:])))
        #     print(inst)
        if hull.find_simplex(vox) >= 0:
            print(hull.find_simplex(vox))
            plane1 = verts[0:3,:]
            plane2 = verts[3:, :]
            mindist = np.min([self.disttopoint(plane1,vox), self.disttopoint(plane2,vox)])
            print mindist
            return fminbound(self.distevalpoint, 0, 1, (verts[:,0:3]-verts[:,3:], verts[:,3:], vox)), 1, mindist
        else:
            return 0, 0, 0

    def disttopoint(self, points, test):
        N = np.cross(points[1, :] - points[0, :], points[2, :] - points[0, :])
        n = N / np.linalg.norm(N)
        p_ = test - points[0, :]
        return np.dot(p_, n)

    def distevalpoint(self, depth, deltapoints, whitepoints, vox):
        points = np.zeros([4,3])
        for p in xrange(3):
            points[p, :] = d * deltapoints[p, :] + whitepoints[p, :]
        points[:,3] = vox
        N = np.cross(points[1,:]-points[0,:], points[2,:]-points[0,:])
        n = N / np.linalg.norm(N)
        return np.dot(n, points[3,:]-points[0,:])**2

    def _run_interface(self, runtime):
        t0 = time.time()
        # load in mask and obtain coords
        mask = nib.load(self.inputs.mask_file)
        img = mask.get_data()
        sx, sy, sz = mask.shape
        img = np.reshape(img, [sx*sy*sz,1])
        coords = np.ones([sy, sx, sz, 4]);
        [coords[:,:,:,0], coords[:,:,:,1], coords[:,:,:,2]]=np.meshgrid(range(0,sx), range(0,sy), range(0,sz));
        coords = np.reshape(np.transpose(coords, [3, 1, 0, 2]), [4,-1])
        grid = np.dot(mask.affine, coords)
        # grid = np.reshape(grid, [4, sx, sy, sz])

        # load in surfaces
        [vpfull, fpfull] = self.read_freesurfer_ascii_surf(self.inputs.pial_surf);
        [vwfull, fwfull] = self.read_freesurfer_ascii_surf(self.inputs.white_surf);

        # now we loop over voxels in mask and find their depths
        radius = 50*max(mask.header.get_zooms())
        depthimg=np.zeros(img.shape)
        inimg=np.zeros(img.shape)
        countimg=np.zeros(img.shape)
        countvox = map(int, np.linspace(0,sz*sy*sx,100))
        maskcount=0
        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # ax.scatter(grid[0,img>0], grid[1,img>0], grid[2,img>0])
        # ax.scatter(vpfull[:,0], vpfull[:,1], vpfull[:,2])
        # plt.show()
        for vox in xrange(grid.shape[1]):#xrange(3699000, 3699999): #xrange(grid.shape[1]):
            if not img[vox]>0:
                continue
            maskcount=maskcount+1
            # only query faces that are less than six voxels away
            gvp = np.sqrt(np.sum((vpfull[:,0:3]-grid[0:3,vox])**2,1))<radius
            gvw = np.sqrt(np.sum((vpfull[:,0:3]-grid[0:3,vox])**2,1))<radius
            gv = np.flatnonzero(np.array(gvp)+np.array(gvw))
            if len(gv)==0:
                continue
            gf=[]
            for v in xrange(gv.shape[0]):
                gf=gf+list(np.nonzero(fpfull==gv[v])[0])
            gf = set(gf)
            voxdepth=[]
            voxface=[]
            voxdist=[]
            for face in xrange(len(gf)):
                verts = map(int,fpfull[face,0:3])
                depth, inside, dist=self.incell(grid[0:3,vox],np.concatenate((vpfull[verts,0:3],vwfull[verts,0:3])))
                if inside==1:
                    print(depth)
                    voxdepth.append(depth)
                    voxface.append(face)
                    voxdist.append(dist)
            if len(voxdepth)==0:
                inimg[vox]=0
                depthimg[vox]=-1
            elif len(voxdepth)==1:
                inimg[vox]=voxface[0]
                depthimg[vox]=voxdepth[0]
            else:
                countimg=len(voxdepth)
                ind = np.argmin(voxdist)
                inimg[vox] = voxface[ind]
                depthimg[vox] = voxdepth[ind]
        _, outname, _ = split_filename(self.inputs.mask_file)
        outnii = nib.Nifti1Image(np.reshape(countimg, [sx, sy, sz]), None, header=mask.get_header())
        outnii.to_filename(outname + '_depthcount.nii.gz')
        outnii = nib.Nifti1Image(np.reshape(depthimg, [sx, sy, sz]), None, header=mask.get_header())
        outnii.to_filename(outname + '_depths.nii.gz')
        print(time.time()-t0)
        print('DepthCalc complete')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.mask_file)
        outputs["depths"] = os.path.abspath(base + '_depths.nii.gz')
        outputs["counts"] = os.path.abspath(base + '_depthcount.nii.gz')
        return outputs
