from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.interfaces.afni.base import (
    AFNICommandBase, AFNICommand, AFNICommandInputSpec, AFNICommandOutputSpec,
    Info, no_afni)
from nipype.utils.filemanip import split_filename

class RetinoPhaseInputSpec(AFNICommandInputSpec):
    in_file = File(desc='file to analyze', argstr='%s', position=-1)
    desc = traits.String(desc='experiment type: exp, con, clw, ccw', position=-2, argstr='%s')
    tstim = traits.Float(desc='duration of stimulus in seconds', argstr='-Tstim %s')
    nrings = traits.Int(desc='number of rings', argstr='-nrings s')
    nwedges = traits.Int(desc='number of wedges', argstr='-nwedges s')
    prestim = traits.Float(desc='time in seconds before stimulus began', argstr='-pre_stim %s')

class RetinoPhase(AFNICommand):
    """ Run 3dRetinoPhase in AFNI """

    _cmd = '3dRetinoPhase'
    input_spec = RetinoPhaseInputSpec
    output_spec = AFNICommandOutputSpec

    def _list_outputs(self):
        outputs = super(AFNICommand, self)._list_outputs()
        metadata = dict(name_source=lambda t: t is not None)
        out_names = list(self.inputs.traits(**metadata).keys())
        descdict = {'-exp': '.ecc+',
                    '-con': '.ecc-',
                    '-clw': '.pol+',
                    '-ccw': '.pol-'}
        desc = self.inputs.desc
        if out_names:
            for name in out_names:
                if outputs[name]:
                    p, b, ext = split_filename(outputs[name])
                    outputs[name] = p +'/' + b + descdict[desc] + ext
                    if ext == "":
                        outputs[name] = outputs[name] + "+orig.BRIK"

        return outputs