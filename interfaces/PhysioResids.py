from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec, InputMultiPath
from nipype.utils.filemanip import split_filename
from nilearn import image
import nibabel as nb
import numpy as np
import os


class PhysioResidsInputSpec(BaseInterfaceInputSpec):
    pefiles = traits.List(exists=True, desc='parameter_estimates', mandatory=True)
    residuals = File(exists=True, desc='residuals', mandatory=True)
    regressorlist = InputMultiPath(desc='expsetup', mandatory=True)


class PhysioResidsOutputSpec(TraitedSpec):
    out_res = File(exists=True, desc="summed residuals")
    # out_std = File(exists=True, desc="std summed residuals")


class PhysioResids(BaseInterface):
    input_spec = PhysioResidsInputSpec
    output_spec = PhysioResidsOutputSpec

    def _run_interface(self, runtime):
        regressortitles = self.inputs.regressorlist[0].regressor_names
        regressors = self.inputs.regressorlist[0].regressors
        nregress = len(regressortitles)
        offset = len(self.inputs.pefiles)-nregress

        resdata = nb.load(self.inputs.residuals).get_data()
        for i in range(nregress):
            if "compcor" in regressortitles[i]:
                pe = nb.load(self.inputs.pefiles[i+offset]).get_data()
                resdata = np.repeat(pe[:, :, :, np.newaxis], len(regressors[i]), axis=3)*regressors[i] + resdata

        _, base, _ = split_filename(self.inputs.residuals)
        new_img = nb.Nifti1Image(np.asarray(resdata), nb.load(self.inputs.residuals).affine, nb.load(self.inputs.residuals).header)
        nb.save(new_img, base + '_residuals.nii.gz')
        # nb.save(np.std(resdata,axis=2), base + '_std.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.residuals
        _, base, _ = split_filename(fname)
        outputs['out_res'] = os.path.abspath(base + '_residuals.nii.gz')
        # outputs['out_std'] = os.path.abspath(base + '_std.nii.gz')
        return outputs
