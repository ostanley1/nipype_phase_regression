from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, traits, File
import os
import nibabel as nb
from math import pi as pi
from numpy.fft import ifftn, fftn, fftshift
from numpy import angle, exp, cos, zeros, ones, dot, transpose
from nipype.utils.filemanip import split_filename

class MakeSWIInputSpec(BaseInterfaceInputSpec):
    phase_file = File(exists=True, mandatory=True, desc='filtered phase file in rad')
    mag_file = File(exists=True, desc='mag file')

class MakeSWIOutputSpec(TraitedSpec):
    out_file = File(exists=True)

class MakeSWI(BaseInterface):
    input_spec = MakeSWIInputSpec
    output_spec = MakeSWIOutputSpec

    def _run_interface(self, runtime):
        phase = nb.load(self.inputs.phase_file)
        mag = nb.load(self.inputs.mag_file)
        ima = -phase.get_data()
        temp = ima >= 0
        m = (pi + ima) / pi
        m[temp == 1] = 1
        swi = mag.get_data() * (m ** 4)
        _, outname, _ = split_filename(self.inputs.mag_file)
        outnii = nb.Nifti1Image(swi, None, header=phase.get_header())
        outnii.to_filename(outname + '_swi.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.mag_file)
        outputs["out_file"] = os.path.abspath(base + '_swi.nii.gz')
        return outputs