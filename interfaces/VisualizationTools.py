from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
import nibabel as nb
import numpy as np
import os
from niworkflows.viz.utils import cuts_from_bbox, compose_view, plot_registration
from nilearn.image import threshold_img, load_img

class RegistrationImageInputSpec(BaseInterfaceInputSpec):
    fixed_image = File(desc='fixed image')
    moving_image = File(desc='moving image')
    mask_image = File(desc='mask of interest')
    cuts = traits.Int(default_value=7)


class RegistrationImageOutputSpec(TraitedSpec):
    registration_svg = File(desc='svg')


class RegistrationImage(BaseInterface):
    input_spec = RegistrationImageInputSpec
    output_spec = RegistrationImageOutputSpec

    def _run_interface(self, runtime):
        moving_image = load_img(self.inputs.moving_image)
        fixed_image = load_img(self.inputs.fixed_image)
        mask = load_img(self.inputs.mask_image)
        cuts = cuts_from_bbox(mask, self.inputs.cuts)
        _, base, _ = split_filename(self.inputs.moving_image)
        compose_view(plot_registration(fixed_image,
                                       'fixed-image',
                                       estimate_brightness=True,
                                       cuts=cuts,
                                       label='fixed'),
                     plot_registration(moving_image,
                                       'moving-image',
                                       estimate_brightness=True,
                                       cuts=cuts,
                                       label='moving'),
                     out_file=base+'_check.svg')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.moving_image)
        outputs["registration_svg"] = os.path.abspath(base+'_check.svg')
        return outputs
