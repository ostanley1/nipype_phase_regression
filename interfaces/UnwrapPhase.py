from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, traits, File
import os
from nipype_phase_regression.interfaces.pyQSM import unwrap
import nibabel as nb
from nipype.utils.filemanip import split_filename
import numpy as np

class UnwrapPhaseInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='phase file in rad')
    mask_file = File(exists=True, desc='mask file')

class UnwrapPhaseOutputSpec(TraitedSpec):
    out_file = File(exists=True)

class UnwrapPhase(BaseInterface):
    input_spec = UnwrapPhaseInputSpec
    output_spec = UnwrapPhaseOutputSpec

    def _run_interface(self, runtime):
        phase = nb.load(self.inputs.in_file).get_data().astype('float32')
        mask = nb.load(self.inputs.mask_file).get_data().astype(bool)
        vols = phase.shape[-1]
        unwrapped = np.zeros_like(phase)

        for v in range(vols):
            unwrapped[:,:,:,v] = unwrap.unwrap3d(phase[:,:,:,v], mask)

        _, outname, _ = split_filename(self.inputs.in_file)
        outnii = nb.Nifti1Image(unwrapped, None, header=nb.load(self.inputs.in_file).get_header())
        outnii.to_filename(outname + '_phaseuw.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.in_file)
        outputs["out_file"] = os.path.abspath(base + '_phaseuw.nii.gz')
        return outputs