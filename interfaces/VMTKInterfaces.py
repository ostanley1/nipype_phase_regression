from nipype.interfaces.base import (
    TraitedSpec,
    CommandLineInputSpec,
    CommandLine,
    File,
    traits
)
import os

class VMTKImageVesselEnhancementInputSpec(CommandLineInputSpec):
    in_file = File(desc="File", exists=True, mandatory=True, argstr="-ifile %s")
    out_file = File(argstr='-ofile %s', name_source=['in_file'],
                        hash_files=False, name_template='%s_vesselenhanced',
                    keep_extension=True)
    method = traits.Str(desc="Vessel Enhancement Method", argstr="-m %s")
    sigma_steps = traits.Int(5, desc="number of smoothing steps to use",
                            argstr="-sigmasteps %s", use_default=True)
    sigma_max = traits.Float(0.2, desc="largest smoothing step to use in mm",
                            argstr="-sigmamax %s",  use_default=True)
    sigma_min = traits.Float(1, desc="smallest smoothing step to use in mm",
                            argstr="-sigmamin %s",  use_default=True)
    gamma = traits.Int(100, desc="background estimate",
                            argstr="-gamma %s",  use_default=True)


class VMTKImageVesselEnhancementOutputSpec(TraitedSpec):
    out_file = File(desc = "vessel enhanced image", exists = True)


class VMTKImageVesselEnhancement(CommandLine):
    input_spec = VMTKImageVesselEnhancementInputSpec
    output_spec = VMTKImageVesselEnhancementOutputSpec
    _cmd = 'vmtkimagevesselenhancement'
