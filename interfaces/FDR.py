import os
import os.path as op
import re
from glob import glob
import tempfile

import numpy as np

from nipype.utils.filemanip import (load_json, save_json, split_filename,
                                fname_presuffix)

from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec, Info
from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, File, traits

class FDRInputSpec(FSLCommandInputSpec):
    in_file = File(
        exists=True,
        argstr="-i %s",
        mandatory=True,
        desc='pvalues')
    mask_file = File(
        exists=True,
        argstr="-m %s",
        mandatory=True,
        desc='mask file for pvalues')
    pval = traits.Float(
        argstr="-q %s",
        mandatory=True,
        desc=("pvalue to FDR correct"))

class FDROutputSpec(TraitedSpec):
    out_thresh = traits.Float(desc='p value for significance')


class FDR(FSLCommand):
    input_spec = FDRInputSpec
    output_spec = FDROutputSpec

    _cmd = 'fdr'

    def aggregate_outputs(self, runtime=None, needed_outputs=None):
        outputs = self._outputs()
        # # local caching for backward compatibility
        outfile = os.path.join(os.getcwd(), 'stat_result.json')
        if runtime is None:
            try:
                out_stat = load_json(outfile)['thresh']
            except IOError:
                return self.run().outputs
        else:
            out_stat = []
            for line in runtime.stdout.split('\n'):
                if line:
                    values = line.split()
                    if len(values) == 1:
                        out_stat = float(values[0])
        save_json(outfile, dict(thresh=out_stat))
        outputs.out_thresh = out_stat
        return outputs
