from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os
from ..core import calc_pool_size
import nipype.algorithms.modelgen as modelgen
import matplotlib.pyplot as plt

class PercentChangeInputSpec(BaseInterfaceInputSpec):
    func = File(exists=True, desc="functional file")
    blockLength = traits.Int(desc="length of a single stimulus block in s", mandatory=True)
    blockPattern = traits.ListInt(desc="pattern of stimulus blocks (1 for stim, 0 for cont)", mandatory=True)
    TR = traits.Float(desc="rep time interval", mandatory=True)


class PercentChangeOutputSpec(TraitedSpec):
    pc_func = File(exists=True, desc="percent change map")
    out_dict = traits.Dict(desc="output dictionary")


class PercentChange(BaseInterface):
    input_spec = PercentChangeInputSpec
    output_spec = PercentChangeOutputSpec

    def pcvoxel(self, tc):
        if np.count_nonzero(tc-np.mean(tc)) != 0:
            ave_stim = float(sum(tc[self.blockArray != 0])) / float(np.count_nonzero(self.blockArray))
            ave_cont = float(sum(tc[self.blockArray == 0])) / \
                float(len(self.blockArray) - np.count_nonzero(self.blockArray))
            pc = 100 * (ave_stim - ave_cont)/ave_cont
            if not np.isnan(pc):
                return pc
            else:
                return 0
        else:
            return 0

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.func
        img = nb.load(fname)
        datashape = img.get_data()
        tr = self.inputs.tr
        volsperblock = float(self.inputs.blockLength) / float(tr)
        if volsperblock * len(self.inputs.blockPattern) != datashape.shape[3]:
            raise ValueError("error in model setup, aborting")

        data = datashape.reshape(-1, datashape.shape[-1])

        self.blockArray = np.repeat(self.inputs.blockPattern, volsperblock)

        # calculates the voxel by voxel map of (ave(stim)-ave(cont))/ave(cont) as is done in Stimulate
        poolsize = calc_pool_size()
        print "POOL SIZE: " + str(poolsize)
        parapool = Pool(poolsize)
        try:
            result = parapool.map(self.pcvoxel, data[xrange(data.shape[0]), :])
            parapool.close()
        except Exception as e:
            parapool.close()
            raise e
        pcmap = np.asarray(result).reshape(img.shape[:-1])
        new_img = nb.Nifti1Image(pcmap, img.affine, img.header)
        _, base, _ = split_filename(fname)
        nb.save(new_img, base + '_pcmap.nii.gz')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.func)
        outputs['pc_func'] = os.path.abspath(base + '_pcmap.nii.gz')
        return outputs


class CorrelateCanonicalHrfInputSpec(BaseInterfaceInputSpec):
    func = File(exists=True, desc="functional file")
    block_pattern = traits.ListInt(desc="pattern of stimulus blocks in units of volumes (1 for stim, 0 for cont)",
                                   mandatory=True)
    TR = traits.Float(desc="rep time interval", mandatory=True)


class CorrelateCanonicalHrfOutputSpec(TraitedSpec):
    hrfcorr = File(exists=True, desc="correlation with spm hrf")


class CorrelateCanonicalHrf(BaseInterface):
    input_spec = CorrelateCanonicalHrfInputSpec
    output_spec = CorrelateCanonicalHrfOutputSpec

    def correlate_with_hrf(self, tc):
        if np.count_nonzero(tc - np.mean(tc)) != 0:
            return np.correlate(tc, self.timecourse)/np.sqrt(np.correlate(tc, tc) *
                                                          np.correlate(self.timecourse,self.timecourse))
        else:
            return 0

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.func
        img = nb.load(fname)
        datashape = img.get_data()
        data = datashape.reshape(-1, datashape.shape[-1])

        # Calculate canonical hrf and convolve with block_pattern
        hrf = modelgen.spm_hrf(self.inputs.TR)
        self.timecourse = np.convolve(hrf, self.inputs.block_pattern, mode='full').tolist()[0:datashape.shape[3]]

        plt.plot(np.linspace(0,478.75, 384), self.timecourse)
        _, base, _ = split_filename(fname)
        plt.savefig(base + '_hrfmodel.png')

        # Deploy correlation calculation to the pool
        # TODO: stop pickling error
        # pool_size = calc_pool_size()
        # print "POOL SIZE: " + str(pool_size)
        # parapool = Pool(pool_size)
        # try:
        #     result = parapool.map(self.correlate_with_hrf, data[xrange(data.shape[0]), :])
        #     parapool.close()
        # except Exception as e:
        #     parapool.close()
        #     raise e

        result = []
        for i in xrange(data.shape[0]):
            result.append(self.correlate_with_hrf(data[i, :]))

        corrmap = np.asarray(result).reshape(img.shape[:-1])
        new_img = nb.Nifti1Image(corrmap, img.affine, img.header)

        nb.save(new_img, base + '_correlationhrf.nii.gz')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.func)
        outputs['hrfcorr'] = os.path.abspath(base + '_correlationhrf.nii.gz')
        return outputs