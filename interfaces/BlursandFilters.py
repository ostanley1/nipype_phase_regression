from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from nilearn import image
import nibabel as nb
import numpy as np
import os
from ..core import calc_pool_size
from numpy.fft import irfft, rfft, rfftfreq
from multiprocess import Pool


class NilearnBlurInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='image to blur', mandatory=True)
    fwhm = traits.Float(desc='fwhm of gaussian in mm', mandatory=True)


class NilearnBlurOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc="blurred image")


class NilearnBlur(BaseInterface):
    input_spec = NilearnBlurInputSpec
    output_spec = NilearnBlurOutputSpec

    def _run_interface(self, runtime):
        smoothimg = image.smooth_img(self.inputs.in_file, self.inputs.fwhm)
        _, base, _ = split_filename(self.inputs.in_file)
        nb.save(smoothimg, base + '_smooth.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_smooth.nii.gz')
        return outputs


class WindowFilterInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='input image', mandatory=True)
    low_freq = traits.Float(desc='lower frequency to filter', mandatory=True)
    high_freq = traits.Float(desc='higher frequency to filter', mandatory=True)
    TR = traits.Float(desc='scan repetition time', mandatory=True)


class WindowFilterOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc="filtered image")


class WindowFilter(BaseInterface):
    input_spec = WindowFilterInputSpec
    output_spec = WindowFilterOutputSpec

    def filter_tc(self, in_tc):
        if np.count_nonzero(in_tc-np.mean(in_tc)) != 0:
            print(in_tc.shape)
            spectra = rfft(in_tc)
            print(spectra.shape)
            freq = rfftfreq(len(in_tc), d=self.inputs.TR)
            # filter sets all frequencies outside the bounds to zero
            sig_idx = np.where((freq > self.inputs.low_freq) * (freq < self.inputs.high_freq))[0]
            filtspectra = np.zeros_like(spectra)
            filtspectra[sig_idx] = spectra[sig_idx]
            print((irfft(filtspectra) + np.mean(in_tc)).shape)
            return irfft(filtspectra) + np.mean(in_tc)
        else:
            print((np.zeros_like(in_tc)).shape)
            return np.zeros_like(in_tc)

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)

        img = nb.load(fname)
        print(img.shape)
        in_tc = np.array(img.get_data()).astype(float)
        in_tc = in_tc.reshape(-1, in_tc.shape[-1])
        pool_size = calc_pool_size()
        print("POOL SIZE: " + str(pool_size))
        parapool = Pool(pool_size)
        print(in_tc.shape)
        try:
            result = parapool.map(self.filter_tc, in_tc[range(in_tc.shape[0]), :])
            parapool.close()
        except Exception as e:
            print(e)
            parapool.close()
            return runtime

        print(np.asanyarray(result).shape)
        # save figure
        new_img = nb.Nifti1Image(np.asarray(result).reshape(img.shape), img.affine, img.header)
        print("BASE: " + base)
        nb.save(new_img, base + '_window_tc.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_window_tc.nii.gz')
        return outputs
