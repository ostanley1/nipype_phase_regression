from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
import nibabel as nb
import os
import dicom


class AddStudyDescriptionInputSpec(BaseInterfaceInputSpec):
    nii = File(exists=True, desc="nii to be modified", mandatory=True)
    dcm = traits.Str(desc="dcm to be modified", mandatory=True)


class AddStudyDescriptionOutputSpec(TraitedSpec):
    fixednii = File(exists=True, desc="modified nii")


class AddStudyDescription(BaseInterface):
    input_spec = AddStudyDescriptionInputSpec
    output_spec = AddStudyDescriptionOutputSpec

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.nii
        img = nb.load(fname)
        hdr = img.header

        # want to change intent name to hold CCOMB or SVDCOMB and _M or _P

        ds = dicom.read_file(self.inputs.dcm, stop_before_pixels=True)
        recon = ""
        if ds.StudyDescription == "Menon^Olivia":
            recon = "CCOMB_"
        elif ds.StudyDescription == "Gadgetron^SVD":
            recon = "SVDCOMB_"
        hdr['intent_name'] = str(recon + str(ds.ImageType[2]))

        new_img = nb.Nifti1Image(img.get_data(), img.affine, hdr)
        _, base, _ = split_filename(fname)
        nb.save(new_img, base + '_desript.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.nii
        _, base, _ = split_filename(fname)
        outputs["fixednii"] = os.path.abspath(base + '_desript.nii.gz')
        return outputs
