from scipy import odr as odr
import sys
from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
import nibabel as nb
import numpy as np
from numpy.fft import ifft, fft, fftshift
import os
from traits.trait_base import _Undefined

class PhaseFitOdrMultiRegressInputSpec(BaseInterfaceInputSpec):
    phase = File(exists=True, desc='phase image', mandatory=True)
    mag = File(exists=True, desc='mag image', mandatory=True)
    regress = File(exists=True, desc='regressor file')
    TR = traits.Float(desc='repetition time of the scan', mandatory=True)
    sig_ub = traits.Float(desc='Window filter upper bound')
    sig_lb = traits.Float(desc='Window filter lower bound')
    noise_lb = traits.Float(desc='Noise filer lower bound; should be higher than pyiological signal')


class PhaseFitOdrMultiRegressOutputSpec(TraitedSpec):
    sim = File(exists=True, desc="sim")
    filt = File(exists=True, desc="filt")
    corr = File(exists=True, desc="corr")
    residuals = File(exists=True, desc="residuals")
    resx = File(exists=True, desc="residuals x only")
    resy = File(exists=True, desc="residuals y only")
    stdm = File(exists=True, desc="std error in mag")
    stdp = File(exists=True, desc="std error in phase")
    fit = File(exists=True, desc="fit")


class PhaseFitOdrMultiRegress(BaseInterface):
    input_spec = PhaseFitOdrMultiRegressInputSpec
    output_spec = PhaseFitOdrMultiRegressOutputSpec

    def _run_regression(self, mag, ph, ):
        # get dimension of data
        saveshape = np.array(mag.shape)
        nt = mag.shape[-1]

        # initialize output arrays
        scales = np.zeros(np.prod(saveshape[0:-1]))
        filt = np.zeros_like(np.reshape(mag, (-1, nt)))
        sim = np.zeros_like(np.reshape(mag, (-1, nt)))

        stdm = np.zeros_like(scales)
        stdp = np.zeros_like(scales)
        r2 = np.zeros_like(scales)

        # create mask where mean of ts is >3% of max
        mm = np.mean(mag, axis=-1)
        mask = mm > 0.03 * np.max(mm)

        # apply any temporal filtering
        # freqs for FT indices
        freqs = np.linspace(-1.0, 1.0, nt) / (2 * self.inputs.TR)

        # if filtering is included create the idx which will be considered signal
        if self.inputs.sig_ub > 0 and self.inputs.sig_lb > 0:
            sig_idx = (freqs > self.inputs.sig_lb) * (freqs < self.inputs.sig_ub)
        elif self.inputs.sig_ub > 0 and self.inputs.sig_lb <= 0:
            sig_idx = (freqs < self.inputs.sig_ub)
        elif self.inputs.sig_lb > 0 and self.inputs.sig_ub <= 0:
            sig_idx = (freqs < self.inputs.sig_lb)
        else:
            sig_idx = np.ones_like(freqs)

        # in order to estimate noise create noise indicies
        noise_mask = fftshift((abs(freqs) > self.inputs.noise_lb))

        # now that we have initialized everything we want to flatten the array to operate voxel-wise
        mag = np.reshape(mag, (-1, nt))
        ph = np.reshape(ph, (-1, nt))
        mask = np.reshape(mask, (-1, 1))

        if isinstance(self.inputs.regress, _Undefined):
            model = odr.unilinear
            beta = np.zeros((np.prod(saveshape[:-1]),2))
        else:
            model = odr.multilinear
            beta = np.zeros_like(np.reshape(mag, (-1, nt)))
            # load in regressors

        for x in range(mag.shape[0]):
            if mask[x]:
                # Estimate std() from power spectrum of high-freq tc
                stdm[x] = abs(np.std(ifft(fft(mag[x, :] - np.mean(mag[x, :], -1)) * noise_mask), -1))
                stdp[x] = abs(np.std(ifft(fft(ph[x, :] - np.mean(ph[x, :], -1)) * noise_mask), -1))

                # Notch filter data according to sig idx
                magfilt = abs(ifft(fft(mag[x, :]) * fftshift(sig_idx)))
                phfilt = abs(ifft(fft(ph[x, :]) * fftshift(sig_idx)))

                if self.inputs.sig_lb >= 0:
                    magfilt  = magfilt + np.mean(mag[x,:])
                    phfilt = phfilt + np.mean(ph[x, :])

                # if linear model is Mag = A*phase + B
                if isinstance(self.inputs.regress, _Undefined):
                    ests = [stdm[x] / stdp[x], np.mean(magfilt) / np.mean(phfilt)]
                    mydata = odr.RealData(phfilt, magfilt, sx=stdm[x], sy=stdp[x])
                    odr_obj = odr.ODR(mydata, model, beta0=ests, maxit=200)
                    res = odr_obj.run()

                    # take out scaled phase signal and re-mean
                    sim[x, :] = res.beta[0] * ph[x,:] + res.beta[1]
                    filt[x, :] = mag[x, :] - res.y + np.mean(mag[x, :])
                    beta[x,:] = res.beta

                    # estimate R^2
                    r2[x] = 1.0 - sum((mag[x, :] - res.y) ** 2) / sum((mag[x, :] - np.mean(mag[x, :])) ** 2)

                else:
                    ests = [stdm / stdp, np.mean(mag[x, :]) / np.mean(ph[x, :])]
                    mydata = odr.RealData(ph[x, :], mag[x, :], sx=stdm[x], sy=stdp[x])
                    odr_obj = odr.ODR(mydata, model, beta0=ests, maxit=200)
                    res = odr_obj.run()

                    # take out scaled phase signal and re-mean
                    sim[x, :] = res.y
                    filt[x, :] = mag[x, :] - res.y + mean(mag[x, :])

                    # estimate R^2
                    r2[x] = 1.0 - sum((mag[x, :] - res.y) ** 2) / sum((mag[x, :] - mean(mag[x, :])) ** 2)

        return np.reshape(sim,saveshape),np.reshape(filt,saveshape), \
               np.reshape(r2,saveshape[:-1]), np.reshape(stdm,saveshape[:-1]), \
               np.reshape(stdp,saveshape[:-1]), np.reshape(beta, np.append(saveshape[:-1],-1))

    def _run_interface(self, runtime):
        #load in magnitude and phase time course
        f = nb.load(self.inputs.mag)
        mag = f.get_data()

        f = nb.load(self.inputs.phase)
        ph = f.get_data()

        sim, filt, r2, stdm, stdp = self._run_regression(mag, ph)

        _, outname, _ = split_filename(self.inputs.mag)
        print(outname)
        outnii = nb.Nifti1Image(sim, affine=f.affine, header=f.get_header())
        outnii.to_filename(outname + '_sim.nii.gz')
        outnii = nb.Nifti1Image(filt, affine=f.affine, header=f.get_header())
        outnii.to_filename(outname + '_filt.nii.gz')
        # outnii = nb.Nifti1Image(residuals, affine=f.affine, header=f.get_header())
        # outnii.to_filename(outname + '_residuals.nii.gz')
        # outnii = nb.Nifti1Image(delta, affine=f.affine, header=f.get_header())
        # outnii.to_filename(outname + '_xres.nii.gz')
        # outnii = nb.Nifti1Image(eps, affine=f.affine, header=f.get_header())
        # outnii.to_filename(outname + '_yres.nii.gz')
        # outnii = nb.Nifti1Image(xshift, affine=f.affine, header=f.get_header())
        # outnii.to_filename(outname + '_xplus.nii.gz')

        # plot fit statistic info
        outnii = nb.Nifti1Image(ratio, affine=f.affine, header=f.get_header())
        outnii.to_filename(outname + '_ratio.nii.gz')
        outnii = nb.Nifti1Image(stdp, affine=f.affine, header=f.get_header())
        outnii.to_filename(outname + '_stdp.nii.gz')
        outnii = nb.Nifti1Image(stdm, affine=f.affine, header=f.get_header())
        outnii.to_filename(outname + '_stdm.nii.gz')
        outnii = nb.Nifti1Image(r2, affine=f.affine, header=f.get_header())
        outnii.to_filename(outname + '_r2.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.mag)
        outputs["sim"] = os.path.abspath(base + '_sim.nii.gz')
        outputs["filt"] = os.path.abspath(base + '_filt.nii.gz')
        outputs["corr"] = os.path.abspath(base + '_r2.nii.gz')
        outputs["residuals"] = os.path.abspath(base + '_residuals.nii.gz')
        # outputs["resx"] = os.path.abspath(base + '_xres.nii.gz')
        # outputs["resy"] = os.path.abspath(base + '_yres.nii.gz')
        outputs["stdm"] = os.path.abspath(base + '_stdm.nii.gz')
        outputs["stdp"] = os.path.abspath(base + '_stdp.nii.gz')
        outputs["fit"] = os.path.abspath(base + '_pgfit.nii.gz')
        return outputs
