from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
import nibabel as nb
import numpy as np
import os
from scipy.signal import correlate2d
from scipy.fftpack import fftn, ifftn

class SpatialCrossCorrelation2DInputSpec(BaseInterfaceInputSpec):
    map1 = File(exists=True, desc="map to cross correlate")
    map2 = File(exists=True, desc="second map to cross correlate")
    thresh = traits.Float(desc='threshold for activation maps')


class SpatialCrossCorrelation2DOutputSpec(TraitedSpec):
    spatialpsf = File(desc="spatial cross correlation on a slice by slice basis")


class SpatialCrossCorrelation2D(BaseInterface):
    input_spec = SpatialCrossCorrelation2DInputSpec
    output_spec = SpatialCrossCorrelation2DOutputSpec

    def _run_interface(self, runtime):
        # read in data
        fnamemag = self.inputs.map1
        img = nb.load(fnamemag)
        data1 = img.get_data()

        fnamesim = self.inputs.map2
        img = nb.load(fnamesim)
        data2 = img.get_data()

        # calculate 2d cross correlations by slice
        result = []

        # use only correlations with hrf above threshold to create psf
        data1[data1 < self.inputs.thresh] = 0
        data2[data2 < self.inputs.thresh] = 0

        for s in xrange(data1.shape[2]):
            result.append(correlate2d(data1[:, :, s], data2[:, :, s], 'full'))

        new_img = nb.Nifti1Image(np.asarray(result), img.affine, img.header)
        _, basesim, _ = split_filename(fnamesim)
        _, basemag, _ = split_filename(fnamemag)
        base = basesim
        if basesim == basemag:
            base = basesim + 'auto'
        nb.save(new_img, base + str(self.inputs.thresh) + '_spatialpsf2d.nii.gz')
        self.filename = base + str(self.inputs.thresh) + '_spatialpsf2d.nii.gz'

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['spatialpsf'] = os.path.abspath(self.filename)

        return outputs


class SpatialCrossCorrelation3DInputSpec(BaseInterfaceInputSpec):
    map1 = File(exists=True, desc="map to cross correlate")
    map2 = File(exists=True, desc="second map to cross correlate")
    thresh = traits.Float(desc='threshold for activation maps')

class SpatialCrossCorrelation3DOutputSpec(TraitedSpec):
    spatialpsf = File(desc="spatial cross correlation on a slice by slice basis")

class SpatialCrossCorrelation3D(BaseInterface):
    """Code borrowed from: http://pastebin.com/x1NJqWWm"""
    input_spec = SpatialCrossCorrelation3DInputSpec
    output_spec = SpatialCrossCorrelation3DOutputSpec

    def ndflip(self, a):
        """Inverts an n-dimensional array along each of its axes"""
        ind = (slice(None, None, -1),) * a.ndim
        return a[ind]

    def _run_interface(self, runtime):
        # read in data
        fnamemag = self.inputs.map1
        img = nb.load(fnamemag)
        a = img.get_data()

        fnamesim = self.inputs.map2
        img = nb.load(fnamesim)
        t = img.get_data()

        # use only correlations with hrf above threshold to create psf
        a[a < self.inputs.thresh] = 0
        t[t < self.inputs.thresh] = 0

        outdims = np.array([a.shape[dd] + t.shape[dd] - 1 for dd in xrange(a.ndim)])
        af = fftn(a, shape=outdims)
        tf = fftn(self.ndflip(t), shape=outdims)

        # 'non-normalized' cross-correlation
        xcorr = np.real(ifftn(tf * af))

        new_img = nb.Nifti1Image(np.asarray(xcorr), img.affine, img.header)
        _, basesim, _ = split_filename(fnamesim)
        _, basemag, _ = split_filename(fnamemag)
        base = basesim
        if basesim == basemag:
            base = basesim + 'auto'
        nb.save(new_img, base + str(self.inputs.thresh) + '_spatialpsf3d.nii.gz')
        self.filename = base + str(self.inputs.thresh) + '_spatialpsf3d.nii.gz'

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['spatialpsf'] = os.path.abspath(self.filename)

        return outputs
