from nipype.interfaces.base import BaseInterface, isdefined, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec, Info
from nipype.utils.filemanip import save_json, load_json, split_filename
import os

class meanTimeCourseInputSpec(FSLCommandInputSpec):
    in_file = traits.File(exists=True, mandatory=True, argstr="-i %s", position=1, desc="file to take mean timecourse of")
    out_file = traits.File(argstr="-o %s", position=2, desc="output file", genfile=True)
    mask = traits.File(exists=True, argstr="-m %s", position=3, desc="mask for input", xor=['coords'])
    coords = traits.List(position=3, desc="coordinate to report timecourse of", xor=['mask'])
    

class meanTimeCourseOutputSpec(TraitedSpec):
    out_file = traits.File(desc='out_file')

class meanTimeCourse(FSLCommand):
    """
    Use fslmeants to output averge timecourse to console or a file.

    Examples
    --------

    >>> import nipype_phase_regression.interfaces as pr
    >>> avetc = pr..meanTimeCourse()
    >>> avetc.inputs.in_file = 'functional.nii.gz'
    >>> avetc.inputs.mask = 'mask.nii.gz'
    >>> avetc.run() #doctest: +SKIP

    """
    _cmd = "fslmeants"
    input_spec = meanTimeCourseInputSpec
    output_spec = meanTimeCourseOutputSpec

    def _format_arg(self, name, spec, value):
        if name == "coords":
            if isinstance(value, list):
                args = " ".join(value)
                return "-c %s" % args
        return super(meanTimeCourse, self)._format_arg(name, spec, value)

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_tc.txt')
        return outputs

    def _gen_filename(self, name):
        if name == 'out_file':
            return self._gen_outfilename()
        return None

    def _gen_outfilename(self):
        out_file = self.inputs.out_file
        print out_file
        print isdefined(out_file)
        print isdefined(self.inputs.in_file)
        print self.inputs
        if isdefined(out_file):
            out_file = os.path.realpath(out_file)
        if not isdefined(out_file) and isdefined(self.inputs.in_file):
            _, base, _ = split_filename(self.inputs.in_file)
            out_file = base + '_tc.txt'
        return os.path.abspath(out_file)