from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os
import scipy.signal as signal

# TODO: determine affects of warning vs no mc
import warnings
warnings.simplefilter('ignore', np.RankWarning)

def calcPoolSize():
    import psutil
    cpustate = psutil.cpu_percent(percpu=True)
    return int((len(cpustate) - sum([x>50 for x in cpustate]))/ 6) # six should be changed to (number of map nodes + 5)
    # This number is owned by node not interface, need multiprocessing equipped node to use that

class SavitzkyGolayInputSpec(BaseInterfaceInputSpec):
    phase = File(exists=True, desc='phase image', mandatory=True)
    mag = File(exists=True, desc='mag image', mandatory=True)

class SavitzkyGolayOutputSpec(TraitedSpec):
    sim = File(exists=True, desc="sim")
    filt = File(exists=True, desc="filt")
    R2 = File(exists=True, desc="R2")
    framesize = File(exists=True, desc="framesize")
    polyorder = File(exists=True, desc="polyorder")
    fit = File(exists=True, desc="fit")

class SavitzkyGolay(BaseInterface):
    input_spec = SavitzkyGolayInputSpec
    output_spec = SavitzkyGolayOutputSpec

    def calcgof(self, magtc, filttc):
        if np.std(magtc) != 0:
            return 1-(np.std(filttc)/np.std(magtc))
        else:
            return 0

    def applyFilter(self, (magtc, phasetc)):
        import pprint
        try:
            # checking for all zeros accounts for the masking
            if np.count_nonzero(magtc) == 0 or np.count_nonzero(phasetc) == 0:
                return 0, 0, 0, np.zeros_like(magtc), np.zeros_like(phasetc), np.array([0,0])
            else:
                framesize = np.arange(5, 50, 4)
                polynomial_order = np.arange(2, 13, 1)
                noSG = np.polyfit(phasetc, magtc, 1)
                sim = np.polyval(noSG, phasetc)
                filt = magtc - np.polyval(noSG, phasetc)
                SGbest = self.calcgof(magtc, filt)
                Nbest = 0
                pbest = 0
                for N in framesize:
                    for p in polynomial_order:
                        if N <= p:
                            continue
                        else:
                            self.filtphase = signal.savgol_filter(phasetc, N, p)
                            SG = np.polyfit(self.filtphase, magtc, 1)
                            SGgof = self.calcgof(magtc, magtc - np.polyval(SG, phasetc))
                            if SGgof > SGbest:
                                Nbest=N
                                pbest=p
                                SGbest=SGgof
                                sim = np.polyval(SG, phasetc)
                                filt = magtc - np.polyval(SG, phasetc)
                return SGbest, Nbest, pbest, sim, filt, SG
        except Exception as e:
            pprint.pprint(magtc)
            pprint.pprint(phasetc)
            pprint.pprint(self.filtphase)
            print e
            raise e


    def _run_interface(self, runtime):
        # read in data
        fnamemag = self.inputs.mag
        _, basemag, _ = split_filename(fnamemag)
        fnamephase = self.inputs.phase
        _, basephase, _ = split_filename(fnamephase)

        print basemag[:30]
        print basephase[:30]

        if "Run" in basemag:
            assert basemag.replace('mag', '') == basephase.replace('phase', '')
        elif "data" in basemag:
            assert basemag[:30] == basephase[:30]
        elif "MENON_OLIVIA" in basemag:
            # catches motion correction and non-motion correction cases
            assert basemag.split('.')[3] == basephase.split('.')[3] or int(basemag.split('.')[3]) == int(basephase.split('.')[3])-1


        img = nb.load(fnamemag)
        magimg = np.array(img.get_data()).astype(float)

        img = nb.load(fnamephase)
        phaseimg = np.array(img.get_data()).astype(float)

        if magimg.shape != phaseimg.shape:
            print "Error: mag and phase dimension do not match"
            return runtime

        mag_arr = magimg.reshape(-1, magimg.shape[-1])
        phase_arr = phaseimg.reshape(-1, phaseimg.shape[-1])
        poolsize = calcPoolSize()
        print "POOL SIZE: " + str(poolsize)
        parapool = Pool(poolsize)
        try:
            result = parapool.map(self.applyFilter, zip(mag_arr[xrange(mag_arr.shape[0]),:],phase_arr[xrange(phase_arr.shape[0]),:]))
            SGgof, N, p, sim, filt, fit = zip(*result)
            parapool.close()
        except Exception as e:
            print e
            parapool.close()
            return runtime

        # save figures
        base = basemag.replace('magprocessed','SGfilt')
        new_img = nb.Nifti1Image(np.asarray(sim).reshape(magimg.shape), img.affine, img.header)
        nb.save(new_img, base + '_sgsim.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(filt).reshape(magimg.shape), img.affine, img.header)
        nb.save(new_img, base + '_sgfilt.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(SGgof).reshape(magimg.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_R2.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(N).reshape(magimg.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_N.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(p).reshape(magimg.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_p.nii.gz')
        magsize = list(magimg.shape)
        magsize[3] = 2
        new_img = nb.Nifti1Image(np.asarray(fit).reshape(magsize), img.affine, img.header)
        nb.save(new_img, base + '_sgfit.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.mag
        _, base, _ = split_filename(fname.replace('magprocessed','SGfilt'))
        outputs["sim"] = os.path.abspath(base + '_sgsim.nii.gz')
        outputs["filt"] = os.path.abspath(base + '_sgfilt.nii.gz')
        outputs["R2"] = os.path.abspath(base + '_R2.nii.gz')
        outputs["framesize"] = os.path.abspath(base + '_N.nii.gz')
        outputs["polyorder"] = os.path.abspath(base + '_p.nii.gz')
        outputs["fit"] = os.path.abspath(base + '_sgfit.nii.gz')
        return outputs

