import nitime
from nitime.timeseries import TimeSeries
from nitime.analysis import FilterAnalyzer
import scipy.signal
from scipy.linalg import lstsq
from scipy import stats as stat
from scipy import odr as odr
from numpy import *
from interfaces.Varian import *
import sys
import nitime
from nitime.timeseries import TimeSeries as TS
from nitime.analysis import SpectralAnalyzer, FilterAnalyzer
from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
# from numpy.fft import ifft, fft, fftshift
import os
import matplotlib.pyplot as plt

def calcPoolSize():
    import psutil
    cpustate = psutil.cpu_percent(percpu=True)
    return int((len(cpustate) - sum([x>50 for x in cpustate]))/ 6) # six should be changed to (number of map nodes + 5)
    # This number is owned by node not interface, need multiprocessing equipped node to use that

class phaseRegressionODRoutlierInputSpec(BaseInterfaceInputSpec):
    phase = File(exists=True, desc='phase image', mandatory=True)
    mag = File(exists=True, desc='mag image', mandatory=True)
    bpphase = File(exists=True, desc='bandpassed phase image', mandatory=True)
    bpmag = File(exists=True, desc='bandpassed mag image', mandatory=True)
    TR = traits.Float(desc='repetition time of the scan', mandatory=True)
    sig_ub = traits.Float(desc='Window filter upper bound')
    sig_lb = traits.Float(desc='Window filter lower bound')
    noise_lb = traits.Float(desc='Noise filer lower bound; should be higher than pyiological signal')


class phaseRegressionODRoutlierOutputSpec(TraitedSpec):
    sim = File(exists=True, desc="sim")
    filt = File(exists=True, desc="filt")
    corr = File(exists=True, desc="corr")
    residuals = File(exists=True, desc="residuals")
    resx = File(exists=True, desc="residuals x only")
    resy = File(exists=True, desc="residuals y only")
    stdm = File(exists=True, desc="std error in mag")
    stdp = File(exists=True, desc="std error in phase")
    # fit = File(exists=True, desc="fit")

class phaseRegressionODRoutlier(BaseInterface):
    input_spec = phaseRegressionODRoutlierInputSpec
    output_spec = phaseRegressionODRoutlierOutputSpec

    def MahalanobisDist(self, x, y):
        covariance_xy = np.cov(x, y, rowvar=0)
        inv_covariance_xy = np.linalg.inv(covariance_xy)
        xy_mean = np.mean(x), np.mean(y)
        x_diff = np.array([x_i - xy_mean[0] for x_i in x])
        y_diff = np.array([y_i - xy_mean[1] for y_i in y])
        diff_xy = np.transpose([x_diff, y_diff])

        md = []
        for i in range(len(diff_xy)):
            md.append(np.sqrt(np.dot(np.dot(np.transpose(diff_xy[i]), inv_covariance_xy), diff_xy[i])))
        return md

    def _run_interface(self, runtime):
        f = nb.load(self.inputs.mag)
        mag = f.get_data()
        f = nb.load(self.inputs.bpmag)
        magn = f.get_data()

        f = nb.load(self.inputs.phase)
        ph = f.get_data()
        f = nb.load(self.inputs.bpphase)
        phn = f.get_data()

        saveshape = array(mag.shape)
        nt = mag.shape[-1]

        scales = zeros(saveshape[0:-1])
        filt = zeros_like(mag)
        sim = zeros_like(mag)
        residuals = zeros_like(mag)
        delta = zeros_like(mag)
        eps = zeros_like(mag)
        xshift = zeros_like(mag)
        stdm = zeros_like(scales)
        stdp = zeros_like(scales)
        r2 = zeros_like(scales)
        numouts = zeros_like(scales)

        mag = array(mag)
        mm = mean(mag, axis=-1)
        mask = mm > 0.03 * np.max(mm)
        outs = mask < 1

        # mag[outs,:] = 0

        linear = odr.unilinear

        # freqs for FT indices
        freqs = linspace(-1.0, 1.0, nt) / (2 * self.inputs.TR)
        print freqs

        sig_idx = where((freqs > self.inputs.sig_lb) * (freqs < self.inputs.sig_ub))[0]
        noise_idx = where((abs(freqs) > self.inputs.noise_lb))[0]
        noise_mask = fftshift(1.0 * (abs(freqs) > self.inputs.noise_lb))
        # Estimate sigmas in one preproc step


        for x in xrange(mag.shape[0]):
            temp = mag[x, :, :, :]
            mu = mean(temp, -1)
            stdm[x, :, :] = std(ifft(fft(temp - mu[..., newaxis]) * noise_mask), -1)
            temp = ph[x, :, :, :]
            mu = mean(temp, -1)
            stdp[x, :, :] = std(ifft(fft(temp - mu[..., newaxis]) * noise_mask), -1)

        for x in xrange(mag.shape[0]):
            print 'Processing xVal {} / {}...\n'.format(x, mag.shape[0])
            for y in xrange(mag.shape[1]):
                for z in xrange(mag.shape[2]):
                    if x == 111 and y == 43 and z == 21:
                    # if mask[x, y, z]:
                        # Estimate std() from power spectrum of high-freq tc

                        # save scalings for investigation
                        s1 = stdm[x, y, z]
                        s2 = stdp[x, y, z]

                        # now reference notch filtered data
                        # make contiguous
                        x1 = magn[x, y, z, :].copy()
                        x2 = phn[x, y, z, :].copy()

                        mm = mean(x1)
                        mp = mean(x2)

                        sm = std(x1)
                        sp = std(x2)

                        ests = [sm / sp, mm / mp]

                        if (s1 / sm > 0.5):
                            s1 = sm / 3.0

                        if (s2 / sp > 0.5):
                            s2 = sp / 3.0

                        s1 = s1 / 4.0
                        s2 = s2 / 5.0

                        md = self.MahalanobisDist(x2, x1)
                        magdata = x1
                        phasedata = x2
                        for i in xrange(len(md)):
                            if md[i] ** 2 > stat.chi2.ppf(0.975, 2):
                                print "in if"
                                magdata = np.delete(magdata, i)
                                phasedata = np.delete(phasedata, i)
                                numouts[x,y,z] = numouts[x,y,z] + 1
                        # and fit model
                        # mag = A*phase + B
                        # (y=mx+b)
                        # call : (x,y,sx,sy)
                        mydata = odr.RealData(phasedata, magdata, sx=s2, sy=s1)
                        odr_obj = odr.ODR(mydata, linear, beta0=ests, maxit=200)
                        res = odr_obj.run()
                        est = res.beta[0] * x2 + res.beta[1]
                        rsq = 1.0 - sum((x1 - est) ** 2) / sum((x1 - mm) ** 2)

                        # take out scaled phase signal and re-mean
                        sim[x, y, z, :] = est

                        filt[x, y, z, :] = x1 - est + mm
                        # estimate R^2
                        r2[x, y, z] = rsq
                        print rsq
                        # estimate residuals
                        # residuals[x,y,z,:] = np.sign(x1-est) * (res.delta**2 + res.eps**2)
                        # delta[x, y, z, :] = res.delta
                        # eps[x, y, z, :] = res.eps
                        # xshift[x,y,z,:] = res.xplus

                        fig,ax = plt.subplots(1,1)
                        plt.scatter(x2,x1)
                        plt.plot(x2, est)
                        plt.plot([np.sum(x2)/len(x2)], [np.sum(x1)/len(x1)], 'ro')
                        plt.savefig('phasefit.png')
                        plt.show()
                        plt.clf()

                        # xstar = s2*np.sqrt( ((s1*res.delta)**2) /( (s1*res.delta)**2 + (s2*res.eps)**2 ) )
                        # ystar = s1 * np.sqrt(((s2 * res.eps) ** 2) /((s1 * res.delta) ** 2 + (s2 * res.eps) ** 2))
                        # xres = res.xplus - xstar
                        # yres = res.y - ystar
                        #
                        # fig, ax = plt.subplots(1, 1)
                        # plt.scatter(xres, yres)
                        # plt.plot(res.delta,res.eps)
                        # plt.savefig('phaseres.png')
                        # plt.show()
                        # plt.clf()
                        #
                        # fig, ax = plt.subplots(1, 1)
                        # plt.errorbar(x2, x1, xerr=res.delta, yerr=res.eps, fmt='r+')
                        # plt.plot(xshift[x, y, z, :], est)
                        # plt.savefig('phasefitres.png')
                        # plt.show()
                        # plt.clf()
                        #
                        # # calculate center of mass
                        # com = [np.sum(x2)/len(x2), np.sum(x1)/len(x1)]
                        # distcom = np.sqrt((x2-com[0])**2 + (x1-com[1])**2)
                        # print np.min(distcom)
                        # print np.max(distcom)
                        # fig, ax = plt.subplots(1,1)
                        # plt.hist(distcom, bins=50)
                        # plt.savefig("com.png")
                        # plt.clf()
                        #
                        # # IQ test of residuals
                        # Q1 = np.percentile(residuals[x,y,z,:], 25)
                        # Q3 = np.percentile(residuals[x,y,z,:], 75)
                        # Median = np.percentile(residuals[x,y,z,:], 50)
                        # lower_outlier = Q1 - 1.5*(Median - Q1)
                        # upper_outlier = Q3 + 1.5*(Q3 - Median)
                        # print "Residuals"
                        # print lower_outlier
                        # print np.sum(residuals[x,y,z,:]<lower_outlier)
                        # print upper_outlier
                        # print np.sum(residuals[x,y,z,:]>upper_outlier)
                        #
                        # # IQ test of distance from COM
                        # Q1 = np.percentile(distcom[:], 25)
                        # Q3 = np.percentile(distcom[:], 75)
                        # Median = np.percentile(distcom[:], 50)
                        # lower_outlier = Q1 - 1.5 * (Median - Q1)
                        # upper_outlier = Q3 + 1.5 * (Q3 - Median)
                        # print "Center of Mass"
                        # print lower_outlier
                        # print np.sum(distcom[:] < lower_outlier)
                        # print upper_outlier
                        # print np.sum(distcom[:] > upper_outlier)

        _,outname,_ = split_filename(self.inputs.mag)
        print outname
        outnii = nb.Nifti1Image(sim, None, header=f.get_header())
        outnii.to_filename(outname + '_sim.nii.gz')
        outnii = nb.Nifti1Image(filt, None, header=f.get_header())
        outnii.to_filename(outname + '_filt.nii.gz')
        outnii = nb.Nifti1Image(residuals, None, header=f.get_header())
        outnii.to_filename(outname + '_residuals.nii.gz')
        outnii = nb.Nifti1Image(delta, None, header=f.get_header())
        outnii.to_filename(outname + '_xres.nii.gz')
        outnii = nb.Nifti1Image(eps, None, header=f.get_header())
        outnii.to_filename(outname + '_yres.nii.gz')
        outnii = nb.Nifti1Image(xshift, None, header=f.get_header())
        outnii.to_filename(outname + '_xplus.nii.gz')

        # plot fit statistic info
        ratio = stdp / stdm
        ratio[isinf(ratio)] = 0
        ratio[isnan(ratio)] = 0
        outnii = nb.Nifti1Image(ratio, None, header=f.get_header())
        outnii.to_filename(outname + '_ratio.nii.gz')
        outnii = nb.Nifti1Image(stdp, None, header=f.get_header())
        outnii.to_filename(outname + '_stdp.nii.gz')
        outnii = nb.Nifti1Image(stdm, None, header=f.get_header())
        outnii.to_filename(outname + '_stdm.nii.gz')
        outnii = nb.Nifti1Image(r2, None, header=f.get_header())
        outnii.to_filename(outname + '_r2.nii.gz')
        outnii = nb.Nifti1Image(numouts, None, header=f.get_header())
        outnii.to_filename(outname + '_outliercount.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, _ = split_filename(self.inputs.mag)
        outputs["sim"] = os.path.abspath(base + '_sim.nii.gz')
        outputs["filt"] = os.path.abspath(base + '_filt.nii.gz')
        outputs["corr"] = os.path.abspath(base + '_r2.nii.gz')
        outputs["residuals"] = os.path.abspath(base + '_residuals.nii.gz')
        outputs["resx"] = os.path.abspath(base + '_xres.nii.gz')
        outputs["resy"] = os.path.abspath(base + '_yres.nii.gz')
        outputs["stdm"] = os.path.abspath(base + '_stdm.nii.gz')
        outputs["stdp"] = os.path.abspath(base + '_stdp.nii.gz')
        # outputs["fit"] = os.path.abspath(base + '_pgfit.nii.gz')
        return outputs