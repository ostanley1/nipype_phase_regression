from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os

def calcPoolSize():
    import psutil
    cpustate = psutil.cpu_percent(percpu=True)
    return int((len(cpustate) - sum([x>50 for x in cpustate]))/ 6) # six should be changed to (number of map nodes + 5)
    # This number is owned by node not interface, need multiprocessing equipped node to use that

class applyFitInputSpec(BaseInterfaceInputSpec):
    phase = File(exists=True, desc='phase image', mandatory=True)
    mag = File(exists=True, desc='mag image', mandatory=True)
    fit = File(exists=True, desc='fit results from any phase regression', mandatory=True)

class applyFitOutputSpec(TraitedSpec):
    out_sim = File(exists=True, desc="simulated magnitude image")
    out_filt = File(exists=True, desc="filtered magnitude image")

class applyFit(BaseInterface):
    input_spec = applyFitInputSpec
    output_spec = applyFitOutputSpec

    def fit(self, (mag_tc, phase_tc, fit_tc)):
        if np.count_nonzero(mag_tc - np.mean(mag_tc)) != 0 and np.count_nonzero(phase_tc - np.mean(phase_tc)) != 0:
            return np.polyval(fit_tc, phase_tc), mag_tc - np.polyval(fit_tc, phase_tc)
        else:
            return np.zeros_like(mag_tc), np.zeros_like(mag_tc)


    def _run_interface(self, runtime):
        # read in data
        fnamemag = self.inputs.mag
        _, basemag, _ = split_filename(fnamemag)
        fnamephase = self.inputs.phase
        _, basephase, _ = split_filename(fnamephase)
        fnamefit = self.inputs.fit
        _, basefit, _ = split_filename(fnamefit)

        if "Run" in basemag:
            assert basemag.replace('mag', '') == basephase.replace('phase', '')
        elif "data" in basemag:
            assert basemag[:30] == basephase[:30]
        elif "MENON_OLIVIA" in basemag:
            assert basemag.split('.')[3] == basephase.split('.')[3] or int(basemag.split('.')[3]) == int(basephase.split('.')[3])-1

        img = nb.load(fnamemag)
        magimg = np.array(img.get_data()).astype(float)

        img = nb.load(fnamephase)
        phaseimg = np.array(img.get_data()).astype(float)

        img = nb.load(fnamefit)
        fitimg = np.array(img.get_data()).astype(float)

        mag_arr = magimg.reshape(-1, magimg.shape[-1])
        phase_arr = phaseimg.reshape(-1, phaseimg.shape[-1])
        fit_arr = fitimg.reshape(-1, fitimg.shape[-1])
        poolsize = calcPoolSize()
        print "POOL SIZE: " + str(poolsize)
        parapool = Pool(poolsize)
        try:
            result = parapool.map(self.fit, zip(mag_arr[xrange(mag_arr.shape[0]), :],
                                                        phase_arr[xrange(phase_arr.shape[0]), :],
                                                        fit_arr[xrange(phase_arr.shape[0]), :]))
            sim, filt= zip(*result)
            parapool.close()
        except Exception as e:
            print e
            parapool.close()
            return runtime

        # save figure
        base = basefit.replace('magprocessed', 'PGfiltapplied')
        new_img = nb.Nifti1Image(np.asarray(sim).reshape(magimg.shape), img.affine, img.header)
        nb.save(new_img, base + '_appliedsim.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(filt).reshape(magimg.shape), img.affine, img.header)
        nb.save(new_img, base + '_appliedfilt.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.fit
        _, base, _ = split_filename(fname)
        base = base.replace('magprocessed', 'PGfiltapplied')
        outputs["out_sim"] = os.path.abspath(base + '_appliedsim.nii.gz')
        outputs["out_filt"] = os.path.abspath(base + '_appliedfilt.nii.gz')
        return outputs
