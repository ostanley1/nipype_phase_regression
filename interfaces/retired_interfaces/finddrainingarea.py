from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os
from scipy.signal import correlate2d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import optimize, fftpack

def calcPoolSize():
    import psutil
    cpustate = psutil.cpu_percent(percpu=True)
    return int((len(cpustate) - sum([x>50 for x in cpustate]))/ 6) # six should be changed to (number of map nodes + 5)
    # This number is owned by node not interface, need multiprocessing equipped node to use that

class finddrainingareaInputSpec(BaseInterfaceInputSpec):
    simmap = File(exists=True, desc='sim map', mandatory=True)
    magmap = File(exists=True, desc='mag map', mandatory=True)

class finddrainingareaOutputSpec(TraitedSpec):
    crosscorr_spatial = File(exists=True, desc="spatial cross correlation image")
    sigma = traits.Float(desc='sigma value for the gaussian')

class finddrainingarea(BaseInterface):
    input_spec = finddrainingareaInputSpec
    output_spec = finddrainingareaOutputSpec

    def crosscorr_spatial(self, mag_slice, sim_slice):
        return correlate2d(mag_slice, sim_slice, 'full')

    def radialslice(self, corrmap, slice, title):
        xarray = np.linspace(-(corrmap.shape[0]-1)/2, (corrmap.shape[0]-1)/2, corrmap.shape[0])
        yarray = np.linspace(-(corrmap.shape[1] - 1) / 2, (corrmap.shape[1] - 1) / 2, corrmap.shape[1])
        X,Y = np.meshgrid(xarray, yarray)
        radius = np.sqrt(X**2 + Y**2)
        index = np.unique(radius)
        radius = radius.flatten()
        correlations = corrmap.flatten()
        count = np.zeros_like(index)
        results = [[] for i in range(index.size)]
        for i in xrange(correlations.size):
            rindex = radius[i]
            loc = np.asarray(np.where(index==rindex))[0]
            count[loc] += 1
            results[loc].append(correlations[i])
        means = np.zeros_like(index)
        stddevs = np.zeros_like(index)
        for j in xrange(len(results)):
            means[j] = np.mean(results[j])
            stddevs[j] = np.mean(results[j])
        fig, ax = plt.subplots(1,1)
        # ax.set_yscale("log", nonposy='clip')
        ax.plot(index,means)
        if title == 'PowerDomain':
            ax.plot(index, means)
            plt.xlim([0, 300])
        if title=='SpatialDomain':
            ax.errorbar(index, means, yerr=stddevs)
            plt.xlim([0, 50])
            plt.ylim([-means[0]/5, means[0]*1.2])
        fnamemag = self.inputs.magmap
        _, basemag, _ = split_filename(fnamemag)
        plt.title(basemag + title + 'Radial Plot for Slice ' + str(slice))
        plt.savefig(basemag + title + 'RadialPlotSlice' + str(slice) + '.png')
        plt.clf()


    def setweights(self, corrmap):
        noisemask = np.ones_like(corrmap, dtype=bool)
        noisemask[((corrmap.shape[0]-1)/2)-5:((corrmap.shape[0]-1)/2)+5, ((corrmap.shape[1]-1)/2)-5:((corrmap.shape[1]-1)/2)+5] = False
        noisemap = np.asarray(corrmap.copy())
        noisemap[noisemask==False] = 0
        noisestd = np.std(noisemap[np.nonzero(noisemap)])
        noisemean = np.mean(noisemap[np.nonzero(noisemap)])
        SNR = corrmap/noisestd
        SNRnoise = np.mean(SNR[np.nonzero(noisemap)])
        print "SNR in noise: " + str(SNRnoise)
        print "Max SNR: " + str(np.max(SNR[:]))
        print "Difference: " + str(np.max(SNR[:])/SNRnoise)
        # weights = np.abs(1/SNR)
        # weights[np.isnan(weights)] = 10000
        # weights[np.isinf(weights)] = 10000
        weights = np.ones_like(corrmap) * 10000
        weights[SNR > 5*np.abs(SNRnoise)] = 1
        return weights, SNR

    def powerSpectrum(self, corrmap):
        frequnsorted = fftpack.fft2(corrmap)
        freq = fftpack.fftshift(frequnsorted)
        power = np.abs(freq)**2
        # fig = plt.figure()
        # ax = fig.add_subplot(111)
        # x = fftpack.fftshift(fftpack.fftfreq(corrmap.shape[0], 0.6))
        # y = fftpack.fftshift(fftpack.fftfreq(corrmap.shape[1], 0.6))
        # X,Y=np.meshgrid(x,y)
        # plt.imshow(np.log10(corrmap))
        # plt.show()
        return power

    def gaussian(self, xdata, height, center_x, center_y, sigma, intercept):
        """Returns a gaussian function with the given parameters"""
        """Requires square input matrix"""
        sigma = float(sigma)
        if np.sqrt(xdata.size) - np.floor(np.sqrt(xdata.size)) > 0.0000001:
            raise(ValueError)
        else:
            grid = np.indices((np.sqrt(xdata.size), np.sqrt(xdata.size)))
            result = np.reshape(height * np.exp( - (((center_x - grid[0]) / sigma) ** 2 + ((center_y - grid[1]) / sigma) ** 2) / 2) + intercept, xdata.shape)
            return result

    def fitgaussian(self, data, bounds, weights):
        """Returns (height, x, y, width_x, width_y)
         the gaussian parameters of a 2D distribution found by a fit"""
        params = (np.max(data), (np.sqrt(data.size)+1)/2, (np.sqrt(data.size)+1)/2, 1, 0)
        try:
            popt, pcov = optimize.curve_fit(self.gaussian, np.linspace(0,len(data)-1, len(data)), data, params, sigma=weights, bounds=bounds)
            return popt, pcov, 1
        except Exception as e:
            print e
            return np.zeros_like(params), np.zeros(len(params)), 0

    def _run_interface(self, runtime):
        # read in data
        fnamemag = self.inputs.magmap
        _, basemag, _ = split_filename(fnamemag)
        fnamesim = self.inputs.simmap
        _, basesim, _ = split_filename(fnamesim)

        # if basemag[:5] != basesim[:5]:
        #     raise TypeError('NotSameImageSet')

        img = nb.load(fnamemag)
        magimg = np.array(img.get_data()).astype(float)

        img = nb.load(fnamesim)
        hdr = img.header
        simimg = np.array(img.get_data()).astype(float)

        #Need to eliminate any percent changes above or below ten because they can't be from BOLD
        # TODO: Intelligent ceiling/floor detection
        invalidmagabove = magimg > 5
        invalidmagbelow = magimg < -5
        magimg[invalidmagabove] = 0
        magimg[invalidmagbelow] = 0

        invalidsimabove = simimg > 5
        invalidsimbelow = simimg < -5
        simimg[invalidsimabove] = 0
        simimg[invalidsimbelow] = 0

        corrmap = []
        resmap = []
        sigmas = []
        weights = []
        SNR = []
        power = []

        for slice in xrange(magimg.shape[2]):
            corrmap.append(self.crosscorr_spatial(magimg[:,:,slice], simimg[:,:,slice]))
            self.radialslice(corrmap[slice], slice, 'SpatialDomain')
            weightslice, SNRslice = self.setweights(corrmap[slice])
            weights.append(weightslice)
            SNR.append(SNRslice)
            power.append(self.powerSpectrum(corrmap[slice]))
            self.radialslice(power[slice], slice, "PowerDomain")
            bounds = ([0.8*np.max(corrmap[slice]), -np.Inf, -np.Inf, 0, -np.Inf], [1.2*np.max(corrmap[slice]),np.Inf,np.Inf,np.Inf,np.Inf])
            data = np.reshape(corrmap[slice], [corrmap[slice].size])#[center-20:center+20,center-20:center+20]
            weightsflat = np.reshape(weights[slice], [weights[slice].size])
            popt, pcov, result = self.fitgaussian(data, bounds, weightsflat)
            mean = np.mean(data)
            TSS=np.sum((corrmap[slice][((corrmap[slice].shape[0]-1)/2)-5:((corrmap[slice].shape[0]-1)/2)+5, ((corrmap[slice].shape[1]-1)/2)-5:((corrmap[slice].shape[0]-1)/2)+5]-mean)**2)
            if result < 1:
                resmap.append(np.zeros_like(corrmap[slice]))
            else:
                resmap.append(np.reshape(data[:]-self.gaussian(np.linspace(0,len(data)-1,len(data)),popt[0],popt[1],popt[2],popt[3],popt[4]),corrmap[slice].shape))
            RSS=np.sum((resmap[slice][((corrmap[slice].shape[0]-1)/2)-5:((corrmap[slice].shape[0]-1)/2)+5, ((corrmap[slice].shape[1]-1)/2)-5:((corrmap[slice].shape[0]-1)/2)+5])**2)
            R2 = 1 - (RSS/TSS)
            print "R2: " + str(R2)
            # print popt
            # print pcov
            sigmas.append(popt[3])

        self.width = np.mean(sigmas)*hdr['pixdim'][1]
        print "WIDTH: " + str(self.width)
        print "Width Error: " + str(np.std(sigmas))
        # print "R-squareds: " + str(rsquareds)

        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # xlist = np.linspace(-99,99,199)
        # ylist = np.linspace(-99,99,199)
        # [X,Y] = np.meshgrid(xlist, ylist)
        # ax.plot_surface(X, Y, corrmap[10], cmap='winter', vmax=np.max(corrmap[10])*1.05, rstride=1, cstride=1)
        # plt.show()

        # save figure
        new_img = nb.Nifti1Image(np.asarray(corrmap), img.affine, img.header)
        nb.save(new_img, basesim + '_crosscorrpsf.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(resmap), img.affine, img.header)
        nb.save(new_img, basesim + '_crosscorrres.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(weights), img.affine, img.header)
        nb.save(new_img, basesim + '_crosscorrweights.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(SNR), img.affine, img.header)
        nb.save(new_img, basesim + '_crosscorrSNR.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(power), img.affine, img.header)
        nb.save(new_img, basesim + '_crosscorrpower.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.simmap
        _, base, _ = split_filename(fname)
        outputs["crosscorr_spatial"] = os.path.abspath(base + '_crosscorrpsf.nii.gz')
        outputs["sigma"] = np.mean(self.width)
        return outputs