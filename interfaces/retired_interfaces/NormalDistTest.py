from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os
import scipy.stats.mstats as stat

def calcPoolSize():
    import psutil
    cpustate = psutil.cpu_percent(percpu=True)
    return int((len(cpustate) - sum([x>50 for x in cpustate]))/ 6) # six should be changed to (number of map nodes + 5)
    # This number is owned by node not interface, need multiprocessing equipped node to use that

class NormalDistTestInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='image to test whether voxel timecourses are normal', mandatory=True)

class NormalDistTestOutputSpec(TraitedSpec):
    result_file = File(exists=True, desc='two sided p value with null hypothesis normal')
    kscore = File(exists=True, desc='score of normality test')
    skew = File(exists=True, desc='voxel by voxel skew')
    kurtosis = File(exists=True, desc='voxel by voxel kurtosis')

class NormalDistTest(BaseInterface):
    input_spec = NormalDistTestInputSpec
    output_spec = NormalDistTestOutputSpec

    def testnorm(self, (in_tc)):
        if np.count_nonzero(in_tc - np.mean(in_tc)) != 0:
            kscore, pvalue = stat.normaltest(np.asarray(in_tc).transpose())
            [skew] = stat.skew(np.asarray(in_tc).transpose())
            [kurt] = stat.kurtosis(np.asarray(in_tc).transpose())
            return kscore, pvalue, skew, kurt
        else:
            return 1000, -1, 1000, 10000


    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)

        img = nb.load(fname)
        in_img = np.array(img.get_data()).astype(float)

        in_arr = in_img.reshape(-1, in_img.shape[-1])

        poolsize = calcPoolSize()
        print "POOL SIZE: " + str(poolsize)
        parapool = Pool(poolsize)
        try:
            result = parapool.map(self.testnorm, zip(in_arr[xrange(in_arr.shape[0]), :]))
            kscore, pval, skew, kurtosis = zip(*result)
            parapool.close()
        except Exception as e:
            print e
            parapool.close()
            return runtime

        # save figure
        new_img = nb.Nifti1Image(np.asarray(pval).reshape(img.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_normalpval.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(kscore).reshape(img.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_kscore.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(skew).reshape(img.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_skew.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(kurtosis).reshape(img.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_kurtosis.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["result_file"] = os.path.abspath(base + '_normalpval.nii.gz')
        outputs["kscore"] = os.path.abspath(base + '_kscore.nii.gz')
        outputs["skew"] = os.path.abspath(base + '_skew.nii.gz')
        outputs["kurtosis"] = os.path.abspath(base + '_kurtosis.nii.gz')
        return outputs