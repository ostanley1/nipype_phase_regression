from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os
import matplotlib.pyplot as plt

class sampleandPlotInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='images containing timecourses to plot', mandatory=True)
    mask_file = File(exists=True, desc='metric file to pick values from and to plot in title')
    plot_type = traits.String(desc='Type of plot: histo or timecourse', default='histo')
    number_of_voxels = traits.Int(desc='numbers of voxels to plot must be divisble by 5 or sqrt', default=100)
    invalid_number = traits.Float(desc='number in mask to ignore, value for outside brain', default=-1)

class sampleandPlotOutputSpec(TraitedSpec):
    result_file = File(exists=True, desc='plot of voxels')

class sampleandPlot(BaseInterface):
    input_spec = sampleandPlotInputSpec
    output_spec = sampleandPlotOutputSpec

    def getvoxels(self, mask, invalid_number, number_of_voxels):
        X,Y,Z = np.meshgrid(np.linspace(0,mask.shape[0]-1, mask.shape[0]), np.linspace(0,mask.shape[1]-1, mask.shape[1]),
                            np.linspace(0,mask.shape[2]-1, mask.shape[2]))
        X = X.flatten()
        Y = Y.flatten()
        Z = Z.flatten()
        maskflat = mask.flatten()
        Xtight = X[maskflat != invalid_number]
        Ytight = Y[maskflat != invalid_number]
        Ztight = Z[maskflat != invalid_number]
        masktight = maskflat[maskflat != invalid_number]
        if len(mask) < number_of_voxels:
            raise(ValueError)
        indcies = np.random.choice(range(masktight.shape[0]), size=number_of_voxels)
        voxels = np.zeros([number_of_voxels,3])
        for i in xrange(len(indcies)):
            voxels[i, 0] = Xtight[indcies[i]]
            voxels[i, 1] = Ytight[indcies[i]]
            voxels[i, 2] = Ztight[indcies[i]]
        print voxels[:,0]
        print Xtight[indcies]
        # for i in xrange(voxels.shape[0]):
        #     if mask[voxels[i,0], voxels[i,1], voxels[i,2]] == -1:
        #         print Xtight[i]
        #         print voxels[i,0]
        return voxels

    def _run_interface(self, runtime):
        # read in data
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        img = nb.load(fname).get_data()

        fname = self.inputs.mask_file
        mask = nb.load(fname).get_data()

        voxels = self.getvoxels(mask, self.inputs.invalid_number, self.inputs.number_of_voxels)

        xplots = 0
        yplots = 0

        if np.sqrt(self.inputs.number_of_voxels)-np.floor(np.sqrt(self.inputs.number_of_voxels)) < 1e-5:
            xplots = int(np.sqrt(self.inputs.number_of_voxels))
            yplots = xplots
        else:
            xplots = self.inputs.number_of_voxels/5
            yplots = 5

        # defining plot list as xindex*yplot + yindex
        plotlist = np.linspace(0, xplots*yplots-1, xplots*yplots)

        fig, axarr = plt.subplots(xplots,yplots, figsize=(50,50))
        fig.tight_layout()

        for i in xrange(len(voxels)):
            if self.inputs.plot_type == "histo":
                axarr[plotlist[i]/yplots, plotlist[i]%yplots].hist(img[voxels[i][0], voxels[i][1], voxels[i][2], :], bins=20)
                axarr[plotlist[i] / yplots, plotlist[i] % yplots].set_title(str([voxels[i][0], voxels[i][1], voxels[i][2]]) + str(mask[voxels[i][0], voxels[i][1], voxels[i][2]]))
            else:
                axarr[plotlist[i] / yplots, plotlist[i] % yplots].plot(img[voxels[i][0], voxels[i][1], voxels[i][2],:])
                axarr[plotlist[i] / yplots, plotlist[i] % yplots].set_title( str([voxels[i][ 0], voxels[i][ 1], voxels[i][ 2]]) + str(mask[voxels[i, 0], voxels[i][ 1], voxels[i][ 2]]))

        plt.savefig(base + '_hists.png')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["result_file"] = os.path.abspath(base + '_hists.png')
        return outputs