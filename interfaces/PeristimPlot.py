# coding=utf-8
from nipype.interfaces.matlab import MatlabCommand
from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, File, traits
import os
from string import Template
from nipype.utils.filemanip import split_filename
import numpy as np
import nibabel as nb

class PeristimPlotInputSpec(BaseInterfaceInputSpec):
    in_file =  File(exists=True, mandatory=True)
    TR = traits.Float(desc='Repetition Time (s)')
    activation_time = traits.Float(desc='stim length (s)')
    rest_time = traits.Float(desc='rest length (s)')
    blocks = traits.Int(desc="number of blocks")

class PeristimPlotOutputSpec(TraitedSpec):
    out_file = File(exists=True)

class PeristimPlot(BaseInterface):
    input_spec = PeristimPlotInputSpec
    output_spec = PeristimPlotOutputSpec

    def _run_interface(self, runtime):
        img = nb.load(self.inputs.in_file)
        magimg = np.array(img.get_data()).astype(float)

        stimlength = self.inputs.activation_time/self.inputs.TR
        restlength = self.inputs.rest_time/self.inputs.TR

        trs = np.linspace(0,stimlength+restlength-1, stimlength+restlength)
        trstotal = np.concatenate([trs]*self.inputs.blocks)
        trstotal = np.concatenate((trs[int(stimlength):], trstotal))

        peristim = np.zeros((magimg.shape[0], magimg.shape[1], magimg.shape[2], int(stimlength+restlength)))
        # print(magimg.shape, peristim.shape, trstotal)
        for s in range(int(stimlength+restlength)):
            # print(s,trstotal.shape)
            peristim[:,:,:,s]=np.mean(magimg[:,:,:,trstotal==s], axis=3)

        # save figure
        _, base, _ = split_filename(self.inputs.in_file)
        new_img = nb.Nifti1Image(peristim, img.affine, img.header)
        nb.save(new_img, base + '_peristim.nii.gz')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_peristim.nii.gz')
        return outputs