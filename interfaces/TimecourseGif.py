from nipype.interfaces.base import TraitedSpec, BaseInterfaceInputSpec, BaseInterface, File, traits
import numpy as np
import nibabel as nb
from nipype.utils.filemanip import split_filename
import scipy.misc
import subprocess
import os

class TimecourseGifInputSpec(BaseInterfaceInputSpec):
    in_file = File(desc="Input file", mandatory=True)
    downsample = traits.Int(desc="How much to downsample images in the gif", usedefault=20)
    #TODO:slice dimension support
    # slice_dim = traits.Float(desc="smallest dim in 3D", usedefault=2)

class TimecourseGifOutputSpec(TraitedSpec):
    out_file = File(desc="Outputgif", exists=True)


class TimecourseGif(BaseInterface):
    input_spec = TimecourseGifInputSpec
    output_spec = TimecourseGifOutputSpec

    def getMosaicSize(self, n):
        x = int(np.sqrt(n))
        y = np.ceil(float(n)/x)
        return x,int(y)

    def _run_interface(self, runtime):
        img = nb.load(self.inputs.in_file)
        data = img.get_data()
        x,y = self.getMosaicSize(data.shape[2])
        for t in xrange(data.shape[3]):
            mosaic = np.zeros([x * data.shape[0], y * data.shape[1]])
            i=0
            j=0
            for s in xrange(data.shape[2]):
                mosaic[i*data.shape[0]:(i+1)*data.shape[0], j*data.shape[1]:(j+1)*data.shape[1]] = data[:,:,s,t]
                if j==y-1:
                    i += 1
                    j = 0
                else:
                    j += 1
            scipy.misc.imsave('mosaic' + str(t).zfill(3) + '.tiff', mosaic)

        _,base,_=split_filename(self.inputs.in_file)
        self.outname=base+'_tc.gif'
        subprocess.call('pwd', shell=False )
        command = ['convert','-delay', '10', '-loop', '2', 'mosaic*.tiff', '-resize', str(self.inputs.downsample) + '%', self.outname]
        print command
        subprocess.call(command, shell=False)
        # subprocess.call(['rm', 'mosaic*.tiff'], shell=False)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['out_file'] = os.path.abspath(self.outname)
        return outputs

