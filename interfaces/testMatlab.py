# coding=utf-8
from nipype.interfaces.matlab import MatlabCommand
from nipype.interfaces.base import TraitedSpec, BaseInterface, BaseInterfaceInputSpec, File, traits
import os
from string import Template

class MatlabSumInputSpec(BaseInterfaceInputSpec):
    var1 = traits.Int(mandatory=True)
    var2 = traits.Int(mandatory=True)


class MatlabSumOutputSpec(TraitedSpec):
    sum = traits.Int(mandatory=True)

class MatlabSum(BaseInterface):
    input_spec = MatlabSumInputSpec
    output_spec = MatlabSumOutputSpec

    def _run_interface(self, runtime):
        d = dict(var1=self.inputs.var1,
                 var2=self.inputs.var2)
        #this is your MATLAB code template
        print(d)
        script = Template("""v1 = $var1;
        v2 = $var2
        v1 + v2
        exit;
        """).substitute(d)
        print(script)
        # mfile = True  will create an .m file with your script and executed.
        # Alternatively
        # mfile can be set to False which will cause the matlab code to be
        # passed
        # as a commandline argument to the matlab executable
        # (without creating any files).
        # This, however, is less reliable and harder to debug
        # (code will be reduced to
        # a single line and stripped of any comments).

        mlab = MatlabCommand(script=script, mfile=True)
        result = mlab.run()
        print(result.__dict__)
        return result.runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['sum'] = 2
        return outputs