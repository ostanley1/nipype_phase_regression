from nipype.interfaces.base import TraitedSpec, BaseInterfaceInputSpec, BaseInterface, File, traits
from nipype.utils.filemanip import split_filename
import os
import subprocess
import shlex


class McflirtPhaseInputSpec(BaseInterfaceInputSpec):
    in_file_list = traits.List(desc="Input files to transform", mandatory=True)
    reference_list = traits.List(desc="Input ref volumes", mandatory=True)
    mat_list = traits.List(desc="Input mat files", mandatory=True)


class McflirtPhaseOutputSpec(TraitedSpec):
    out_file_list = traits.List(desc="mcflirted phase_list", exists=True)


class McflirtPhase(BaseInterface):
    input_spec = McflirtPhaseInputSpec
    output_spec = McflirtPhaseOutputSpec

    def _run_interface(self, runtime):

        if len(self.inputs.in_file_list) != len(self.inputs.mat_list):
            print("MISMATCH TRANSFORMS AND IMAGES")

        command = "flirt -in %s -ref %s -applyxfm -init %s -paddingsize 10 -out %s"

        self.file_list_output = []

        for i in range(len(self.inputs.mat_list)):
            _, base, _ = split_filename(self.inputs.mat_list[i][:-20])
            self.file_list_output.append(os.path.abspath(base + str('%04d' % i) + "_mcfphase.nii.gz"))
            command_final = command % (self.inputs.in_file_list[i], self.inputs.reference_list[i],
                                       self.inputs.mat_list[i], self.file_list_output[i][:-7])
            # if i == 0:
            #     print "Starting Command: " + command_final
            subprocess.call(shlex.split(command_final))
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['out_file_list'] = self.file_list_output
        return outputs
