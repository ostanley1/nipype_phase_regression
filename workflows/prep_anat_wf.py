import nipype.pipeline.engine as pe
import nipype.interfaces.freesurfer as freesurf
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as preproc

def create_prep_anat_wf():
    workflow = pe.Workflow(name="prepanat")
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    # #collect anat series
    # patterns=["mprage*"]
    # anat_series = []
    # for series in os.listdir(dcm_dir_sorted):
    #     for pattern in patterns:
    #         if fnmatch.fnmatch(series, pattern):
    #             dicomlist = os.listdir(dcm_dir_sorted + series)
    #             anat_series.append(dcm_dir_sorted + series + '/' + dicomlist[0])
    #             continue
    #
    # if not os.path.exists(nifti_dir):
    #     os.makedirs(nifti_dir)
    # print anat_series

    # convert dicom to nifti
    converter = pe.Node(interface=freesurf.MRIConvert(), name="converter")

    # add combination method and Mag/Phase to intent field
    dataCleaner = pe.Node(interface=preproc.addStudyDescription(), name="dataCleaner")

    # make sure image is in standard orientation
    flipper = pe.Node(interface=fsl.Reorient2Std(), name="flipper")

    # extract brain with fsl and save the mask
    extractor = pe.Node(interface=fsl.BET(), name="extractor")
    extractor.inputs.frac = 0.27

    # make connections for workflow
    workflow.connect(converter, 'out_file', dataCleaner, 'nii')
    workflow.connect(dataCleaner, 'fixednii', flipper, 'in_file')
    workflow.connect(flipper, 'out_file', extractor, 'in_file')

    return workflow

if __name__=="__main__":
    workflow=create_prep_anat_wf()


    # apparently pycharm's interactive console takes over stdin but does not implement the close function
    # this causes the multiprocessing module to fail
    # we must create a dummy function for sys.stdin.close
    import sys
    if not hasattr(sys.stdin,'close'):
        def dummy_close():
            pass
        sys.stdin.close = dummy_close
    exec_graph = workflow.run(plugin='Linear')

    # view output
    # rawimg = getoutimglocs(exec_graph, workflow.get_node("ds"), 'anat')
    # betimg=getoutimglocs(exec_graph,workflow.get_node("ds"),'anat.@BET')
    # print rawimg
    # for i in xrange(len(rawimg)):
    #     fslview([rawimg[i],betimg[i]])
    # print "DONE preprocess anatomical images"
