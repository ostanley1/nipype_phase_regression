# emacs: -*- mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vi: set ft=python sts=4 ts=4 sw=4 et:

import nipype.interfaces.fsl as fsl          # fsl
from nipype.algorithms.misc import TSNR
import nipype.interfaces.utility as util     # utility
import nipype.pipeline.engine as pe          # pypeline engine

def zstatlist(zstatresult):
    return [item for sublist in zstatresult for item in sublist]

def extract_noise_components(in_file, noise_mask_file, num_components):
    """Derive components most reflective of physiological noise
    """
    import os
    from nibabel import load
    import numpy as np
    import scipy as sp
    from scipy.signal import detrend
    imgseries = load(in_file)
    noise_mask = load(noise_mask_file)
    voxel_timecourses = imgseries.get_data()[np.nonzero(noise_mask.get_data())]
    for timecourse in voxel_timecourses:
        timecourse[:] = detrend(timecourse, type='constant')
    u,s,v = sp.linalg.svd(voxel_timecourses, full_matrices=False)
    components_file = os.path.join(os.getcwd(), 'noise_components.txt')
    np.savetxt(components_file, v[:num_components, :].T)
    return components_file

# def select_volume(filename, which):
#     """Return the middle index of a file
#     """
#     from nibabel import load
#     import numpy as np
#     if which.lower() == 'first':
#         idx = 0
#     elif which.lower() == 'middle':
#         idx = int(np.ceil(load(filename).get_shape()[3]/2))
#     else:
#         raise Exception('unknown value for volume selection : %s'%which)
#     return idx


def create_compcor_wf(name):
    """Create a "resting" time series preprocessing workflow

    The noise removal is based on Behzadi et al. (2007)

    Parameters
    ----------

    name : name of workflow (default: compcor_wf)

    Inputs::

        inputspec.func : functional run (filename or list of filenames)

    Outputs::

        outputspec.noise_mask_file : voxels used for PCA to derive noise components
        outputspec.filtered_file : bandpass filtered and noise-reduced time series

    Example
    -------

    >>> TR = 3.0
    >>> wf = create_resting_preproc()
    >>> wf.inputs.inputspec.func = 'f3.nii'
    >>> wf.inputs.inputspec.num_noise_components = 6
    >>> wf.inputs.inputspec.highpass_sigma = 100/(2*TR)
    >>> wf.inputs.inputspec.lowpass_sigma = 12.5/(2*TR)
    >>> wf.inputs.inputspec.zstat = 'f3z.nii'
    >>> wf.run() # doctest: +SKIP

    """

    compcor_wf = pe.Workflow(name=name)

    # Define nodes
    inputnode = pe.Node(interface=util.IdentityInterface(fields=['func',
                                                                 'num_noise_components',
                                                                 'highpass_sigma',
                                                                 'lowpass_sigma',
                                                                 'zstat'
                                                                 ]),
                        name='inputspec')
    outputnode = pe.Node(interface=util.IdentityInterface(fields=[
                                                              'noise_mask_file',
                                                              'filtered_file',
                                                              ]),
                     name='outputspec')

    zstatList = pe.Node(interface=util.Function(input_names=["zstatresult"], output_names=["zstat_list"],function=zstatlist), name="zstatlist")

    # slicetimer = pe.Node(fsl.SliceTimer(), name='slicetimer')
    # realigner = create_realign_flow()
    tsnr = pe.MapNode(TSNR(regress_poly=2), name='tsnr', iterfield=['in_file'])
    getthresh = pe.MapNode(interface=fsl.ImageStats(op_string='-p 98'),
                           name='getthreshold', iterfield=['in_file'])
    getzthresh = pe.MapNode(interface=fsl.ImageStats(op_string='-p 80'),
                        name='getzthreshold', iterfield=['in_file'])
    threshold_stddev = pe.MapNode(fsl.Threshold(), name='threshold', iterfield=['in_file', 'thresh'])
    threshold_z = pe.MapNode(fsl.Threshold(), name='thresholdz', args='-bin', direction='above', iterfield=['in_file', 'thresh'])
    mask_stddev = pe.MapNode(fsl.ApplyMask(), name='maskstddev', iterfield=['in_file', 'mask_file'])
    compcor = pe.MapNode(util.Function(input_names=['in_file',
                                                 'noise_mask_file',
                                                 'num_components'],
                                     output_names=['noise_components'],
                                     function=extract_noise_components),
                       name='compcor', iterfield=['in_file', 'noise_mask_file'])

    rename = pe.MapNode(interface=util.Rename(), name='rename', iterfield=['in_file', 'format_string'])
    rename.inputs.format_string=['Run1', 'Run2', 'Run3']
    rename.inputs.keep_ext=True

    remove_noise = pe.MapNode(fsl.FilterRegressor(filter_all=True),
                           name='remove_noise', iterfield=['in_file','design_file'])
    bandpass_filter = pe.MapNode(fsl.TemporalFilter(),
                             name='bandpass_filter', iterfield=['in_file'])
    addmean = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name='addmean', iterfield=['in_file', 'operand_files'])
    addmean.inputs.op_string='-add %s'

    # Define connections
    compcor_wf.connect(inputnode, 'func', tsnr, 'in_file')
    # compcor_wf.connect(slicetimer, 'slice_time_corrected_file',
    #                     realigner, 'inputspec.func')
    # compcor_wf.connect(realigner, 'outputspec.realigned_file', tsnr, 'in_file')
    compcor_wf.connect(tsnr, 'stddev_file', threshold_stddev, 'in_file')
    compcor_wf.connect(tsnr, 'stddev_file', getthresh, 'in_file')
    compcor_wf.connect(getthresh, 'out_stat', threshold_stddev, 'thresh')


    compcor_wf.connect(inputnode, 'zstat', zstatList, 'zstatresult')
    compcor_wf.connect(zstatList, 'zstat_list', threshold_z, 'in_file')
    compcor_wf.connect(zstatList, 'zstat_list', getzthresh, 'in_file')
    compcor_wf.connect(getzthresh, 'out_stat', threshold_z, 'thresh')

    compcor_wf.connect(threshold_stddev, 'out_file', mask_stddev, 'in_file')
    compcor_wf.connect(threshold_z, 'out_file', mask_stddev, 'mask_file')

    compcor_wf.connect(inputnode, 'func',
                        compcor, 'in_file')
    compcor_wf.connect(mask_stddev, 'out_file',
                        compcor, 'noise_mask_file')
    compcor_wf.connect(inputnode, 'num_noise_components',
                        compcor, 'num_components')
    compcor_wf.connect(tsnr, 'detrended_file',
                        remove_noise, 'in_file')
    compcor_wf.connect(compcor, 'noise_components',
                        remove_noise, 'design_file')
    compcor_wf.connect(inputnode, 'highpass_sigma',
                        bandpass_filter, 'highpass_sigma')
    compcor_wf.connect(inputnode, 'lowpass_sigma',
                        bandpass_filter, 'lowpass_sigma')
    compcor_wf.connect(remove_noise, 'out_file', bandpass_filter, 'in_file')
    compcor_wf.connect(threshold_stddev, 'out_file',
                        outputnode, 'noise_mask_file')
    compcor_wf.connect(bandpass_filter, 'out_file', addmean, 'in_file')
    compcor_wf.connect(tsnr, 'mean_file', addmean, 'operand_files')
    compcor_wf.connect(addmean, 'out_file', rename, 'in_file')
    compcor_wf.connect(rename, 'out_file', outputnode, 'filtered_file')
    return compcor_wf
