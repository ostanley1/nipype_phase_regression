import nipype.pipeline.engine as pe
import nipype.interfaces.utility as ul
import nipype.interfaces.freesurfer as fsurf
import nipype.interfaces.afni as afni
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype_phase_regression.interfaces as pr
from nipype.interfaces.ants import RegistrationSynQuick
import copy, pprint
from nipype.interfaces.utility import Merge

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def picksecond(files):
    if isinstance(files, list):
        return files[1]
    else:
        return files

def create_reg_wf():
    reg = pe.Workflow(name="reg")
    reg.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['mean_func_ge', 'mean_func_ge_uw', 'mean_func_se', 'norm_struct', 'norm_struct_brain',
                                                     'funcs_ge', 'funcs_se', 'funcs_filt', 'funcs_pcamacro', 'funcs_pcamicro',
                                                     'cnr_ge', 'cnr_se', 'cnr_filt', 'cnr_pcamacro', 'cnr_pcamicro',
                                                     'snr_ge', 'snr_se', 'snr_filt', 'snr_pcamacro', 'snr_pcamicro',
                                                     'multiechomag', 'multiechophase', 'white',
                                                     'V1mask', 'V1box', 'ldepth', 'rdepth']),
                        name='inputspec')

    # need to trim epi by 1/3 in order for masking to work.
    # Masked voxels need to be larger than 1/16 of the image volume.
    frontcut = pe.Node(interface=fsl.ExtractROI(), name='frontcut')
    frontcut.inputs.x_min=0
    frontcut.inputs.x_size=-1
    frontcut.inputs.z_min=0
    frontcut.inputs.z_size=-1
    frontcut.inputs.y_min=0
    frontcut.inputs.y_size=160

    n3 = pe.Node(interface=fsurf.MNIBiasCorrection(), name='n3')
    n3.inputs.iterations = 2

    n3se = n3.clone('n3se')

    n3gre = n3.clone('n3gre')

    n3geuw = n3.clone('n3geuw')

    betgre = pe.Node(interface=fsl.BET(), name="betgre")
    betgre.inputs.padding = True

    corrbet = pe.Node(interface=fsl.CopyGeom(), name="corrbet")

    al_ea = pe.Node(interface=ants.Registration(), name='al_ea')
    al_ea.inputs.dimension = 3
    al_ea.inputs.transforms = ['Rigid', 'Affine']
    al_ea.inputs.transform_parameters = [(2.0,), (2.0,)]
    al_ea.inputs.number_of_iterations = [[1500, 200], [1500, 200]]
    al_ea.inputs.write_composite_transform = True
    al_ea.inputs.collapse_output_transforms = False
    al_ea.inputs.initialize_transforms_per_stage = False
    al_ea.inputs.metric = ['Mattes'] * 2
    al_ea.inputs.metric_weight = [1] * 2  # Default (value ignored currently by ANTs)
    al_ea.inputs.radius_or_number_of_bins = [32] * 2
    al_ea.inputs.sampling_strategy = ['Random', 'Random']
    al_ea.inputs.sampling_percentage = [0.1, 0.05]
    al_ea.inputs.convergence_threshold = [1.e-7, 1.e-8]
    al_ea.inputs.convergence_window_size = [20] * 3
    al_ea.inputs.smoothing_sigmas = [[2, 1], [1, 0]]
    al_ea.inputs.sigma_units = ['vox'] * 2
    al_ea.inputs.shrink_factors = [[2, 1], [2, 1]]
    al_ea.inputs.use_estimate_learning_rate_once = [True, True]
    al_ea.inputs.use_histogram_matching = [True, True]  # This is the default
    al_ea.inputs.restrict_deformation = [[1, 1, 1], [1, 1, 1]]
    al_ea.inputs.output_warped_image = True
    al_ea.inputs.output_inverse_warped_image = True
    al_ea.inputs.interpolation = 'BSpline'
    al_ea.inputs.interpolation_parameters = (3,)

    al_ease = al_ea.clone('al_ease')
    al_eagre = al_ea.clone('al_eagre')
    al_eageuw = al_ea.clone('al_eageuw')

    # al_eanl = pe.Node(interface=ants.Registration(), name='al_eanl')
    # al_eanl.inputs.dimension=3
    # al_eanl.inputs.transforms = ['SyN']
    # al_eanl.inputs.transform_parameters = [(0.25, 3.0, 0.0)]
    # al_eanl.inputs.number_of_iterations = [[100, 50, 30]]
    # al_eanl.inputs.write_composite_transform = True
    # al_eanl.inputs.collapse_output_transforms = False
    # al_eanl.inputs.initialize_transforms_per_stage = False
    # al_eanl.inputs.metric = ['Mattes']
    # al_eanl.inputs.metric_weight = [1]  # Default (value ignored currently by ANTs)
    # al_eanl.inputs.radius_or_number_of_bins = [32]
    # al_eanl.inputs.sampling_strategy = [ None]
    # al_eanl.inputs.sampling_percentage = [None]
    # al_eanl.inputs.convergence_threshold = [1.e-9]
    # al_eanl.inputs.convergence_window_size = [20]
    # al_eanl.inputs.smoothing_sigmas = [[2, 1, 0]]
    # al_eanl.inputs.sigma_units = ['vox']
    # al_eanl.inputs.shrink_factors = [[3, 2, 1]]
    # al_eanl.inputs.use_estimate_learning_rate_once = [True]
    # al_eanl.inputs.use_histogram_matching = [True]  # This is the default
    # al_eanl.inputs.restrict_deformation = [[0,1,0]]
    # al_eanl.inputs.output_warped_image = True
    # al_eanl.inputs.output_inverse_warped_image = True
    # al_eanl.inputs.interpolation = 'BSpline'
    # al_eanl.inputs.interpolation_parameters = (3,)

    reg_V1_mask = pe.Node(interface=ants.ApplyTransforms(), name='reg_V1_mask')

    reg_V1_box = reg_V1_mask.clone('reg_V1_box')

    plot_reg = pe.Node(interface=pr.RegistrationImage(), name='plot_reg')
    plot_reg.inputs.cuts=15

    reg_ldepth_ge = pe.MapNode(interface=ants.ApplyTransforms(), name='reg_ldepth_ge', iterfield=['input_image'])
    reg_ldepth_ge.inputs.interpolation = 'BSpline'
    reg_ldepth_ge.inputs.interpolation_parameters = (5,)

    reg_rdepth_ge = reg_ldepth_ge.clone('reg_rdepth_ge')

    reg_ldepth_se = reg_ldepth_ge.clone('reg_ldepth_se')

    reg_rdepth_se = reg_ldepth_se.clone('reg_rdepth_se')

    reg_white_ge = reg_ldepth_ge.clone('reg_white_ge')

    reg_white_se = reg_ldepth_ge.clone('reg_white_se')

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['xfmge', 'invxfmge',
                                                      'xfmse', 'invxfmse',
                                                      'xfmgre', 'xfmgre']),
                        name='outputspec')

    reg.connect([(inputspec, n3, [('mean_func_ge', 'in_file')]),
                 (inputspec, n3se, [('mean_func_se', 'in_file')]),
                 # (inputspec, n3geuw, [('mean_func_ge_uw', 'in_file')]),
                 (inputspec, al_ea, [('norm_struct_brain', 'fixed_image')]),
                 (inputspec, al_ease, [('norm_struct_brain', 'fixed_image')]),
                 (inputspec, al_eagre, [('norm_struct_brain', 'fixed_image')]),
                 # (inputspec, al_eageuw, [('norm_struct_brain', 'fixed_image')]),
                 (inputspec, n3gre, [(('multiechomag',picksecond),'in_file')]),
                 (inputspec, reg_V1_mask, [('V1mask', 'input_image')]),
                 (inputspec, reg_V1_box, [('V1box', 'input_image')]),
                 (n3gre, betgre, [('out_file', 'in_file')]),
                 (n3gre, corrbet, [('out_file', 'in_file')]),
                 (betgre, corrbet, [('out_file', 'dest_file')]),
                 (corrbet, al_eagre, [('out_file', 'moving_image')]),
                 (n3, al_ea, [('out_file', 'moving_image')]),
                 # (n3geuw, al_eageuw, [('out_file', 'moving_image')]),
                 (n3, frontcut, [('out_file', 'in_file')]),
                 # (frontcut, al_eanl, [('roi_file', 'fixed_image')]),
                 (al_ea, reg_V1_mask, [('inverse_composite_transform', 'transforms'),
                                       ('inverse_warped_image', 'reference_image')]),
                 (al_ea, reg_V1_box, [('inverse_composite_transform', 'transforms'),
                                       ('inverse_warped_image', 'reference_image')]),
                 # (al_ea, al_eanl, [('inverse_warped_image', 'moving_image')]),
                 (n3se, al_ease, [('out_file', 'moving_image')]),
                 (inputspec, reg_ldepth_ge, [('ldepth', 'input_image'),
                                            ('mean_func_ge', 'reference_image')]),
                 (al_ea, reg_ldepth_ge, [('inverse_composite_transform', 'transforms')]),
                 (inputspec, reg_rdepth_ge, [('rdepth', 'input_image'),
                                            ('mean_func_ge', 'reference_image')]),
                 (al_ea, reg_rdepth_ge, [('inverse_composite_transform', 'transforms')]),
                 (inputspec, reg_ldepth_se, [('ldepth', 'input_image'),
                                            ('mean_func_se', 'reference_image')]),
                 (al_ease, reg_ldepth_se, [('inverse_composite_transform', 'transforms')]),
                 (inputspec, reg_rdepth_se, [('rdepth', 'input_image'),
                                            ('mean_func_se', 'reference_image')]),
                 (al_ease, reg_rdepth_se, [('inverse_composite_transform', 'transforms')]),
                 (al_ea, reg_white_ge, [('inverse_composite_transform', 'transforms')]),
                 (inputspec, reg_white_ge, [('white', 'input_image'),
                                            ('mean_func_ge', 'reference_image')]),
                 (al_ease, reg_white_se, [('inverse_composite_transform', 'transforms')]),
                 (inputspec, reg_white_se, [('white', 'input_image'),
                                            ('mean_func_se', 'reference_image')]),
                 (al_ea, outputspec, [('inverse_composite_transform', 'invxfmge'),
                                      ('composite_transform', 'xfmge')]),
                 (al_ease, outputspec, [('inverse_composite_transform', 'invxfmse'),
                                      ('composite_transform', 'xfmse')]),
                 (al_eagre, outputspec, [('inverse_composite_transform', 'invxfmgre'),
                                        ('composite_transform', 'xfmgre')])
                 ])
    return reg

def create_apply_reg_wf():
    applyregfunc = pe.Workflow(name="applyregfunc")
    applyregfunc.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['mean','norm_struct_brain',
                                                     'funcs', 'cnr', 'snr', 'xfm',
                                                     'zstat', 'noise', 'noisephysio']),
                        name='inputspec')

    reg_func = pe.MapNode(interface=ants.ApplyTransforms(), name='reg_func', iterfield=['input_image'],
                             nested=True)
    reg_func.inputs.interpolation = 'BSpline'
    reg_func.inputs.interpolation_parameters = (5,)

    reg_mean = reg_func.clone('reg_mean')

    reg_cnr = reg_func.clone('reg_cnr')

    reg_snr = reg_func.clone('reg_snr')

    reg_noise = reg_func.clone('reg_noise')

    reg_noisephysio = reg_func.clone('reg_noisephysio')

    reg_zstat = reg_func.clone('reg_zstat')

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['reg_funcs', 'reg_mean', 'reg_snr',
                                                      'reg_cnr']),name='outputspec')

    applyregfunc.connect([(inputspec, reg_func, [('xfm', 'transforms'),
                               ('norm_struct_brain', 'reference_image'),
                               ('funcs', 'input_image')]),
        (inputspec, reg_cnr, [('xfm', 'transforms'),
                               ('norm_struct_brain', 'reference_image'),
                               ('cnr', 'input_image')]),
        (inputspec, reg_snr, [('xfm', 'transforms'),
                              ('norm_struct_brain', 'reference_image'),
                              ('snr', 'input_image')]),
        (inputspec, reg_mean, [('xfm', 'transforms'),
                              ('norm_struct_brain', 'reference_image'),
                              ('mean', 'input_image')]),
        (inputspec, reg_noise, [('xfm', 'transforms'),
                             ('norm_struct_brain', 'reference_image'),
                             ('noise', 'input_image')]),
      (inputspec, reg_noisephysio, [('xfm', 'transforms'),
                              ('norm_struct_brain', 'reference_image'),
                              ('noisephysio', 'input_image')]),
        (inputspec, reg_zstat, [('xfm', 'transforms'),
                              ('norm_struct_brain', 'reference_image'),
                              ('zstat', 'input_image')])
                          ])


    return applyregfunc

def create_apply_reg_susc_wf():
    applyregsusc = pe.Workflow(name="applyregsusc")
    applyregsusc.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['mean','norm_struct_brain',
                                                     'mags', 'phases', 'xfm']),
                        name='inputspec')

    reg_mags = pe.MapNode(interface=ants.ApplyTransforms(), name='reg_mags', iterfield=['input_image'])
    reg_mags.inputs.interpolation = 'BSpline'
    reg_mags.inputs.interpolation_parameters = (5,)

    reg_phases = reg_mags.clone('reg_phases')

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['reg_mags', 'reg_phases']),name='outputspec')

    applyregsusc.connect([(inputspec, reg_phases, [('xfm', 'transforms'),
                               ('norm_struct_brain', 'reference_image'),
                               ('phases', 'input_image')]),
        (inputspec, reg_mags, [('xfm', 'transforms'),
                               ('norm_struct_brain', 'reference_image'),
                               ('mags', 'input_image')])])


    return applyregsusc

if __name__=="__main__":
    base_dir = processing_base_dir
    workflow=create_reg_wf()

