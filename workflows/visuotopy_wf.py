import nipype.pipeline.engine as pe
import nipype.interfaces.freesurfer as fsurf
import nipype.interfaces.utility as ul
import nipype_phase_regression.interfaces as pr



def pickleft(files):
    if isinstance(files, list):
        for f in files:
            if 'lh' in f:
                return f
    #else:
    #    return files


def pickright(files):
    if isinstance(files, list):
        for f in files:
            if 'rh' in f:
                return f
    #else:
    #    return files


def gen_out_file(distlist, hemi):
    out_files = []
    for i in distlist:
        out_files.append('%s.expand%.1f' % (hemi, i))
    return (out_files)


def copy2subjdir(in_file, folder, subject_id, subjects_dir):
    """Method to copy an input to the subjects directory"""
    import os
    import shutil
    # check which folder to put the file in
    if folder != None:
        out_dir = os.path.join(subjects_dir, subject_id, folder)
    else:
        out_dir = os.path.join(subjects_dir, subject_id)
    _, filename = os.path.split(in_file)
    out_file = os.path.join(out_dir, filename)
    # make the output folder if it does not exist
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    shutil.copy(in_file, out_file)
    return out_file


def trim_path(file):
    import os
    _, filename = os.path.split(file)
    return filename[3:]


def get_depth(files):
    import re
    if isinstance(files, list):
        depths = []
        print(files)
        for f in files:
            print(re.search('_distance_([0-9.]+)', files[f])[0])
            depths.append(re.search('_distance_([0-9.]+)', files[f])[0])
        print(depths)
        return depths
    else:
        return [re.search('_distance_([0-9.]+)', files)[0]]

def create_visuotopy_wf():
    visuotopy = pe.Workflow(name="visuotopy")
    visuotopy.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['white', 'func', 'subject_id',
                                                     'lhpatch', 'rhpatch', 'stimulus', 'curv']),
                        name='inputspec')

    # distances = [0.0, 0.25, 0.5, 0.75, 1.0]
    distances = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    # distances = [0.1]
    mris_expandlh = pe.Node(interface=fsurf.MRIsExpand(thickness=True), name='mris_expandlh')
    mris_expandlh.iterables = [('distance', distances),
                               ('out_name', gen_out_file(distances, 'lh'))]
    mris_expandlh.synchronize = True
    mris_expandrh = mris_expandlh.clone('mris_expandrh')
    mris_expandrh.iterables = [('distance', distances),
                               ('out_name', gen_out_file(distances, 'rh'))]

    copysurflh = pe.Node(
        interface=ul.Function(['in_file', 'folder', 'subject_id', 'subjects_dir'], ['out_file'], copy2subjdir),
        name='copysurflh')
    copysurflh.inputs.folder = 'surf'
    copysurflh.inputs.subjects_dir = '/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    copysurfrh = copysurflh.clone('copysurfrh')

    mri_vol2surflh = pe.MapNode(interface=fsurf.SampleToSurface(), name='mri_vol2surflh', iterfield=['source_file'])
    mri_vol2surflh.inputs.hemi = 'lh'
    mri_vol2surflh.inputs.reg_header = True
    # mri_vol2surflh.inputs.hits_type = 'niigz'
    # mri_vol2surflh.inputs.hits_file = 'depthhits.nii.gz'
    mri_vol2surflh.inputs.sampling_method = 'point'
    mri_vol2surflh.inputs.sampling_range = 0
    mri_vol2surflh.inputs.sampling_units = "frac"
    mri_vol2surflh.inputs.subjects_dir = '/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    mri_vol2surfrh = pe.MapNode(interface=fsurf.SampleToSurface(), name='mri_vol2surfrh', iterfield=['source_file'])
    mri_vol2surfrh.inputs.hemi = 'rh'
    mri_vol2surfrh.inputs.reg_header = True
    mri_vol2surfrh.inputs.sampling_method = 'point'
    mri_vol2surfrh.inputs.sampling_range = 0
    mri_vol2surfrh.inputs.sampling_units = "frac"
    mri_vol2surfrh.inputs.subjects_dir = '/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    mris_convertlh = pe.MapNode(interface=fsurf.MRIsConvert(), name='mris_convertlh', iterfield=['scalarcurv_file'])
    mris_convertlh.inputs.out_datatype = 'asc'
    mris_convertlh.inputs.patch = True

    mris_convertrh = mris_convertlh.clone('mris_convertrh')
    mris_convertrcurv = mris_convertlh.clone('mris_convertrcurv')
    mris_convertlcurv = mris_convertlh.clone('mris_convertlcurv')

    lhconvert2nifti = pe.Node(interface=pr.SurfacetoNifti(), name='lhconvert2nifti')
    lhconvert2nifti.inputs.depths=[1.0]
    rhconvert2nifti = pe.Node(interface=pr.SurfacetoNifti(), name='rhconvert2nifti')
    rhconvert2nifti.inputs.depths = [1.0]

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['lsurfs', 'rsurfs', 'lcurv', 'rcurv']),
                         name='outputspec')

    visuotopy.connect([(inputspec, mris_expandlh, [(('white', pickleft), 'in_file')]),
                       (inputspec, mris_expandrh, [(('white', pickright), 'in_file')]),
                       (inputspec, copysurflh, [('subject_id', 'subject_id')]),
                       (inputspec, copysurfrh, [('subject_id', 'subject_id')]),
                       (inputspec, mri_vol2surflh, [('subject_id', 'subject_id'),
                                                    ('func', 'source_file')]),
                       (inputspec, mri_vol2surfrh, [('subject_id', 'subject_id'),
                                                    ('func', 'source_file')]),
                       (inputspec, mris_convertlh, [('lhpatch', 'in_file')]),
                       (inputspec, mris_convertrh, [('rhpatch', 'in_file')]),
                       (mris_expandlh, copysurflh, [('out_file', 'in_file')]),
                       (copysurflh, mri_vol2surflh, [(('out_file', trim_path), 'surface')]),
                       (mris_expandrh, copysurfrh, [('out_file', 'in_file')]),
                       (copysurfrh, mri_vol2surfrh, [(('out_file', trim_path), 'surface')]),
                       (mri_vol2surflh, mris_convertlh, [('out_file', 'scalarcurv_file')]),
                       (mri_vol2surfrh, mris_convertrh, [('out_file', 'scalarcurv_file')]),
                       (mris_expandlh, outputspec, [('out_file', 'lsurfs')]),
                       (mris_expandrh, outputspec, [('out_file', 'rsurfs')]),
                       (inputspec, mris_convertlcurv, [(('curv', pickleft), 'scalarcurv_file')]),
                       (inputspec, mris_convertrcurv, [(('curv', pickright), 'scalarcurv_file')]),
                       (inputspec, mris_convertlcurv, [('lhpatch', 'in_file')]),
                       (inputspec, mris_convertrcurv, [('rhpatch', 'in_file')]),
                       (mris_convertlcurv, outputspec, [('converted', 'lcurv')]),
                       (mris_convertrcurv, outputspec, [('converted', 'rcurv')]),
                       (mris_convertlcurv, lhconvert2nifti, [('converted', 'in_files')]),
                       (mris_convertrcurv, rhconvert2nifti, [('converted', 'in_files')]),
                       ])

    return visuotopy

def create_vol2surf_wf(name, nifti_save=False):
    vol2surf = pe.Workflow(name=name)
    vol2surf.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['image', 'subject_id', 'lsurf', 'rsurf',
                                                     'lhpatch', 'rhpatch']),
                        name='inputspec')

    mri_vol2surflh = pe.MapNode(interface=fsurf.SampleToSurface(), name='mri_vol2surflh', iterfield=['source_file'])
    mri_vol2surflh.inputs.hemi = 'lh'
    mri_vol2surflh.inputs.reg_header = True
    mri_vol2surflh.inputs.sampling_method = 'point'
    mri_vol2surflh.inputs.sampling_range = 0
    mri_vol2surflh.inputs.sampling_units = "frac"
    mri_vol2surflh.inputs.subjects_dir = '/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    mri_vol2surfrh = pe.MapNode(interface=fsurf.SampleToSurface(), name='mri_vol2surfrh', iterfield=['source_file'])
    mri_vol2surfrh.inputs.hemi = 'rh'
    mri_vol2surfrh.inputs.reg_header = True
    mri_vol2surfrh.inputs.sampling_method = 'point'
    mri_vol2surfrh.inputs.sampling_range = 0
    mri_vol2surfrh.inputs.sampling_units = "frac"
    mri_vol2surfrh.inputs.subjects_dir = '/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    mris_convertlh = pe.MapNode(interface=fsurf.MRIsConvert(), name='mris_convertlh', iterfield=['scalarcurv_file'])
    mris_convertlh.inputs.out_datatype = 'asc'
    mris_convertlh.inputs.patch = True

    mris_convertrh = mris_convertlh.clone('mris_convertrh')

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['lhemiproj', 'rhemiproj']),
                         name='outputspec')

    vol2surf.connect([(inputspec, mri_vol2surflh, [('subject_id', 'subject_id'),
                                                   ('image', 'source_file')]),
                      (inputspec, mri_vol2surfrh, [('subject_id', 'subject_id'),
                                                   ('image', 'source_file')]),
                      (inputspec, mris_convertlh, [('lhpatch', 'in_file')]),
                      (inputspec, mris_convertrh, [('rhpatch', 'in_file')]),
                      (inputspec, mri_vol2surflh, [(('lsurf', trim_path), 'surface')]),
                      (inputspec, mri_vol2surfrh, [(('rsurf', trim_path), 'surface')]),
                      (mri_vol2surflh, mris_convertlh, [('out_file', 'scalarcurv_file')]),
                      (mri_vol2surfrh, mris_convertrh, [('out_file', 'scalarcurv_file')]),
                      (mris_convertlh, outputspec, [('converted', 'lhemiproj')]),
                      (mris_convertrh, outputspec, [('converted', 'rhemiproj')]),
                      ])

    if nifti_save:
        lhconvert2nifti = pe.JoinNode(interface=pr.SurfacetoNifti(), joinfield=['depths','in_files'], joinsource=['mris_expandlh'], name='lhconvert2nifti')
        rhconvert2nifti = pe.JoinNode(interface=pr.SurfacetoNifti(), joinfield=['depths','in_files'], joinsource=['mris_expandrh'], name='rhconvert2nifti')
        vol2surf.connect([(mris_convertlh, lhconvert2nifti, [('converted', 'in_files'),
                                                             (('converted', get_depth), 'depths')]),
                          (mris_convertrh, rhconvert2nifti, [('converted', 'in_files'),
                                                             (('converted', get_depth), 'depths')]),
                          ])

    return vol2surf


if __name__ == "__main__":
    base_dir = processing_base_dir
    workflow = create_visuotopy_wf()
