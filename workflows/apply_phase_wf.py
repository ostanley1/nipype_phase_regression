import nipype.pipeline.engine as pe
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype.interfaces.utility as ul
import nipype_phase_regression.interfaces as phase
from nipype_phase_regression.workflows.compcor import create_compcor_wf

def matchFile(input, base):
    ## This function looks through a list of possible matches
    # and throws an error if not in the list
    from nipype.utils.filemanip import split_filename
    import pprint
    if len(input) == 0:
        input = list[input]
    for s in input:
        if 'Run' in base[:3] in s and 'Run' in s:
            if 'processed' in s and base[:16] in s:
                return s
        elif 'data' in base and 'data' in s:
            if base[:30] in s:
                return s
        elif 'MENON_OLIVIA' in base and 'MENON_OLIVIA' in s:
            _, inputbase, _ = split_filename(str(s))
            if base.split('.')[3] == inputbase.split('.')[3] or int(base.split('.')[3]) == int(inputbase.split('.')[3])-1:
                return s
        # TODO: full time fix for compcor
        elif 'CCOMBcompcor' in base and 'MENON_OLIVIA' in s:
            return s
        elif 'SVDCOMBcompcor' in base and 'data' in s:
            return s
    else:
        print "Input"
        pprint.pprint(input)
        print "Base"
        pprint.pprint(base)
        raise ValueError('No match found in input list')

def orderFits(mag, phase, fit):
    from nipype.utils.filemanip import split_filename
    from nipype_phase_regression.workflows.apply_phase_wf import matchFile
    mag_list = [None] * len(fit)
    phase_list = [None] * len(fit)
    # iterated by filtered result
    for i in xrange(len(fit)):
        _, base, _ = split_filename(fit[i])
        # select correct raw mag
        mag_list[i] = matchFile(mag, base)
        # select correct raw phase
        phase_list[i] = matchFile(phase, base)
    return mag_list, phase_list, fit

def orderSim(mag, sim):
    from nipype.utils.filemanip import split_filename
    from nipype_phase_regression.workflows.apply_phase_wf import matchFile
    mag_list = [None] * len(sim)
    # iterated by filtered result
    for i in xrange(len(sim)):
        _, base, _ = split_filename(sim[i])
        # select correct sim
        mag_list[i] = matchFile(mag, base)
    return mag_list, sim

def create_apply_phase_wf():
    # Initialise main workflow
    workflow = pe.Workflow(name="apply_phase")
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    # # grab magnitudes and phases for now only Run1 masks exist so only use run 1
    # grabmag = pe.Node(nio.DataGrabber(), name="grabmag")
    # grabmag.inputs.template = 'preprocmag/ds/mag/Run*processed.nii.gz'
    # grabmag.inputs.sort_filelist = True
    #
    # grabphase = pe.Node(nio.DataGrabber(), name="grabphase")
    # grabphase.inputs.template = 'preprocphase/ds/phase/Run*processed.nii.gz'
    # grabphase.inputs.sort_filelist = True

    # # Adding an input spec to govern inputs
    # inputnode = pe.MapNode(interface=ul.IdentityInterface(fields=['mag', 'phase']),name='inputspec', iterfield=['mag', 'phase'])

    # Filtering before PR (two options, bandpass mag and phase from [0.01, 0.1] like Andrew or Stavisky-Golay search like Robert Barry)

    # Option 1: Bandpass Filter with fsl
    bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
    bpftphase.inputs.highpass_sigma = 80
    bpftphase.inputs.lowpass_sigma = 2

    bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
    bpftmag.inputs.highpass_sigma = 80
    bpftmag.inputs.lowpass_sigma = 2

    # Option 2: Bandpass Filter with with hard windows
    windowphase = pe.MapNode(interface=phase.windowFilter(), name="windowphase", iterfield=["in_file"])
    windowphase.inputs.high_freq = 0.1
    windowphase.inputs.low_freq = 0.01
    windowphase.inputs.TR = 1.25

    windowmag = pe.MapNode(interface=phase.windowFilter(), name="windowmag", iterfield=["in_file"])
    windowmag.inputs.high_freq = 0.1
    windowmag.inputs.low_freq = 0.01
    windowmag.inputs.TR = 1.25

    # Option 3: Compcor
    compcormag = create_compcor_wf('compcormag')
    compcormag.inputs.inputspec.num_noise_components = 6
    compcormag.inputs.inputspec.highpass_sigma = 80
    compcormag.inputs.inputspec.lowpass_sigma = 2

    compcorphase = create_compcor_wf('compcorphase')
    compcorphase.inputs.inputspec.num_noise_components = 6
    compcorphase.inputs.inputspec.highpass_sigma = 80
    compcorphase.inputs.inputspec.lowpass_sigma = 2

    # Merge filtered and unfiltered data together so that both go through PR
    mergeprmag = pe.Node(interface=ul.Merge(4), name="mergeprmag")
    mergeprphase = pe.Node(interface=ul.Merge(4), name="mergeprphase")

    # Add phase regression
    phase_regress = pe.MapNode(interface=phase.phaseRegression(), name="phaseregression", iterfield=["mag", "phase"])

    # Option 3: Savitzky Golay Filter
    savitzkygolay = pe.MapNode(interface=phase.SavitzkyGolay(), name="savitzkygolay", iterfield=["mag", "phase"])

    # Merge outputs in order to do analysis on different kinds of filters, images etc.
    mergeoutfit = pe.Node(interface=ul.Merge(2), name="mergeoutfit")
    mergeoutsim = pe.Node(interface=ul.Merge(3), name="mergeoutsim")

    # order inputs and apply filtered fits to unfiltered data
    orderfit = pe.Node(interface=ul.Function(input_names=["mag", "phase", "fit"], output_names=["mag_list", "phase_list", "fit_list"],function=orderFits), name="orderfit")
    applyfit = pe.MapNode(interface=phase.applyFit(), name="applyfit", iterfield=["mag","phase","fit"])

    # order sim and mag inputs to allow for convolve and cross correlation
    ordersim = pe.Node(interface=ul.Function(input_names=["mag", "sim"], output_names=["mag_list", "sim_list"],function=orderSim), name="ordersim")
    convolve = pe.MapNode(interface=phase.convolveMagPhase(), name="convolve", iterfield=["mag","sim"])
    crosscorr = pe.MapNode(interface=phase.crosscorrMagPhase(), name="crosscorr", iterfield=["mag","sim"])

    # Use fsl's susan to blur the simulated images to look for vessels
    mergeblur = pe.Node(interface=ul.Merge(10), name="mergeblur")

    blurmag = pe.MapNode(interface=fsl.SUSAN(), name="blurmag", iterfield=["in_file"])
    blurmag.inputs.brightness_threshold = 100
    blurmag.inputs.fwhm = 8

    blursim = pe.MapNode(interface=fsl.SUSAN(), name="blursim", iterfield=["in_file"])
    blursim.inputs.brightness_threshold = 100
    blursim.inputs.fwhm = 8

    blur = pe.MapNode(interface=fsl.SUSAN(), name="blur", iterfield=["in_file"])
    blur.inputs.brightness_threshold = 100
    blur.inputs.fwhm = 8

    # Filtering section
    # get inputs and fsl bp filter
    workflow.connect(bpftmag, "out_file", mergeprmag, "in2")
    workflow.connect(bpftphase, "out_file", mergeprphase, "in2")

    # get inputs and window bp filter
    workflow.connect(windowmag, "filt_tc", mergeprmag, "in3")
    workflow.connect(windowphase, "filt_tc", mergeprphase, "in3")

    # apply compcor
    workflow.connect(compcormag, "outputspec.filtered_file", mergeprmag, "in4")
    workflow.connect(compcorphase, "outputspec.filtered_file", mergeprphase, "in4")

    # phase regression
    workflow.connect(mergeprmag, "out", phase_regress, "mag")
    workflow.connect(mergeprphase, "out", phase_regress, "phase")

    # apply fits to raw data
    workflow.connect(phase_regress, "fit", mergeoutfit, "in1")
    workflow.connect(savitzkygolay, "fit", mergeoutfit, "in2")
    workflow.connect(mergeoutfit, 'out', orderfit, "fit")
    workflow.connect(orderfit, "mag_list", applyfit, "mag")
    workflow.connect(orderfit, "phase_list", applyfit, "phase")
    workflow.connect(orderfit, "fit_list", applyfit, "fit")

    # convolve and cross-correlate data
    workflow.connect(phase_regress, "sim", mergeoutsim, "in1")
    workflow.connect(savitzkygolay, "sim", mergeoutsim, "in2")
    workflow.connect(applyfit, "out_sim", mergeoutsim, "in3")
    workflow.connect(mergeoutsim, "out", ordersim, "sim")
    workflow.connect(ordersim, "sim_list", blursim, 'in_file')
    workflow.connect(ordersim, "mag_list", blurmag, 'in_file')
    workflow.connect(blursim, 'smoothed_file', convolve, "sim")
    workflow.connect(blurmag, 'smoothed_file', convolve, "mag")
    workflow.connect(blursim, 'smoothed_file', crosscorr, "sim")
    workflow.connect(blurmag, 'smoothed_file', crosscorr, "mag")

    # merge and blur the appropriate data
    workflow.connect([(mergeprphase,mergeblur,[('out','in1')]),
                      (mergeprmag, mergeblur, [('out', 'in2')]),
                      (savitzkygolay,mergeblur,[('sim','in3'),('filt','in4')]),
                      (phase_regress,mergeblur,[('sim','in5'),('filt','in6')]),
                      (applyfit,mergeblur,[('out_sim','in7'),('out_filt','in8')]),
                      (convolve,mergeblur,[('convolve_tc','in9')]),
                      (crosscorr,mergeblur,[('crosscorr_tc','in10')]),
                      (mergeblur, blur, [('out', 'in_file')])])

    workflow.write_graph(graph2use='flat', format='png', simple_form=False)

    return workflow

if __name__=="__main__":

    workflow=create_apply_phase_wf()

    #apparently pycharm's interactive console takes over stdin but does not implement the close function
    #this causes the multiprocessing module to fail
    #we must create a dummy function for sys.stdin.close
    import sys
    if not hasattr(sys.stdin,'close'):
        def dummy_close():
            pass
        sys.stdin.close = dummy_close
    exec_graph = workflow.run(plugin='Linear')
    # workflow.config['execution']['plugin'] = "MultiProc"
    # exec_graph=workflow.run(plugin='SGEGraph', plugin_args={'qsub_args':'-q nipype_medium -N filtering -e /cfmm/data/ostanle2 -o /cfmm/data/ostanle2 -b Y',
    #                                                         'dont_resubmit_completed_jobs': True})
    #view output
    # rawimg = getoutimglocs(exec_graph, workflow.get_node("converter"), 'out_file')
    # betimg=getoutimglocs(exec_graph,workflow.get_node("extractor"),'out_file')
    # mcflirtimg = getoutimglocs(exec_graph, workflow.get_node("mcflirter"), 'out_file')
    # detrendimg = getoutimglocs(exec_graph, workflow.get_node("detrendmag"), 'detrended_mag')
    # SNRimg = getoutimglocs(exec_graph, workflow.get_node("SNRcalc"), 'out_file')
    # for i in xrange(len(rawimg)):
    #     fslview([rawimg[i],betimg[i],mcflirtimg[i],detrendimg[i],SNRimg[i]])
    print "DONE filtering images"
