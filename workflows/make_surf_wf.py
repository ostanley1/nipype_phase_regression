import nipype.pipeline.engine as pe
import nipype.interfaces.freesurfer as fsurf
import nipype.interfaces.fsl as fsl
import nipype.interfaces.utility as ul
from nipype.workflows.smri.freesurfer.utils import copy_file
import nipype_phase_regression.interfaces as pr
from nipype.workflows.smri.freesurfer import create_skullstripped_recon_flow, create_reconall_workflow

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def picksecond(files):
    if isinstance(files, list):
        return files[1]
    else:
        return files

def pickthird(files):
    if isinstance(files, list):
        return files[2]
    else:
        return files

def appendnostrip(string):
    return string + ' -noskullstrip'

def appendmultistrip(string):
    return string + ' -multistrip'

def create_make_surf_wf():
    surf = pe.Workflow(name="surf")
    surf.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['struct', 'midline', 'y_min', 'y_size',
                                                     'subject_id', 'normbrain', 'recon_cmd',
                                                     'recon_cmddef','inv']),
                        name='inputspec')

    # run freesurfer recon-all command on inversion and data

    reconallinv = pe.Node(interface=fsurf.ReconAll(), name='reconallinv')
    reconallinv.inputs.directive = 'autorecon1'
    reconallinv.inputs.hires = True
    reconallinv.inputs.parallel = True
    reconallinv.inputs.subjects_dir='/workspace/ostanle2/SEvsGEBIDS/inv/'

    convertinv = pe.Node(interface=fsurf.MRIConvert(), name='convertinv')
    convertinv.inputs.out_type='niigz'

    betT1 = pe.Node(interface=fsl.BET(), name="betT1")
    betT1.inputs.mask = True

    maskT1 = pe.Node(interface=fsurf.ApplyMask(), name='maskT1')

    # reconall = create_reconall_workflow()
    # reconall.inputs.num_threads = 4
    # reconall.inputs.subjects_dir='/workspace/ostanle2/SEvsGEBIDS/defaultcut'

    autorecon1 = pe.Node(interface=fsurf.ReconAll(), name='autorecon1')
    autorecon1.inputs.parallel=True
    autorecon1.inputs.hires=True
    autorecon1.inputs.directive = 'autorecon1'
    autorecon1.inputs.subjects_dir='/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    autorecon2 = pe.Node(interface=fsurf.ReconAll(), name='autorecon2')
    autorecon2.inputs.parallel = True
    autorecon2.inputs.hires = True
    autorecon2.inputs.directive = 'autorecon2'
    autorecon2.inputs.subjects_dir='/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    autorecon3 = pe.Node(interface=fsurf.ReconAll(), name='autorecon3')
    autorecon3.inputs.parallel = True
    autorecon3.inputs.hires = True
    autorecon3.inputs.directive = 'autorecon3'
    autorecon3.inputs.subjects_dir='/workspace/ostanle2/SEvsGEBIDS/freesurfer'

    # reconallbet = create_skullstripped_recon_flow()
    # reconallbet.inputs.inputspec.subjects_dir='/workspace/ostanle2/SEvsGEBIDS/bet'
    # reconallbet.inputs.autorecon1.hires = True
    # reconallbet.inputs.autorecon1.parallel = True
    # reconallbet.inputs.autorecon_resume.hires = True
    # reconallbet.inputs.autorecon_resume.parallel = True
    # reconallbet.inputs.autorecon_resume.directive = 'autorecon2'

    # cut front of brain off
    frontcut = pe.Node(interface=fsl.ExtractROI(), name='frontcut')
    frontcut.inputs.x_min=0
    frontcut.inputs.x_size=-1
    frontcut.inputs.z_min=0
    frontcut.inputs.z_size=-1

    # cut front of brain off
    frontcutinv = pe.Node(interface=fsl.ExtractROI(), name='frontcutinv')
    frontcutinv.inputs.x_min=0
    frontcutinv.inputs.x_size=-1
    frontcutinv.inputs.z_min=0
    frontcutinv.inputs.z_size=-1

    # intensity correction
    n3 = pe.Node(interface=fsurf.MNIBiasCorrection(), name='n3')
    n3.inputs.iterations = 2

    # brain extraction
    skullstrip = pe.Node(interface=fsurf.WatershedSkullStrip(), name='skullstrip')
    skullstrip.inputs.args='-h 10'

    # normalize brain
    normalize = pe.Node(interface=fsurf.Normalize(), name='normalize')
    normalize.inputs.out_file='brainmask.auto_norm.mgz'

    # white matter segment brain
    segment = pe.Node(interface=fsurf.SegmentWM(), name='segment')
    segment.inputs.out_file = "wm.seg.mgz"

    # convert back to fsl format
    mconvert = pe.Node(interface=fsurf.MRIConvert(), name='mconvert')
    mconvert.inputs.out_type='niigz'

    mwmconvert = pe.Node(interface=fsurf.MRIConvert(), name='mwmconvert')
    mwmconvert.inputs.out_type = 'niigz'

    norm_convert = mconvert.clone('norm_convert')

    # heal wm mask for small holes
    healwm = pe.Node(interface=fsl.maths.MathsCommand(args='-kernel 3D -dilM -ero'), name='healwm')

    # find transform from brain to standard space
    mnireg = pe.Node(interface=fsl.FLIRT(dof=12), name='mnireg')
    mnireg.inputs.reference='/cfmm/apps/fsl-alt/fsl-5.0.10-centos6_64/data/standard/MNI152_T1_2mm.nii.gz'

    nlmnireg = pe.Node(interface=fsl.FNIRT(), name='nlmnireg')
    nlmnireg.inputs.ref_file = '/cfmm/apps/fsl-alt/fsl-5.0.10-centos6_64/data/standard/MNI152_T1_2mm.nii.gz'
    nlmnireg.inputs.config_file = 'T1_2_MNI152_2mm'
    nlmnireg.inputs.warp_resolution = (6,6,6)
    nlmnireg.inputs.field_file = True

    # register masks to anatomical space
    invertmnireg = pe.Node(interface=fsl.ConvertXFM(invert_xfm = True), name='invertmnireg')

    invertnlmnireg = pe.Node(interface=fsl.InvWarp(), name='invertnlmnireg')

    regV1 = pe.Node(interface=fsl.ApplyWarp(), name='regV1')
    regV1.inputs.in_file='/workspace/ostanle2/SEvsGEBIDS/V1prob.nii.gz'

    convertribbon = pe.MapNode(interface=fsurf.ApplyVolTransform(), name='convertribbon', iterfield=['source_file'])
    convertribbon.inputs.reg_header = True

    niiribbon = pe.MapNode(interface=fsurf.MRIConvert(), name='niiribbon', iterfield=['in_file'])
    niiribbon.inputs.out_type = 'niigz'

    convertV1 = pe.Node(interface=fsurf.ApplyVolTransform(), name='convertV1')
    convertV1.inputs.reg_header = True

    addribbon = pe.Node(interface=fsl.MultiImageMaths(),name='addribbon')
    addribbon.inputs.op_string = '-add %s -bin '

    mask2ribbon = pe.Node(interface=fsl.ImageMaths(suffix='_ribbon',
                                                   op_string='-mas'),
                          name='mask2ribbon')

    regV1box = pe.Node(interface=fsl.ApplyWarp(), name='regV1box')
    regV1box.inputs.in_file = '/workspace/ostanle2/SEvsGEBIDS/V1mask.nii.gz'

    mask2box = mask2ribbon.clone('mask2box')

    #
    # # apply transform from brain to standard space to struct and wm
    # orientstruct = pe.Node(interface=fsl.ApplyXFM(apply_xfm=True), name='orientstruct')
    #
    # orientwm = pe.Node(interface=fsl.ApplyXFM(apply_xfm=True), name='orientwm')
    #
    # # cut brain
    # lhemicut = pe.Node(interface=fsl.ExtractROI(), name='lhemicut')
    # lhemicut.inputs.x_min=0
    # lhemicut.inputs.y_min=0
    # lhemicut.inputs.y_size=-1
    # lhemicut.inputs.z_min=0
    # lhemicut.inputs.z_size=-1
    #
    # rhemicut = pe.Node(interface=fsl.ExtractROI(), name='rhemicut')
    # rhemicut.inputs.x_size = -1
    # rhemicut.inputs.y_min=0
    # rhemicut.inputs.y_size=-1
    # rhemicut.inputs.z_min=0
    # rhemicut.inputs.z_size=-1
    #
    # # label white matter
    # lhemilabel = pe.Node(interface=fsl.maths.MathsCommand(args='-bin -mul 255'), name='lhemilabel')
    #
    # rhemilabel = pe.Node(interface=fsl.maths.MathsCommand(args='-bin -mul 127'), name='rhemilabel')
    #
    # # merge images together
    # mergehemi = pe.Node(interface=ul.Merge(2), name='mergehemi')
    #
    # mergewm = pe.Node(interface=fsl.Merge(dimension='x'), name='mergewm')
    #
    # # make wm surfaces
    # ltessellate = pe.Node(interface=fsurf.MRITessellate(label_value=255), name='ltessellate')
    # rtessellate = pe.Node(interface=fsurf.MRITessellate(label_value=127), name='rtessellate')
    #
    # # smooth wm surfaces
    # lsmooth = pe.Node(interface=fsurf.SmoothTessellation(), name='lsmooth')
    # rsmooth = pe.Node(interface=fsurf.SmoothTessellation(), name='rsmooth')
    #
    # # files need to be copied for mris_make_surfaces to work, this can be done with function nodes
    # convertfilled = pe.Node(interface=fsurf.MRIConvert(), name='convertfilled')
    # convertfilled.inputs.out_type = 'mgz'
    #
    # copyfilled = pe.Node(ul.Function(['in_file', 'out_file'], ['out_file'], copy_file),
    #     name='copyfilled')
    # copyfilled.inputs.out_file = 'filled.mgz'
    #
    # copyorig = pe.Node(ul.Function(['in_file', 'out_file'], ['out_file'], copy_file),
    #     name='copyorig')
    # copyorig.inputs.out_file = 'lh.orig'
    #
    # copyorigrh = pe.Node(ul.Function(['in_file', 'out_file'], ['out_file'], copy_file),
    #     name='copyorigrh')
    # copyorigrh.inputs.out_file = 'rh.orig'
    #
    # # make pial surfaces
    # lsurf = pe.Node(interface=fsurf.MakeSurfaces(hemisphere='lh'), name='lsurf')
    # lsurf.inputs.args='-noaseg -noaparc'
    # # lsurf.inputs.copy_inputs = True
    # lsurf.inputs.mgz = True
    #
    # rsurf = lsurf.clone('rsurf')
    # rsurf.inputs.hemisphere='rh'
    #
    convertwhite = pe.MapNode(interface=fsurf.MRIsConvert(), name='convertwhite', iterfield=['in_file'])
    convertwhite.inputs.out_datatype='asc'
    convertwhite.inputs.to_scanner=True
    
    convertwhitenii = pe.MapNode(interface=fsurf.MRIConvert(), name='convertwhitenii', iterfield=['in_file'])
    convertwhitenii.inputs.out_type='niigz'

    convertpial = convertwhite.clone('convertpial')

    ldepth = pe.Node(interface=pr.DepthSample(), name='ldepth')

    rdepth = ldepth.clone('rdepth')

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['norm_struct', 'norm_struct_brain', 'V1mask', 'V1box', 'white']), name='outputspec')

    surf.connect([(inputspec, frontcut, [(('struct',pickfirst), 'in_file'),
                                         ('y_min', 'y_min'),
                                         ('y_size', 'y_size')]),
                  (inputspec, frontcutinv, [(('inv',pickfirst), 'in_file'),
                                         ('y_min', 'y_min'),
                                         ('y_size', 'y_size')]),
                  # (inputspec, lhemicut, [('midline', 'x_size')]),
                  # (inputspec, rhemicut, [('midline', 'x_min')]),
                  # (inputspec, lsurf, [('subject_id', 'subject_id')]),
                  # (inputspec, rsurf, [('subject_id', 'subject_id')]),
                  (inputspec, autorecon1, [('subject_id', 'subject_id'),
                                         ('recon_cmd', 'args'),
                                         (('struct',pickfirst), 'T1_files'),
                                        ]),
                  (autorecon1, autorecon2, [('subject_id', 'subject_id')]),
                  (inputspec, autorecon2, [
                                         # ('subject_id', 'subject_id'),
                                         ('recon_cmddef', 'args'),
                                         (('struct', pickfirst), 'T1_files'),
                                         ]),
                  (inputspec, autorecon3, [
                                        # ('subject_id', 'subject_id'),
                                        ('recon_cmddef', 'args'),
                                        (('struct', pickfirst), 'T1_files'),
                                        ]),
                  (autorecon2, autorecon3, [('subject_id', 'subject_id')]),
                  # (inputspec, reconallbet, [('subject_id', 'inputspec.subject_id'),
                  #                        ('recon_cmd', 'autorecon_resume.args'),
                  #                        ('recon_cmd', 'autorecon1.args'),
                  #                        ]),
                  (inputspec, reconallinv, [('subject_id', 'subject_id'),
                                          # (('inv', pickfirst), 'T1_files'),
                                          ('recon_cmd', 'args')]),
                  (frontcutinv, reconallinv, [('roi_file', 'T1_files')]),
                  (reconallinv, convertinv, [('nu', 'in_file')]),
                  (convertinv, betT1, [('out_file','in_file')]),
                  (betT1, maskT1, [('mask_file', 'mask_file')]),
                  (frontcut, maskT1, [('roi_file', 'in_file')]),
                  # (maskT1, reconallbet, [('out_file', 'inputspec.T1_files')]),
                  # (frontcut, reconall, [('roi_file', 'T1_files')]),
                  (frontcut, n3, [('roi_file', 'in_file')]),
                  (n3, skullstrip, [('out_file', 'in_file')]),
                  (skullstrip, normalize, [('out_file', 'in_file')]),
                  (normalize, mconvert, [('out_file', 'in_file')]),
                  (normalize, segment, [('out_file', 'in_file')]),
                  (segment, mwmconvert, [('out_file', 'in_file')]),
                  (n3, mnireg, [('out_file', 'in_file')]),
                  # (mnireg, orientstruct, [('out_matrix_file','in_matrix_file')]),
                  (mnireg, invertmnireg, [('out_matrix_file', 'in_file')]),
                  (mnireg, nlmnireg, [('out_matrix_file', 'affine_file')]),
                  (n3, nlmnireg, [('out_file','in_file')]),
                  (nlmnireg, invertnlmnireg, [('field_file', 'warp')]),
                  (n3, invertnlmnireg, [('out_file', 'reference')]),
                  (invertnlmnireg, regV1, [('inverse_warp', 'field_file')]),
                  (n3, regV1, [('out_file', 'ref_file')]),
                  (invertnlmnireg, regV1box, [('inverse_warp', 'field_file')]),
                  (n3, regV1box, [('out_file', 'ref_file')]),
                  # (mconvert, orientstruct, [('out_file', 'in_file'),
                  #                           ('out_file', 'reference')]),
                  # (mnireg, orientwm, [('out_matrix_file', 'in_matrix_file')]),
                  # (mwmconvert,healwm, [('out_file', 'in_file')]),
                  # (healwm, orientwm, [('out_file', 'in_file'),
                  #                           ('out_file', 'reference')]),
                  # (healwm, lhemicut, [('out_file', 'in_file')]),
                  # (healwm, rhemicut, [('out_file', 'in_file')]),
                  # (lhemicut, lhemilabel, [('roi_file', 'in_file')]),
                  # (rhemicut, rhemilabel, [('roi_file', 'in_file')]),
                  # (lhemilabel, mergehemi, [('out_file', 'in2')]),
                  # (rhemilabel, mergehemi, [('out_file', 'in1')]),
                  # (mergehemi, mergewm, [('out', 'in_files')]),
                  # (mergewm, ltessellate, [('merged_file', 'in_file')]),
                  # (mergewm, rtessellate, [('merged_file', 'in_file')]),
                  # (ltessellate, lsmooth, [('surface', 'in_file')]),
                  # (rtessellate, rsmooth, [('surface', 'in_file')]),
                  # (mergewm, convertfilled, [('merged_file','in_file')]),
                  # (convertfilled, copyfilled,[('out_file','in_file')]),
                  # (normalize, lsurf, [('out_file', 'in_T1')]),
                  # (lsmooth, lsurf, [('surface', 'in_white')]),
                  # (copyfilled, lsurf, [('out_file', 'in_filled')]),
                  # (ltessellate, copyorig, [('surface', 'in_file')]),
                  # (copyorig, lsurf, [('out_file', 'in_orig')]),
                  # (segment, lsurf, [('out_file','in_wm')]),
                  # (normalize, rsurf, [('out_file', 'in_T1')]),
                  # (rsmooth, rsurf, [('surface', 'in_white')]),
                  # (copyfilled, rsurf, [('out_file', 'in_filled')]),
                  # (rtessellate, copyorigrh, [('surface', 'in_file')]),
                  # (copyorigrh, rsurf, [('out_file', 'in_orig')]),
                  # (segment, rsurf, [('out_file', 'in_wm')]),
                  (autorecon3, convertpial, [('pial', 'in_file')]),
                  (autorecon3, convertwhite, [('white', 'in_file')]),
                  (autorecon3, convertwhitenii, [('wm', 'in_file')]),
                  (autorecon3, convertribbon, [('ribbon', 'source_file')]),
                  (convertribbon, niiribbon, [('transformed_file', 'in_file')]),
                  (autorecon3, convertribbon, [('orig', 'target_file')]),
                  (niiribbon, addribbon, [(('out_file',picksecond), 'in_file'),
                                              (('out_file', pickthird), 'operand_files')]),
                  (addribbon, mask2ribbon, [('out_file', 'in_file2')]),
                  (regV1box, convertV1, [('out_file', 'source_file')]),
                  (autorecon3, convertV1, [('orig', 'target_file')]),
                  (convertV1, mask2ribbon, [('transformed_file', 'in_file')]),
                  # (mask2ribbon, ldepth, [('out_file', 'mask_file')]),
                  # (convertwhite, ldepth, [(('converted', picksecond),'white_surf')]),
                  # (convertpial, ldepth, [(('converted', picksecond), 'pial_surf')]),
                  # (mask2ribbon, rdepth, [('out_file', 'mask_file')]),
                  # (convertwhite, rdepth, [(('converted', pickfirst), 'white_surf')]),
                  # (convertpial, rdepth, [(('converted', pickfirst), 'pial_surf')]),
                  (regV1, outputspec, [('out_file', 'V1mask')]),
                  (regV1box, outputspec, [('out_file', 'V1box')]),
                  (n3, outputspec, [('out_file', 'norm_struct')]),
                  (mconvert, outputspec, [('out_file', 'norm_struct_brain')]),
                  (autorecon3, outputspec, [('white', 'white')])

    ])

    return surf

if __name__=="__main__":
    base_dir = processing_base_dir
    workflow=make_surf_wf()

