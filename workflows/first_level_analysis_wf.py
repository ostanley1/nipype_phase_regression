import nipype.pipeline.engine as pe
import nipype.algorithms.modelgen as model
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
from nipype_phase_regression.workflows.fslestimate import create_modelfit_workflow

def create_first_level_analysis_wf(name="first_level", custom_results_dir=True):
    # Initialise main workflow
    workflow = pe.Workflow(name=name)
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    # grab all timecourse except for raw phase from upstream gadgets
    # grabtc = pe.Node(nio.DataGrabber(), name="grabtc")
    # grabtc.inputs.template = 'apply_phase/ds/tc/Run1*.nii.gz'
    # grabtc.inputs.sort_filelist = True

    makemodel = pe.Node(interface=model.SpecifyModel(), name="makemodel")

    modelfit = create_modelfit_workflow(custom_results_dir=custom_results_dir)
    modelfit.inputs.inputspec.film_threshold = 0
    modelfit.inputs.modelestimate.output_pwdata = True

    # level1design = pe.MapNode(interface=fsl.Level1Design(), name="level1design", iterfield=["session_info"])
    #
    # modelgen = pe.MapNode(interface=fsl.FEATModel(), name='modelgen',
    #                       iterfield=['fsf_file', 'ev_files'])
    #
    # modelestimate = pe.MapNode(interface=fsl.FILMGLS(),
    #                            name='modelestimate',
    #                            iterfield=['design_file', 'in_file'])
    #
    # merge_contrasts = pe.MapNode(interface=util.Merge(2), name='merge_contrasts',
    #                              iterfield=iterfield)
    #
    # ztopval = pe.MapNode(interface=fsl.ImageMaths(op_string='-ztop',
    #                                               suffix='_pval'),
    #                      nested=True,
    #                      name='ztop',
    #                      iterfield=['in_file'])

    workflow.connect([(makemodel, modelfit, [('session_info', 'inputspec.session_info')]),
                      # (level1design, modelgen, [('fsf_files', 'fsf_file'),('ev_files', 'ev_files')]),
                      # (modelgen, modelestimate, [('design_file', 'design_file')]),
                      # (modelgen, conestimate, [('con_file', 'tcon_file')]),
                      # (modelestimate,ds, [('copes', 'contrasts@copes'),
                      #                   ('varcopes', 'contrasts@varcopes'),
                      #                   ('tstats', 'contrasts@tstats'),
                      #                   ('tstats', 'contrasts@zstats')])
                      ])

    return workflow