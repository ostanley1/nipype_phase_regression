import nipype.pipeline.engine as pe
import nipype.interfaces.utility as ul
import nipype.interfaces.freesurfer as fsurf
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr
import nipype.interfaces.afni as afni

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def getrest(in_file):
    if isinstance(in_file, list):
        rest = []
        print(in_file)
        for i in range(len(in_file)):
            print(in_file[i])
            if 'checkerboard' in in_file[i]:
                rest.append(6)
            else:
                rest.append(4)
        print(rest)
        return rest
    else:
        if 'checkerboard' in in_file:
            return 6
        else:
            return 4

def create_venous_map_wf():
    map = pe.Workflow(name="venousmap")
    map.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['tof', 'gremag', 'grephase', 'mask_file', 'moco_pars', 'run_txfm']),
                        name='inputspec')

    #TOF Pipeline

    # extract brain from mag with fsl and save the mask
    extractortof = pe.Node(interface=fsl.BET(), name="extractortof")
    extractortof.inputs.padding = True
    extractortof.inputs.frac = 0.1

    # bias correct time-of-flight
    n3tof = pe.Node(interface=fsurf.MNIBiasCorrection(), name='n3tof')
    n3tof.inputs.iterations = 4

    # threshold time-of-flight for arterial mask
    threshtof = pe.Node(interface=fsl.maths.MathsCommand(args='-thr 125'), name='threshtof')

    #SWI Pipeline
    # make one swi per timepoint then average

    # motion correct magnitude and keep parameters
    mocomag = pe.MapNode(interface=afni.Allineate(), name='mocomag', iterfield=['in_file', 'in_matrix'])
    mocomag.inputs.outputtype = 'NIFTI_GZ'
    mocomag.inputs.out_file = 'mocomag.nii.gz'

    # extract brain from mag with fsl and save the mask
    extractor = pe.MapNode(interface=fsl.BET(), name="extractor", iterfield='in_file')
    extractor.inputs.mask = True
    extractor.inputs.padding = True
    extractor.inputs.frac = 0.1

    # erode brainmask for unwraping
    erodebm = pe.MapNode(interface=fsl.ErodeImage(), name='erodebm', iterfield='in_file')

    # average magnitude together
    avemagpre = pe.MapNode(interface=pr.RestAverage(), name='avemagpre', iterfield=['func', 'rest'])
    avemagpre.inputs.task = 6

    # intensity correct magnitude image
    n3mag = pe.MapNode(interface=fsurf.MNIBiasCorrection(), name='n3mag', iterfield='in_file')
    n3mag.inputs.iterations = 4

    # convert phase to radians from siemens units
    convphase = pe.MapNode(interface=fsl.maths.MathsCommand(output_datatype='float',
                                                         args='-mul 6.28318 -div 4095 -sub 3.14159'),
                        name='convphase', iterfield='in_file')

    # moco, unwrap, and average before homodyne
    # apply moco to the swi data
    mocopre = pe.MapNode(interface=afni.Allineate(), name='mocopre', iterfield=['in_file', 'in_matrix'])
    mocopre.inputs.outputtype = 'NIFTI_GZ'
    mocopre.inputs.out_file = 'mocophase.nii.gz'

    # unwrap
    unwrappre = pe.MapNode(interface=pr.UnwrapPhase(), name='unwrappre', iterfield=['in_file', 'mask_file'])
    unwrapero = unwrappre.clone('unwrapero')

    # averagepre
    avephasepre = pe.MapNode(interface=pr.RestAverage(), name='avephasepre', iterfield=['func', 'rest'])
    avephasepre.inputs.task = 6

    # homodyne single image
    homodynesingle =  pe.MapNode(interface=pr.HomodyneFilter(no_mask=True), name='homodynesingle', iterfield='in_file')
    homodynesingle.inputs.filter_size=96

    # create swi image
    swi = pe.MapNode(interface=pr.MakeSWI(), name='swi', iterfield=['mag_file', 'phase_file'])

    # invert the swi image for use in the vessel filter
    invertswi = pe.MapNode(interface=fsl.ImageMaths(suffix='_inv', op_string='-mul -1'), name='invertswi', iterfield='in_file')

    # apply the mask to swi data
    maskswi = pe.MapNode(interface=fsl.ImageMaths(suffix='_bet', op_string='-mas'), name='maskswi', iterfield=['in_file', 'in_file2'])

    # use 3d frangi vessel filter from the vmtk toolbox
    frangi = pe.MapNode(interface=pr.VMTKImageVesselEnhancement(), name='frangi', iterfield=['in_file'])
    # frangi.inputs.sigma_min = 0.5
    # frangi.iterables = ("gamma", [1, 10, 100, 200, 500])
    # frangi.inputs.sigma_max = 3
    frangi.inputs.gamma=100
    # frangi.inputs.sigma_steps = 5

    # frangi results in bad header info need to correct
    cpgeom = pe.MapNode(interface=fsl.CopyGeom(), name='cpgeom', iterfield=['dest_file', 'in_file'])

    # need to align the runs prior to merge
    align_runs = pe.MapNode(interface=fsl.ApplyXFM(), name='align_runs', iterfield=['in_file', 'reference', 'in_matrix_file'])
    align_runs.inputs.apply_xfm=True
    align_runs.inputs.interp = 'spline'

    # merge the vessel filtered images together
    mergevessels = pe.Node(interface=fsl.Merge(), name='mergevessels')
    mergevessels.inputs.dimension = 't'

    # mean
    meanvessels = pe.Node(interface=fsl.ImageMaths(suffix='mean', op_string='-Tmean'), name='meanvessels')

    # randomise
    randomise = pe.Node(interface=fsl.Randomise(), name='randomise')
    randomise.inputs.num_perm = 0
    randomise.inputs.one_sample_group_mean = True
    randomise.inputs.vox_p_values = True

    # vessel masking at p>0.05
    maskvessels = pe.MapNode(interface=fsl.Threshold(), name='maskvessels', iterfield=['in_file'])
    maskvessels.inputs.thresh = 0.95
    maskvessels.inputs.args = '-bin'

    # apply moco to the swi data
    moco = pe.MapNode(interface=afni.Allineate(), name='moco', iterfield=['in_file', 'in_matrix'])
    moco.inputs.outputtype = 'NIFTI_GZ'
    moco.inputs.out_file = 'mocophase.nii.gz'



    #average rest volumes of SWI
    aveswi = pe.MapNode(interface=pr.RestAverage(), name='aveswi', iterfield=['func', 'rest'])
    aveswi.inputs.task = 6

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['artmask', 'veinmask', 'swi', 'frangi', 'vessel']),
                        name='outputspec')

    map.connect([(inputspec, extractortof, [('tof', 'in_file')]),
                 (extractortof, n3tof, [('out_file', 'in_file')]),
                 (n3tof, threshtof, [('out_file', 'in_file')]),
                 (threshtof, outputspec, [('out_file', 'artmask')])])

    map.connect([(inputspec, convphase, [('grephase', 'in_file')]),
                 (inputspec, align_runs, [('run_txfm', 'in_matrix_file')]),
                 # path 1
                 # (convphase, homodyne, [('out_file', 'in_file')]),
                 # path 2
                 (convphase, mocopre, [('out_file', 'in_file')]),
                 (inputspec, mocopre, [('moco_pars', 'in_matrix')]),
                 (mocopre, unwrappre, [('out_file', 'in_file')]),
                 (extractor, unwrappre, [('mask_file', 'mask_file')]),
                 (mocopre, unwrapero, [('out_file', 'in_file')]),
                 (extractor, erodebm, [('mask_file', 'in_file')]),
                 (erodebm, unwrapero, [('out_file', 'mask_file')]),
                 (unwrappre, avephasepre, [('out_file', 'func')]),
                 (inputspec, avephasepre, [(('grephase', getrest), 'rest')]),
                 (avephasepre, homodynesingle, [('mean', 'in_file')]),
                 (homodynesingle, swi, [('out_file', 'phase_file')]),
                 (inputspec, mocomag, [('gremag', 'in_file'),
                                       ('moco_pars', 'in_matrix')]),
                 # (inputspec, n3mag, [('gremag', 'in_file')]),
                 (mocomag, extractor, [('out_file', 'in_file')]),
                 (mocomag, avemagpre, [('out_file', 'func')]),
                 (inputspec, avemagpre, [(('gremag', getrest), 'rest')]),
                 (avemagpre, swi, [('mean', 'mag_file')]),
                 (swi,invertswi,[('out_file', 'in_file')]),
                 (invertswi, maskswi, [('out_file','in_file')]),
                 (inputspec, maskswi, [('mask_file', 'in_file2')]),
                 (maskswi, frangi, [('out_file', 'in_file')]),
                 (swi, cpgeom, [('out_file','in_file')]),
                 (frangi, cpgeom, [('out_file', 'dest_file')]),
                 (cpgeom, align_runs,[('out_file', 'reference'),
                                      ('out_file', 'in_file')]),
                 (align_runs, mergevessels, [('out_file', 'in_files')]),
                 (mergevessels, randomise, [('merged_file', 'in_file')]),
                 (mergevessels, meanvessels, [('merged_file', 'in_file')]),
                 (randomise, maskvessels, [('t_corrected_p_files', 'in_file')]),
                 # (swi, moco, [('out_file', 'in_file')]),
                 # (volreg, moco, [('oned_matrix_save','in_matrix')]),
                 # (moco, maskswi, [('out_file', 'in_file')]),
                 # (extractor, maskswi, [('mask_file','in_file2')]),
                 # (maskswi, aveswi, [('out_file', 'func')]),
                 # (inputspec, aveswi, [(('grephase',getrest), 'rest')]),
                 (swi, outputspec, [('out_file', 'swi')]),
                 (frangi, outputspec, [('out_file', 'frangi')]),
                 (meanvessels, outputspec, [('out_file', 'vessel')])
                 ])

    return map

if __name__=="__main__":
    base_dir = processing_base_dir
    workflow=create_venous_map_wf()

