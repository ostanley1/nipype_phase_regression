import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype.algorithms.modelgen as model
import nipype.interfaces.utility as ul
import nipype_phase_regression.interfaces as pr

def getthreshop(thresh):
    if isinstance(thresh[0], list):
        ops=[]
        for i in range(len(thresh)):
            ops.append('-thr %.10f -Tmin -bin' % (0.1 * thresh[i][1]))
        return ops
    else:
        return '-thr %.10f -Tmin -bin' % (0.1 * thresh[0][1])

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def pickfirstmap(files):
    if isinstance(files, list):
        outlist=[]
        for file in files:
            if isinstance(files,list):
                outlist.append(files[0])
            else:
                outlist.append(file)
        return outlist
    else:
        return files

def getinormscale(medianvals):
    return ['-mul %.10f' % (10000. / val) for val in medianvals]

def getrevinormscale(medianvals):
    return ['-div %.10f' % (10000. / val) for val in medianvals]

def num_copes(files):
    return len(files)

def getPPheight(in_file):
    op_string = []
    with open(in_file) as f:
        for line in f:
            if "/PPheights" in line:
                numbers_float = [float(i) for i in line.strip("/PPheights").split()]
    for i in range(len(numbers_float)):
        op_string.append(''.join(['-mul ',str(numbers_float[i]), ' -mul 100 -div %s']))
    print(op_string[0])
    return op_string[0]

def nestmeans(in_file, op_string):
    return [in_file] * len(op_string)

def grabfirst(in_file):
    return in_file[0]

def getrest(in_file):
    if 'checkerboard' in in_file:
        return 6
    else:
        return 4

def highpasssetup(highpass, TR):
    return '-bptf %d -1 -add ' % (highpass / TR)

def create_physio_regressors_wf():
    import spynoza.spynoza.denoising.nodes as pnm
    physio = pe.Workflow(name='physio_regressors')

    # define inputs
    inputspec = pe.MapNode(ul.IdentityInterface(fields=[]),
                        name='inputspec')

    write_physio = pe.MapNode(interface=ul.Function(function=writephysio, input_names=["cardiac", "resp"],
                                  output_names=["out_file", "slicefile"]), name="write_physio", iterfield=["cardiac", "resp"])

    popp = pe.MapNode(interface=pnm.PreparePNM(), name='popp')

def create_feat_preproc_wf():
    feat = pe.Workflow(name="feat_preproc")
    feat.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['input', 'highpass', 'TR']),
                        name='inputspec')

    # Get 2 and 98th percentiles
    getthresh = pe.MapNode(interface=fsl.ImageStats(op_string='-p 2 -p 98'),
                           iterfield=['in_file'],
                           name='getthreshold')

    # Threshold the first run of the functional data at 10% of the 98th percentile
    threshold = pe.MapNode(interface=fsl.ImageMaths(out_data_type='char',
                                                 suffix='_thresh'),
                        name='threshold', iterfield=['in_file', 'op_string'])

    # get median value using the mask
    medianval = pe.MapNode(interface=fsl.ImageStats(op_string='-k %s -p 50'),
                           iterfield=['in_file', 'mask_file'],
                           name='medianval')

    # dilate the mask
    dilatemask = pe.MapNode(interface=fsl.ImageMaths(suffix='_dil',
                                                  op_string='-dilF'),
                         name='dilatemask', iterfield=['in_file'])

    # mask the data with dilated mask
    maskfunc2 = pe.MapNode(interface=fsl.ImageMaths(suffix='_mask',
                                                    op_string='-mas'),
                           iterfield=['in_file', 'in_file2'],
                           name='maskfunc2')

    # get the mean from each run
    meanfunc2 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                    suffix='_mean'),
                           iterfield=['in_file'],
                           name='meanfunc2')

    # # merge the mean and median
    # mergenode = pe.Node(interface=ul.Merge(2, axis='hstack'),
    #                     name='merge')

    # scale the run to have a median of 10000
    intnorm = pe.MapNode(interface=fsl.ImageMaths(suffix='_intnorm'),
                         iterfield=['in_file', 'op_string'],
                         name='intnorm')

    # get the mean from each run
    meanfunc3 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                    suffix='_mean'),
                           iterfield=['in_file'],
                           name='meanfunc3')

    # calculate the cutoff
    calculatehpcutoff = pe.Node(interface=ul.Function(function=highpasssetup, input_names=["highpass", "TR"],
                                  output_names=["op_string"]), name="calculatehpcutoff")

    # highpass the data
    highpass = pe.MapNode(interface=fsl.ImageMaths(suffix='_tempfilt'),
                          iterfield=['in_file','in_file2'],
                          name='highpass')
    highpass.inputs.suffix = '_hpf'

    # Need to prevent negative numbers cause smoothing doesn't work
    thresh = pe.MapNode(interface=fsl.Threshold(direction='below', nan2zeros=True, thresh=0), name='thresh', iterfield=['in_file'])

    calcSNR = pe.MapNode(interface=pr.RestAverage(), name='calcSNR', iterfield=['func'])
    calcSNR.inputs.task = 6

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['filtered_functional_data',
                                                      'mean_file','snr_file', 'mean_run',
                                                      'noise_file', 'scalingfactor']),
                        name='outputspec')

    feat.connect([(inputspec, getthresh, [('input', 'in_file')]),
                  (inputspec, threshold, [('input', 'in_file')]),
                  (inputspec, medianval, [('input', 'in_file')]),
                  (inputspec, calculatehpcutoff, [('highpass', 'highpass'),
                                         ('TR', 'TR')]),
                  (getthresh, threshold, [(('out_stat', getthreshop), 'op_string')]),
                  (threshold, medianval, [('out_file', 'mask_file')]),
                  (threshold, dilatemask, [('out_file', 'in_file')]),
                  (inputspec, maskfunc2, [('input', 'in_file')]),
                  (dilatemask, maskfunc2, [('out_file', 'in_file2')]),
                  (maskfunc2, meanfunc2, [('out_file','in_file')]),
                  # (meanfunc2, mergenode, [('out_file', 'in1')]),
                  # (medianval, mergenode, [('out_stat', 'in2')]),
                  (maskfunc2, intnorm, [('out_file', 'in_file')]),
                  (medianval, intnorm, [(('out_stat', getinormscale), 'op_string')]),
                  (intnorm, meanfunc3, [('out_file', 'in_file')]),
                  (calculatehpcutoff, highpass, [('op_string', 'op_string')]),
                  (intnorm, highpass, [('out_file', 'in_file')]),
                  (meanfunc3, highpass, [('out_file', 'in_file2')]),
                  (highpass, thresh, [('out_file', 'in_file')]),
                  (thresh, calcSNR, [(('out_file', getrest), 'rest'),
                                       ('out_file', 'func')]),
                  (thresh, outputspec, [('out_file','filtered_functional_data')]),
                  (meanfunc3, outputspec, [(('out_file', pickfirst), 'mean_file')]),
                  (calcSNR, outputspec, [('tsnr', 'snr_file'),
                                         ('mean', 'mean_run'),
                                         ('noise', 'noise_file')]),
                  (medianval, outputspec, [('out_stat', 'scalingfactor')])
                  ])

    return feat

def create_feat_stats_wf():
    feat = pe.Workflow(name="feat_stats")
    feat.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['input', 'TR', 'subject_info',
                                                     'contrasts', 'mean', 'scalingfactor',
                                                     'hpcutoff']),name='inputspec')


    modelspec = pe.MapNode(interface=model.SpecifyModel(), name="modelspec",
                           iterfield=["subject_info","functional_runs"])
    modelspec.inputs.input_units = 'secs'

    level1design = pe.MapNode(interface=fsl.Level1Design(), name="level1design",
                              iterfield=["session_info", "contrasts"])
    level1design.inputs.bases = {'dgamma': {'derivs': True}}
    level1design.inputs.model_serial_correlations = True

    modelgen = pe.MapNode(interface=fsl.FEATModel(), name='modelgen',
                          iterfield=['fsf_file', 'ev_files'])

    modelestimate = pe.MapNode(interface=fsl.FILMGLS(smooth_autocorr=True,
                                                     mask_size=5,
                                                     threshold=1000),
                               name='modelestimate',
                               iterfield=['design_file', 'in_file', 'tcon_file'])

    regressorheight = pe.MapNode(interface=ul.Function(function=getPPheight, input_names=["in_file"],
                                  output_names=["op_string"]), name="regressorheight", iterfield=["in_file"])

    nestmeanfuncs = pe.MapNode(interface=ul.Function(function=nestmeans, input_names=["in_file", "op_string"],
                                  output_names=["out_files"]), name="nestmeanfuncs", iterfield=["op_string"])

    grabpe1 = pe.MapNode(interface=ul.Function(function=grabfirst, input_names=["in_file"],
                                  output_names=["out_file"]), name="grabpe1", iterfield=["in_file"])

    mergeCNR = pe.MapNode(interface=ul.Merge(1), name='mergeCNR', iterfield=['in1'])

    calcCNR = pe.MapNode(interface=fsl.MultiImageMaths(), name='calcCNR', iterfield=['in_file', 'operand_files'])
    calcCNR.inputs.op_string='-Tstd -div %s -recip'

    sumres = pe.MapNode(interface=pr.PhysioResids(), name='sumres', iterfield=['residuals',
                                                                               'pefiles',
                                                                               'regressorlist'])



    # scale the run to have a median of 10000
    revintnorm = pe.MapNode(interface=fsl.ImageMaths(suffix='_intnormrev'),
                         iterfield=['in_file', 'op_string'],
                         name='intnormrev')

    revintnormphysio = revintnorm.clone('revintnormphysio')

    calcnoise = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tstd',
                                                   suffix='_std'),
                          name='calcnoise', iterfield=['in_file'])

    calcnoisephysio = calcnoise.clone('calcnoisephysio')

    pccalc = pe.MapNode(interface=fsl.MultiImageMaths(), name='pccalc', iterfield=['in_file', 'operand_files'], nested=True)

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['filtered_functional_data',
                                                      'func_maps', 'pcfunc_maps',
                                                      'mean_file', 'cnr_file', 'snr_file',
                                                      'zstat_file', 'mean_run', 'noise_file',
                                                      'physionoise_file', 'copes', 'varcopes']),
                        name='outputspec')

    feat.connect([(inputspec, modelspec, [('subject_info', 'subject_info'),
                                          ('hpcutoff', 'high_pass_filter_cutoff'),
                                          ('TR', 'time_repetition')
                                          ]),
                  (inputspec, level1design, [('contrasts', 'contrasts'),
                                             ('TR', 'interscan_interval')]),
                  (inputspec, modelspec, [('input', 'functional_runs')]),
                  (inputspec, modelestimate, [('input', 'in_file')]),
                  (inputspec, revintnorm, [(('scalingfactor', getrevinormscale), 'op_string')]),
                  (inputspec, revintnormphysio, [(('scalingfactor', getrevinormscale), 'op_string')]),
                  (modelestimate, grabpe1, [('param_estimates', 'in_file')]),
                  (modelestimate, calcCNR, [('residual4d', 'in_file')]),
                  (modelestimate, revintnorm, [('residual4d', 'in_file')]),
                  (revintnorm, calcnoise, [('out_file', 'in_file')]),
                  (modelestimate, calcCNR, [('copes', 'operand_files')]),
                  (modelspec, level1design, [('session_info', 'session_info')]),
                  (level1design, modelgen, [('fsf_files', 'fsf_file'),
                                            ('ev_files', 'ev_files')]),
                  (modelgen, regressorheight, [('design_file', 'in_file')]),
                  (regressorheight, nestmeanfuncs, [('op_string', 'op_string')]),
                  (inputspec, nestmeanfuncs, [('mean', 'in_file')]),
                  (nestmeanfuncs, pccalc, [('out_files', 'operand_files')]),
                  (regressorheight, pccalc, [(('op_string', pickfirst),'op_string')]),
                  (modelgen, modelestimate, [('design_file', 'design_file'),
                                             ('con_file', 'tcon_file')]),
                  (grabpe1, pccalc, [('out_file', 'in_file')]),
                  (inputspec, sumres, [('subject_info', 'regressorlist')]),
                  (modelestimate, sumres, [('residual4d', 'residuals'),
                                           ('param_estimates', 'pefiles')]),
                  (sumres, revintnormphysio, [('out_res', 'in_file')]),
                  (revintnormphysio, calcnoisephysio, [('out_file','in_file')]),
                  (modelestimate, outputspec, [('param_estimates','func_maps')]),
                  (pccalc, outputspec, [('out_file', 'pcfunc_maps')]),
                  (calcCNR, outputspec, [('out_file', 'cnr_file')]),
                  (calcnoise, outputspec, [('out_file', 'noise_file')]),
                  (calcnoisephysio, outputspec, [('out_file', 'physionoise_file')]),
                  (modelestimate, outputspec, [('zstats', 'zstat_file'),
                                               ('copes', 'copes'),
                                               ('varcopes', 'varcopes')]),

                  ])

    return feat

def create_feat_fixed_wf():
    feat = pe.Workflow(name="feat_fixed")
    feat.config['execution']['remove_unnecessary_outputs'] = False

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['copes','varcopes','numcopes', 'mask_file']), name='inputspec')

    copemerge = pe.Node(interface=fsl.Merge(dimension='t') , name="copemerge")

    varcopemerge = pe.Node(interface=fsl.Merge(dimension='t'), name="varcopemerge")

    level2model = pe.Node(interface=fsl.L2Model(), name='l2model')

    flameo = pe.Node(interface=fsl.FLAMEO(run_mode='fe'), name="flameo")

    converttop = pe.Node(interface=fsl.ImageMaths(op_string='-ztop', suffix='_pval'), name='converttop')

    fdrthresh = pe.Node(interface=pr.FDR(pval=0.01), name='fdrthresh')

    threshp = pe.Node(interface=fsl.Threshold(direction='above'), name='threshp')

    maskp = pe.Node(interface=fsl.ImageMaths(op_string='-bin'), name='maskp')



    threshz = pe.Node(interface=fsl.MultiImageMaths(op_string='-mas %s'), name='threshz')

    # results in bad header info need to correct
    cpgeommaskp = pe.MapNode(interface=fsl.CopyGeom(), name='cpgeommaskp', iterfield=['dest_file', 'in_file'])

    # outputspec
    outputspec = pe.Node(ul.IdentityInterface(fields=['pval']),
                         name='outputspec')

    feat.connect([(inputspec, copemerge, [('copes', 'in_files')]),
                  (inputspec, varcopemerge, [('varcopes', 'in_files')]),
                  (inputspec, level2model, [(('copes', num_copes), 'num_copes')]),
                  (inputspec, flameo, [('mask_file', 'mask_file')]),
                  (inputspec, fdrthresh, [('mask_file', 'mask_file')]),
                  (copemerge, flameo, [('merged_file', 'cope_file')]),
                  (varcopemerge, flameo, [('merged_file', 'var_cope_file')]),
                  (level2model, flameo, [('design_mat', 'design_file'),
                                         ('design_con', 't_con_file'),
                                         ('design_grp','cov_split_file')]),
                  (flameo, converttop, [('zstats', 'in_file')]),
                  (converttop, fdrthresh, [('out_file', 'in_file')]),
                  (converttop, threshp, [('out_file', 'in_file')]),
                  (fdrthresh, threshp, [('out_thresh', 'thresh')]),
                  (threshp, maskp, [('out_file', 'in_file')]),
                  (maskp, threshz, [('out_file', 'operand_files')]),
                  (flameo, threshz, [('zstats', 'in_file')]),
                  (maskp, cpgeommaskp, [('out_file', 'dest_file')]),
                  (inputspec, cpgeommaskp, [(('copes', pickfirst), 'in_file')]),
                  (cpgeommaskp, outputspec, [('out_file', 'pval')])
                  ])

    return feat

if __name__=="__main__":
    base_dir = processing_base_dir
    workflow=create_feat_wf()
