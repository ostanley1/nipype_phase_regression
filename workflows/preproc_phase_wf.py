import nipype.pipeline.engine as pe
import nipype.interfaces.freesurfer as freesurf
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as preproc

def create_preproc_phase_wf():
    workflow = pe.Workflow(name="preprocphase")
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    # convert dicom to nifti
    converter = pe.MapNode(interface=freesurf.MRIConvert(), name="converter", iterfield=["in_file"])

    # add combination method and Mag/Phase to intent field
    dataCleaner = pe.MapNode(interface=preproc.AddStudyDescription(), name="dataCleaner", iterfield=["nii", "dcm"])

    # make sure image is in standard orientation
    flipper = pe.MapNode(interface=fsl.Reorient2Std(), name="flipper", iterfield=["in_file"])

    # set up preprocessing interface
    prepPhase = pe.MapNode(interface=preproc.PreprocessPhase(), name="prepPhase", iterfield=["phase", 'siemens'])

    # use fslmaths to calculate phase var
    varcalc = pe.MapNode(interface=fsl.maths.MathsCommand(), name="varcalc", iterfield=["in_file"])
    varcalc.inputs.args = "-Tstd -sqr"

    # connect workflow
    workflow.connect(converter, 'out_file', dataCleaner, 'nii')
    workflow.connect(dataCleaner, 'fixednii', flipper, 'in_file')
    workflow.connect(flipper, 'out_file', prepPhase, 'phase')
    workflow.connect(prepPhase, 'detrended_phase', varcalc, 'in_file')

    return workflow

if __name__=="__main__":
    workflow=create_preproc_phase_wf()

    #apparently pycharm's interactive console takes over stdin but does not implement the close function
    #this causes the multiprocessing module to fail
    #we must create a dummy function for sys.stdin.close
    import sys
    if not hasattr(sys.stdin,'close'):
        def dummy_close():
            pass
        sys.stdin.close = dummy_close
    exec_graph=workflow.run(plugin='Linear')

    #view output
    # rawimg = getoutimglocs(exec_graph, workflow.get_node("converter"), 'out_file')
    # # radimg = getoutimglocs(exec_graph, workflow.get_node("prepPhase"), 'rad_phase')
    # deltaimg = getoutimglocs(exec_graph, workflow.get_node("prepPhase"), 'delta_phase')
    # uwimg = getoutimglocs(exec_graph, workflow.get_node("prepPhase"), 'uw_phase')
    # detrendimg = getoutimglocs(exec_graph, workflow.get_node("prepPhase"), 'detrended_phase')
    # varimg = getoutimglocs(exec_graph, workflow.get_node("varcalc"), 'out_file')
    # for i in xrange(len(rawimg)):
    #     fslview([rawimg[i],deltaimg[i],uwimg[i],detrendimg[i],varimg[i]])
    # print "DONE preprocess phase images"