#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  5 11:07:39 2017

@author: akuurstr
@mod: ostanle2

-in custom python nodes, using nibabel to open niftis and always copy the input
 affine matrix to the output nifti.
-in matlab nodes, using load_untouch_nii to load data without applying
 affine transformations. output nifti is saved using input's header
-user is expected to provide which dimension the nifti file stores the 
 B0 direction
"""
import numpy as np
from nipype.interfaces import utility as niu
import nipype.pipeline.engine as pe
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
from ..interfaces.qsm_sstv.interfaces_CFMM import GetAvgAndWeightsFromMag,CalculatReliabilityMask,\
TrimMaskUsingReliability,EstimateFrequncyFromWrappedPhase,GetCFFromJson,SS_TV,SS_TV_mcr,\
CalcR2Star,CalcR2Star_cmd,SiemensPhasePreprocess,replace_slash
from nipype.interfaces.utility import Function
from bids.grabbids import BIDSLayout
import os
import nipype.interfaces.utility as ul

def niftitojson(filename):
    if isinstance(filename, list):
        for f in range(len(filename)):
            filename[f] = filename[f][:-7]+'.json'
        return filename
    else:
        return filename[:-7]+'.json'


def create_pipeline_SS_TV():

    # define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['mag_images', 'phase_images']),
                        name='inputspec')
    
    #this node must change depending on the scanner vendor
    susc_phase_preprocess=pe.Node(SiemensPhasePreprocess(),name='susc_phase_preprocess')
    
    avg_and_freq_estimate_weights=pe.Node(GetAvgAndWeightsFromMag(),name='avg_and_freq_estimate_weights')
    avg_and_freq_estimate_weights.inputs.snr_window_sz=15
    avg_and_freq_estimate_weights.inputs.avg_out_filename="avg.nii.gz"
    avg_and_freq_estimate_weights.inputs.weight_out_filename="weights.nii.gz"          
    
    """
    #spm worked better for varian 7T data
    #if using spm, these prameters are needed
    bias_regularization=.001
    sampling_distance=2.0
    bias_fwhm=30
    
    nonuniformityCorrect_spm=pe.Node(spm.preprocess.Segment(),name='nonuniformityCorrect_spm')
    nonuniformityCorrect_spm.inputs.bias_regularization=bias_regularization
    nonuniformityCorrect_spm.inputs.sampling_distance=sampling_distance
    nonuniformityCorrect_spm.inputs.bias_fwhm=bias_fwhm
    nonuniformityCorrect_spm.inputs.save_bias_corrected=True
    """
    
    nonuniformity_correct_fsl=pe.Node(fsl.FAST(),name='nonuniformity_correct_fsl')       
    nonuniformity_correct_fsl.inputs.img_type=2 #1 for t1, 2 for t2
    nonuniformity_correct_fsl.inputs.bias_iters=5 #higher for larger nonuniformity
    nonuniformity_correct_fsl.inputs.bias_lowpass=20 #spm uses 30
    nonuniformity_correct_fsl.inputs.number_classes=3 #spm uses 5
    nonuniformity_correct_fsl.inputs.output_biasfield=True
    nonuniformity_correct_fsl.inputs.output_biascorrected=True
    # nonuniformity_correct_fsl.mem_gb = 10
    
    brain_extract=pe.Node(fsl.BET(),name='brain_extract')
    brain_extract.inputs.frac=0.2
    brain_extract.inputs.mask=True
    brain_extract.inputs.robust=True
    
    freq_est=pe.Node(EstimateFrequncyFromWrappedPhase(),'freq_est')
    freq_est.inputs.truncate_echo = 0
    freq_est.inputs.freq_filename = "freq_est.nii.gz"
    # freq_est.interface.mem_gb = 4
    
    R2Star=pe.Node(CalcR2Star_cmd(),'R2Star')
    R2Star.inputs.R2star='R2star.nii.gz'
    R2Star.inputs.neg_mask='negMask.nii.gz'
    R2Star.inputs.nan_mask='nanMask.nii.gz'
    #R2Star.interface.estimated_memory_gb = 5
    
    trim_mask=pe.Node(TrimMaskUsingReliability(),name='trim_mask')
    trim_mask.inputs.erosion_sz = 15.0 #in mm
    trim_mask.inputs.threshold = 1000000.0
    trim_mask.inputs.trimmed_mask_filename = "trim_mask.nii.gz"
    trim_mask.inputs.reliability_filename="unreliableMap.nii.gz"
    trim_mask.interface.mem_gb = 25

    R2star_reorient=pe.Node(fsl.Reorient2Std(),name='R2star_reorient')
    R2star_fit_reorient=pe.Node(fsl.Reorient2Std(),name='R2star_fit_reorient')
    R2star_neg_mask_reorient=pe.Node(fsl.Reorient2Std(),name='R2star_neg_mask_reorient')

    ### PIPELINE CONNECTION
    wf=pe.Workflow(name="SS_R2only")
    wf.config['execution']['remove_unnecessary_outputs']=False #useful for debugging
    wf.connect([
            (inputspec, avg_and_freq_estimate_weights, [('mag_images', 'mag')]),
            (inputspec, susc_phase_preprocess, [('phase_images', 'infiles')]),
            (avg_and_freq_estimate_weights, nonuniformity_correct_fsl, [('avg_out_filename', 'in_files')]),        
            (nonuniformity_correct_fsl, brain_extract, [('restored_image', 'in_file')]),
            (susc_phase_preprocess, freq_est, [('outfiles', 'phase')]),
            (inputspec, freq_est, [(('phase_images', niftitojson), 'json')]),
            (brain_extract, freq_est, [('mask_file', 'mask')]),
            # (brain_extract, trim_mask, [('mask_file', 'mask')]),
            (avg_and_freq_estimate_weights, freq_est, [('weight_out_filename', 'weight')]),            
            # (freq_est, trim_mask, [('freq_filename', 'phase')]),
            (inputspec, R2Star, [('mag_images', 'mag')]),
            (susc_phase_preprocess, R2Star, [('outfiles', 'phase')]),
            (freq_est, R2Star, [('freq_filename','freq_loc')]),            
            (brain_extract, R2Star, [('mask_file','mask')]),
            (inputspec, R2Star, [(('mag_images', niftitojson), 'json')]),
            (R2Star, R2star_reorient, [('R2star', 'in_file')]),
            (R2Star, R2star_fit_reorient, [('R2star_fit', 'in_file')]),
            (R2Star, R2star_neg_mask_reorient, [('neg_mask', 'in_file')]),
            ])
    return wf