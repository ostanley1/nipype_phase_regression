import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype.interfaces.utility as ul
import nipype_phase_regression.interfaces as pr
import nipype.interfaces.io as nio

def formatmerge(in_file, out_file_list):
    from nipype.utils.filemanip import split_filename
    import numpy as np
    import nibabel as nb
    import os
    _,base,_ = split_filename(in_file)
    out_file = os.path.abspath(base + '_mcf.nii.gz')
    img = nb.load(in_file).get_data()
    out_img = np.zeros_like(img)
    for i in range(len(out_file_list)):
        out_img[:,:,0,i]=nb.load(out_file_list[i]).get_data()
    new_img = nb.Nifti1Image(out_img, nb.load(in_file).affine, nb.load(in_file).header)
    nb.save(new_img, out_file)
    return out_file

def create_mcflirt_phase_wf():
    workflow = pe.Workflow(name="mcflirtphase")
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    inputspec = pe.Node(ul.IdentityInterface(fields=['input_phase',
                                                       'input_mag',
                                                       'mat_dir']),
                        name='inputspec')

    splitmag = pe.MapNode(interface=fsl.Split(), name="splitmag", iterfield=['in_file'])
    splitmag.inputs.dimension='t'

    splitphase = pe.MapNode(interface=fsl.Split(), name="splitphase", iterfield=['in_file'])
    splitphase.inputs.dimension='t'

    mcflirtimage = pe.MapNode(interface=pr.McflirtPhase(), name="mcflirtimage", iterfield=['in_file_list', 'reference_list', 'mat_list'])

    mergephase = pe.MapNode(interface=ul.Function(function=formatmerge, input_names=["in_file", "out_file_list"],
                             output_names=["out_file"]), name="mergephase", iterfield=["in_file", "out_file_list"])

    outputspec = pe.MapNode(ul.IdentityInterface(fields=['out_file']),
                         name='outputspec', iterfield=['out_file'])

    workflow.connect([(inputspec, splitphase, [('input_phase', 'in_file')]),
                      (inputspec, splitmag, [('input_mag', 'in_file')]),
                      (inputspec, mcflirtimage, [('mat_dir', 'mat_list')]),
                      (splitphase, mcflirtimage, [('out_files', 'in_file_list')]),
                      (splitmag, mcflirtimage, [('out_files', 'reference_list')]),
                      (mcflirtimage, mergephase, [('out_file_list','out_file_list')]),
                      (inputspec, mergephase, [('input_phase', 'in_file')]),
                      (mergephase, outputspec, [('out_file','out_file')])])


    return workflow