import nipype.pipeline.engine as pe
import nipype.interfaces.freesurfer as freesurf
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
import nipype.interfaces.utility as ul

def grab_first_output(file_list):
    return file_list[0]

def create_preproc_mag_highres_wf():
    workflow = pe.Workflow(name="preprocmag")
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    #define inputs
    inputspec = pe.Node(ul.IdentityInterface(fields=['input_mag']),
                        name='inputspec')

    # make sure image is in standard orientation
    # not used to keep minimum slices in z for BET and tcgifs
    flipper = pe.MapNode(interface=fsl.Reorient2Std(), name="flipper", iterfield=["in_file"])

    # need to perform at least 4mm smoothing to get good motion correction
    susan = pe.MapNode(interface=fsl.SUSAN(), name="susan", iterfield=["in_file"])
    susan.inputs.use_median=0
    susan.iterables = [('brightness_threshold', [150, 175, 200]), ('fwhm', [2, 4, 6])]

    # grab the first run in the smoothed list
    grabfirst = pe.Node(
        interface=ul.Function(input_names=["file_list"], output_names=["out_file"],
                              function=grab_first_output), name="grabfirst")

    # need to grab the first volume of the 1st run as a registration point for mcflirt
    firstvol = pe.Node(interface=fsl.ExtractROI(), name="firstvol")
    firstvol.inputs.t_min=0
    firstvol.inputs.t_size=1

    # motion correct timecourse with mcflirt and save the mats and plots
    mcflirter = pe.MapNode(interface=fsl.MCFLIRT(), name="mcflirter", iterfield=["in_file"])
    mcflirter.inputs.save_mats = True
    mcflirter.inputs.save_plots = True
    mcflirter.inputs.save_rms = True
    mcflirter.inputs.cost='normmi'

    # MoCo monitors
    # need to plot rot and translations using fsl_tsplot
    # need two timecourse gif nodes one before and one after motion correction in order to check it
    tcgifbeforemc = pe.MapNode(interface=pr.TimecourseGif(), name="tcgifbeforemc", iterfield=["in_file"])
    tcgifbeforemc.inputs.downsample=20
    tcgifaftermc = pe.MapNode(interface=pr.TimecourseGif(), name="tcgifaftermc", iterfield=["in_file"])
    tcgifaftermc.inputs.downsample = 20
    plottrans = pe.MapNode(interface=fsl.PlotTimeSeries(), name="plottrans", iterfield=["in_file"])
    plottrans.inputs.title="Translations (mm)"
    plottrans.inputs.labels=["x", "y", "z"]
    plottrans.inputs.plot_start=1
    plottrans.inputs.plot_finish=3
    plotrot = pe.MapNode(interface=fsl.PlotTimeSeries(), name="plotrot", iterfield=["in_file"])
    plotrot.inputs.title="Rotations (Degrees)"
    plotrot.inputs.labels=["aboutx", "abouty", "aboutz"]
    plotrot.inputs.plot_start=4
    plotrot.inputs.plot_finish=6

    # have to apply smoothed mats to real data
    mcflirtphase_wf = create_mcflirt_phase_wf()

    # calculate mean mc'd image to BET
    meanBET = pe.MapNode(interface=fsl.maths.MeanImage(), name="meanBET", iterfield=["in_file"])

    # extract brain with fsl and save the mask
    extractor = pe.MapNode(interface=fsl.BET(), name="extractor", iterfield=["in_file"])
    extractor.inputs.mask = True
    extractor.inputs.padding = True
    extractor.inputs.frac=0.3

    # apply BET mask to data
    applyBET = pe.MapNode(interface=fsl.MultiImageMaths(), name='applyBET', iterfield=['in_file', 'operand_files'])
    applyBET.inputs.op_string='-mas %s '

    # detrend the magnitude images but leave 0 freq
    detrendmag = pe.MapNode(interface=pr.DetrendMag(),name="detrendmag", iterfield=["mag"])

    # employ fsl maths to get tSNR
    meancalc = pe.MapNode(interface=fsl.maths.MeanImage(), name="meancalc", iterfield=["in_file"])
    stdcalc = pe.MapNode(interface=fsl.maths.MathsCommand(), name="stdcalc", iterfield=["in_file"])
    stdcalc.inputs.args= '-Tstd'
    SNRcalc = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name="SNRcalc", iterfield=["in_file", "operand_files"])
    SNRcalc.inputs.op_string = '-div %s'

    # make connections for workflow
    workflow.connect(inputspec, 'input_mag', tcgifbeforemc, 'in_file')
    workflow.connect(inputspec, 'input_mag', susan, 'in_file')
    workflow.connect(susan, 'smoothed_file', grabfirst, 'file_list')
    workflow.connect(grabfirst, 'out_file', firstvol, 'in_file')
    workflow.connect(susan, 'smoothed_file', mcflirter, 'in_file')
    workflow.connect(firstvol, 'roi_file', mcflirter, 'ref_file')
    workflow.connect(mcflirter, 'par_file', plottrans, 'in_file')
    workflow.connect(mcflirter, 'par_file', plotrot, 'in_file')
    workflow.connect(inputspec, 'input_mag', mcflirtphase_wf, 'inputspec.input_mag')
    workflow.connect(inputspec, 'input_mag', mcflirtphase_wf, 'inputspec.input_phase')
    workflow.connect(mcflirter, 'mat_file', mcflirtphase_wf, 'inputspec.mat_dir')
    workflow.connect(mcflirtphase_wf, 'outputspec.out_file', meanBET, 'in_file')
    workflow.connect(meanBET, 'out_file', extractor, 'in_file')
    workflow.connect(extractor, 'mask_file', applyBET, 'operand_files')
    workflow.connect(mcflirtphase_wf, 'outputspec.out_file', detrendmag, 'mag')
    workflow.connect(detrendmag, 'detrended_mag', applyBET, 'in_file')
    workflow.connect(applyBET, 'out_file', tcgifaftermc, 'in_file')
    workflow.connect(detrendmag, 'detrended_mag', stdcalc, 'in_file')
    workflow.connect(detrendmag, 'detrended_mag', meancalc, 'in_file')
    workflow.connect(meancalc, 'out_file', SNRcalc, 'in_file')
    workflow.connect(stdcalc, 'out_file', SNRcalc, 'operand_files')

    return workflow

if __name__=="__main__":
    base_dir = processing_base_dir
    workflow=create_preproc_mag_wf()

    #apparently pycharm's interactive console takes over stdin but does not implement the close function
    #this causes the multiprocessing module to fail
    #we must create a dummy function for sys.stdin.close
    import sys
    if not hasattr(sys.stdin,'close'):
        def dummy_close():
            pass
        sys.stdin.close = dummy_close
    exec_graph=workflow.run(plugin='Linear')

    print "DONE preprocess magnitude images"
