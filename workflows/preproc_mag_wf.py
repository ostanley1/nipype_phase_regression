import nipype.pipeline.engine as pe
import nipype.interfaces.freesurfer as freesurf
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr

def create_preproc_mag_wf():
    workflow = pe.Workflow(name="preprocmag")
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    # convert dicom to nifti
    converter = pe.MapNode(interface=freesurf.MRIConvert(), name="converter", iterfield=["in_file"])

    # add combination method and Mag/Phase to intent field
    dataCleaner = pe.MapNode(interface=pr.AddStudyDescription(), name="dataCleaner", iterfield=["nii","dcm"])

    # make sure image is in standard orientation
    flipper = pe.MapNode(interface=fsl.Reorient2Std(), name="flipper", iterfield=["in_file"])

    # extract brain with fsl and save the mask
    extractor = pe.MapNode(interface=fsl.BET(), name="extractor", iterfield=["in_file"])
    extractor.inputs.mask = True
    extractor.inputs.functional = True

    # motion correct timecourse with mcflirt and save the mats and plots
    mcflirter = pe.MapNode(interface=fsl.MCFLIRT(), name="mcflirter", iterfield=["in_file"])
    mcflirter.inputs.save_mats = True
    mcflirter.inputs.save_plots = True
    mcflirter.inputs.save_rms = True

    # detrend the magnitude images but leave 0 freq
    detrendmag = pe.MapNode(interface=pr.DetrendMag(),name="detrendmag", iterfield=["mag"])

    # employ fsl maths to get tSNR
    meancalc = pe.MapNode(interface=fsl.maths.MeanImage(), name="meancalc", iterfield=["in_file"])
    stdcalc = pe.MapNode(interface=fsl.maths.MathsCommand(), name="stdcalc", iterfield=["in_file"])
    stdcalc.inputs.args= '-Tstd'
    SNRcalc = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name="SNRcalc", iterfield=["in_file", "operand_files"])
    SNRcalc.inputs.op_string = '-div %s'

    # make connections for workflow
    workflow.connect(converter, 'out_file', dataCleaner, 'nii')
    workflow.connect(dataCleaner, 'fixednii', flipper, 'in_file')
    workflow.connect(flipper, 'out_file', mcflirter, 'in_file')
    # workflow.connect(extractor, 'out_file', detrendmag, 'mag')
    workflow.connect(mcflirter, 'out_file', detrendmag, 'mag')
    workflow.connect(detrendmag, 'detrended_mag', meancalc, 'in_file')
    workflow.connect(detrendmag, 'detrended_mag', stdcalc, 'in_file')
    workflow.connect(detrendmag, 'detrended_mag', extractor, 'in_file')
    workflow.connect(meancalc, 'out_file', SNRcalc, 'in_file')
    workflow.connect(stdcalc, 'out_file', SNRcalc, 'operand_files')

    return workflow

if __name__=="__main__":
    base_dir = processing_base_dir
    workflow=create_preproc_mag_wf()

    #apparently pycharm's interactive console takes over stdin but does not implement the close function
    #this causes the multiprocessing module to fail
    #we must create a dummy function for sys.stdin.close
    import sys
    if not hasattr(sys.stdin,'close'):
        def dummy_close():
            pass
        sys.stdin.close = dummy_close
    exec_graph=workflow.run(plugin='Linear')

    #view output
    # rawimg = getoutimglocs(exec_graph, workflow.get_node("converter"), 'out_file')
    # betimg=getoutimglocs(exec_graph,workflow.get_node("extractor"),'out_file')
    # mcflirtimg = getoutimglocs(exec_graph, workflow.get_node("mcflirter"), 'out_file')
    # detrendimg = getoutimglocs(exec_graph, workflow.get_node("detrendmag"), 'detrended_mag')
    # SNRimg = getoutimglocs(exec_graph, workflow.get_node("SNRcalc"), 'out_file')
    # for i in xrange(len(rawimg)):
    #     fslview([rawimg[i],betimg[i],mcflirtimg[i],detrendimg[i],SNRimg[i]])
    print "DONE preprocess magnitude images"
