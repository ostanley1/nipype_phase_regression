import nipype.pipeline.engine as pe
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype.interfaces.utility as ul
import nipype_phase_regression.interfaces as stats

def orderInputs(mag_list, phase_list, filt_list, sim_list, blur_list, maskList):
    from nipype.utils.filemanip import split_filename
    import pprint
    out_lists = []
    print "LIST TYPES"
    print type(mag_list)
    print type(sim_list)
    # iterated by filtered result
    for i in filt_list:
        new_set = {'filt':i}
        _, base, _ = split_filename(i)
        print base[:5]
        # append original magnitude to set
        if hasattr(mag_list, "__iter__"):
            for s in mag_list:
                if base[:5] in s and 'magprocessed' in s:
                    new_set['mag']=s
        elif base[:5] in mag_list and 'magprocessed' in mag_list:
            new_set['mag']=mag_list
        # append original phase to set
        if hasattr(phase_list, "__iter__"):
            for s in phase_list:
                if base[:5] in s and 'phaseprocessed' in s:
                    new_set['phase'] = s
        elif base[:5] in phase_list and 'phaseprocessed' in phase_list:
            new_set['phase'] = phase_list
        # look for a simulated phase image
        for s in sim_list:
            if base[:-4] + 'sim' in s:
                new_set['sim']=s
        for s in blur_list:
            if base[:-4] + '_smooth' in s:
                new_set['blur'] = s
        # add a mask
        if "SVDCOMB" in i:
            mask = [ind for ind, str in enumerate(maskList) if base[:4] + 'SVD' in str]
            new_set['mask'] = maskList[mask[0]]
        elif "CCOMB" in i:
            mask = [ind for ind, str in enumerate(maskList) if base[:4] + 'SoS' in str]
            new_set['mask'] = maskList[mask[0]]
        out_lists.append(new_set)
    pprint.pprint(out_lists)
    return out_lists

def grabTimecourses(in_dict_list):
    import pprint
    plot_list = []
    mask_list = []
    voxel_list = []
    for in_dict in in_dict_list:
        plot_list = plot_list + [in_dict['mag'], in_dict['filt'], in_dict['sim']]
        mask_list = mask_list + [in_dict['mask'], in_dict['mask'], in_dict['mask']]
        print in_dict['mag']
        if 'SVDCOMB' in in_dict['mag']:
            voxel_list = voxel_list + [[43, 24, 5], [43, 24, 5], [43, 24, 5]]
        if 'CCOMB' in in_dict['mag']:
            voxel_list = voxel_list + [[55, 24, 5], [55, 24, 5], [55, 24, 5]]
        else:
            print "ERROR: NOT SVD OR CONVENTIONAL COMB"
    return plot_list, mask_list, voxel_list

def orderTimecourses(in_list):
    import pprint
    pprint.pprint(in_list)
    tc_list = []
    label_list = []
    voxel_list = []
    for s in xrange(len(in_list)/3):
        tc_list.append([in_list[3*s+2], in_list[3*s+1], in_list[3*s]])
        label_list.append(['sim', 'filt', 'mag'])
    pprint.pprint(tc_list)
    pprint.pprint(label_list)
    return tc_list, label_list

def create_plot_result_wf():
    # Initialise main workflow
    workflow = pe.Workflow(name="plot_result")
    workflow.config['execution']['remove_unnecessary_outputs'] = False

    # grab magnitudes and phases for now only Run1 masks exist so only use run 1
    grabmag = pe.Node(nio.DataGrabber(), name="grabmag")
    grabmag.inputs.template = 'preprocmag/ds/mag/Run1*processed.nii.gz'
    grabmag.inputs.sort_filelist = False

    grabphase = pe.Node(nio.DataGrabber(), name="grabphase")
    grabphase.inputs.template = 'preprocphase/ds/phase/Run1*processed.nii.gz'
    grabphase.inputs.sort_filelist = False

    grabsim = pe.Node(nio.DataGrabber(), name="grabsim")
    grabsim.inputs.template = 'apply_phase/ds/*/Run1*sim.nii.gz'
    grabsim.inputs.sort_filelist = False

    grabfilt = pe.Node(nio.DataGrabber(), name="grabfilt")
    grabfilt.inputs.template = 'apply_phase/ds/*/Run1*_*gfilt.nii.gz'
    grabfilt.inputs.sort_filelist = False

    grabconvolve = pe.Node(nio.DataGrabber(), name="grabconvolve")
    grabconvolve.inputs.template = 'apply_phase/ds/*/Run1*convolve.nii.gz'
    grabconvolve.inputs.sort_filelist = False

    grabcrosscorr = pe.Node(nio.DataGrabber(), name="grabcrosscorr")
    grabcrosscorr.inputs.template = 'apply_phase/ds/*/Run1*crosscorr.nii.gz'
    grabcrosscorr.inputs.sort_filelist = False

    mergeblur =
    mergepc = pe.Node(interface=ul.Merge(6), name="mergepc")

    # I made venous masks of run 1 for both SVD and CCOMB, use grabber to find
    grabmask = pe.Node(nio.DataGrabber(), name="grabmask")
    grabmask.inputs.template = 'Run1*veins_masksingle.nii.gz'
    grabmask.inputs.sort_filelist = True

    # Make a function which will group mag, filt and sim timecourses together
    orderinputs = pe.Node(interface=ul.Function(input_names=["mag_list", "phase_list", "sim_list", "filt_list", "blur_list", "maskList"], output_names=["out_dict"], function=orderInputs), name="orderinputs")


    # Calculate percent change maps as in Stimulate
    percentchange = pe.MapNode(interface=stats.percentChange(), name="percentchange", iterfield=["func"])
    percentchange.inputs.blockLength = 25
    percentchange.inputs.blockPattern = [0,1,0,1,0,1,0]

    # plot the individual voxels in the mask
    plotfits = pe.MapNode(interface=stats.plotFits(), name="plotfits", iterfield=["in_dict"])

    # Make a function which will pass relevant image sets to meants
    grabtimecourses = pe.Node(interface=ul.Function(input_names=["in_dict_list"], output_names=["plot_list", "mask_list", "voxel_list"],function=grabTimecourses), name="grabtimecourses")

    # Use fsl maths to ensure all images are demeaned
    meancalc = pe.MapNode(interface=fsl.maths.MeanImage(), name="meancalc", iterfield=["in_file"])
    zeroimage = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name="zeroimage", iterfield=["in_file", "operand_files"])
    zeroimage.inputs.op_string = "-sub %s"
    percentimage = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name="percentimage",
                           iterfield=["in_file", "operand_files"])
    percentimage.inputs.op_string = "-div %s -Tstd"

    # Get the output timeseries
    meants = pe.MapNode(interface=stats.meanTimeCourse(), name="meants", iterfield=["in_file","mask"])
    extractvox = pe.MapNode(interface=stats.meanTimeCourse(), name="extractvox", iterfield=["in_file", "coords"])

    # Make a function which will group mag, filt and sim timecourses together
    ordertimecourses = pe.Node(interface=ul.Function(input_names=["in_list"], output_names=["tc_list", "label_list"],function=orderTimecourses), name="ordertimecourses")
    ordertimecourses2 = pe.Node(interface=ul.Function(input_names=["in_list"], output_names=["tc_list", "label_list"],function=orderTimecourses), name="ordertimecourses2")

    # Plot the output timeseries
    plotts = pe.MapNode(interface=fsl.PlotTimeSeries(), name="plotts", iterfield=["in_file", "labels"])
    plotts2 = pe.MapNode(interface=fsl.PlotTimeSeries(), name="plotts2", iterfield=["in_file", "labels"])

    # Plot the R^2 values
    # plotvoxels = pe.MapNode(interface=stats.correlationVsPercentChange(), name="plotvoxels", iterfield=["in_dict"])

    # Sink the data
    ds = pe.Node(interface=nio.DataSink(), name="ds")
    ds.inputs.regexp_substitutions = [('_[A-z]+[0-9]/', '/'),
                                      ('_[A-z]+[0-9][0-9]/', '/')] # removes mapnode folders so every run is in one folder

    # Run workflow

    # grab data and collate into dictionaries
    workflow.connect(grabmag, "outfiles", orderinputs, "mag_list")
    workflow.connect(grabphase, "outfiles", orderinputs, "phase_list")
    workflow.connect(grabsim, "outfiles", orderinputs, "sim_list")
    workflow.connect(grabfilt, "outfiles",  orderinputs, "filt_list")
    workflow.connect(grabmask, "outfiles", orderinputs,"maskList")

    # merge the data that will be blurred
    workflow.connect(grabmag, "outfiles", mergeblur, "in1")
    workflow.connect(grabsim, "outfiles", mergeblur, "in2")
    workflow.connect(grabfilt, "outfiles", mergeblur, "in3")
    workflow.connect(grabconvolve, "outfiles", mergeblur, "in4")
    workflow.connect(grabcrosscorr, "outfiles", mergeblur, "in5")

    # blur the simulated magnitudes
    workflow.connect(mergeblur, "out", blur, "in_file")
    workflow.connect(blur, "smoothed_file", orderinputs, "blur_list")

    workflow.connect(grabmag, "outfiles", mergepc, "in1")
    workflow.connect(grabsim, "outfiles", mergepc, "in2")
    workflow.connect(grabfilt, "outfiles",  mergepc, "in3")
    workflow.connect(grabconvolve, "outfiles", mergepc, "in4")
    workflow.connect(grabcrosscorr, "outfiles",  mergepc, "in5")
    workflow.connect(blur, "smoothed_file", mergepc, "in6")

    # calculate percent change
    workflow.connect(mergepc, "out", percentchange, "func")

    # plot time series over mask
    workflow.connect(orderinputs, "out_dict", grabtimecourses, "in_dict_list")
    workflow.connect(grabtimecourses, "plot_list", meancalc, "in_file")
    workflow.connect(grabtimecourses, "plot_list", zeroimage, "in_file")
    # workflow.connect(zeroimage, "out_file", percentimage, "in_file")
    workflow.connect(meancalc, "out_file", zeroimage, "operand_files")
    # workflow.connect(grabtimecourses, "plot_list", percentimage, "operand_files")
    # workflow.connect(percentimage, "out_file", meants, "in_file")
    workflow.connect(zeroimage, "out_file", meants, "in_file")
    workflow.connect(grabtimecourses, "mask_list", meants, "mask")
    workflow.connect(meants, "out_file", ordertimecourses, "in_list")
    workflow.connect(ordertimecourses, "label_list", plotts, "labels")
    workflow.connect(ordertimecourses, "tc_list", plotts, "in_file")

    # plot time series over single voxel
    workflow.connect(zeroimage, "out_file", extractvox, "in_file")
    workflow.connect(grabtimecourses, "voxel_list", extractvox, "coords")
    workflow.connect(extractvox, "out_file", ordertimecourses2, "in_list")
    workflow.connect(ordertimecourses2, "label_list", plotts2, "labels")
    workflow.connect(ordertimecourses2, "tc_list", plotts2, "in_file")

    # plot correlation vs percent change
    # workflow.connect(percentchange, "out_dict", plotvoxels, "in_dict")

    # plot voxel fits for masked voxels
    workflow.connect(orderinputs, "out_dict", plotfits, "in_dict")

    # save outputs
    workflow.connect(plotts, "out_file", ds, "tc")
    workflow.connect(plotts2, "out_file", ds, "tcvox")
    workflow.connect(blur, "smoothed_file", ds, "blur")
    workflow.connect(percentchange, "pc_func", ds, "pcmaps")
    workflow.connect(plotvoxels, "plot", ds, "r2vspc")

    workflow.write_graph(graph2use='flat', format='png', simple_form=False)
    return workflow

if __name__=="__main__":

    workflow=create_plot_result_wf()

    #apparently pycharm's interactive console takes over stdin but does not implement the close function
    #this causes the multiprocessing module to fail
    #we must create a dummy function for sys.stdin.close
    import sys
    if not hasattr(sys.stdin,'close'):
        def dummy_close():
            pass
        sys.stdin.close = dummy_close
    exec_graph = workflow.run(plugin='Linear')
    # workflow.config['execution']['plugin'] = "MultiProc"
    # exec_graph = workflow.run(plugin='SGEGraph', plugin_args={
    #     'qsub_args': '-q nipype_medium -N filtering -e /cfmm/data/ostanle2 -o /cfmm/data/ostanle2 -b Y',
    #     'dont_resubmit_completed_jobs': True})

    print "DONE plotting results"
