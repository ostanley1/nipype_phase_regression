nipype_phase_regression
Author: Olivia Stanley

This python module is an add on to nipype enabling processing of phase data.

It has three sections:

    1) interfaces - holds nipype nodes (python classes) for various phase applications
    2) workflows - generalized workflows connecting various nodes to accomplish standard tasks
    3) pipelines - specific code to run various data through the pipeline (kept as legacy for each case ideally with commit id)

Rules for the project:

    Each interface should do one thing but will be grouped by concept
    Pipelines should have a commit id for when they were last modified (shows when they were used in the project)
    Workflows should be short and have a specific workflow goal (pipelines govern the whole analysis)
    PEP8 should be followed wherever possible
    Paralellization should be used for speed but not always
    Homemade fundemental functions will be kept in core.py