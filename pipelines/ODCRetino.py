from __future__ import print_function
from __future__ import division
from builtins import str
from builtins import range

import os                                    # system functions

import nipype.interfaces.io as io           # Data i/o
from nipype.interfaces import fsl, freesurfer, afni          # fsl
import nipype.interfaces.utility as ul     # utility
import nipype.pipeline.engine as pe          # pypeline engine
import nipype.algorithms.modelgen as model   # model generation
import nipype_phase_regression.interfaces as pr
from nipype.interfaces.base import Bunch

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def pickthird(files):
    if isinstance(files, list):
        return files[2]
    else:
        return files

def grabODCruns(files):
    files.pop(4)
    files.pop(4)
    return files

def getmiddlevolume(func):
    from nibabel import load
    funcfile = func
    if isinstance(func, list):
        funcfile = func[0]
    _, _, _, timepoints = load(funcfile).shape
    return int(timepoints / 2) - 1

def getthreshop(thresh):
    return '-thr %.10f -Tmin -bin' % (0.1 * thresh[0][1])

def getinormscale(medianvals):
    return ['-mul %.10f' % (10000. / val) for val in medianvals]

def sort_copes(files):
    numelements = len(files[0])
    outfiles = []
    for i in range(numelements):
        outfiles.insert(i, [])
        for j, elements in enumerate(files):
            outfiles[i].append(elements[i])
    return outfiles

def num_copes(files):
    return len(files)

subject_list = ['001']

infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

info=dict(mag = [['subject_id']],
          struct = [['subject_id']],
          fmapmag = [['subject_id']],
          fmapdphase = [['subject_id']],
          # fmapdphasewh = [['subject_id']],
          # fmapmagwh = [['subject_id']],
          epi800 = [['subject_id']]
          )

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id'],
                                              outfields=['mag',
                                                         'struct',
                                                         'fmapmag',
                                                         'fmapdphase',
                                                         # 'fmapmagwh',
                                                         # 'fmapdphasewh',
                                                         'epi800'
                                                         ]),
                     name='datasource')
datasource.inputs.base_directory = '/cfmm/data/ostanle2/Ongoing_Work/ODC/ODC3Ret2/orig/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(mag='Run*ODCRetino%s*.nii.gz',
                                        struct='ODCRetino%smp2rage.nii.gz',
                                        fmapmag='gremagODCRetino%s.nii.gz',
                                        fmapdphase='grephaseODCRetino%s.nii.gz',
                                        # fmapmagwh='Retino%sfieldmapwh_mag.nii.gz',
                                        # fmapdphasewh='Retino%sfieldmapwh_dphase.nii.gz',
                                        epi800='ODCRetino%s800isofunc.nii.gz'
                                        )
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True

# convert image to float
img2float = pe.MapNode(interface=fsl.ImageMaths(out_data_type='float', op_string='', suffix='_dtype'),
                       iterfield=['in_file'],
                       name='img2float')

# motion correct each run
volreg = pe.MapNode(interface = afni.Volreg(), name='volreg', iterfield='in_file')
volreg.inputs.outputtype='NIFTI_GZ'

# register each run to first volume of first run

# extract the middle volume of the first run
extract_ref = pe.Node(interface=fsl.ExtractROI(t_size=1), name='extract_ref')

# registration
align2first = pe.MapNode(interface=fsl.FLIRT(dof=6), name='align2first', iterfield=['in_file'])

# apply first volume registration
applyalign = pe.MapNode(interface=fsl.ApplyXfm(), name='applyalign', iterfield=['in_file', 'in_matrix_file'])
applyalign.inputs.apply_xfm=True

# get the mean functional of run 1
meanfunc = pe.Node(interface=fsl.ImageMaths(op_string='-Tmean',
                                            suffix='_mean'),
                   name='meanfunc')

# extract brain with fsl and save the mask
extractor = pe.Node(interface=fsl.BET(), name="extractor")
extractor.inputs.mask = True
extractor.inputs.padding = True
extractor.inputs.frac = 0.3

# apply the mask to all runs
maskfunc = pe.MapNode(interface=fsl.ImageMaths(suffix='_bet',
                                               op_string='-mas'),
                      iterfield=['in_file'],
                      name='maskfunc')

# Get 2 and 98th percentiles
getthresh = pe.MapNode(interface=fsl.ImageStats(op_string='-p 2 -p 98'),
                       iterfield=['in_file'],
                       name='getthreshold')

# Threshold the first run of the functional data at 10% of the 98th percentile
threshold = pe.Node(interface=fsl.ImageMaths(out_data_type='char',
                                             suffix='_thresh'),
                    name='threshold')

# to use the mask on the retinotopy and ODC runs it needs to be one volume
pickfirstimage = pe.Node(interface=fsl.ExtractROI(), name='pickfirstimage')
pickfirstimage.inputs.t_min=0
pickfirstimage.inputs.t_size=1

# get median value using the mask
medianval = pe.MapNode(interface=fsl.ImageStats(op_string='-k %s -p 50'),
                       iterfield=['in_file'],
                       name='medianval')

# dilate the mask
dilatemask = pe.Node(interface=fsl.ImageMaths(suffix='_dil',
                                              op_string='-dilF'),
                     name='dilatemask')

# mask the data with dilated mask
maskfunc2 = pe.MapNode(interface=fsl.ImageMaths(suffix='_mask',
                                                op_string='-mas'),
                       iterfield=['in_file'],
                       name='maskfunc2')

# get the mean from each run
meanfunc2 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc2')

# merge the mean and median
mergenode = pe.Node(interface=ul.Merge(2, axis='hstack'),
                    name='merge')

# scale the run to have a median of 10000
intnorm = pe.MapNode(interface=fsl.ImageMaths(suffix='_intnorm'),
                     iterfield=['in_file', 'op_string'],
                     name='intnorm')

# get the mean from each run
meanfunc3 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc3')

# highpass the data
highpass = pe.MapNode(interface=fsl.ImageMaths(suffix='_tempfilt'),
                      iterfield=['in_file','in_file2'],
                      name='highpass')
hpcutoff = 100
TR = 2.
highpass.inputs.suffix = '_hpf'
highpass.inputs.op_string = '-bptf %d -1 -add ' % (hpcutoff / TR)

outputnode = pe.Node(ul.IdentityInterface(fields=['highpass', 'meanfunc']), name='outputnode')

preproc = pe.Workflow(name='preproc')
preproc.connect(img2float, 'out_file', volreg, 'in_file')
preproc.connect(volreg, ('out_file', pickfirst), extract_ref, 'in_file')
preproc.connect(volreg, 'out_file', align2first, 'in_file')
preproc.connect(extract_ref, 'roi_file', align2first, 'reference')
preproc.connect(extract_ref, 'roi_file', applyalign, 'reference')
preproc.connect(volreg, 'out_file', applyalign, 'in_file')
preproc.connect(align2first, 'out_matrix_file', applyalign, 'in_matrix_file')
preproc.connect(align2first, ('out_file', pickfirst), meanfunc, 'in_file')
preproc.connect(meanfunc, 'out_file', extractor, 'in_file')
preproc.connect(applyalign, 'out_file', maskfunc, 'in_file')
preproc.connect(extractor, 'mask_file', maskfunc, 'in_file2')
preproc.connect(maskfunc, 'out_file', getthresh, 'in_file')
preproc.connect(maskfunc, ('out_file', pickfirst), threshold, 'in_file')
preproc.connect(applyalign, 'out_file', medianval, 'in_file')
preproc.connect(threshold, 'out_file', pickfirstimage, 'in_file'),
preproc.connect(pickfirstimage, 'roi_file', medianval, 'mask_file')
preproc.connect(threshold, 'out_file', dilatemask, 'in_file')
preproc.connect(applyalign, 'out_file', maskfunc2, 'in_file')
preproc.connect(dilatemask, 'out_file', maskfunc2, 'in_file2')
preproc.connect(maskfunc2, 'out_file', meanfunc2, 'in_file')
preproc.connect(meanfunc2, 'out_file', mergenode, 'in1')
preproc.connect(medianval, 'out_stat', mergenode, 'in2')
preproc.connect(maskfunc2, 'out_file', intnorm, 'in_file')
preproc.connect(medianval, ('out_stat', getinormscale), intnorm, 'op_string')
preproc.connect(intnorm, 'out_file', meanfunc3, 'in_file')
preproc.connect(intnorm, 'out_file', highpass, 'in_file')
preproc.connect(meanfunc3, 'out_file', highpass, 'in_file2')
preproc.connect(highpass, 'out_file', outputnode, 'highpass')
preproc.connect(meanfunc, 'out_file', outputnode, 'out_file')

inputnode = pe.Node(ul.IdentityInterface(fields=[ 'struct',
                                                   'fmapmag',
                                                   'fmapdphase',
                                                   # 'fmapmagwh',
                                                   # 'fmapdphasewh',
                                                   'epi800',
#                                                    'epi600'
 ]), name='inputnode')

reorifmop = pe.Node(interface=fsl.Reorient2Std(), name='reorifmop')
reorifmdpop = pe.Node(interface=fsl.Reorient2Std(), name='reorifmdpop')
reoristruct = pe.Node(interface=fsl.Reorient2Std(), name='reoristruct')
reoriepi = pe.Node(interface=fsl.Reorient2Std(), name='reoriepi')
reoribigepi = pe.Node(interface=fsl.Reorient2Std(), name='reoribigepi')

betfmop = pe.Node(interface=fsl.BET(), name='betfmop')
betstruct = pe.Node(interface=fsl.BET(frac=0.3, vertical_gradient=-0.3), name='betstruct')

prepfmop = pe.Node(interface=fsl.PrepareFieldmap(), name='prepfmop')
prepfmop.inputs.delta_TE = 1.02

meanrefepi = pe.Node(interface=fsl.ImageMaths(), name='meanrefepi')
meanrefepi.inputs.op_string='-Tmean'

trimrefepi = pe.Node(interface=fsl.ExtractROI(x_min=0, x_size=-1, y_min=0, y_size=135, z_min=0, z_size=-1), name='trimrefepi')

regbig2smallepi = pe.Node(interface=fsl.FLIRT(dof=6), name='regbig2smallepi')

epiregnm = pe.Node(interface=fsl.EpiReg(), name='EpiRegnm')

concatxfm = pe.Node(interface=fsl.ConvertXFM(), name='concatxfm')
concatxfm.inputs.concat_xfm=True

faststruct = pe.Node(interface=fsl.FAST(), name='faststruct')

wmseg = pe.Node(interface=fsl.ImageMaths(), name='wmseg')
wmseg.inputs.op_string='-thr 0.5 -bin'

register = pe.Workflow(name='register')
register.connect([(inputnode, reorifmop, [('fmapmag', 'in_file')]),
                  (inputnode, reorifmdpop, [('fmapdphase', 'in_file')]),
                  (inputnode, reoristruct, [('struct', 'in_file')]),
                  (inputnode, reoribigepi, [('epi800', 'in_file')]),
                  (reorifmdpop, prepfmop, [('out_file', 'in_phase')]),
                  (reorifmop, betfmop, [('out_file', 'in_file')]),
                  (reoristruct, betstruct, [('out_file', 'in_file')]),
                  (reoribigepi, meanrefepi, [('out_file', 'in_file')]),
                  (meanrefepi, trimrefepi, [('out_file', 'in_file')]),
                  (trimrefepi, epiregnm, [('roi_file', 'epi')]),
                  (betfmop, prepfmop, [('out_file', 'in_magnitude')]),
                  (betstruct, faststruct, [('out_file', 'in_files')]),
                  (faststruct, wmseg, [(('partial_volume_files', pickthird),'in_file')]),
                  (wmseg, epiregnm, [('out_file', 'wmseg')]),
                  (reoristruct, epiregnm, [('out_file', 't1_head')]),
                  (betstruct, epiregnm, [('out_file', 't1_brain')]),
                  (reoriepi, regbig2smallepi, [('out_file', 'in_file')]),
                  (reoribigepi, regbig2smallepi, [('out_file', 'reference')]),
                  (regbig2smallepi, concatxfm, [('out_matrix_file', 'in_file')]),
                  (epiregnm, concatxfm, [('epi2str_mat', 'in_file2')])
                  ])

# First Level Workflow
hpcutoff = 10.
TR = 2.

onsetsL = list([32., 128., 224., 320., 416.])
onsetsR = list([80., 176., 272., 368.])
exp_info = [Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[16] * 5, [16] * 4]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[16] * 5, [16] * 4]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[16] * 5, [16] * 4]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[16] * 5, [16] * 4]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[16] * 5, [16] * 4]),]

cont1 = ['Left>Baseline', 'T', ['Left', 'Right'], [1, 0]]
cont2 = ['Right>Baseline', 'T', ['Left', 'Right'], [0, 1]]
cont3 = ['Right>Left', 'T', ['Left', 'Right'], [-1, 1]]
cont4 = ['Task>Baseline', 'T', ['Left', 'Right'], [0.5,0.5]]
contrasts = [cont1, cont2, cont3, cont4]

modelfit = pe.Workflow(name='modelfit')

modelspec = pe.Node(interface=model.SpecifyModel(), name="modelspec")
modelspec.inputs.high_pass_filter_cutoff=hpcutoff
modelspec.inputs.time_repetition=TR
modelspec.inputs.input_units='secs'
modelspec.inputs.subject_info=exp_info

level1design = pe.Node(interface=fsl.Level1Design(), name="level1design")
level1design.inputs.interscan_interval=TR
level1design.inputs.bases = {'dgamma': {'derivs': False}}
level1design.inputs.model_serial_correlations = True
level1design.inputs.contrasts = [cont1, cont2, cont3, cont4]

modelgen = pe.MapNode(interface=fsl.FEATModel(), name='modelgen',
                      iterfield=['fsf_file', 'ev_files'])

modelestimate = pe.MapNode(interface=fsl.FILMGLS(smooth_autocorr=True,
                                                 mask_size=5,
                                                 threshold=1000),
                           name='modelestimate',
                           iterfield=['design_file', 'in_file', 'tcon_file'])

conestimate = pe.MapNode(interface=fsl.ContrastMgr(), name='conestimate',
                         iterfield=['tcon_file', 'param_estimates',
                                    'sigmasquareds', 'dof_file',
                                    ])

modelfit.connect([
    (modelspec, level1design, [('session_info', 'session_info')]),
    (level1design, modelgen, [('fsf_files', 'fsf_file'),
                              ('ev_files', 'ev_files')]),
    (modelgen, modelestimate, [('design_file', 'design_file'),
                               ('con_file', 'tcon_file')]),
])

fixed_fx = pe.Workflow(name='fixedfx')
copemerge = pe.MapNode(interface=fsl.Merge(dimension='t'),
                       iterfield=['in_files'],
                       name="copemerge")

varcopemerge = pe.MapNode(interface=fsl.Merge(dimension='t'),
                          iterfield=['in_files'],
                          name="varcopemerge")
level2model = pe.Node(interface=fsl.L2Model(),
                      name='l2model')
flameo = pe.MapNode(interface=fsl.FLAMEO(run_mode='fe'), name="flameo",
                    iterfield=['cope_file', 'var_cope_file'])

fixed_fx.connect([(copemerge, flameo, [('merged_file', 'cope_file')]),
                  (varcopemerge, flameo, [('merged_file', 'var_cope_file')]),
                  (level2model, flameo, [('design_mat', 'design_file'),
                                         ('design_con', 't_con_file'),
                                         ('design_grp', 'cov_split_file')]),
                  ])

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = '/workspace/ostanle2/ODC3Ret2'

metaflow.connect([(infosource, datasource, [('subject_id', 'subject_id')]),
                  (datasource, preproc, [('mag', 'img2float.in_file'),
                                         (('mag', getmiddlevolume), 'extract_ref.t_min')]),
                  (datasource, register, [('struct', 'inputnode.struct'),
                                          ('fmapmag', 'inputnode.fmapmag'),
                                          ('fmapdphase', 'inputnode.fmapdphase'),
                                          # ('fmapmagwh', 'inputnode.fmapmagwh'),
                                          # ('fmapdphasewh', 'inputnode.fmapdphasewh'),
                                          ('epi800', 'inputnode.epi800')
                                          ]),
                  (preproc, register, [(('meanfunc.out_file', pickfirst), 'reoriepi.in_file')]),
                  (preproc, modelfit, [(('highpass.out_file', grabODCruns), 'modelspec.functional_runs'),
                                         (('highpass.out_file', grabODCruns), 'modelestimate.in_file')]),
                  (preproc, fixed_fx, [(('maskfunc2.out_file', pickfirst), 'flameo.mask_file')]),
                  (modelfit, fixed_fx, [(('modelestimate.copes', sort_copes), 'copemerge.in_files'),
                                        (('modelestimate.varcopes', sort_copes), 'varcopemerge.in_files'),
                                        (('modelestimate.copes', num_copes), 'l2model.num_copes'),
                                        ]),
                  ])

# metaflow.run(updatehash=True)
metaflow.write_graph(graph2use='colored', format='png', simple_form=False)
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 10})