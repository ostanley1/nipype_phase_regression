# last working commit id 270a7ae552697f29bcc8b8089688c013f55d4bc0
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr

basedir = '/workspace/ostanle2/SEvsGE/' #!!Working directory!!
maglist = [basedir+'gebold_mag.nii.gz'] #etc]
phaselist = [basedir+'gebold_phase.nii.gz'] #etc]

#The finished files will be in the working directory under metaflow/phaseregress/mapflow/_phaseregress#/*
# They will have a bunch of prefixes.

# Initialize Magnitude Preprocessing
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

# Add inputs
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])
preproc_mag_wf.inputs.flipper.in_file=maglist

# Specify BET threshold (used defaults so was not needed)

# Initialize Phase Preprocessing
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

# Add inputs
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])
preproc_phase_wf.inputs.flipper.in_file=phaselist

# Set conversion parameters
preproc_phase_wf.inputs.prepPhase.siemens=[True]*len(phaselist)
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# BET PHASE
betphase = pe.MapNode(interface=fsl.ApplyMask(), name='betphase', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=basedir

# Bandpass data
bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
bpftphase.inputs.highpass_sigma = 80
bpftphase.inputs.lowpass_sigma = 2

bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
bpftmag.inputs.highpass_sigma = 80
bpftmag.inputs.lowpass_sigma = 2

# Regress magnitude and phase
phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = 2.5
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb = 0.15

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = basedir

# Preprocess and motion correct
metaflow.connect([(preproc_phase_wf, betphase, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase, [('detrendmag.detrended_mag','mask_file')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('detrendmag.detrended_mag', 'inputspec.input_mag')]),
                  (preproc_phase_wf, mcflirtphase_wf, [('prepPhase.detrended_phase', 'inputspec.input_phase')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file','inputspec.mat_dir')]),
                  (mcflirtphase_wf, bpftphase, [('outputspec.out_file', 'in_file')]),
                  (preproc_mag_wf, bpftmag, [('detrendmag.detrended_mag','in_file')]),
                  ])

# Perform phase regression
metaflow.connect([(mcflirtphase_wf, phaseregress, [('outputspec.out_file', 'phase')]),
                  (preproc_mag_wf, phaseregress, [('detrendmag.detrended_mag', 'mag')]),
                  (preproc_mag_wf, phaseregress, [('detrendmag.detrended_mag', 'bpmag')]),
                  (mcflirtphase_wf, phaseregress, [('outputspec.out_file', 'bpphase')])])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)

metaflow_exec_graph = metaflow.run(plugin='MultiProc')
