import nipype_phase_regression.interfaces as pr
import nipype.pipeline.engine as pe

mag='/workspace/ostanle2/phtest/magcut.nii.gz'
phase='/workspace/ostanle2/phtest/phasecut.nii.gz'

# phaseregress = pe.MapNode(interface=pr.RestAverage(), name='meansnrtrim', iterfield=['func'])
# phaseregress.base_dir='./phtest'
# phaseregress.inputs.task=6
# phaseregress.inputs.rest=6
# phaseregress.inputs.func=mag
# phaseregress.run()

phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.base_dir='./phtest'
phaseregress.inputs.TR = 2.5
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb=0
phaseregress.inputs.mag = mag
phaseregress.inputs.bpmag = mag
phaseregress.inputs.phase = phase
phaseregress.inputs.bpphase = phase

phaseregress2 = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregress2', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress2.base_dir='./phtest'
phaseregress2.inputs.TR = 2.5
phaseregress2.inputs.sig_lb = 0.01
phaseregress2.inputs.sig_ub = 0.1
phaseregress2.inputs.noise_lb=0.15
phaseregress2.inputs.mag = mag
phaseregress2.inputs.bpmag = mag
phaseregress2.inputs.phase = phase
phaseregress2.inputs.bpphase = phase

phaseregress2.run()
