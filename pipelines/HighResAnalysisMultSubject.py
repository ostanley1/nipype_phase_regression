from nipype_phase_regression.workflows.preproc_mag_highres_wf import create_preproc_mag_highres_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr
from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                       create_fixed_effects_flow)
import nipype.algorithms.modelgen as model
from nipype.interfaces.base import Bunch
import nipype.interfaces.utility as ul
import nipype.interfaces.io as io
from nipype.interfaces import spm, fsl

basedir = '/workspace/ostanle2/ODC_Stim2'

subject_list=['20170222']
func_list=['Run1','Run2','Run3','Run4','Run5']
info=dict(func = [['subject_id', 'func_list']], struct = [['subject_id', 'mp2rage600']])
infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id', 'func_list']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list), ('func_list', func_list)]

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id', 'func_list'],
                                               outfields=['func', 'struct']),
                     name='datasource')
datasource.inputs.base_directory = basedir + '/orig/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(func='%s%sODCStimmag.nii.gz',
                                  struct='%s%s.nii.gz')
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True
datasource.inputs.subject_id=subject_list[0]

meancalc = pe.Node(interface=fsl.maths.MeanImage(), name="meancalc")

mergecalc = pe.JoinNode(interface=fsl.Merge(), name="mergecalc", joinsource='infosource', join_name='func_list')


datasink = pe.Node(io.DataSink(), name='sinker')
datasink.inputs.base_directory = '/cfmm/data/ostanle2/testsink'

l1pipeline = pe.Workflow(name="level1")
l1pipeline.base_dir = basedir
l1pipeline.connect([(infosource, datasource, [('subject_id', 'subject_id'), ('func_list', 'func_list')]),
                    (datasource, meancalc, [('func', 'in_file')]),
                    (meancalc, datasink, [('out_file', 'means')]),
                    (meancalc, mergecalc, [('out_file', 'in_files')]),
                    (mergecalc, datasink, [('merged_file', 'merged')])])


l1pipeline.run()
