# last working commit id 40912f734d20fd5fcc8f8c3a1500814eacba4257
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl

phaselist = ['/cfmm/data/ostanle2/RaviMeetingJune14/comparenoise/PhaseRS.nii.gz', '/cfmm/data/ostanle2/RaviMeetingJune14/comparenoise/rs_phase1.nii']
maglist = ['/cfmm/data/ostanle2/RaviMeetingJune14/comparenoise/MagRS.nii.gz', '/cfmm/data/ostanle2/RaviMeetingJune14/comparenoise/rs_mag1.nii']
basedir = '/cfmm/data/ostanle2/RaviMeetingJune14/comparenoise/'

preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])

preproc_mag_wf.inputs.flipper.in_file=maglist
preproc_mag_wf.inputs.extractor.frac=0.15

preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])

preproc_phase_wf.inputs.flipper.in_file=phaselist
preproc_phase_wf.inputs.prepPhase.siemens=[True, False]

# Out of workflow for customization
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# BET PHASE
betphase_wf = pe.MapNode(interface=fsl.ApplyMask(), name='betphase_wf', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=basedir

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = basedir
metaflow.connect([(preproc_phase_wf, betphase_wf, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase_wf, [('extractor.mask_file','mask_file')]),
                  (betphase_wf, mcflirtphase_wf, [('out_file','inputspec.input_phase')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file', 'inputspec.mat_dir'),
                                                     ('detrendmag.detrended_mag', 'inputspec.input_mag')]),])

metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})