from nipype.interfaces.base import BaseInterface, \
    BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
from multiprocess import Pool
import nibabel as nb
import numpy as np
import os
from ..core import calc_pool_size
import nipype.algorithms.modelgen as modelgen
import matplotlib.pyplot as plt
import cmath as c
import scipy.optimize as op

class MakeMapsInputSpec(BaseInterfaceInputSpec):
    magimages = traits.List(desc='list of magnitude files')
    phaseimages = traits.List(desc='list of phase files')


class MakeMapsOutputSpec(TraitedSpec):
    r2star = File(desc='r2star')
    omega = File(desc='omega')

class MakeMaps(BaseInterface):
    input_spec = MakeMapsInputSpec
    output_spec = MakeMapsOutputSpec

    def calcdecay(self, t, params):
        return params[0]*np.exp(-(params[1]*t))

    def calcvoxel(self, (mag, phase, t)):
        if np.count_nonzero(tc-np.mean(tc)) != 0:
            decay = c.rect(mag, phase)
            deltaw = decay[0]*np.conj(decay[1])/(t[1]-t[0])
            popt = op.curve_fit(self.calcdecay, t, mag)
            return (deltaw, popt[1])
        else:
            return 0

    def _run_interface(self, runtime):
        # read in data
        assert(len(self.inputs.magimages, self.inputs.phaseimages))
        magimages = []
        phaseimages = []
        for img in xrange(len(magimages)):
            magname = self.imput.magimages[img]
            phasename = self.input.phaseimages[img]
            magfile = nb.load(magname)
            phasefile = nb.load(phasename)
            magimages.append(magfile.get_data())
            phaseimages.append(phasefile.get_data())

        magimages = np.asarray(magimages)
        phaseimages = np.asarray(phaseimages)

        magimages = np.permute(magimages, [2,3,4,1])
        phaseimages = np.permute(phaseimages, [2, 3, 4, 1])

        magimages = magimages.reshape(-1, magimages.shape[-1])
        phaseimages = phaseimages.reshape(-1, phaseimages.shape[-1])

        # calculates the voxel by voxel map of (ave(stim)-ave(cont))/ave(cont) as is done in Stimulate
        parapool = Pool(4)
        try:
            result = parapool.map(self.calcvoxel, (magimage[xrange(magimges.shape[0]), :], phaseimage[xrange(phaseimges.shape[0]), :]))
            omega, r2star = zip(*result)
            parapool.close()
        except Exception as e:
            parapool.close()
            raise e
        _, base, _ = split_filename(fname)
        new_img = nb.Nifti1Image(np.asarray(pval).reshape(omega.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_omega.nii.gz')
        new_img = nb.Nifti1Image(np.asarray(pval).reshape(r2star.shape[:-1]), img.affine, img.header)
        nb.save(new_img, base + '_r2star.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["omega"] = os.path.abspath(base + '_omega.nii.gz')
        outputs["r2star"] = os.path.abspath(base + '_r2star.nii.gz')
        return outputs


