def trim_nifti(in_file):
    from nipype.utils.filemanip import split_filename
    _, base, _ = split_filename(in_file)
    return base

def get_niftis(layout, subject_id, type, modality, tags):
    #tags is a list with the bids tag in it
    if type==[]:
        files = [f.filename for f in layout.get(subject=subject_id, modality=modality,  extensions=['nii', 'nii.gz'])]
    else:
        files = [f.filename for f in layout.get(subject=subject_id, type=type, extensions=['nii', 'nii.gz'])]

    filtfiles = []
    for j in range(len(files)):
        inc=True
        for i in range(len(tags)):
            if tags[i] not in files[j]:
                inc = False
        if inc:
            filtfiles.append(files[j])
    if len(filtfiles)==1:
        return filtfiles[0]
    else:
        return filtfiles

def matchMagPhase(maglist, phaselist):
    import re
    newmag = []
    newphase = []
    for f in maglist:
        run = re.search("run-0\\d_", f)
        task = re.search("task-(.*)_acq", f)
        print(run.group(0), task.group(0))
        for test in phaselist:
            if run.group(0) in test and task.group(0) in test:
                newmag.append(f)
                newphase.append(test)
                phaselist.remove(test)
    return newmag, newphase

def run_pipeline(parser):
    import nipype.pipeline.engine as pe
    import nipype.interfaces.fsl as fsl
    import nipype.interfaces.io as nio
    import nipype_phase_regression.interfaces as pr
    import nipype.interfaces.utility as ul
    from nipype_phase_regression.workflows.preproc_mag_wf_SEvsGE import create_preproc_magsevsge_wf
    from nipype_phase_regression.workflows.preproc_phase_wf_SEvsGE import create_preproc_phasesevsge_wf
    from nipype_phase_regression.workflows.feat_wf import create_feat_preproc_wf
    from nipype.interfaces import afni
    import os
    import os.path as op
    from bids.layout import BIDSLayout
    from nipype import config, logging

    args = parser.parse_args()

    # Required inputs
    bids_dir = args.bids_dir
    participant = args.participant_label

    # Optional inputs
    nthreads = int(args.nthreads)
    bet_thr = float(args.bet_thr)
    radians = bool(args.radians)

    deriv_dir = op.join(op.realpath(bids_dir), "derivatives")

    # Set work & crash directories
    if args.work_dir:
        work_dir = op.realpath(args.work_dir)
        crash_dir = op.join(op.realpath(args.work_dir),"crash")
    else:
        work_dir = op.join(bids_dir, "derivatives/work")
        crash_dir = op.join(op.join(op.realpath(bids_dir),"derivatives/work"), "crash")

    if not op.exists(work_dir):
        os.makedirs(work_dir)
    if not op.exists(crash_dir):
        os.makedirs(crash_dir)

    if args.out_dir:
        out_dir = op.realpath(args.out_dir)
    else:
        out_dir = op.join(deriv_dir, 'restingstate')

    config.update_config({'logging': {'log_directory': work_dir,
                                      'log_to_file': True,
                                      },
                          'execution': {'crashdump_dir': crash_dir,
                                        'crashfile_format': 'txt',
                                        'hash_method': 'content'
                                        }})
    logging.update_logging(config)

    # BIDSDataGrabber
    layout = BIDSLayout(bids_dir)

    if args.subjects:
        subjid = [s for s in args.subjects.split(',')]
        print(subjid)
    else:
        subjid=layout.get_subjects()

    infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id']),
                         name="infosource")
    infosource.iterables = [('subject_id', subjid)]

    BIDSMagGrabber = pe.Node(ul.Function(function=get_niftis, input_names=["layout","subject_id","type","modality","tags"],
                                       output_names=["mag"]), name="BIDSMagGrabber")
    BIDSMagGrabber.inputs.type='bold'
    BIDSMagGrabber.inputs.modality=[]
    BIDSMagGrabber.inputs.layout=layout
    BIDSMagGrabber.inputs.tags=['part-mag']

    BIDSPhaseGrabber = pe.Node(ul.Function(function=get_niftis, input_names=["layout","subject_id","type","modality","tags"],
                                       output_names=["phase"]), name="BIDSPhaseGrabber")
    BIDSPhaseGrabber.inputs.type='bold'
    BIDSPhaseGrabber.inputs.modality=[]
    BIDSPhaseGrabber.inputs.layout=layout
    BIDSPhaseGrabber.inputs.tags=['part-phase']

    # need to confirm all magnitude and phase files match and the runs are from the same time
    sortmagphase = pe.Node(interface=ul.Function(['maglist', 'phaselist'], ['maglist', 'phaselist'], matchMagPhase), name='sortmagphase')

    preproc_mag_wf = create_preproc_magsevsge_wf()
    preproc_mag_wf.inputs.inputspec.frac=bet_thr

    preproc_phase_wf = create_preproc_phasesevsge_wf()
    preproc_phase_wf.inputs.inputspec.siemensbool=(radians==False)

    # modes = ['ge', 'filt', 'sim']
    # featpre={}
    # for m in modes:
    #     featpre_wf[m] = create_feat_preproc_wf()
    #     featpre_wf[m].inputs.inputspec.TR=TR
    #     featpre_wf[m].inputs.inputspec.highpass=-100
    #
    # bandpass = pe.MapNode(interface=afni.Bandpass(outputtype='NIFTI_GZ', lowpass=0.1 , highpass=0.02 ), name='bandpass', iterfield=['in_file'])
    #
    # meanfuncrest = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean'), name='meanfuncrest', iterfield=['in_file'])
    # varfuncrest = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tstd -sqr'), name='varfuncrest', iterfield=['in_file'])
    # mergefornormalize = pe.MapNode(interface=ul.Merge(2), name='mergefornormalize', iterfield=['in1', 'in2'])
    # normalize = pe.MapNode(interface=fsl.MultiImageMaths(op_string='-sub %s -div %s'), name='normalize', iterfield=['in_file', 'operand_files'])
    #
    # mergeruns = pe.Node(interface=fsl.Merge(dimension='t'), name='mergeruns')
    #
    # blur = pe.MapNode(interface=afni.Merge(outputtype='NIFTI_GZ', doall=True), name='blur', iterfield=['in_files'])
    # blur.inputs.blurfwhm=2

    restingstate =  pe.Workflow(name='restingstate')
    restingstate.base_dir=work_dir

    restingstate.connect([(infosource, BIDSMagGrabber, [('subject_id', 'subject_id')]),
                          (infosource, BIDSPhaseGrabber, [('subject_id', 'subject_id')]),
                          (BIDSMagGrabber, sortmagphase, [('mag', 'maglist')]),
                          (sortmagphase, preproc_mag_wf, [('maglist', 'inputspec.input_mag')]),
                          (BIDSPhaseGrabber, sortmagphase, [('phase', 'phaselist')]),
                          (sortmagphase, preproc_phase_wf, [('phaselist', 'inputspec.input_phase')]),
                          (preproc_mag_wf, preproc_phase_wf, [('outputspec.motion_par', 'inputspec.motion_par'),
                                                                  ('outputspec.run_txfm', 'inputspec.run_txfm'),
                                                                  ('outputspec.mask_file', 'inputspec.mask_file')]),
                        ])


    restingstate.run(plugin='MultiProc', plugin_args = {'n_procs' : 16})

if __name__=='__main__':
    from argparse import ArgumentParser, RawTextHelpFormatter
    parser = ArgumentParser(description=__doc__, formatter_class=RawTextHelpFormatter)

    # Required arguments
    g_req = parser.add_argument_group("Required arguments")
    g_req.add_argument("bids_dir", help="Directory with input dataset, "
                                        "formatted according to the BIDS "
                                        "standard")
    g_req.add_argument('participant_label', help="Participant id to perform "
                                                 "pipeline execution on (may be list)")

        # Optional arguments
    g_opt = parser.add_argument_group("Optional arguments")
    g_opt.add_argument("-s", "--subjects", dest="subjects",
                                       help="List of subjects to limit analysis to delimit with ,")
    g_opt.add_argument("-b", "--bet_thr", dest="bet_thr", default=0.3,
                                       help="User provided bet parameter, default 0.3")
    g_opt.add_argument("--radians", dest="radians", default=False,
                                       help="Data is in radians not siemens units")
    g_opt.add_argument("-w", "--work_dir", dest="work_dir",
                                           help="Work directory. Defaults to "
                                            "<bids_dir>/derivatives/scratch")
    g_opt.add_argument("-o", "--out_dir", dest="out_dir",
                                          help="Output directory. Defaults to "
                                           "<bids_dir>/derivatives/restingstate")
    g_opt.add_argument("-n", "--nthreads", dest="nthreads", default=1,
                                           help="The number of threads to use "
                                           "for pipeline execution where "
                                           "applicable.")

    run_pipeline(parser)
