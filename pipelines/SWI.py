from __future__ import print_function
from __future__ import division
from builtins import str
from builtins import range

import os                                    # system functions

import nipype.interfaces.io as io           # Data i/o
from nipype.interfaces import fsl, freesurfer          # fsl
import nipype.interfaces.utility as ul     # utility
import nipype.pipeline.engine as pe          # pypeline engine
import nipype.algorithms.modelgen as model   # model generation
from nipype.interfaces.base import Bunch

def makelist(file):
    print(type(file))
    return [file]*4

basedir = '/workspace/ostanle2/aobid'
output_dir = '/workspace/ostanle2/aobid/outputs'

subject_list = ['04','06','16','17','18','20']


info=dict(swimag=[['subject_id']],
          swipha=[['subject_id']],
          struct=[['subject_id']],)
infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id'],
                                               outfields=['swimag', 'swipha', 'struct']),
                     name='datasource')
datasource.inputs.base_directory = '/cfmm/data/ostanle2/aobid/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(swimag='sub0%s/anatomy/other/swi001_mag.nii.gz',
                                        swipha='sub0%s/anatomy/other/swi001_pha.nii.gz',
                                        struct='sub0%s/anatomy/highres001.nii.gz')
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True
datasource.inputs.subject_id=subject_list

#process SWI images

cutoutneck = pe.Node(interface=fsl.ExtractROI(), name='cutoutneck')
cutoutneck.inputs.z_min=100
cutoutneck.inputs.z_size=-1
cutoutneck.inputs.x_min=0
cutoutneck.inputs.x_size=-1
cutoutneck.inputs.y_min=0
cutoutneck.inputs.y_size=-1

T1cut2T1 = pe.Node(interface=fsl.FLIRT(dof=3), name = 'T1cut2T1')

concatxfm = pe.Node(interface=fsl.ConvertXFM(), name='concatxfm')
concatxfm.inputs.concat_xfm=True

betstruct = pe.Node(interface=fsl.BET(frac=0.3, vertical_gradient=-0.5, mask=True), name='betstruct')

betmag = pe.Node(interface=fsl.BET(frac=0.3, mask=True), name='betmag')

uwphase = pe.Node(interface=fsl.PRELUDE(), name='uwphase')
uwphase.inputs.process2d=True

hpfilt = pe.Node(interface=fsl.SpatialFilter(), name="hpfilt")
hpfilt.inputs.kernel_shape='box'
hpfilt.inputs.kernel_size=65
hpfilt.inputs.operation='meanu'

maskphase = pe.Node(interface=fsl.maths.MathsCommand(), name='maskphase')
maskphase.inputs.args='-thr 0 -bin'

scalephase = pe.Node(interface=fsl.MultiImageMaths(), name='scalephase')
scalephase.inputs.op_string='-uthr 0 -sub 3.14159 -mul -1 -div 3.14159 -add %s'

makevessels = pe.Node(interface=fsl.MultiImageMaths(), name='makevessels')
makevessels.inputs.op_string='-mul %s -mul %s -mul %s -mul %s -uthr 1000 -bin'

register = pe.Node(interface=fsl.FLIRT(dof=12), name='register')

mask2struct = pe.Node(interface=fsl.preprocess.FLIRT(), name = 'mask2struct')
mask2struct.iterables = [('apply_isoxfm', [3.0,2.0,1.4,0.8])]

swi = pe.Workflow(name='swi')
swi.base_dir = basedir
swi.connect([(infosource, datasource, [('subject_id', 'subject_id')]),
             (betmag, uwphase, [('mask_file', 'mask_file')]),
             (datasource, betmag, [('swimag', 'in_file')]),
             (datasource, uwphase, [('swimag', 'magnitude_file'),
                                    ('swipha', 'phase_file')]),
             (uwphase, hpfilt, [('unwrapped_phase_file', 'in_file')]),
             (hpfilt, maskphase, [('out_file', 'in_file')]),
             (hpfilt, scalephase, [('out_file', 'in_file')]),
             (maskphase, scalephase, [('out_file', 'operand_files')]),
             (datasource, makevessels, [('swimag', 'in_file')]),
             (scalephase, makevessels, [(('out_file', makelist), 'operand_files')]),
             (betmag, register, [('out_file', 'in_file')]),
             (datasource, cutoutneck, [('struct', 'in_file')]),
             (cutoutneck, betstruct, [('roi_file', 'in_file')]),
             (cutoutneck, T1cut2T1, [('roi_file','in_file')]),
             (datasource, T1cut2T1, [('struct','reference')]),
             (register, concatxfm, [('out_matrix_file','in_file')]),
             (T1cut2T1, concatxfm, [('out_matrix_file','in_file2')]),
             (betstruct, register, [('out_file', 'reference')]),
             (concatxfm, mask2struct, [('out_file', 'in_matrix_file')]),
             (datasource, mask2struct, [('struct', 'reference')]),
             (makevessels, mask2struct, [('out_file', 'in_file')])])

swi.write_graph(graph2use='colored', format='png', simple_form=False)
swi.run(plugin='MultiProc', plugin_args={'n_procs' : 15})