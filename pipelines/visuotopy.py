import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                       create_fixed_effects_flow)
import nipype.algorithms.modelgen as model
from nipype.interfaces.base import Bunch
import nipype.interfaces.utility as ul
import nipype.interfaces.io as io
from nipype.interfaces import spm, fsl, afni
from nipype_phase_regression.workflows.preproc_mag_wf_SEvsGE import create_preproc_magsevsge_wf
from nipype_phase_regression.workflows.feat_wf import create_feat_wf

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

# grab the data
basedir = '/workspace/ostanle2/visuotopy'
output_dir = '/cfmm/data/ostanle2/Ongoing_Work/visuotopy'
TR = 2.5
hpcutoff = 100

subject_list=['20171114']

info=dict(mag = [['subject_id']])
infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id'],
                                               outfields=['mag']),
                     name='datasource')
datasource.inputs.base_directory = basedir + '/orig/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(mag='%sGERun*Mag.nii.gz')
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True
datasource.inputs.subject_id=subject_list[0]

grabber = pe.Workflow(name='grabber')
grabber.connect(infosource, 'subject_id', datasource, 'subject_id')

# preprocess GE data
preproc_mag_wfge = create_preproc_magsevsge_wf()
preproc_mag_wfge.base_dir=basedir

# Set up experiment
onsetsA = [10, 60, 110]
onsetsB = [35, 85, 135]
exp_info = [Bunch(conditions=["CondA","CondB"], onsets=[onsetsA, onsetsB], durations=[[15] * 3, [15] * 3])] * 5

cont1 = ['TaskA>Baseline', 'T', ['CondA', 'CondB'], [1,0]]
cont2 = ['TaskB>Baseline', 'T', ['CondA', 'CondB'], [0,1]]
cont3 = ['Task>Baseline', 'T', ['CondA', 'CondB'], [0.5,0.5]]
cont4 = ['TaskA>TaskB', 'T', ['CondA', 'CondB'], [1,-1]]
contrasts = [cont1, cont2, cont3, cont4]

feat_wf = create_feat_wf()
feat_wf.inputs.inputspec.TR=2.5
feat_wf.inputs.inputspec.hpcutoff=100
feat_wf.inputs.inputspec.subject_info = exp_info
feat_wf.inputs.inputspec.contrasts = contrasts

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir = basedir
metaflow.connect([(grabber, preproc_mag_wfge, [('datasource.mag', 'inputspec.input_mag')]),
                  (preproc_mag_wfge, feat_wf, [('outputspec.proc_mag', 'inputspec.input')])
                  ])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 10})
