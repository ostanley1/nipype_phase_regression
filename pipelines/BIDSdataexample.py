import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr
from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                       create_fixed_effects_flow)
import nipype.algorithms.modelgen as model
from nipype.interfaces.base import Bunch
import nipype.interfaces.utility as ul
import nipype.interfaces.io as io
from nipype.interfaces import spm, fsl, afni
from nipype_phase_regression.workflows.preproc_mag_wf_SEvsGE import create_preproc_magsevsge_wf
from nipype_phase_regression.workflows.preproc_phase_wf_SEvsGE import create_preproc_phasesevsge_wf
from nipype_phase_regression.workflows.feat_wf import create_feat_wf
from nipype_phase_regression.workflows.make_surf_wf import create_make_surf_wf
from bids.grabbids import BIDSLayout

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def get_niftis(subject_id, data_dir, type, tags):
    #tags is a list with the bids tag in it
    from bids.grabbids import BIDSLayout

    layout = BIDSLayout(data_dir)

    files = [f.filename for f in layout.get(subject=subject_id, type=type,  extensions=['nii', 'nii.gz'])]

    filtfiles = []
    for j in xrange(len(files)):
        inc=True
        for i in xrange(len(tags)):
            if tags[i] not in files[j]:
                inc = False
        if inc:
            filtfiles.append(files[j])

    print(filtfiles)
    return filtfiles

# grab the data
basedir = '/workspace/ostanle2/SEvsGEBIDS'
output_dir = '/cfmm/data/ostanle2/Ongoing_Work/bids/GEvSE'

BIDSDataGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id",
                                       "data_dir","type","tags"],
                                   output_names=["bold"]), name="BIDSDataGrabber", iterfield=["tags"])
BIDSDataGrabber.inputs.data_dir = output_dir
BIDSDataGrabber.inputs.subject_id='01'
BIDSDataGrabber.inputs.type='bold'
BIDSDataGrabber.inputs.tags=[['acq-ge'], ['acq-se']]

selectGE=pe.Node(interface=ul.Select(index=1),name="selectGE")
selectSE=pe.Node(interface=ul.Select(index=1),name="selectSE")

grabber = pe.Workflow(name='grabber')
grabber.connect(BIDSDataGrabber, 'bold', selectGE, 'inlist')
grabber.connect(BIDSDataGrabber, 'bold', selectSE, 'inlist')
res=grabber.run()
