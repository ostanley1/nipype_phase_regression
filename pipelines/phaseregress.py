import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr
from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                       create_fixed_effects_flow)
import nipype.algorithms.modelgen as model
from nipype.interfaces.base import Bunch
import nipype.interfaces.utility as ul
import nipype.interfaces.io as io
from nipype.interfaces import spm, fsl, afni
from nipype_phase_regression.workflows.preproc_mag_wf_SEvsGE import create_preproc_magsevsge_wf
from nipype_phase_regression.workflows.preproc_phase_wf_SEvsGE import create_preproc_phasesevsge_wf
from nipype_phase_regression.workflows.feat_wf import create_feat_wf


# grab the data
basedir = '/workspace/ostanle2/SEvsGE2/mannualPR'
magdata = ['/workspace/ostanle2/SEvsGE2/metaflow/preprocmag/_subject_id_20171004/maskfunc/mapflow/_maskfunc0/20171004GERun1Mag_dtype_volreg_flirt_bet.nii.gz',
           '/workspace/ostanle2/SEvsGE2/metaflow/preprocmag/_subject_id_20171004/maskfunc/mapflow/_maskfunc1/20171004GERun2Mag_dtype_volreg_flirt_bet.nii.gz',
           '/workspace/ostanle2/SEvsGE2/metaflow/preprocmag/_subject_id_20171004/maskfunc/mapflow/_maskfunc2/20171004GERun3Mag_dtype_volreg_flirt_bet.nii.gz',
           basedir + '/mocomag1_physio.nii.gz', basedir + '/mocomag2_physio.nii.gz', basedir + '/mocomag3_physio.nii.gz']
bpmagdata = ['/workspace/ostanle2/SEvsGE2/metaflow/_subject_id_20171004/bpftmag/mapflow/_bpftmag0/20171004GERun1Mag_dtype_volreg_flirt_bet_filt.nii.gz',
             '/workspace/ostanle2/SEvsGE2/metaflow/_subject_id_20171004/bpftmag/mapflow/_bpftmag1/20171004GERun2Mag_dtype_volreg_flirt_bet_filt.nii.gz',
             '/workspace/ostanle2/SEvsGE2/metaflow/_subject_id_20171004/bpftmag/mapflow/_bpftmag2/20171004GERun3Mag_dtype_volreg_flirt_bet_filt.nii.gz']
phasedata = [basedir+'/mocophase1.nii.gz',basedir+'/mocophase2.nii.gz',basedir+'/mocophase3.nii.gz',
             basedir+'/mocophase1_physio.nii.gz',basedir+'/mocophase2_physio.nii.gz',basedir+'/mocophase3_physio.nii.gz']
bpphasedata = [basedir+'/mocophase1_bptf.nii.gz',basedir+'/mocophase2_bptf.nii.gz',basedir+'/mocophase3_bptf.nii.gz']

phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = 2.5
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb = 0.15
phaseregress.inputs.mag = magdata
phaseregress.inputs.bpmag = magdata
phaseregress.inputs.phase = phasedata
phaseregress.inputs.bpphase = phasedata

outputspec = pe.Node(ul.IdentityInterface(fields=['filt_mag']),
                        name='outputspec')

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir = basedir
metaflow.connect(phaseregress, 'filt', outputspec, 'filt_mag')
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 6})
