# last working commit id 270a7ae552697f29bcc8b8089688c013f55d4bc0
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr

def getoutimglocs(exec_graph, node, output_name, img_num=-1):
    #build the heirarchical name for the desired node
    hierarchyName=str(node)
    while type(node)==pe.Workflow:
        node=node.get_node(output_name.split(".")[0])
        hierarchyName=hierarchyName+"."+node.name
        output_name=".".join(output_name.split(".")[1:])
    #get all nodes from the workflow
    nodelist=exec_graph.nodes()
    #get all nodes hierarchical string
    str_nodelist=np.array(nodelist,dtype=str)
    for i in range(len(str_nodelist)):
        if str_nodelist[i]==hierarchyName:
            break
    if not str_nodelist[i]==hierarchyName:
        print "getoutimglocs: Couldn't find node "+str(node)
        return
    else:
        imglocs=nodelist[i].get_output(output_name)
        return imglocs

basedir = '/workspace/ostanle2/highresanalysis/new'
phaselist = [basedir+'/raw/500isosiemensphase.nii.gz', basedir+'/raw/600isosiemensphase.nii.gz',
             basedir+'/raw/800isosiemensphase.nii.gz', basedir+'/raw/500isosvdphase.nii.gz',
             basedir+'/raw/600isosvdphase.nii.gz', basedir+'/raw/800isosvdphase.nii.gz']
maglist = [basedir+'/raw/500isosiemensmag.nii.gz', basedir+'/raw/600isosiemensmag.nii.gz',
           basedir+'/raw/800isosiemensmag.nii.gz', basedir+'/raw/500isosvdmag.nii.gz',
           basedir+'/raw/600isosvdmag.nii.gz', basedir+'/raw/800isosvdmag.nii.gz']

# Initialize Magnitude Preprocessing
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

# Add inputs
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])
preproc_mag_wf.inputs.flipper.in_file=maglist

# Specify BET threshold
preproc_mag_wf.inputs.extractor.frac=0.2
preproc_mag_wf.inputs.extractor.vertical_gradient=0
preproc_mag_wf.inputs.extractor.radius=110

# Initialize Phase Preprocessing
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

# Add inputs
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])
preproc_phase_wf.inputs.flipper.in_file=phaselist

# Set conversion parameters
preproc_phase_wf.inputs.prepPhase.siemens=[True] * 6
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# BET PHASE
betphase = pe.MapNode(interface=fsl.ApplyMask(), name='betphase', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=basedir

# PCA on the raw mag and phase timecourses
pca = pe.MapNode(interface=pr.MagandPhasePCA(), name="pca", iterfield=['mag_file','phase_file'])

# Bandpass data
bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
bpftphase.inputs.highpass_sigma = 80
bpftphase.inputs.lowpass_sigma = 2

bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
bpftmag.inputs.highpass_sigma = 80
bpftmag.inputs.lowpass_sigma = 2

# Calculate percent change maps for all nodes
percentchangemag = pe.MapNode(interface=pr.PercentChange(), name="percentchangemag", iterfield=["func"])
percentchangemag.inputs.blockLength = 15
percentchangemag.inputs.TR = 1.25
percentchangemag.inputs.blockPattern = [0, 0, 1] * 10 + [0, 0]

percentchangesim = pe.MapNode(interface=pr.PercentChange(), name="percentchangesim", iterfield=["func"])
percentchangesim.inputs.blockLength = 15
percentchangesim.inputs.TR = 1.25
percentchangesim.inputs.blockPattern = [0, 0, 1] * 10 + [0, 0]

addmeantosim = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name='addmeantosim', iterfield=['in_file','operand_files'])
addmeantosim.inputs.op_string='-add %s'

addmeantomag = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name='addmeantomag', iterfield=['in_file','operand_files'])
addmeantomag.inputs.op_string='-add %s'

submeanfrommag = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name='submeanfrommag', iterfield=['in_file','operand_files'])
submeanfrommag.inputs.op_string='-sub %s'

# Regress magnitude and phase
phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = 1.25
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb = 0.15

# # Output the spatial cross correlation
# findpsf = pe.MapNode(interface=pr.finddrainingarea(), name='findpsf', iterfield=['magmap', 'simmap'])
#
# Blur the images to the fit gaussians (from matlab: FitPSFSlices.m on local) FWHM=1.6523 voxels
blursim = pe.MapNode(interface=pr.NilearnBlur(), name='blursim', iterfield=['in_file'])
blursim.inputs.fwhm=2

# crosscorrelate the mag and sim timecourse across time
crosscorr = pe.MapNode(interface=pr.CrosscorrMagPhase(), name="crosscorr", iterfield=["mag", "sim"])
# crosscorrsvd = pe.MapNode(interface=pr.crosscorrMagPhase(), name="crosscorrsvd", iterfield=["mag", "sim"])
#
# # test the residuals for normality
# norm = pe.MapNode(interface=pr.NormalDistTest(), name="norm", iterfield=["in_file"])
# normx = pe.MapNode(interface=pr.NormalDistTest(), name="normx", iterfield=["in_file"])
# normy = pe.MapNode(interface=pr.NormalDistTest(), name="normy", iterfield=["in_file"])
#
# # randomly plot the residuals of 100 voxels
# sample = pe.MapNode(interface=pr.sampleandPlot(), name='sample', iterfield=["in_file", "mask_file"])
# sample.inputs.plot_type='histo'
# sample.inputs.invalid_number=-1
# sample.inputs.number_of_voxels=100

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = basedir

# Preprocess and motion correct
metaflow.connect([(preproc_phase_wf, betphase, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase, [('extractor.mask_file','mask_file')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('extractor.out_file', 'inputspec.input_mag')]),
                  (betphase, mcflirtphase_wf, [('out_file', 'inputspec.input_phase')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file','inputspec.mat_dir')]),
                  (mcflirtphase_wf, bpftphase, [('outputspec.out_file', 'in_file')]),
                  # (preproc_mag_wf, level1_wf, [('extractor.out_file', 'makemodel.functional_runs'),
                  #                            ('extractor.out_file', 'modelfit.inputspec.functional_data')]),
                  (preproc_mag_wf, bpftmag, [('extractor.out_file','in_file')]),
                  # (preproc_mag_wf, compcormag, [('extractor.out_file','inputspec.func')]),
                  # (level1_wf, compcormag, [('modelfit.outputspec.zfiles','inputspec.zstat')]),
                  # (mcflirtphase_wf, compcorphase, [('outputspec.out_file','inputspec.func')]),
                  # (level1_wf, compcorphase, [('modelfit.outputspec.zfiles','inputspec.zstat')]),
                  # ])
                  ])

# Perform phase regression
metaflow.connect([(mcflirtphase_wf, phaseregress, [('outputspec.out_file', 'phase')]),
                  (preproc_mag_wf, phaseregress, [('extractor.out_file', 'mag')]),
                  (bpftmag, phaseregress, [('out_file', 'bpmag')]),
                  (bpftphase, phaseregress, [('out_file', 'bpphase')])])

# # Calculate percent change and perform spatial correlation to find psf
# metaflow.connect([(phaseregress, addmeantosim, [('sim', 'in_file')]),
#                   (preproc_mag_wf, addmeantosim, [('meancalc.out_file', 'operand_files')]),
#                   (bpftmag, addmeantomag, [('out_file', 'in_file')]),
#                   (preproc_mag_wf, addmeantomag, [('meancalc.out_file', 'operand_files')]),
#                   (addmeantosim, percentchangesim,[('out_file', 'func')]),
#                   (addmeantomag, percentchangemag,[('out_file', 'func')]),
#                   (percentchangesim, findpsf, [('pc_func', 'simmap')]),
#                   (percentchangemag, findpsf, [('pc_func', 'magmap')])
#                   ])

# Blur and cross correlate
metaflow.connect([(phaseregress, blursim, [('sim', 'in_file')]),
                  # (findpsf, blursim, [('sigma', 'fwhm')]),
                  (blursim, crosscorr, [('out_file','sim')]),
                  # (preproc_mag_wf, submeanfrommag, [('extractor.out_file', 'in_file'),
                  #                                   ('meancalc.out_file', 'operand_files')]),
                  (bpftmag, crosscorr, [('out_file','mag')]),
                  ])

# # Find if residuals are normal
# metaflow.connect([(phaseregress, norm, [('residuals', 'in_file')]),
#                   (phaseregress, normx, [('resx', 'in_file')]),
#                   (phaseregress, normy, [('resy', 'in_file')]),
#                   (norm, sample, [('result_file','mask_file')]),
#                   (phaseregress, sample, [('residuals','in_file')]),])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)

metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 4})

# slowflow = pe.Workflow(name='slowflow')
# slowflow.base_dir  = basedir
#
# bpftmagimgs = getoutimglocs(metaflow_exec_graph, metaflow.get_node('bpftmag'), 'out_file')
# bpftphaseimgs = getoutimglocs(metaflow_exec_graph, metaflow.get_node('bpftphase'), 'out_file')
# betimgs = getoutimglocs(metaflow_exec_graph, preproc_mag_wf, 'extractor.out_file')
#
# pca.inputs.mag_file=bpftmagimgs
# pca.inputs.phase_file=bpftphaseimgs
# crosscorrsvd.inputs.mag=betimgs
#
# # Perform PCA
# slowflow.connect([(pca, crosscorrsvd, [('tc1', 'sim')])])
#
# slowflow_exec_graph = slowflow.run(plugin='Linear')