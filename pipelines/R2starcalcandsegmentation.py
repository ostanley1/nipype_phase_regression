
# coding: utf-8

# In[1]:


import nibabel as nb
import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from bids import BIDSLayout
from nilearn import plotting
import re
import pandas
import matplotlib.pyplot as plt
import json
from nipype_phase_regression.interfaces.qsm_sstv.CalcR2Star import *


# In[2]:


base_dir = '/cfmm/data/ostanle2/Ongoing_Work/bids/GEvSE/'
layout = BIDSLayout(base_dir)
layout


# In[8]:


subj=['03']
susc_files = layout.get(type='GRE', subject=subj, extensions='.nii.gz')

mag_img_obj=nb.load(susc_files[0].filename)
shape=mag_img_obj.shape

complex_img=np.empty(shape+(int(len(susc_files)/2),),dtype='complex')        
te=np.empty(int(len(susc_files)/2))
mag_files = ['']*(int(len(susc_files)/2))
phase_files = ['']*(int(len(susc_files)/2))

for s in susc_files:
    part = re.search('part-([a-zA-Z0-9]+)', s.filename)[0][5:]
    echo = re.search('echo-([a-zA-Z0-9]+)', s.filename)[0][5:]
    te[int(echo)-1]=json.loads(open(s.filename[:-7]+'.json').read())['EchoTime']
    if part=='mag':
        mag_files[int(echo)-1]=s.filename
    if part=='phase':
        phase_files[int(echo)-1]=s.filename
        
for e in range(len(te)):
    complex_img[...,e]=(nb.load(mag_files[e]).get_data()).astype('float')*np.exp(1j*(nb.load(phase_files[e]).get_data()))
    
#plotting.plot_epi(susc_files[0].filename, colorbar=True, vmin=200)


# In[9]:


mask = mag_img_obj.get_data()>200
R2star_img,goodness_of_fit,neg_mask,nan_mask=calc_R2star_fn(complex_img,mask,te)  

niftifile=nib.Nifti1Pair(R2star_img,mag_img_obj.affine)
nib.save(niftifile,'out_r2star.nii.gz')
niftifile=nib.Nifti1Pair(goodness_of_fit,mag_img_obj.affine)
nib.save(niftifile,'out_r2star_gof.nii.gz')
niftifile=nib.Nifti1Pair(neg_mask,mag_img_obj.affine)
nib.save(niftifile,'out_r2star_negs.nii.gz')
niftifile=nib.Nifti1Pair(nan_mask,mag_img_obj.affine)
nib.save(niftifile,'out_r2star_nans.nii.gz')

