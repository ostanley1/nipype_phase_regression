def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def picksecond(files):
    if isinstance(files, list):
        return files[1]
    else:
        return files

def trimvisuotopy(files):
    return files[-2:]

def get_niftis(subject_id, data_dir, type, modality, tags):
    #tags is a list with the bids tag in it
    from bids.grabbids import BIDSLayout

    layout = BIDSLayout(data_dir)

    if type==[]:
        files = [f.filename for f in layout.get(subject=subject_id, modality=modality,  extensions=['nii', 'nii.gz'])]
    else:
        files = [f.filename for f in layout.get(subject=subject_id, type=type, extensions=['nii', 'nii.gz'])]

    filtfiles = []
    for j in range(len(files)):
        inc=True
        for i in range(len(tags)):
            if tags[i] not in files[j]:
                inc = False
        if inc:
            filtfiles.append(files[j])
    if len(filtfiles)==1:
        return filtfiles[0]
    else:
        return filtfiles

def getBIDSmeta(in_file):
    from bids.grabbids import BIDSLayout
    from nipype.utils.filemanip import split_filename

    if isinstance(in_file, list):
        in_file=in_file[0]

    path,_,_=split_filename(in_file)

    layout = BIDSLayout(path)

    return layout.get_metadata(in_file)

def extendMotion(in_file):
    import numpy as np
    from nipype.utils.filemanip import split_filename
    moco = np.loadtxt(in_file)
    moco_ext=np.zeros([moco.shape[0], 24])
    moco_ext[:,0:6] = moco**2
    moco_ext[1:,6:12] = np.diff(moco, axis=0)
    moco_ext[:,12:18] = moco_ext[:,6:12]**2
    moco_ext[:, 18:24] = moco
    path, base,_=split_filename(in_file)
    out_file = path+'/'+base+'_ext.tsv'
    np.savetxt(out_file, moco_ext, delimiter='\t')
    return out_file

def get_exp_info(file, mocofile, physfile, physbool):
    from nipype.interfaces.base import Bunch
    import numpy as np
    with open(mocofile, 'r+') as f:
        regressors = np.loadtxt(f)
    regressor_list=[]
    for col in range(regressors.shape[1]):
        regressor_list.append(list(regressors[:,col]))
    regress_names=['roll2', 'pitch2', 'yaw2', 'dS2', 'dL2', 'dP2',
                   'droll', 'dpitch', 'dyaw', 'ddS', 'ddL', 'ddP',
                   'droll2', 'dpitch2', 'dyaw2', 'ddS2', 'ddL2', 'ddP2',
                   'roll', 'pitch', 'yaw', 'dS', 'dL', 'dP']
    if physbool==True:
        with open(physfile, 'r+') as f:
            regressors = np.loadtxt(f, skiprows=1)
        for col in range(regressors.shape[1]):
            regressor_list.append(list(regressors[:, col]))
            regress_names.append('compcor'+str(col))
        print(regress_names)
        print(regressor_list)
    if 'checkerboard' in file:
        onsets = [15, 45, 75, 105, 135, 165, 195, 225]
        exp_info=Bunch(conditions=["Activation"], onsets=[onsets], durations=[[15] * 8],
                       regressor_names=regress_names, regressors=regressor_list)
        return exp_info
    elif 'visuotopy' in file:
        onsets_pos = [10, 60, 110]
        onsets_neg = [35, 85, 135]
        exp_info=Bunch(conditions=["CondA","CondB"],
                       onsets=[onsets_pos, onsets_neg],
                       durations=[[15] * 3, [15] * 3],
                       regressor_names=regress_names,
                       regressors=regressor_list)
        return exp_info
    else:
        return None


def get_cont(file):
    if 'checkerboard' in file:
        return [['Activation>Baseline', 'T', ["Activation"], [1]]]
    elif 'visuotopy' in file:
        return [['TaskA>TaskB', 'T', ['CondA', 'CondB'], [1,-1]]]

def freesurf_args(subject_id):
    recon_args = {'01': '-cc-crs 127 122 181 -pons-crs 127 125 184 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA  -no-wsgcaatlas -wsthresh 45',
                  '02': '-cc-crs 132 133 182 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA  -no-wsgcaatlas -wsthresh 45',
                  '03': '-cc-crs 127 114 181 -pons-crs 127 166 182 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '04': '-cc-crs 138 118 183 -pons-crs 138 166 183 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '05': '-cc-crs 127 117 181 -pons-crs 127 167 183 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '06': '-cc-crs 127 125 180 -pons-crs 127 169 186 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '07': '-cc-crs 127 119 175 -pons-crs 127 173 173 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA '}
    return recon_args[subject_id]

def freesurf_argsdefault(subject_id):
    recon_args = {'01': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA -no-wsgcaatlas -wsthresh 45',
                  '02': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA  -no-wsgcaatlas -wsthresh 45',
                  '03': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '04': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '05': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '06': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '07': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA '}
    return recon_args[subject_id]

def compcor_argsdefault(subject_id):
    recon_args = {'01': ['fake.txt']*4,
                  '02': ['fake.txt']*5,
                  '03': ['fake.txt']*5,
                  '04': ['fake.txt']*5,
                  '05': ['fake.txt']*5,
                  '06': ['fake.txt']*5,
                  '07': ['fake.txt']*5}
    return recon_args[subject_id]

def compcorse_argsdefault(subject_id):
    recon_args = {'01': ['fake.txt']*5,
                  '02': ['fake.txt']*5,
                  '03': ['fake.txt']*5,
                  '04': ['fake.txt']*5,
                  '05': ['fake.txt']*5,
                  '06': ['fake.txt']*5,
                  '07': ['fake.txt']*5}
    return recon_args[subject_id]

def matchMagPhase(maglist, phaselist):
    import re
    newmag = []
    newphase = []
    for f in maglist:
        run = re.search("run-0\\d_", f)
        task = re.search("task-(.*)_acq", f)
        for test in phaselist:
            print(test, run.group(0), task.group(0))
            if run.group(0) in test and task.group(0) in test and 'acq-ge' in test:
                newmag.append(f)
                newphase.append(test)
                phaselist.remove(test)
    print(newmag)
    print(newphase)
    return newmag, newphase

def sort_copes(copes, in_files):
    outcopes=[]
    for j, f in enumerate(in_files):
        if 'check' in f:
            outcopes.append(copes[j])
    print(outcopes)
    return outcopes

def getdepth(filename):
    import re
    if isinstance(filename, list):
        files = []
        for f in filename:
            depth = re.search('distance_([a-zA-Z0-9.]+)', f)[0][-3:]
            modality = re.search('/([a-zA-Z0-9]+)surfsample', f)[0][1:-10]
            files.append(depth+modality)
        return files
    else:
        depth = re.search('distance_([a-zA-Z0-9.]+)', filename)[0][-3:]
        modality = re.search('/([a-zA-Z0-9]+)surfsample', filename)[0][1:-10]
        return (depth + modality)

def run_pipeline(subject_list):
    # grab the data
    basedir = '/workspace/ostanle2/SEvsGEBIDStest'
    output_dir = '/cfmm/data/ostanle2/Ongoing_Work/bids/GEvSE'
    TR=2.5

    print('Running on subjects: ', subject_list)

    import nipype.pipeline.engine as pe
    import nipype.interfaces.fsl as fsl
    import numpy as np
    import nipype_phase_regression.interfaces as pr
    from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                           create_fixed_effects_flow)
    import nipype.algorithms.modelgen as model
    from nipype.interfaces.base import Bunch
    import nipype.interfaces.utility as ul
    import nipype.interfaces.io as nio
    from nipype.interfaces import spm, fsl, afni
    from nipype_phase_regression.workflows.preproc_mag_wf_SEvsGE import create_preproc_magsevsge_wf
    from nipype_phase_regression.workflows.preproc_phase_wf_SEvsGE import create_preproc_phasesevsge_wf
    from nipype_phase_regression.workflows.feat_wf import create_feat_preproc_wf, create_feat_stats_wf, create_feat_fixed_wf
    from nipype_phase_regression.workflows.make_surf_wf import create_make_surf_wf
    from nipype_phase_regression.workflows.register_wf import create_reg_wf, create_apply_reg_wf, create_apply_reg_susc_wf
    from nipype_phase_regression.workflows.vessel_map_wf import create_venous_map_wf
    from nipype_phase_regression.workflows.visuotopy_wf import create_visuotopy_wf, create_vol2surf_wf
    from nipype_phase_regression.workflows.pipeline_SS_R2only_Siemens import create_pipeline_SS_TV
    # from bids.grabbids import BIDSLayout
    # from fmriprep.workflows.fieldmap.phdiff import init_phdiff_wf
    # from fmriprep.workflows.fieldmap.unwarp import init_sdc_unwarp_wf
    from niworkflows.interfaces.bids import DerivativesDataSink
    from nipype.algorithms.confounds import ACompCor
    import nipype.interfaces.freesurfer as fsurf
    import nipype.interfaces.ants as ants


    from nipype import config, logging

    import time
    start = time.time()

    from nipype import config, logging

    import time
    start = time.time()

    crash_dir = './crashes'

    config.update_config({'execution': {'crashdump_dir': crash_dir,
                                        'crashfile_format': 'txt',
                                        'hash_method':'content'},
                          # 'logging': {'workflow_level':'DEBUG'}
                         })

    logging.update_logging(config)

    infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id','recon_cmd']),
                         name="infosource")
    infosource.iterables = [('subject_id', subject_list)]

    BIDSFuncGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id",
                                           "data_dir","type","modality","tags"],
                                       output_names=["bold"]), name="BIDSFuncGrabber", iterfield=["tags"])
    BIDSFuncGrabber.inputs.data_dir = output_dir
    BIDSFuncGrabber.inputs.type='bold'
    BIDSFuncGrabber.inputs.modality=[]
    BIDSFuncGrabber.inputs.tags=[['acq-ge','part-mag'], ['acq-ge', 'part-phase'], ['acq-se']]

    BIDSStructGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id","data_dir","type","modality","tags"],
                                               output_names=["anat"]), name="BIDSStructGrabber", iterfield=["tags"])
    BIDSStructGrabber.inputs.data_dir = output_dir
    BIDSStructGrabber.inputs.type='T1w'
    BIDSStructGrabber.inputs.modality=[]
    BIDSStructGrabber.inputs.tags=[['acq-MP2RAGE']]

    BIDSInvGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id","data_dir","type","modality","tags"],
                                               output_names=["inv"]), name="BIDSInvGrabber", iterfield=["tags"])
    BIDSInvGrabber.inputs.data_dir = output_dir
    BIDSInvGrabber.inputs.type='MP2RAGE'
    BIDSInvGrabber.inputs.modality=[]
    BIDSInvGrabber.inputs.tags=[['inv-2']]

    BIDSAngioGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id","data_dir","type","modality","tags"],
                                               output_names=["angio"]), name="BIDSAngioGrabber", iterfield=["tags"])
    BIDSAngioGrabber.inputs.data_dir = output_dir
    BIDSAngioGrabber.inputs.type='angio'
    BIDSAngioGrabber.inputs.modality=[]
    BIDSAngioGrabber.inputs.tags=[['acq-TOF']]

    BIDSGREGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id","data_dir","type","modality","tags"],
                                               output_names=["gre"]), name="BIDSGREGrabber", iterfield=["tags"])
    BIDSGREGrabber.inputs.data_dir = output_dir
    BIDSGREGrabber.inputs.type='GRE'
    BIDSGREGrabber.inputs.modality=[]
    BIDSGREGrabber.inputs.tags=[['part-mag'],['part-phase']]

    BIDSfmapGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id","data_dir","type","modality","tags"],
                                               output_names=["fmap"]), name="BIDSfmapGrabber", iterfield=["tags"])
    BIDSfmapGrabber.inputs.data_dir = output_dir
    BIDSfmapGrabber.inputs.type=[]
    BIDSfmapGrabber.inputs.modality='fmap'
    BIDSfmapGrabber.inputs.tags=[['magnitude'],['phasediff']]

    selectGEmag=pe.Node(interface=ul.Select(index=0),name="selectGEmag")
    selectGEphase=pe.Node(interface=ul.Select(index=1),name="selectGEphase")
    selectSE=pe.Node(interface=ul.Select(index=2),name="selectSE")
    selectstruct=pe.Node(interface=ul.Select(index=0),name="selectstruct")
    selectinv=pe.Node(interface=ul.Select(index=0),name="selectinv")
    selecttof=pe.Node(interface=ul.Select(index=0),name="selecttof")
    selectgremag=pe.Node(interface=ul.Select(index=0),name="selectgremag")
    selectgrephase=pe.Node(interface=ul.Select(index=1),name="selectgrephase")
    selectfmapmag=pe.Node(interface=ul.Select(index=0),name="selectfmapmag")
    selectfmapphase=pe.Node(interface=ul.Select(index=1),name="selectfmapphase")

    # need to also grab flatpatches from the freesurfer directories
    patchgrab = pe.Node(nio.DataGrabber(infields=['subject_id'], outfields=['lhpatch', 'rhpatch']), name='patchgrab')
    patchgrab.inputs.base_directory = '/workspace/ostanle2/SEvsGEBIDS/freesurfer/'
    patchgrab.inputs.template = '%s/surf/.occip.patch.flat'
    patchgrab.inputs.sort_filelist = False
    patchgrab.inputs.field_template = dict(lhpatch='%s/surf/lh.occip.patch.flat',
                                            rhpatch='%s/surf/rh.occip.patch.flat')
    patchgrab.inputs.template_args = dict(lhpatch=[['subject_id']],
                                           rhpatch=[['subject_id']])

    # need to also grab depths from the freesurfer directories
    depthgrab = pe.Node(nio.DataGrabber(infields=['subject_id'], outfields=['ldepth', 'rdepth',
                                                                            'lcols', 'rcols']),
                        name='depthgrab')
    depthgrab.inputs.base_directory = '/cfmm/data/ostanle2/Ongoing_Work/SEvsGE2/surfanalysis/'
    depthgrab.inputs.sort_filelist = False
    depthgrab.inputs.template = ''
    depthgrab.inputs.field_template = dict(ldepth='L2depthssub%s.nii.gz',
                                           rdepth='R2depthssub%s.nii.gz',
                                           lcols='L2colssub%s.nii.gz',
                                           rcols='R2colssub%s.nii.gz',
                                           )
    depthgrab.inputs.template_args = dict(ldepth=[['subject_id']],
                                           rdepth=[['subject_id']],
                                          lcols=[['subject_id']],
                                           rcols=[['subject_id']],)

    grabber = pe.Workflow(name='grabber')
    grabber.connect(infosource, 'subject_id', BIDSFuncGrabber, 'subject_id')
    grabber.connect(infosource, 'subject_id', BIDSStructGrabber, 'subject_id')
    grabber.connect(infosource, 'subject_id', BIDSInvGrabber, 'subject_id')
    grabber.connect(infosource, 'subject_id', BIDSAngioGrabber, 'subject_id')
    grabber.connect(infosource, 'subject_id', BIDSGREGrabber, 'subject_id')
    grabber.connect(infosource, 'subject_id', BIDSfmapGrabber, 'subject_id')
    grabber.connect(infosource, 'subject_id', patchgrab, 'subject_id')
    grabber.connect(infosource, 'subject_id', depthgrab, 'subject_id')
    grabber.connect(BIDSFuncGrabber, 'bold', selectGEmag, 'inlist')
    grabber.connect(BIDSFuncGrabber, 'bold', selectGEphase, 'inlist')
    grabber.connect(BIDSFuncGrabber, 'bold', selectSE, 'inlist')
    grabber.connect(BIDSStructGrabber, 'anat', selectstruct, 'inlist')
    grabber.connect(BIDSInvGrabber, 'inv', selectinv, 'inlist')
    grabber.connect(BIDSAngioGrabber, 'angio', selecttof, 'inlist')
    grabber.connect(BIDSGREGrabber, 'gre', selectgremag, 'inlist')
    grabber.connect(BIDSGREGrabber, 'gre', selectgrephase, 'inlist')
    grabber.connect(BIDSfmapGrabber, 'fmap', selectfmapmag, 'inlist')
    grabber.connect(BIDSfmapGrabber, 'fmap', selectfmapphase, 'inlist')

    r2star_wf = create_pipeline_SS_TV()

    # preprocess GE data
    preproc_mag_wfge = create_preproc_magsevsge_wf()
    preproc_mag_wfge.base_dir=basedir
    preproc_mag_wfge.inputs.inputspec.TR = TR
    preproc_mag_wfge.inputs.inputspec.frac = 0.7

    # preprocess SE data
    preproc_mag_wfse=preproc_mag_wfge.clone('preproc_mag_wfse')
    preproc_mag_wfse.inputs.inputspec.frac = 0.35

    # preprocess the phase sets
    preproc_phase_wfge = create_preproc_phasesevsge_wf()
    preproc_phase_wfge.base_dir = basedir
    preproc_phase_wfge.inputs.inputspec.siemensbool = True

    # need to confirm all magnitude and phase files match
    sortmagphase = pe.Node(interface=ul.Function(['maglist', 'phaselist'], ['maglist', 'phaselist'], matchMagPhase), name='sortmagphase')

    # bandpass the magnitude and phase ge sets
    # Bandpass data
    bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
    bpftphase.inputs.highpass_sigma = 80
    bpftphase.inputs.lowpass_sigma = 2

    bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
    bpftmag.inputs.highpass_sigma = 80
    bpftmag.inputs.lowpass_sigma = 2

    # Regress ge magnitude and phase
    phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregressodr', iterfield=['phase','mag', 'bpphase', 'bpmag'])
    phaseregress.inputs.TR = TR
    phaseregress.inputs.sig_lb = 0.01
    phaseregress.inputs.sig_ub = 0.1
    phaseregress.iterables = [('noise_lb', [0.15])]

    # Start structural pipeline
    make_surfs_wf = create_make_surf_wf()
    make_surfs_wf.basedir = basedir
    make_surfs_wf.inputs.inputspec.midline = 105
    make_surfs_wf.inputs.inputspec.y_min = 0
    make_surfs_wf.inputs.inputspec.y_size = 115

    # Start venous mask pipeline
    vessel_map_wf = create_venous_map_wf()

    reg_wf = create_reg_wf()
    reg_wf.basedir = basedir

    funcreg_wfge = create_apply_reg_wf()
    funcreg_wfge.basedir = basedir

    funcreg_wfse=funcreg_wfge.clone('funcreg_wfse')
    funcreg_wffilt=funcreg_wfge.clone('funcreg_wffilt')
    funcreg_wfsim=funcreg_wfge.clone('funcreg_wfsim')

    funcregfixed_wfge=funcreg_wfge.clone('funcregfixed_wfge')
    funcregfixed_wfse=funcreg_wfge.clone('funcregfixed_wfse')
    funcregfixed_wffilt=funcreg_wfge.clone('funcregfixed_wffilt')
    funcregfixed_wfsim=funcreg_wfge.clone('funcregfixed_wfsim')
    funcregfixed_wfvessel=funcreg_wfge.clone('funcregfixed_wfvessel')

    suscreg_wf = create_apply_reg_susc_wf()
    suscreg_wf.basedir = basedir

    # Set up experiment
    motion_extend = pe.MapNode(interface=ul.Function(function=extendMotion, input_names=["in_file"],
                                                     output_names=["out_file"]), name="motion_extend",
                               iterfield=["in_file"])

    erodewm = pe.Node(fsl.maths.MathsCommand(), name='erodewm')
    erodewm.inputs.args = '-thr 100 -uthr 120 -bin -ero -ero'

    erodewmse = erodewm.clone('erodewmse')

    compcorge = pe.MapNode(interface=ACompCor(), name='compcorge', iterfield=['realigned_file'])

    compcorse = compcorge.clone('compcorse')

    motion_extendse = pe.MapNode(interface=ul.Function(function=extendMotion, input_names=["in_file"],
                                                     output_names=["out_file"]), name="motion_extendse",
                               iterfield=["in_file"])

    expsetup = pe.MapNode(interface=ul.Function(function=get_exp_info, input_names=["file", "mocofile", "physfile", "physbool"],
                                      output_names=["exp_info"]), name="expsetup", iterfield=["file", "mocofile", "physfile"])
    expsetup.iterables = [('physbool', [True])]

    expsetupse = expsetup.clone('expsetupse')
    expsetupfilt = expsetup.clone('expsetupfilt')
    expsetupsim = expsetup.clone('expsetupsim')

    contsetup = pe.MapNode(interface=ul.Function(function=get_cont, input_names=["file"],
                                      output_names=["cont"]), name="contsetup", iterfield=["file"])

    contsetupse = contsetup.clone('contsetupse')
    contsetupfilt = contsetup.clone('contsetupfilt')
    contsetupsim = contsetup.clone('contsetupsim')

    featpre_wf = create_feat_preproc_wf()
    featpre_wf.inputs.inputspec.TR=2.5
    featpre_wf.inputs.inputspec.highpass=100

    featpre_wfse=featpre_wf.clone('featpre_wfse')

    featpre_wffilt=featpre_wf.clone('featpre_wffilt')

    featpre_wfsim=featpre_wf.clone('featpre_wsim')

    # Create peristim plots
    peristim_ge = pe.MapNode(interface=pr.PeristimPlot(TR=TR, blocks=8, activation_time=15, rest_time=15),
                             name='peristim_ge', iterfield=['in_file'])
    peristim_filt=peristim_ge.clone('peristim_filt')
    peristim_se=peristim_ge.clone('peristim_se')
    peristim_sim=peristim_ge.clone('peristim_sim')

    featfit_wf = create_feat_stats_wf()
    featfit_wf.inputs.inputspec.TR=2.5
    featfit_wf.inputs.inputspec.hpcutoff = 100

    featfit_wfse=featfit_wf.clone('featfit_wfse')
    featfit_wffilt=featfit_wf.clone('featfit_wffilt')
    featfit_wfsim=featfit_wf.clone('featfit_wfsim')

    sortcopesge = pe.Node(interface=ul.Function(function=sort_copes, input_names=["copes", "in_files"],
                                                     output_names=["out_file"]), name="sortcopesge")
    sortcopesse = sortcopesge.clone('sortcopesse')
    sortcopesfilt = sortcopesge.clone('sortcopesfilt')
    sortcopessim = sortcopesge.clone('sortcopessim')

    sortvarcopesge = sortcopesge.clone('sortvarcopesge')
    sortvarcopesse = sortcopesge.clone('sortvarcopesse')
    sortvarcopesfilt = sortcopesge.clone('sortvarcopesfilt')
    sortvarcopessim = sortcopesge.clone('sortvarcopessim')

    featfixed_wfge = create_feat_fixed_wf()
    featfixed_wfse=featfixed_wfge.clone('featfixed_wfse')
    featfixed_wffilt=featfixed_wfge.clone('featfixed_wffilt')
    featfixed_wfsim=featfixed_wfge.clone('featfixed_wfsim')

    n3meange = pe.MapNode(fsurf.MNIBiasCorrection(), name='n3meange', iterfield='in_file')
    n3meange.inputs.iterations = 2

    n3meanse = n3meange.clone('n3meanse')
    n3meanfilt = n3meange.clone('n3meanfilt')
    n3meansim = n3meange.clone('n3meansim')

    visuotopy_wf = create_visuotopy_wf()

    # We have a lot of things that need to go to a surface lets make a list
    surf = ['ge', 'se', 'filt', 'sim', 'r2', 'r2star', 'r2starvessel', 'gemean', 'semean', 'V1', 'T1w']
    vol2surf_wfs={}
    for d in surf:
        if d =='T1w':
            vol2surf_wfs[d]=create_vol2surf_wf(d+'surfsample')#, nifti_save=True)

        else:
            vol2surf_wfs[d]=create_vol2surf_wf(d+'surfsample')

    labeldepthsl = pe.Node(interface=fsl.MultiImageMaths(), name='labeldepthsl')
    labeldepthsl.inputs.op_string='-bin -sub 1 -add %s'

    labeldepthsr = labeldepthsl.clone('labeldepthsr')

    reg_r2 = pe.MapNode(interface=ants.ApplyTransforms(), name='reg_r2', iterfield=['input_image'],
                          nested=True)
    reg_r2.inputs.interpolation = 'BSpline'
    reg_r2.inputs.interpolation_parameters = (5,)

    reg_swi = reg_r2.clone('reg_swi')
    reg_vessel = reg_r2.clone('reg_vessel')
    regV1tofunc = pe.Node(interface=ants.ApplyTransforms(), name='regV1tofunc')
    regV1tofunc.inputs.interpolation = 'BSpline'
    regV1tofunc.inputs.interpolation_parameters = (5,)

    # results in bad header info need to correct
    cpgeomswi = pe.MapNode(interface=fsl.CopyGeom(), name='cpgeomswi', iterfield=['dest_file', 'in_file'])
    cpgeomvessel = cpgeomswi.clone('cpgeomvessel')


    metaflow = pe.Workflow(name='metaflow')
    metaflow.config['execution'] = {'remove_unnecessary_outputs': 'False', 'stop_on_first_rerun':'True'}
    metaflow.config['logging'] = {'workflow_level':'DEBUG'}
    metaflow.base_dir = basedir

    metaflow.connect([(grabber, sortmagphase,[('selectGEmag.out', 'maglist')]),
                      (grabber, sortmagphase, [('selectGEphase.out', 'phaselist')]),
                      (sortmagphase, preproc_mag_wfge, [('maglist', 'inputspec.input_mag')]),
                      (sortmagphase, preproc_phase_wfge, [('phaselist', 'inputspec.input_phase')]),
                      (grabber, preproc_mag_wfse, [('selectSE.out', 'inputspec.input_mag')]),
                      (sortmagphase, preproc_phase_wfge, [(('maglist', pickfirst), 'inputspec.ref_phase')]),
                      (grabber, make_surfs_wf, [('selectstruct.out','inputspec.struct'),
                                                ('infosource.subject_id', 'inputspec.subject_id'),
                                                (('infosource.subject_id', freesurf_args), 'inputspec.recon_cmd'),
                                                (('infosource.subject_id', freesurf_argsdefault), 'inputspec.recon_cmddef'),
                                                ('selectinv.out', 'inputspec.inv')]),
                      # (grabber, visuotopy_wf, [('infosource.subject_id', 'inputspec.subject_id'),
                      #                          ('patchgrab.lhpatch', 'inputspec.lhpatch'),
                      #                          ('patchgrab.rhpatch', 'inputspec.rhpatch'),]),
                      # (grabber, vessel_map_wf, [(('selecttof.out', pickfirst), 'inputspec.tof'),
                      #                           (('selectgremag.out', picksecond), 'inputspec.echo2mag'),
                      #                           (('selectgrephase.out', picksecond), 'inputspec.echo2phase')]),
                      # (sortmagphase, vessel_map_wf, [('maglist', 'inputspec.gremag'),
                      #                           ('phaselist', 'inputspec.grephase')]),
                      (grabber, reg_wf, [('selectgremag.out', 'inputspec.multiechomag'),
                                         ('selectgrephase.out', 'inputspec.multiechophase')]),
                      (grabber, suscreg_wf, [('selectgremag.out', 'inputspec.mags'),
                                         ('selectgrephase.out', 'inputspec.phases')]),
                      # (grabber, r2star_wf, [('selectgremag.out', 'inputspec.mag_images')]),
                      # (grabber, r2star_wf, [('selectgrephase.out', 'inputspec.phase_images')]),
                      # (r2star_wf, suscreg_wf, [('R2Star.R2star', 'inputspec.r2star')]),
                      # (r2star_wf, vessel_map_wf, [('brain_extract.mask_file', 'inputspec.echomask'),
                      #                             ('R2Star.R2star', 'inputspec.r2star'),
                      #                             ('freq_est.freq_filename', 'inputspec.freq')]),
                      # (grabber, labeldepthsl, [('depthgrab.ldepth', 'operand_files'),
                      #                          ('depthgrab.lcols', 'in_file')]),
                      # (grabber, labeldepthsr, [('depthgrab.rdepth', 'operand_files'),
                      #                          ('depthgrab.rcols', 'in_file')]),
                      (labeldepthsl, reg_wf, [('out_file', 'inputspec.ldepth')]),
                      (labeldepthsr, reg_wf, [('out_file', 'inputspec.rdepth')]),
                      # (grabber, phasediff_wf, [('selectfmapmag.out', 'inputnode.magnitude'),
                      #                      ('selectfmapphase.out', 'inputnode.phasediff')]),
                      # (sortmagphase, get_metadata, [(('maglist',pickfirst), 'in_file')]),
                      # (get_metadata, unwarp_wf, [('metadata', 'inputnode.metadata')]),
                      # (phasediff_wf, unwarp_wf, [('outputnode.fmap', 'inputnode.fmap'),
                      #                            ('outputnode.fmap_ref', 'inputnode.fmap_ref'),
                      #                            ('outputnode.fmap_mask', 'inputnode.fmap_mask')]),
                      # (featpre_wf, unwarp_wf, [('outputspec.mean_file', 'inputnode.in_reference'),
                      #                       ('outputspec.mean_file', 'inputnode.in_reference_brain')]),
                      # (preproc_mag_wfge, unwarp_wf, [('outputspec.mask_file', 'inputnode.in_mask')]),
                      # (unwarp_wf, reg_wf, [('outputnode.out_reference', 'inputspec.mean_func_ge_uw')]),
                      (make_surfs_wf, reg_wf, [('outputspec.norm_struct', 'inputspec.norm_struct'),
                                               ('outputspec.norm_struct_brain', 'inputspec.norm_struct_brain'),
                                               ('outputspec.V1mask', 'inputspec.V1mask'),
                                               ('outputspec.V1box', 'inputspec.V1box'),
                                               ('convertwhitenii.out_file', 'inputspec.white')]),
                      # (make_surfs_wf, funcreg_wfge, [('outputspec.norm_struct_brain','inputspec.norm_struct_brain')]),
                      # (make_surfs_wf, funcreg_wfse, [('outputspec.norm_struct_brain','inputspec.norm_struct_brain')]),
                      # (make_surfs_wf, funcreg_wffilt, [('outputspec.norm_struct_brain','inputspec.norm_struct_brain')]),
                      # (make_surfs_wf, funcreg_wfsim, [('outputspec.norm_struct_brain','inputspec.norm_struct_brain')]),
                      # (make_surfs_wf, suscreg_wf, [('outputspec.norm_struct_brain','inputspec.norm_struct_brain')]),
                      # (reg_wf, erodewm, [(('reg_white_ge.output_image', pickfirst), 'in_file')]),
                      # (erodewm, compcorge, [('out_file', 'mask_files')]),
                      # (reg_wf, erodewmse, [(('reg_white_se.output_image', pickfirst), 'in_file')]),
                      # (erodewmse, compcorse, [('out_file', 'mask_files')]),
                      # (preproc_mag_wfge, compcorge, [('outputspec.proc_mag', 'realigned_file')]),
                      # (preproc_mag_wfse, compcorse, [('outputspec.proc_mag', 'realigned_file')]),
                      # (preproc_mag_wfge, preproc_phase_wfge, [('outputspec.motion_par', 'inputspec.motion_par'),
                      #                                         ('outputspec.run_txfm', 'inputspec.run_txfm'),
                      #                                         ('outputspec.mask_file', 'inputspec.mask_file')]),
                      # (preproc_mag_wfge, vessel_map_wf, [('outputspec.mask_file', 'inputspec.mask_file'),
                      #                                    ('outputspec.motion_par', 'inputspec.moco_pars'),
                      #                                    ('outputspec.run_txfm', 'inputspec.run_txfm')]),
                      # (preproc_mag_wfge, bpftmag, [('outputspec.proc_mag', 'in_file')]),
                      # (preproc_phase_wfge, bpftphase, [('outputspec.proc_phase', 'in_file')]),
                      # (preproc_phase_wfge, phaseregress, [('outputspec.proc_phase', 'phase')]),
                      # (preproc_mag_wfge, phaseregress, [('outputspec.proc_mag', 'mag')]),
                      # (preproc_mag_wfge, phaseregress, [('outputspec.proc_mag', 'bpmag')]),
                      # (preproc_phase_wfge, phaseregress, [('outputspec.proc_phase', 'bpphase')]),
                      # (phaseregress, reg_r2, [('corr', 'input_image')]),
                      # (reg_wf, reg_r2, [('outputspec.xfmge', 'transforms')]),
                      # (make_surfs_wf, reg_r2, [('outputspec.norm_struct_brain', 'reference_image')]),
                      # (make_surfs_wf, regV1tofunc, [('outputspec.V1mask', 'input_image')]),
                      # (reg_wf, regV1tofunc, [('outputspec.invxfmge', 'transforms')]),
                      # (reg_wf, regV1tofunc, [('n3.out_file', 'reference_image')]),
                      # (vessel_map_wf, cpgeomswi, [('outputspec.swi','dest_file')]),
                      # (featfit_wf, cpgeomswi, [('outputspec.pcfunc_maps','in_file')]),
                      # (cpgeomswi, reg_swi, [('out_file', 'input_image')]),
                      # (reg_wf, reg_swi, [('outputspec.xfmge', 'transforms')]),
                      # (make_surfs_wf, reg_swi, [('outputspec.norm_struct_brain', 'reference_image')]),
                      # (vessel_map_wf, reg_vessel, [('cpgeomr2star.out_file', 'input_image')]),
                      # (reg_wf, reg_vessel, [('outputspec.xfmge', 'transforms')]),
                      # (make_surfs_wf, reg_vessel, [('outputspec.norm_struct_brain', 'reference_image')]),
                      (featpre_wf, reg_wf, [('outputspec.mean_file','inputspec.mean_func_ge')]),
                      (featpre_wfse, reg_wf, [('outputspec.mean_file','inputspec.mean_func_se')]),
                      # # # (preproc_mag_wfge, reg_wf, [('outputspec.mean_file','inputspec.mean_func_ge')]),
                      # # # (preproc_mag_wfse, reg_wf, [('outputspec.mean_file','inputspec.mean_func_se')]),
                      # (preproc_mag_wfge, motion_extend, [('outputspec.motion_data', 'in_file')]),
                      # (preproc_mag_wfse, motion_extendse, [('outputspec.motion_data', 'in_file')]),
                      # (motion_extend, expsetup, [('out_file', 'mocofile')]),
                      # (motion_extendse, expsetupse, [('out_file', 'mocofile')]),
                      # (motion_extend, expsetupfilt, [('out_file', 'mocofile')]),
                      # (motion_extend, expsetupsim, [('out_file', 'mocofile')]),
                      # (compcorge, expsetup, [('components_file', 'physfile')]),
                      # (compcorse, expsetupse, [('components_file', 'physfile')]),
                      # (compcorge, expsetupfilt, [('components_file', 'physfile')]),
                      # (compcorge, expsetupsim, [('components_file', 'physfile')]),
                      # (preproc_mag_wfge, expsetup, [('outputspec.proc_mag', 'file')]),
                      # (preproc_mag_wfge, contsetup, [('outputspec.proc_mag', 'file')]),
                      # (expsetup, featfit_wf, [('exp_info', 'inputspec.subject_info')]),
                      # (contsetup, featfit_wf, [('cont', 'inputspec.contrasts')]),
                      # (preproc_mag_wfse, expsetupse, [('outputspec.proc_mag', 'file')]),
                      # (preproc_mag_wfse, contsetupse, [('outputspec.proc_mag', 'file')]),
                      # (expsetupse, featfit_wfse, [('exp_info', 'inputspec.subject_info')]),
                      # (contsetupse, featfit_wfse, [('cont', 'inputspec.contrasts')]),
                      # (preproc_mag_wfge, expsetupfilt, [('outputspec.proc_mag', 'file')]),
                      # (preproc_mag_wfge, contsetupfilt, [('outputspec.proc_mag', 'file')]),
                      # (expsetupfilt, featfit_wffilt, [('exp_info', 'inputspec.subject_info')]),
                      # (contsetupfilt, featfit_wffilt, [('cont', 'inputspec.contrasts')]),
                      # (preproc_mag_wfge, expsetupsim, [('outputspec.proc_mag', 'file')]),
                      # (preproc_mag_wfge, contsetupsim, [('outputspec.proc_mag', 'file')]),
                      # (expsetupsim, featfit_wfsim, [('exp_info', 'inputspec.subject_info')]),
                      # (contsetupsim, featfit_wfsim, [('cont', 'inputspec.contrasts')]),
                      (preproc_mag_wfge, featpre_wf, [('outputspec.proc_mag', 'inputspec.input')]),
                      (preproc_mag_wfse, featpre_wfse, [('outputspec.proc_mag', 'inputspec.input')]),
                      # (phaseregress, featpre_wffilt, [('filt', 'inputspec.input')]),
                      # (phaseregress, featpre_wfsim, [('sim', 'inputspec.input')]),
                      # (featpre_wf, featfit_wf, [('outputspec.filtered_functional_data', 'inputspec.input'),
                      #                           ('outputspec.mean_file', 'inputspec.mean'),
                      #                           ('outputspec.scalingfactor', 'inputspec.scalingfactor')]),
                      # (featpre_wfse, featfit_wfse, [('outputspec.filtered_functional_data', 'inputspec.input'),
                      #                               ('outputspec.mean_file', 'inputspec.mean'),
                      #                               ('outputspec.scalingfactor', 'inputspec.scalingfactor')]),
                      # (featpre_wffilt, featfit_wffilt, [('outputspec.filtered_functional_data', 'inputspec.input'),
                      #                                   ('outputspec.mean_file', 'inputspec.mean'),
                      #                                   ('outputspec.scalingfactor', 'inputspec.scalingfactor')]),
                      # (featpre_wfsim, featfit_wfsim, [('outputspec.filtered_functional_data', 'inputspec.input'),
                      #                                   ('outputspec.mean_file', 'inputspec.mean'),
                      #                                   ('outputspec.scalingfactor', 'inputspec.scalingfactor')]),
                      # (reg_wf, funcreg_wfge, [('outputspec.xfmge', 'inputspec.xfm')]),
                      # (reg_wf, funcreg_wfse, [('outputspec.xfmse', 'inputspec.xfm')]),
                      # (reg_wf, funcreg_wffilt, [('outputspec.xfmge', 'inputspec.xfm')]),
                      # (reg_wf, funcreg_wfsim, [('outputspec.xfmge', 'inputspec.xfm')]),
                      # (reg_wf, suscreg_wf, [('outputspec.xfmgre', 'inputspec.xfm')]),
                      # (featfit_wf, funcreg_wfge, [('outputspec.pcfunc_maps', 'inputspec.funcs'),
                      #                          ('outputspec.cnr_file', 'inputspec.cnr'),
                      #                          # ('outputspec.snr_file', 'inputspec.snr'),
                      #                          ('outputspec.zstat_file', 'inputspec.zstat'),
                      #                             ('outputspec.noise_file', 'inputspec.noise'),
                      #                                   ('outputspec.physionoise_file', 'inputspec.noisephysio')]),
                      #
                      # (featpre_wf, funcreg_wfge, [('outputspec.snr_file', 'inputspec.snr'),
                      #                             # ('outputspec.noise_file', 'inputspec.noise')
                      #                             ]),
                      # (featpre_wf, n3meange, [('outputspec.mean_run', 'in_file')]),
                      # (n3meange, funcreg_wfge, [('out_file', 'inputspec.mean')]),
                      # (featfit_wfse, funcreg_wfse, [('outputspec.pcfunc_maps', 'inputspec.funcs'),
                      #                            # ('outputspec.snr_file', 'inputspec.snr'),
                      #                            ('outputspec.cnr_file', 'inputspec.cnr'),
                      #                            ('outputspec.zstat_file', 'inputspec.zstat'),
                      #                             ('outputspec.noise_file', 'inputspec.noise'),
                      #                                   ('outputspec.physionoise_file', 'inputspec.noisephysio')]),
                      # (featpre_wfse, funcreg_wfse, [('outputspec.snr_file', 'inputspec.snr'),
                      #                             # ('outputspec.noise_file', 'inputspec.noise')
                      #                               ]),
                      # (featpre_wfse, n3meanse, [('outputspec.mean_run', 'in_file')]),
                      # (n3meanse, funcreg_wfse, [('out_file', 'inputspec.mean')]),
                      # (featfit_wffilt, funcreg_wffilt, [('outputspec.pcfunc_maps', 'inputspec.funcs'),
                      #                                ('outputspec.cnr_file', 'inputspec.cnr'),
                      #                                ('outputspec.zstat_file', 'inputspec.zstat'),
                      #                             ('outputspec.noise_file', 'inputspec.noise'),
                      #                                   ('outputspec.physionoise_file', 'inputspec.noisephysio'),]),
                      # (featpre_wffilt, funcreg_wffilt, [('outputspec.snr_file', 'inputspec.snr'),
                      #                             # ('outputspec.noise_file', 'inputspec.noise')
                      #                                   ]),
                      # (featpre_wffilt, n3meanfilt, [('outputspec.mean_run', 'in_file')]),
                      # (n3meanfilt, funcreg_wffilt, [('out_file', 'inputspec.mean')]),
                      # (featfit_wfsim, funcreg_wfsim, [('outputspec.pcfunc_maps', 'inputspec.funcs'),
                      #                                   ('outputspec.cnr_file', 'inputspec.cnr'),
                      #                                   ('outputspec.zstat_file', 'inputspec.zstat'),
                      #                                   ('outputspec.noise_file', 'inputspec.noise'),
                      #                                   ('outputspec.physionoise_file', 'inputspec.noisephysio'), ]),
                      # (featpre_wfsim, funcreg_wfsim, [('outputspec.snr_file', 'inputspec.snr'),
                      #                                   # ('outputspec.noise_file', 'inputspec.noise')
                      #                                   ]),
                      # (featpre_wfsim, n3meansim, [('outputspec.mean_run', 'in_file')]),
                      # (featfit_wf, sortcopesge, [('inputspec.input', 'in_files'),
                      #                            ('outputspec.copes', 'copes')]),
                      # (featfit_wf, sortvarcopesge, [('inputspec.input', 'in_files'),
                      #                            ('outputspec.varcopes', 'copes')]),
                      # (sortcopesge, featfixed_wfge, [('out_file', 'inputspec.copes')]),
                      # (sortvarcopesge, featfixed_wfge, [('out_file', 'inputspec.varcopes')]),
                      # (featfit_wfse, sortcopesse, [('inputspec.input', 'in_files'),
                      #                            ('outputspec.copes', 'copes')]),
                      # (featfit_wfse, sortvarcopesse, [('inputspec.input', 'in_files'),
                      #                               ('outputspec.varcopes', 'copes')]),
                      # (sortcopesse, featfixed_wfse, [('out_file', 'inputspec.copes')]),
                      # (sortvarcopesse, featfixed_wfse, [('out_file', 'inputspec.varcopes')]),
                      # (featfit_wffilt, sortcopesfilt, [('inputspec.input', 'in_files'),
                      #                            ('outputspec.copes', 'copes')]),
                      # (featfit_wffilt, sortvarcopesfilt, [('inputspec.input', 'in_files'),
                      #                               ('outputspec.varcopes', 'copes')]),
                      # (sortcopesfilt, featfixed_wffilt, [('out_file', 'inputspec.copes')]),
                      # (sortvarcopesfilt, featfixed_wffilt, [('out_file', 'inputspec.varcopes')]),
                      # (featfit_wfsim, sortcopessim, [('inputspec.input', 'in_files'),
                      #                            ('outputspec.copes', 'copes')]),
                      # (featfit_wfsim, sortvarcopessim, [('inputspec.input', 'in_files'),
                      #                               ('outputspec.varcopes', 'copes')]),
                      # (sortcopessim, featfixed_wfsim, [('out_file', 'inputspec.copes')]),
                      # (sortvarcopessim, featfixed_wfsim, [('out_file', 'inputspec.varcopes')]),
                      # (preproc_mag_wfge, featfixed_wfge, [('outputspec.mask_file', 'inputspec.mask_file')]),
                      # (preproc_mag_wfse, featfixed_wfse, [('outputspec.mask_file', 'inputspec.mask_file')]),
                      # (preproc_mag_wfge, featfixed_wffilt, [('outputspec.mask_file', 'inputspec.mask_file')]),
                      # (preproc_mag_wfge, featfixed_wfsim, [('outputspec.mask_file', 'inputspec.mask_file')]),
                      # # (featpre_wf, peristim_ge, [('outputspec.filtered_functional_data', 'in_file')]),
                      # # (featpre_wfse, peristim_se, [('outputspec.filtered_functional_data', 'in_file')]),
                      # # (featpre_wffilt, peristim_filt, [('outputspec.filtered_functional_data', 'in_file')]),
                      # # (featpre_wfsim, peristim_sim, [('outputspec.filtered_functional_data', 'in_file')]),
                      # (n3meansim, funcreg_wfsim, [('out_file', 'inputspec.mean')]),
                      # (featfixed_wfge, funcregfixed_wfge, [('outputspec.pval', 'inputspec.funcs')]),
                      # (featfixed_wfse, funcregfixed_wfse, [('outputspec.pval', 'inputspec.funcs')]),
                      # (featfixed_wffilt, funcregfixed_wffilt, [('outputspec.pval', 'inputspec.funcs')]),
                      # (featfixed_wfsim, funcregfixed_wfsim, [('outputspec.pval', 'inputspec.funcs')]),
                      # (make_surfs_wf, funcregfixed_wfge, [('outputspec.norm_struct_brain', 'inputspec.norm_struct_brain')]),
                      # (make_surfs_wf, funcregfixed_wfse, [('outputspec.norm_struct_brain', 'inputspec.norm_struct_brain')]),
                      # (make_surfs_wf, funcregfixed_wffilt, [('outputspec.norm_struct_brain', 'inputspec.norm_struct_brain')]),
                      # (make_surfs_wf, funcregfixed_wfsim, [('outputspec.norm_struct_brain', 'inputspec.norm_struct_brain')]),
                      # (reg_wf, funcregfixed_wfge, [('outputspec.xfmge', 'inputspec.xfm')]),
                      # (reg_wf, funcregfixed_wfse, [('outputspec.xfmse', 'inputspec.xfm')]),
                      # (reg_wf, funcregfixed_wffilt, [('outputspec.xfmge', 'inputspec.xfm')]),
                      # (reg_wf, funcregfixed_wfsim, [('outputspec.xfmge', 'inputspec.xfm')]),
                      # (funcreg_wfge, visuotopy_wf, [(('reg_func.output_image', trimvisuotopy), 'inputspec.func')]),
                      # (make_surfs_wf, visuotopy_wf, [('outputspec.white', 'inputspec.white'),
                      #                                ('outputspec.curv', 'inputspec.curv')]),
                      ])



    # for d in surf:
    #     metaflow.connect([(visuotopy_wf, vol2surf_wfs[d], [('outputspec.lsurfs', 'inputspec.lsurf'),
    #                                  ('outputspec.rsurfs', 'inputspec.rsurf')]),
    #                       (grabber, vol2surf_wfs[d], [('infosource.subject_id', 'inputspec.subject_id'),
    #                                               ('patchgrab.lhpatch', 'inputspec.lhpatch'),
    #                                               ('patchgrab.rhpatch', 'inputspec.rhpatch')]),
    #                       ])
    #
    # metaflow.connect([(funcreg_wfge, vol2surf_wfs['ge'], [(('reg_func.output_image', trimcheckboard), 'inputspec.image')]),
    #                   (funcreg_wfse, vol2surf_wfs['se'], [(('reg_func.output_image', trimcheckboard), 'inputspec.image')]),
    #                   (funcreg_wfsim, vol2surf_wfs['sim'], [(('reg_func.output_image', trimcheckboard), 'inputspec.image')]),
    #                   (funcreg_wffilt, vol2surf_wfs['filt'], [(('reg_func.output_image', trimcheckboard), 'inputspec.image')]),
    #                   (funcreg_wfge, vol2surf_wfs['gemean'], [(('reg_mean.output_image', trimcheckboard), 'inputspec.image')]),
    #                   (funcreg_wfse, vol2surf_wfs['semean'], [(('reg_mean.output_image', trimcheckboard), 'inputspec.image')]),
    #                   (reg_r2, vol2surf_wfs['r2'], [(('output_image', trimcheckboard), 'inputspec.image')]),
    #                   (reg_vessel, vol2surf_wfs['r2starvessel'], [('output_image', 'inputspec.image')]),
    #                   (suscreg_wf, vol2surf_wfs['r2star'], [('reg_r2star.output_image', 'inputspec.image')]),
    #                   (make_surfs_wf, vol2surf_wfs['V1'], [('outputspec.V1mask', 'inputspec.image')]),
    #                   (make_surfs_wf, vol2surf_wfs['T1w'], [('outputspec.norm_struct_brain', 'inputspec.image')])
    #                   ])
    #
    # # test fmriprep sinking
    # output_dir = '/cfmm/data/ostanle2/Ongoing_Work/SEvsGE2/sink'
    # registersink = pe.MapNode(DerivativesDataSink(base_directory=output_dir,suffix='mean',
    #                                               space='T1w'),
    #                           name='registersink', iterfield=['source_file', 'in_file'])
    #
    # tsnrsink = pe.MapNode(DerivativesDataSink(base_directory=output_dir,suffix='tsnr',
    #                                               space='native'),
    #                           name='tsnrsink', iterfield=['source_file', 'in_file'])
    #
    # phasevarsink = pe.MapNode(DerivativesDataSink(base_directory=output_dir,suffix='tstd',
    #                                               space='native'),
    #                           name='phasevarsink', iterfield=['source_file', 'in_file'])
    #
    # mocosink = pe.MapNode(DerivativesDataSink(base_directory=output_dir,
    #                                           suffix='mocoparams'),
    #                name='mocosink', iterfield=['source_file', 'in_file'])
    #
    # pvalgesink = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                             desc='ge',suffix='pval', keep_dtype=True),
    #                         name='pvalgesink', iterfield=['source_file', 'in_file'])
    #
    # pvalsesink = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                             desc='se', suffix='pval', keep_dtype=True),
    #                         name='pvalsesink', iterfield=['source_file', 'in_file'])
    #
    # pvalfiltsink = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                           suffix='pval', desc='filt', keep_dtype=True),
    #                name='pvalfiltsink', iterfield=['source_file', 'in_file'])
    #
    # pvalsimsink = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                           suffix='pval', desc='sim', keep_dtype=True),
    #                name='pvalsimsink', iterfield=['source_file', 'in_file'])
    #
    # vesselsink = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                           suffix='swi'),
    #                name='vesselsink', iterfield=['source_file', 'in_file'])
    #
    # anatsink = pe.Node(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                        keep_dtype=True), name='anatsink')
    #
    # r2star_regsink = pe.Node(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                              desc='r2starreg',
    #                                        keep_dtype=True), name='r2star_regsink')
    #
    # V1sink = pe.Node(DerivativesDataSink(base_directory=output_dir, space='native',
    #                                      suffix='mask'), name='V1sink')
    #
    # r2sink = pe.MapNode(DerivativesDataSink(base_directory=output_dir,suffix='rsquared',
    #                                               space='native'),
    #                           name='r2sink', iterfield=['source_file', 'in_file'])
    #
    # # We have a lot of things that need to go to a surface lets make a list
    # lsurfsink={}
    # rsurfsink={}
    # for d in surf:
    #     lsurfsink[d] = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='lhemi',
    #                                                keep_dtype=True),
    #                    name='lsurfsink'+d, iterfield=['source_file', 'in_file', 'desc'], nested=True)
    #
    #     rsurfsink[d] = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='rhemi',
    #                                                   keep_dtype=True),
    #                              name='rsurfsink'+d, iterfield=['source_file', 'in_file', 'desc'], nested=True)
    #
    # lcurvsink = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='lhemi',
    #                                            keep_dtype=True, desc='lcurv'),
    #                name='lcurvsink', iterfield=['source_file', 'in_file'], nested=True)
    #
    # rcurvsink = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='rhemi',
    #                                               keep_dtype=True, desc='rcurv'),
    #                          name='rcurvsink', iterfield=['source_file', 'in_file'], nested=True)
    #
    # funcsink={}
    # volsink = ['ge', 'se', 'filt', 'sim']
    # for d in surf:
    #     funcsink[d] = pe.MapNode(DerivativesDataSink(base_directory=output_dir, space='T1w',
    #                                                keep_dtype=True),
    #                    name='funcsink'+d, iterfield=['source_file', 'in_file'], nested=True)
    #
    # metaflow.connect([(sortmagphase,registersink, [('maglist', 'source_file')]),
    #                   (sortmagphase,mocosink, [('maglist', 'source_file')]),
    #                   (sortmagphase,pvalgesink, [(('maglist', pickfirst), 'source_file')]),
    #                   (sortmagphase,pvalfiltsink, [(('maglist', pickfirst), 'source_file')]),
    #                   (sortmagphase,pvalsimsink, [(('maglist', pickfirst), 'source_file')]),
    #                   (sortmagphase,V1sink, [(('maglist', pickfirst), 'source_file')]),
    #                   (sortmagphase,tsnrsink, [('maglist', 'source_file')]),
    #                   (sortmagphase,r2sink, [('maglist', 'source_file')]),
    #                   (sortmagphase,phasevarsink, [('phaselist', 'source_file')]),
    #                   (sortmagphase,vesselsink, [(('maglist', pickfirst), 'source_file')]),
    #                   (grabber,pvalsesink, [(('selectSE.out', pickfirst), 'source_file')]),
    #                   (grabber, anatsink, [('selectstruct.out', 'source_file')]),
    #                   (grabber, lcurvsink, [('selectstruct.out', 'source_file')]),
    #                   (grabber, rcurvsink, [('selectstruct.out', 'source_file')]),
    #                   (grabber, r2star_regsink, [('selectstruct.out', 'source_file')]),
    #                   (funcreg_wfge, registersink, [('reg_mean.output_image', 'in_file')]),
    #                   (preproc_mag_wfge, mocosink, [('outputspec.motion_par', 'in_file')]),
    #                   (funcregfixed_wfge, pvalgesink, [('reg_func.output_image', 'in_file')]),
    #                   (funcregfixed_wfse, pvalsesink, [('reg_func.output_image', 'in_file')]),
    #                   (funcregfixed_wffilt, pvalfiltsink, [('reg_func.output_image', 'in_file')]),
    #                   (funcregfixed_wfsim, pvalsimsink, [('reg_func.output_image', 'in_file')]),
    #                   (reg_vessel, vesselsink, [('output_image', 'in_file')]),
    #                   (make_surfs_wf, anatsink, [('outputspec.norm_struct_brain', 'in_file')]),
    #                   (regV1tofunc, V1sink, [('output_image', 'in_file')]),
    #                   (preproc_phase_wfge, phasevarsink, [('outputspec.std_phase', 'in_file')]),
    #                   (preproc_mag_wfge, tsnrsink, [('outputspec.snr', 'in_file')]),
    #                   (phaseregress, r2sink, [('corr', 'in_file')]),
    #                   (visuotopy_wf, lcurvsink, [('outputspec.lcurv', 'in_file')]),
    #                   (visuotopy_wf, rcurvsink, [('outputspec.rcurv', 'in_file')]),
    #                   (suscreg_wf, r2star_regsink, [('reg_r2star.output_image', 'in_file')]),
    #                   ])
    #
    # for d in surf:
    #     metaflow.connect([
    #         (vol2surf_wfs[d], lsurfsink[d], [('outputspec.lhemiproj', 'in_file'),
    #                                            (('outputspec.lhemiproj', getdepth), 'desc')]),
    #         (vol2surf_wfs[d], rsurfsink[d], [('outputspec.rhemiproj', 'in_file'),
    #                                            (('outputspec.rhemiproj', getdepth), 'desc')]),
    #     ])
    #     if d=='r2star' or d=='r2starvessel':
    #         metaflow.connect([
    #             (grabber, lsurfsink[d], [(('selectgremag.out', pickfirst), 'source_file')]),
    #             (grabber, rsurfsink[d], [(('selectgremag.out', pickfirst), 'source_file')]),
    #         ])
    #     elif d=='V1':
    #         metaflow.connect([
    #             (grabber, lsurfsink[d], [('selectstruct.out', 'source_file')]),
    #             (grabber, rsurfsink[d], [('selectstruct.out', 'source_file')]),
    #         ])
    #     else:
    #         metaflow.connect([
    #             (sortmagphase, lsurfsink[d], [(('maglist', trimcheckboard), 'source_file')]),
    #             (sortmagphase, rsurfsink[d], [(('maglist', trimcheckboard), 'source_file')]),
    #         ])
    #     if d in volsink:
    #         if d != 'se' or d != 'semean':
    #             metaflow.connect([(sortmagphase,funcsink[d], [('maglist', 'source_file')])])
    #             if d=='ge':
    #                 metaflow.connect([(funcreg_wfge, funcsink[d], [('reg_func.output_image', 'in_file')])])
    #             elif d=='filt':
    #                 metaflow.connect([(funcreg_wffilt, funcsink[d], [('reg_func.output_image', 'in_file')])])
    #             elif d=='sim':
    #                 metaflow.connect([(funcreg_wfsim, funcsink[d], [('reg_func.output_image', 'in_file')])])
    #             elif d=='gemean':
    #                 metaflow.connect([(funcreg_wfge, funcsink[d], [('reg_mean.output_image', 'in_file')])])
    #         else:
    #             metaflow.connect([(grabber, funcsink[d], [('selectSE.out', 'source_file')])])
    #             if d=='se':
    #                 metaflow.connect([(funcreg_wfse, funcsink[d], [('reg_func.output_image', 'in_file')])])
    #             elif d=='semean':
    #                 metaflow.connect([(funcreg_wfse, funcsink[d], [('reg_mean.output_image', 'in_file')])])


    metaflow.write_graph(graph2use='colored', format='png', simple_form=False)

    metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 16}) #, updatehash=True)

    print('Ran ' + str(len(subject_list)) + ' subjects, took ' + str(time.time()-start) + ' seconds.')

if __name__=='__main__':
    from argparse import ArgumentParser, RawTextHelpFormatter
    parser = ArgumentParser(description=__doc__, formatter_class=RawTextHelpFormatter)
    parser.add_argument('subject_list', default=['.*'], nargs='+', help='labels of subjects to analyze')
    args = parser.parse_args()

    subject_list = args.subject_list
    run_pipeline(subject_list)
