# last working commit id 40912f734d20fd5fcc8f8c3a1500814eacba4257

from nipype import config
config.enable_debug_mode()
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.prep_anat_wf import create_prep_anat_wf
from nipype_phase_regression.workflows.apply_phase_wf import create_apply_phase_wf
# from nipype_phase_regression.workflows.plot_result_wf import create_plot_result_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
import nipype_phase_regression.interfaces as pr
import dicom
import os
import nipype.pipeline.engine as pe
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype.interfaces.utility as ul
from numpy import linspace
from nipype.interfaces.base import Bunch

# set default output type
fsl.FSLCommand.set_default_output_type('NIFTI_GZ')

#a bunch of directories used in the workflows
data_base_dir="/cfmm/data/ostanle2/RAWDATA/"
dcm_folder_name="2016_04_11"
dcm_dir=data_base_dir+dcm_folder_name+"/"

base_dir="/workspace/ostanle2/regression_wf_test"
processing_base_dir=base_dir+"processed/"+dcm_folder_name+"_processed_nomc/"
dcm_dir_sorted=base_dir+"data/"+dcm_folder_name+"_sorted/"
mask_dir=base_dir+"data/masks/"
nifti_dir=processing_base_dir+dcm_folder_name+"_nii/"

imageList = ['mbep2d_bold_mb2_p2_2iso_nmc','mprage_sag_750iso_p3_914']
ignoreSeries = [11,12]

#create a folder to store processed data
if not os.path.exists(processing_base_dir):
    os.makedirs(processing_base_dir)

# create a sorted directory of symbolic links to the original data
seriesName = ""
if not os.path.exists(dcm_dir_sorted):
    os.makedirs(dcm_dir_sorted)
    print 'creating sorted directory of symbolic links to original data'
    for path, subdirs, files in os.walk(dcm_dir):
        for name in files:  # this could be parallelized
            src_file = os.path.join(path, name)
            try:
                ds = dicom.read_file(src_file, stop_before_pixels=True)
            except dicom.filereader.InvalidDicomError:
                continue  # skip non-dicom file
            if ds.ProtocolName in imageList and ds.SeriesNumber not in ignoreSeries:
                if ds.StudyDescription == "Menon^Olivia":
                    seriesName = ds.ProtocolName + "ImageKind_" + str(ds.ImageType[2]) + '_' + "Conventional_Combination" + "_Series" + str(ds.SeriesNumber).zfill(4)
                elif ds.StudyDescription == "Gadgetron^SVD":
                    seriesName = ds.ProtocolName + "ImageKind_" + str(ds.ImageType[2]) + '_' + "SVD_Combination" + "_Series" + str(ds.SeriesNumber).zfill(4)
                dst_dir = os.path.join(dcm_dir_sorted, seriesName)
                if not os.path.exists(dst_dir):
                    os.makedirs(dst_dir)
                os.symlink(src_file, os.path.join(dst_dir, name))
    print 'symlinks made'
else:
    print 'using existing symlinks'

# PREPROCESS MAGNITUDE DATA
# Run preprocessing workflows separately (as recommended to avoid re-runs)
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=processing_base_dir

# Take grabbing initial data out of dicom worflows to allow for good templating
# grab data
grabber1 = pe.Node(interface=nio.DataGrabber(), name="grabber1")
grabber1.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_M_S*/*Image_SLC0_CON0_PHS0_REP0*.dcm'
grabber1.inputs.sort_filelist = True
grabber1.inputs.base_directory=dcm_dir_sorted

grabber2 = pe.Node(interface=nio.DataGrabber(), name="grabber2")
grabber2.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_M_C*/*.0001*.IMA'
grabber2.inputs.sort_filelist = True
grabber2.inputs.base_directory=dcm_dir_sorted

mergemag = pe.Node(interface=ul.Merge(2), name="mergemag")

preproc_mag_wf.disconnect(preproc_mag_wf.get_node('extractor'), 'out_file', preproc_mag_wf.get_node('mcflirter'), 'in_file')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('mcflirter'), 'out_file', preproc_mag_wf.get_node('detrendmag'), 'mag')
mcflirt=preproc_mag_wf.get_node('mcflirter')
preproc_mag_wf.remove_nodes([mcflirt])

preproc_mag_wf.connect(grabber1, "outfiles", mergemag, "in1")
preproc_mag_wf.connect(grabber2, "outfiles", mergemag, "in2")
preproc_mag_wf.connect(mergemag, "out", preproc_mag_wf.get_node('dataCleaner'), 'dcm')
preproc_mag_wf.connect(mergemag, 'out', preproc_mag_wf.get_node('converter'), 'in_file')
preproc_mag_wf.connect(preproc_mag_wf.get_node('extractor'), 'out_file', preproc_mag_wf.get_node('detrendmag'), 'mag')

# preproc_mag_exec_graph = preproc_mag_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4, 'n_procs' : 12})

# PREPROCESS PHASE DATA
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=processing_base_dir

# grab data
grabberp1 = pe.Node(interface=nio.DataGrabber(), name="grabberp1")
grabberp1.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_P_S*/*Image_SLC0_CON0_PHS0_REP0*.dcm'
grabberp1.inputs.sort_filelist = True
grabberp1.inputs.base_directory=dcm_dir_sorted

grabberp2 = pe.Node(interface=nio.DataGrabber(), name="grabberp2")
grabberp2.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_P_C*/*.0001.*.IMA'
grabberp2.inputs.sort_filelist = True
grabberp2.inputs.base_directory=dcm_dir_sorted

mergephase = pe.Node(interface=ul.Merge(2), name="mergephase")

preproc_phase_wf.connect(grabberp1, "outfiles", mergephase, "in1")
preproc_phase_wf.connect(grabberp2, "outfiles", mergephase, "in2")
preproc_phase_wf.connect(mergephase, "out", preproc_phase_wf.get_node('dataCleaner'), 'dcm')
preproc_phase_wf.connect(mergephase, 'out', preproc_phase_wf.get_node('converter'), 'in_file')

# Out of workflow for customization
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# preproc_phase_exec_graph = preproc_phase_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4, 'n_procs' : 12})

# PREPROCESS ANATOMICAL DATA
prep_anat_wf = create_prep_anat_wf()
prep_anat_wf.base_dir=processing_base_dir

# grab data
grabber = pe.Node(nio.DataGrabber(), name="grabber")
grabber.inputs.base_directory = dcm_dir_sorted
grabber.inputs.template = 'mprage*/*.0019.0001.*.IMA'
grabber.inputs.sort_filelist = True

prep_anat_wf.connect(grabber, 'outfiles', prep_anat_wf.get_node('converter'), 'in_file')
prep_anat_wf.connect(grabber, 'outfiles', prep_anat_wf.get_node('dataCleaner'), 'dcm')

# APPLY FILTERS
apply_phase_wf = create_apply_phase_wf()
apply_phase_wf.base_dir=processing_base_dir
# apply_phase_wf.inputs.grabmag.base_directory = processing_base_dir
# apply_phase_wf.inputs.grabphase.base_directory = processing_base_dir

# apply_phase_exec_graph = apply_phase_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})

# apply_phase_exec_graph = apply_phase_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4, 'n_procs' : 12})

mergefilters = pe.Node(interface=ul.Merge(8),name="mergefilters")

# Perform GLM on data
level1_wf = create_first_level_analysis_wf()
level1_wf.base_dir=processing_base_dir
# level1_wf.inputs.grabtc.base_directory=processing_base_dir

# set out experiment
onsets = list(linspace(25, 175, 3))
exp_info = [Bunch(conditions=["Checkerboard"], onsets=[onsets], durations=[[25]])]
level1_wf.inputs.makemodel.input_units = 'secs'
level1_wf.inputs.makemodel.time_repetition = 1.25
level1_wf.inputs.makemodel.high_pass_filter_cutoff = 0
level1_wf.inputs.makemodel.subject_info = exp_info * 46

level1_wf.inputs.level1design.interscan_interval = 1.25
level1_wf.inputs.level1design.bases = {'dgamma':{'derivs': False}}
level1_wf.inputs.level1design.model_serial_correlations = True

# level1_wf_exec_graph = level1_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4, 'n_procs' : 12})

# Calculate percent change maps for all nodes
percentchange = pe.MapNode(interface=pr.percentChange(), name="percentchange", iterfield=["func"])
percentchange.inputs.blockLength = 25
percentchange.inputs.TR = 1.25
percentchange.inputs.blockPattern = [0, 1, 0, 1, 0, 1, 0]

# Create data sink
# data sink to dump stuff in
ds = pe.Node(interface=nio.DataSink(), name="ds")
ds.inputs.regexp_substitutions = [('_[A-z]+[0-9]/', '/'),
                                  ('_[A-z]+[0-9][0-9]/', '/'),
                                  ('_[A-z]+[0-9][0-9][0-9]/', '/')]
ds.inputs.substitutions = [('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0013.0001.2016.04.11.10.13.53.671875.47514717_out_desript_reoriented',
    'Run1CCOMBmag'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0015.0001.2016.04.11.10.13.53.671875.47521889_out_desript_reoriented',
    'Run2CCOMBmag'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0017.0001.2016.04.11.10.13.53.671875.47529061_out_desript_reoriented',
    'Run3CCOMBmag'),
    ('data_2016_04_11_09_45_09_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run1SVDCOMBmag'),
    ('data_2016_04_11_09_49_58_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run2SVDCOMBmag'),
    ('data_2016_04_11_09_54_06_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run3SVDCOMBmag'),
    ('_brain_detrendmag','processed'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0014.0001.2016.04.11.10.13.53.671875.47515146_out_desript_reoriented', 'Run1CCOMBphase'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0016.0001.2016.04.11.10.13.53.671875.47522408_out_desript_reoriented', 'Run2CCOMBphase'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0018.0001.2016.04.11.10.13.53.671875.47529634_out_desript_reoriented', 'Run3CCOMBphase'),
    ('data_2016_04_11_09_45_09_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run1SVDCOMBphase'),
    ('data_2016_04_11_09_49_58_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run2SVDCOMBphase'),
    ('data_2016_04_11_09_54_06_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run3SVDCOMBphase'),
    ('_detrendphase','processed')]

# # PLOT RESULTS
# plot_result_wf = create_plot_result_wf()
# plot_result_wf.base_dir=processing_base_dir
# plot_result_wf.inputs.grabmag.base_directory=processing_base_dir
# plot_result_wf.inputs.grabphase.base_directory=processing_base_dir
# plot_result_wf.inputs.grabsim.base_directory=processing_base_dir
# plot_result_wf.inputs.grabfilt.base_directory=processing_base_dir
# plot_result_wf.inputs.grabconvolve.base_directory=processing_base_dir
# plot_result_wf.inputs.grabcrosscorr.base_directory=processing_base_dir
# plot_result_wf.inputs.grabmask.base_directory=mask_dir
#
# plot_result_exec_graph = plot_result_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})

# CREATE META WF AND ATTACH AND RUN
metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = processing_base_dir
metaflow.connect([(preproc_phase_wf, apply_phase_wf, [('prepPhase.detrended_phase', 'bpftphase.in_file'),
                                                     ('prepPhase.detrended_phase', 'mergeprphase.in1'),
                                                     ('prepPhase.detrended_phase', 'windowphase.in_file'),
                                                     ('prepPhase.detrended_phase', 'savitzkygolay.phase'),
                                                     ('prepPhase.detrended_phase', 'orderfit.phase')]),
                  (preproc_mag_wf, apply_phase_wf, [('detrendmag.detrended_mag', 'bpftmag.in_file'),
                                                    ('detrendmag.detrended_mag', 'mergeprmag.in1'),
                                                    ('detrendmag.detrended_mag', 'windowmag.in_file'),
                                                    ('detrendmag.detrended_mag', 'savitzkygolay.mag'),
                                                    ('detrendmag.detrended_mag', 'orderfit.mag'),
                                                    ('detrendmag.detrended_mag', 'ordersim.mag')]),
                  (preproc_mag_wf, mergefilters, [('flipper.out_file','in1')]),
                  (preproc_phase_wf, mergefilters, [('flipper.out_file','in2')]),
                  (apply_phase_wf, mergefilters, [('bpftmag.out_file','in3'),
                                                  ('windowmag.filt_tc', 'in4'),
                                                  ('mergeoutsim.out', 'in5'),
                                                  ('convolve.convolve_tc', 'in6'),
                                                  ('crosscorr.crosscorr_tc', 'in7'),
                                                  ('blur.smoothed_file', 'in8')]),
                  # (mergefilters, level1_wf, [('out', 'makemodel.functional_runs'),
                  #                            ('out', 'modelestimate.in_file')]),
                  (mergefilters, percentchange, [('out', 'func')]),
                  (preproc_mag_wf, ds, [('detrendmag.detrended_mag', 'mag'),
                                        ('SNRcalc.out_file', 'mag.@tSNR')]),
                  (preproc_phase_wf, ds, [('prepPhase.detrended_phase', 'phase'),
                                          ('varcalc.out_file', 'phase.@var')]),
                  (prep_anat_wf, ds, [('extractor.out_file', 'anat.@BET'),
                                      ('flipper.out_file', 'anat')]),
                  (apply_phase_wf, ds, [('bpftmag.out_file', 'tc.@BPmag'),
                                        ('bpftphase.out_file', 'tc.@phase.BP'),
                                        ('windowmag.filt_tc', 'tc.@WINmag'),
                                        ('windowphase.filt_tc', 'tc.@phase.WIN'),
                                        ('applyfit.out_sim', 'tc.@applysim'),
                                        ('applyfit.out_filt', 'tc.@applyfilt'),
                                        ('savitzkygolay.sim', 'tc.@SGsim'),
                                        ('savitzkygolay.filt', 'tc.@SGfilt'),
                                        ('phaseregression.sim', 'tc.@PRsim'),
                                        ('phaseregression.filt', 'tc.@PRfilt'),
                                        ('convolve.convolve_tc', 'tc.@convove'),
                                        ('crosscorr.crosscorr_tc', 'tc.@crosscorr'),
                                        ('blur.smoothed_file', 'tc.@blur')]),
                  (percentchange, ds, [('pc_func', 'pcmaps')])
                  ])
metaflow.write_graph(graph2use='colored', format='png', simple_form=False)
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})
