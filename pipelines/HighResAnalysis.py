from nipype_phase_regression.workflows.preproc_mag_highres_wf import create_preproc_mag_highres_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr
from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                       create_fixed_effects_flow)
import nipype.algorithms.modelgen as model
from nipype.interfaces.base import Bunch
import nipype.interfaces.utility as ul
import nipype.interfaces.io as io
from nipype.interfaces import spm, fsl, afni

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def getmiddlevolume(func):
    from nibabel import load
    funcfile = func
    if isinstance(func, list):
        funcfile = func[0]
    _, _, _, timepoints = load(funcfile).shape
    return int(timepoints / 2) - 1

def getthreshop(thresh):
    return '-thr %.10f -Tmin -bin' % (0.1 * thresh[0][1])

def getinormscale(medianvals):
    return ['-mul %.10f' % (10000. / val) for val in medianvals]

def sort_copes(files):
    numelements = len(files[0])
    outfiles = []
    for i in range(numelements):
        outfiles.insert(i, [])
        for j, elements in enumerate(files):
            outfiles[i].append(elements[i])
    return outfiles

def num_copes(files):
    return len(files)


# basedir = '/workspace/ostanle2/ODC_Stim2'
# maglist = [basedir+'/orig/20170222Run1ODCStimmag.nii.gz',
#            basedir+'/orig/20170222Run2ODCStimmag.nii.gz',
#            basedir+'/orig/20170222Run3ODCStimmag.nii.gz',
#            basedir + '/orig/20170222Run4ODCStimmag.nii.gz',
#            basedir + '/orig/20170222Run5ODCStimmag.nii.gz']
# phaselist = [basedir+'/orig/20170222Run1ODCStimphase.nii.gz',
#              basedir+'/orig/20170222Run2ODCStimphase.nii.gz',
#              basedir+'/orig/20170222Run3ODCStimphase.nii.gz',
#              basedir + '/orig/20170222Run4ODCStimphase.nii.gz',
#              basedir + '/orig/20170222Run5ODCStimphase.nii.gz']
# output_dir = '/cfmm/data/ostanle2/ODC_Stim2/results'

basedir = '/workspace/ostanle2/ODC_Stim2'
output_dir = '/cfmm/data/ostanle2/ODC_Stim2'

subject_list=['20170222']

info=dict(mag = [['subject_id']], phase=[['subject_id']], struct = [['subject_id', 'mp2rage600']])
infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id'],
                                               outfields=['mag', 'phase', 'struct']),
                     name='datasource')
datasource.inputs.base_directory = basedir + '/orig/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(mag='%sRun*ODCStimmag.nii.gz',
                                  phase='%sRun*ODCStimphase.nii.gz',
                                  struct='%s%s.nii.gz')
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True
datasource.inputs.subject_id=subject_list[0]


# convert image to float
img2float = pe.MapNode(interface=fsl.ImageMaths(out_data_type='float', op_string='', suffix='_dtype'),
                       iterfield=['in_file'],
                       name='img2float')

# motion correct each run
volreg = pe.MapNode(interface = afni.Volreg(), name='volreg', iterfield='in_file')
volreg.inputs.outputtype='NIFTI_GZ'

# register each run to first volume of first run

# extract the middle volume of the first run
extract_ref = pe.Node(interface=fsl.ExtractROI(t_size=1), name='extract_ref')

# registration
align2first = pe.MapNode(interface=fsl.FLIRT(dof=6), name='align2first', iterfield=['in_file'])

# apply first volume registration
applyalign = pe.MapNode(interface=fsl.ApplyXfm(), name='applyalign', iterfield=['in_file', 'in_matrix_file'])
applyalign.inputs.apply_xfm=True

# get the mean functional of run 1
meanfunc = pe.Node(interface=fsl.ImageMaths(op_string='-Tmean',
                                            suffix='_mean'),
                   name='meanfunc')

# extract brain with fsl and save the mask
extractor = pe.Node(interface=fsl.BET(), name="extractor")
extractor.inputs.mask = True
extractor.inputs.padding = True
extractor.inputs.frac = 0.3

# apply the mask to all runs
maskfunc = pe.MapNode(interface=fsl.ImageMaths(suffix='_bet',
                                               op_string='-mas'),
                      iterfield=['in_file'],
                      name='maskfunc')

# Get 2 and 98th percentiles
getthresh = pe.MapNode(interface=fsl.ImageStats(op_string='-p 2 -p 98'),
                       iterfield=['in_file'],
                       name='getthreshold')

# Threshold the first run of the functional data at 10% of the 98th percentile
threshold = pe.Node(interface=fsl.ImageMaths(out_data_type='char',
                                             suffix='_thresh'),
                    name='threshold')

# get median value using the mask
medianval = pe.MapNode(interface=fsl.ImageStats(op_string='-k %s -p 50'),
                       iterfield=['in_file'],
                       name='medianval')

# dilate the mask
dilatemask = pe.Node(interface=fsl.ImageMaths(suffix='_dil',
                                              op_string='-dilF'),
                     name='dilatemask')

# mask the data with dilated mask
maskfunc2 = pe.MapNode(interface=fsl.ImageMaths(suffix='_mask',
                                                op_string='-mas'),
                       iterfield=['in_file'],
                       name='maskfunc2')

# get the mean from each run
meanfunc2 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc2')

# merge the mean and median
mergenode = pe.Node(interface=ul.Merge(2, axis='hstack'),
                    name='merge')

# scale the run to have a median of 10000
intnorm = pe.MapNode(interface=fsl.ImageMaths(suffix='_intnorm'),
                     iterfield=['in_file', 'op_string'],
                     name='intnorm')

# get the mean from each run
meanfunc3 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc3')

# highpass the data
highpass = pe.MapNode(interface=fsl.ImageMaths(suffix='_tempfilt'),
                      iterfield=['in_file','in_file2'],
                      name='highpass')
hpcutoff = 100
TR = 2.
highpass.inputs.suffix = '_hpf'
highpass.inputs.op_string = '-bptf %d -1 -add ' % (hpcutoff / TR)

preproc = pe.Workflow(name='preproc')
preproc.connect(infosource, 'subject_id', datasource, 'subject_id')
preproc.connect(datasource, 'mag', img2float, 'in_file')
preproc.connect(img2float, 'out_file', volreg, 'in_file')
preproc.connect(volreg, ('out_file', pickfirst), extract_ref, 'in_file')
preproc.connect(datasource, ('mag', getmiddlevolume), extract_ref, 't_min')
preproc.connect(volreg, 'out_file', align2first, 'in_file')
preproc.connect(extract_ref, 'roi_file', align2first, 'reference')
preproc.connect(extract_ref, 'roi_file', applyalign, 'reference')
preproc.connect(volreg, 'out_file', applyalign, 'in_file')
preproc.connect(align2first, 'out_matrix_file', applyalign, 'in_matrix_file')
preproc.connect(align2first, ('out_file', pickfirst), meanfunc, 'in_file')
preproc.connect(meanfunc, 'out_file', extractor, 'in_file')
preproc.connect(applyalign, 'out_file', maskfunc, 'in_file')
preproc.connect(extractor, 'mask_file', maskfunc, 'in_file2')
preproc.connect(maskfunc, 'out_file', getthresh, 'in_file')
preproc.connect(maskfunc, ('out_file', pickfirst), threshold, 'in_file')
preproc.connect(applyalign, 'out_file', medianval, 'in_file')
preproc.connect(threshold, 'out_file', medianval, 'mask_file')
preproc.connect(threshold, 'out_file', dilatemask, 'in_file')
preproc.connect(applyalign, 'out_file', maskfunc2, 'in_file')
preproc.connect(dilatemask, 'out_file', maskfunc2, 'in_file2')
preproc.connect(maskfunc2, 'out_file', meanfunc2, 'in_file')
preproc.connect(meanfunc2, 'out_file', mergenode, 'in1')
preproc.connect(medianval, 'out_stat', mergenode, 'in2')
preproc.connect(maskfunc2, 'out_file', intnorm, 'in_file')
preproc.connect(medianval, ('out_stat', getinormscale), intnorm, 'op_string')
preproc.connect(intnorm, 'out_file', meanfunc3, 'in_file')
preproc.connect(intnorm, 'out_file', highpass, 'in_file')
preproc.connect(meanfunc3, 'out_file', highpass, 'in_file2')

# Initialize Phase Preprocessing
preproc_phase = pe.MapNode(interface=pr.PreprocessPhase(), name='preproc_phase', iterfield=['phase', 'siemens'])

# Set conversion parameters
preproc_phase.inputs.siemens=[True] * 5
preproc_phase.inputs.bit_depth = 12

# use fslmaths to calculate phase var
varcalc = pe.MapNode(interface=fsl.maths.MathsCommand(), name="varcalc", iterfield=["in_file"])
varcalc.inputs.args = "-Tstd -sqr"


# First Level Workflow
hpcutoff = 120.
TR = 1.25

onsetsL = list([30., 120., 210., 300., 390.])
onsetsR = list([75., 165., 255., 345., 435.])
exp_info = [Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[15] * 5, [15] * 5]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[15] * 5, [15] * 5]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[15] * 5, [15] * 5]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[15] * 5, [15] * 5]),
            Bunch(conditions=["Left", "Right"], onsets=[onsetsL, onsetsR], durations=[[15] * 5, [15] * 5]),]

cont1 = ['Left>Baseline', 'T', ['Left', 'Right'], [1, 0]]
cont2 = ['Right>Baseline', 'T', ['Left', 'Right'], [0, 1]]
cont3 = ['Right>Left', 'T', ['Left', 'Right'], [-1, 1]]
cont4 = ['Task>Baseline', 'T', ['Left', 'Right'], [0.5,0.5]]
contrasts = [cont1, cont2, cont3, cont4]

modelfit = pe.Workflow(name='modelfit')

modelspec = pe.Node(interface=model.SpecifyModel(), name="modelspec")
modelspec.inputs.high_pass_filter_cutoff=hpcutoff
modelspec.inputs.time_repetition=TR
modelspec.inputs.input_units='secs'
modelspec.inputs.subject_info=exp_info

level1design = pe.Node(interface=fsl.Level1Design(), name="level1design")
level1design.inputs.interscan_interval=TR
level1design.inputs.bases = {'dgamma': {'derivs': False}}
level1design.inputs.model_serial_correlations = True
level1design.inputs.contrasts = [cont1, cont2, cont3, cont4]

modelgen = pe.MapNode(interface=fsl.FEATModel(), name='modelgen',
                      iterfield=['fsf_file', 'ev_files'])

modelestimate = pe.MapNode(interface=fsl.FILMGLS(smooth_autocorr=True,
                                                 mask_size=5,
                                                 threshold=1000),
                           name='modelestimate',
                           iterfield=['design_file', 'in_file', 'tcon_file'])

conestimate = pe.MapNode(interface=fsl.ContrastMgr(), name='conestimate',
                         iterfield=['tcon_file', 'param_estimates',
                                    'sigmasquareds', 'dof_file',
                                    ])

modelfit.connect([
    (modelspec, level1design, [('session_info', 'session_info')]),
    (level1design, modelgen, [('fsf_files', 'fsf_file'),
                              ('ev_files', 'ev_files')]),
    (modelgen, modelestimate, [('design_file', 'design_file'),
                               ('con_file', 'tcon_file')]),
])

fixed_fx = pe.Workflow(name='fixedfx')
copemerge = pe.MapNode(interface=fsl.Merge(dimension='t'),
                       iterfield=['in_files'],
                       name="copemerge")

varcopemerge = pe.MapNode(interface=fsl.Merge(dimension='t'),
                          iterfield=['in_files'],
                          name="varcopemerge")
level2model = pe.Node(interface=fsl.L2Model(),
                      name='l2model')
flameo = pe.MapNode(interface=fsl.FLAMEO(run_mode='fe'), name="flameo",
                    iterfield=['cope_file', 'var_cope_file'])

fixed_fx.connect([(copemerge, flameo, [('merged_file', 'cope_file')]),
                  (varcopemerge, flameo, [('merged_file', 'var_cope_file')]),
                  (level2model, flameo, [('design_mat', 'design_file'),
                                         ('design_con', 't_con_file'),
                                         ('design_grp', 'cov_split_file')]),
                  ])

reorientresults = pe.MapNode(interface=fsl.Reorient2Std(), name='reorientresults', iterfield=['in_file'])

func2struct = pe.MapNode(interface=fsl.ApplyXfm(), name='func2struct', iterfield=['in_file'])
func2struct.inputs.apply_xfm=True
func2struct.inputs.in_matrix_file='/cfmm/data/ostanle2/ODC_Stim2/orig/reg/Run2struct.mat'
func2struct.inputs.reference='/cfmm/data/ostanle2/ODC_Stim2/orig/mp2rage600_reori.nii.gz'

datasink = pe.Node(io.DataSink(base_directory=output_dir, infields=['image_quality.@phase']),name="datasink")

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = basedir

# connect preprocessing workflow
# metaflow.connect([(preproc, preproc_phase, [('datasource.phase', 'phase')])
#                   ])

metaflow.connect([(preproc, modelfit, [('highpass.out_file', 'modelspec.functional_runs'),
                                         ('highpass.out_file', 'modelestimate.in_file')]),
                  (preproc, fixed_fx, [(('maskfunc2.out_file', pickfirst), 'flameo.mask_file')]),
                  (modelfit, fixed_fx, [(('modelestimate.copes', sort_copes), 'copemerge.in_files'),
                                        (('modelestimate.varcopes', sort_copes), 'varcopemerge.in_files'),
                                        (('modelestimate.copes', num_copes), 'l2model.num_copes'),
                                        ]),
                  (fixed_fx, reorientresults, [('flameo.tstats', 'in_file')]),
                  (reorientresults, func2struct, [('out_file', 'in_file')])
                  ])

# metaflow.connect([(datasource, datasink, [('mag', 'orig.@mag'), ('phase', 'orig.@phase')])])
#                   (preproc, datasink, [('tcgifbeforemc.out_file','motion'),
#                                               ('tcgifaftermc.out_file', 'motion.@post'),
#                                               ('mcflirter.par_file', 'motion.@par'),
#                                               ('mcflirter.rms_files', 'motion.@rms'),
#                                               ('plottrans.out_file', 'motion.@plottrans'),
#                                               ('plotrot.out_file', 'motion.@plotrot'),
#                                               ('susan.smoothed_file', 'motion.@input'),
#                                               ('SNRcalc.out_file', 'image_quality'),
#                                               ('extractor.out_file', 'brain'),
#                                               ]),
#                   (varcalc, datasink, [('out_file', 'image_quality.@phase')])])


metaflow.write_graph(graph2use='colored', format='png', simple_form=False)
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 10})