# last working commit id 270a7ae552697f29bcc8b8089688c013f55d4bc0
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
from nipype_phase_regression.workflows.compcor import create_compcor_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
from numpy import linspace
from nipype.interfaces.base import Bunch
import nipype_phase_regression.interfaces as pr

basedir = '/workspace/ostanle2/crosscorrstudy'
phaselist = [basedir+'/raw/Run1PhaseSVD.nii.gz', basedir+'/raw/Run2PhaseSVD.nii.gz', basedir+'/raw/Run3PhaseSVD.nii.gz',
             basedir + '/raw/Run1PhaseSiemens.nii.gz', basedir + '/raw/Run2PhaseSiemens.nii.gz', basedir + '/raw/Run3PhaseSiemens.nii.gz']
maglist = [basedir+'/raw/Run1MagSVD.nii.gz', basedir+'/raw/Run2MagSVD.nii.gz', basedir+'/raw/Run3MagSVD.nii.gz',
           basedir + '/raw/Run1MagSiemens.nii.gz', basedir + '/raw/Run2MagSiemens.nii.gz', basedir + '/raw/Run3MagSiemens.nii.gz']

# Initialize Magnitude Preprocessing
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

# Add inputs
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])
preproc_mag_wf.inputs.flipper.in_file=maglist

# Specify BET threshold
preproc_mag_wf.inputs.extractor.frac=0.15

# Initialize Phase Preprocessing
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

# Add inputs
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])
preproc_phase_wf.inputs.flipper.in_file=phaselist

# Set conversion parameters
preproc_phase_wf.inputs.prepPhase.siemens=[True] * 6
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# BET PHASE
betphase = pe.MapNode(interface=fsl.ApplyMask(), name='betphase', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=basedir

# Bandpass data
bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
bpftphase.inputs.highpass_sigma = 80
bpftphase.inputs.lowpass_sigma = 2

bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
bpftmag.inputs.highpass_sigma = 80
bpftmag.inputs.lowpass_sigma = 2

# Perform GLM on data
level1_wf = create_first_level_analysis_wf(name='level1_wf', custom_results_dir=False)
level1_wf.base_dir=basedir

# set out experiment
onsets = list(linspace(25, 175, 3))
exp_info = [Bunch(conditions=["Checkerboard"], onsets=[onsets], durations=[[25]])]
level1_wf.inputs.makemodel.input_units = 'secs'
level1_wf.inputs.makemodel.time_repetition = 1.25
level1_wf.inputs.makemodel.high_pass_filter_cutoff = 0
level1_wf.inputs.makemodel.subject_info = exp_info * 6

level1_wf.inputs.modelfit.inputspec.interscan_interval = 1.25
level1_wf.inputs.modelfit.inputspec.bases = {'dgamma':{'derivs': True}}
level1_wf.inputs.modelfit.inputspec.model_serial_correlations = True
level1_wf.inputs.modelfit.inputspec.contrasts = [['Checkerboard>Baseline','T', ['Checkerboard', 'Baseline'],[1,0]]]

# Perform GLM on fitted phase
level1_wfphase = create_first_level_analysis_wf(name='level1_wfphase', custom_results_dir=False)
level1_wfphase.base_dir=basedir

# set out experiment
onsets = list(linspace(25, 175, 3))
exp_info = [Bunch(conditions=["Checkerboard"], onsets=[onsets], durations=[[25]])]
level1_wfphase.inputs.makemodel.input_units = 'secs'
level1_wfphase.inputs.makemodel.time_repetition = 1.25
level1_wfphase.inputs.makemodel.high_pass_filter_cutoff = 0
level1_wfphase.inputs.makemodel.subject_info = exp_info * 6

level1_wfphase.inputs.modelfit.inputspec.interscan_interval = 1.25
level1_wfphase.inputs.modelfit.inputspec.bases = {'dgamma':{'derivs': True}}
level1_wfphase.inputs.modelfit.inputspec.model_serial_correlations = True # prewhitening on
level1_wfphase.inputs.modelfit.inputspec.contrasts = [['Checkerboard>Baseline','T', ['Checkerboard', 'Baseline'],[1, 0]]]

# Calculate percent change maps for all nodes
percentchange = pe.MapNode(interface=pr.percentChange(), name="percentchange", iterfield=["func"])
percentchange.inputs.blockLength = 25
percentchange.inputs.TR = 1.25
percentchange.inputs.blockPattern = [0, 1, 0, 1, 0, 1, 0]

# Calculate percent change maps for all nodes
percentchangemag = pe.MapNode(interface=pr.percentChange(), name="percentchangemag", iterfield=["func"])
percentchangemag.inputs.blockLength = 25
percentchangemag.inputs.TR = 1.25
percentchangemag.inputs.blockPattern = [0, 1, 0, 1, 0, 1, 0]

percentchangesim = pe.MapNode(interface=pr.percentChange(), name="percentchangesim", iterfield=["func"])
percentchangesim.inputs.blockLength = 25
percentchangesim.inputs.TR = 1.25
percentchangesim.inputs.blockPattern = [0, 1, 0, 1, 0, 1, 0]

addmeantosim = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name='addmeantosim', iterfield=['in_file','operand_files'])
addmeantosim.inputs.op_string='-add %s'

submeanfrommag = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name='submeanfrommag', iterfield=['in_file','operand_files'])
submeanfrommag.inputs.op_string='-sub %s'

# Compcor
# compcormag = create_compcor_wf('compcormag')
# compcormag.inputs.inputspec.num_noise_components = 6
# compcormag.inputs.inputspec.highpass_sigma = 80
# compcormag.inputs.inputspec.lowpass_sigma = 2
#
# compcorphase = create_compcor_wf('compcorphase')
# compcorphase.inputs.inputspec.num_noise_components = 6
# compcorphase.inputs.inputspec.highpass_sigma = 80
# compcorphase.inputs.inputspec.lowpass_sigma = 2

# Regress magnitude and phase
phaseregress = pe.MapNode(interface=pr.phaseRegressionODR(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = 1.25
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb = 0.15

# Output the spatial cross correlation
findpsf = pe.MapNode(interface=pr.finddrainingarea(), name='findpsf', iterfield=['magmap', 'simmap'])

# Blur the images to the fit gaussians (from matlab: FitPSFSlices.m on local) FWHM=1.6523 voxels
blursim = pe.MapNode(interface=pr.Nilearnblur(), name='blursim', iterfield=['in_file', 'fwhm'])
blursim.inputs.fwhm=2

# crosscorrelate the mag and sim timecourse across time
crosscorr = pe.MapNode(interface=pr.crosscorrMagPhase(), name="crosscorr", iterfield=["mag", "sim"])

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = basedir

# Preprocess and motion correct
metaflow.connect([(preproc_phase_wf, betphase, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase, [('extractor.mask_file','mask_file')]),
                  (betphase, mcflirtphase_wf, [('out_file', 'inputspec.input_phase')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file', 'inputspec.mat_dir'),
                                                     ('extractor.out_file', 'inputspec.input_mag')]),
                  (preproc_mag_wf, level1_wf, [('extractor.out_file', 'makemodel.functional_runs'),
                                             ('extractor.out_file', 'modelfit.inputspec.functional_data')]),
                  (mcflirtphase_wf, bpftphase, [('outputspec.out_file','in_file')]),
                  (preproc_mag_wf, bpftmag, [('extractor.out_file','in_file')]),
                  # (preproc_mag_wf, compcormag, [('extractor.out_file','inputspec.func')]),
                  # (level1_wf, compcormag, [('modelfit.outputspec.zfiles','inputspec.zstat')]),
                  # (mcflirtphase_wf, compcorphase, [('outputspec.out_file','inputspec.func')]),
                  # (level1_wf, compcorphase, [('modelfit.outputspec.zfiles','inputspec.zstat')]),
                  # ])
                  ])

# Perform phase regression
metaflow.connect([(mcflirtphase_wf, phaseregress, [('outputspec.out_file', 'phase')]),
                  (preproc_mag_wf, phaseregress, [('extractor.out_file', 'mag')]),
                  (bpftmag, phaseregress, [('out_file', 'bpmag')]),
                  (bpftphase, phaseregress, [('out_file', 'bpphase')])])

# Calculate percent change and perform spatial correlation to find psf
metaflow.connect([(phaseregress, addmeantosim, [('sim', 'in_file')]),
                  (preproc_mag_wf, addmeantosim, [('meancalc.out_file', 'operand_files')]),
                  (addmeantosim, percentchangesim,[('out_file', 'func')]),
                  (preproc_mag_wf, percentchangemag,[('extractor.out_file', 'func')]),
                  (percentchangesim, findpsf, [('pc_func', 'simmap')]),
                  (percentchangemag, findpsf, [('pc_func', 'magmap')])
                  ])

# Blur and cross correlate
metaflow.connect([(phaseregress, blursim, [('sim', 'in_file')]),
                  # (findpsf, blursim, [('sigma', 'fwhm')]),
                  (blursim, crosscorr, [('out_file','sim')]),
                  (preproc_mag_wf, submeanfrommag, [('extractor.out_file', 'in_file'),
                                                    ('meancalc.out_file', 'operand_files')]),
                  (submeanfrommag, crosscorr, [('out_file','mag')]),
                  ])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)

metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})