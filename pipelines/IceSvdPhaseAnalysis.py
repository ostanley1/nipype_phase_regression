# last working commit id 270a7ae552697f29bcc8b8089688c013f55d4bc0
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
from nipype_phase_regression.workflows.compcor import create_compcor_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
from numpy import linspace
from nipype.interfaces.base import Bunch
import nipype_phase_regression.interfaces as pr
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
from nipype_phase_regression.workflows.compcor import create_compcor_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr

basedir = '/cfmm/data/ostanle2/IceSvdAnalysis/SVDcompare'
phaselist = [basedir+'/phaseAbsB1.nii.gz', basedir+'/phaseSVD.nii.gz']

# Initialize Phase Preprocessing
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

# Add inputs
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])
preproc_phase_wf.inputs.flipper.in_file=phaselist

# Set conversion parameters
preproc_phase_wf.inputs.prepPhase.siemens=[True, True]
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

metaflow_exec_graph = preproc_phase_wf.run(plugin='MultiProc')
