from __future__ import print_function
from __future__ import division
from builtins import str
from builtins import range

import os                                    # system functions

import nipype.interfaces.io as io           # Data i/o
from nipype.interfaces import fsl, freesurfer, afni          # fsl
import nipype.interfaces.utility as ul     # utility
import nipype.pipeline.engine as pe          # pypeline engine
import nipype.algorithms.modelgen as model   # model generation
from nipype.interfaces.base import Bunch

basedir = '/workspace/ostanle2/aobid'
output_dir = '/workspace/ostanle2/aobid/outputs'
freesurfer_subj_dir = '/workspace/ostanle2/aobid/surf'

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def picksecond(files):
    if isinstance(files, list):
        return files[1]
    else:
        return files

def getmiddlevolume(func):
    from nibabel import load
    funcfile = func
    if isinstance(func, list):
        funcfile = func[0]
    _, _, _, timepoints = load(funcfile).shape
    return int(timepoints / 2) - 1

def getthreshop(thresh):
    return '-thr %.10f -Tmin -bin' % (0.1 * thresh[0][1])

def getinormscale(medianvals):
    return ['-mul %.10f' % (10000. / val) for val in medianvals]

def sort_copes(files):
    numelements = len(files[0])
    outfiles = []
    for i in range(numelements):
        outfiles.insert(i, [])
        for j, elements in enumerate(files):
            outfiles[i].append(elements[i])
    return outfiles

def num_copes(files):
    return len(files)

def parse_event(files):
    from nipype.interfaces.base import Bunch
    EventList = []
    for f in files:
        left = {'none': [], '0': [], '45': [], '90': [], '135': []}
        right = {'none':[],'0':[],'45':[],'90':[],'135':[]}
        with open(f, 'r') as tsv:
            filelines = [line.strip().split('\t') for line in tsv]
        for l in xrange(len(filelines)):
            if l!=0:
                left[filelines[l][2]].append((filelines[l][0], filelines[l][1]))
                right[filelines[l][3]].append((filelines[l][0], filelines[l][1]))

        onsets=[]
        durations=[]
        for key in ['0', '45', '90', '135']:
            onset, duration = zip(*left[key])
            onsets.append([float(i) for i in onset])
            durations.append([float(i) for i in duration])
        for key in ['0', '45', '90', '135']:
            onset, duration = zip(*right[key])
            onsets.append([float(i) for i in onset])
            durations.append([float(i) for i in duration])
        EventList.append(Bunch(conditions=["L0", "L45", "L90", "L135","R0", "R45", "R90", "R135"],
                  onsets=onsets, durations=durations))
    return EventList

def parse_slice(file):
    import json
    import csv
    with open(file) as json_data:
        d = json.load(json_data)
    out=[]
    for f in d['SliceTiming']:
        out.append(float(f))
    with open(file + '.csv', 'wb') as f:
        writer = csv.writer(f)
        for val in out:
            writer.writerow([val])
    return file + '.csv'

def make_labels(subject_id):
    freesurfer_subj_dir = '/workspace/ostanle2/aobid/surf'
    return [freesurfer_subj_dir + '/' + str(subject_id) + '/label/lh.V1.thresh.label',
            freesurfer_subj_dir + '/' + str(subject_id) + '/label/rh.V1.thresh.label']

def make_regheader(subject_id):
    freesurfer_subj_dir = '/workspace/ostanle2/aobid/surf'
    return freesurfer_subj_dir + '/' + str(subject_id) + '/mri/T1.mgz'

def list_hemi(subject_id):
    return ['lh','rh']

def list_params(param_est):
    print(param_est)
    return param_est[0]

def list_ref(param_est):
    a = param_est[0]
    return a[0]

def get_scale(res):
    dictres = {'08':0.8, '14':1.4, '20':2.0, '30':3.0}
    return dictres[res]

def appendpw(dir):
    print(type(dir[0]))
    for d in xrange(len(dir)):
        dir[d] = dir[d] + '/prewhitened_data.nii.gz'
    return dir

subject_list = ['04','06','16','17','18','20']
resolution_list = ['08', '14', '20', '30']

info=dict(func = [['subject_id', 'resolution', 'subject_id', 'resolution']],
          event = [['subject_id', 'resolution', 'subject_id', 'resolution']],
          slice=[['resolution']],
          struct = [['subject_id']],
          lrstruct=[['subject_id']],
          swimag=[['subject_id']],
          swipha=[['subject_id']])
infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id', 'resolution']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list),('resolution', resolution_list)]

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id', 'resolution'],
                                               outfields=['func', 'event', 'struct', 'slice', 'lrstruct', 'swimag', 'swipha']),
                     name='datasource')
datasource.inputs.base_directory = '/cfmm/data/ostanle2/aobid/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(func='ds113c_R1.0.0/sub-%s/ses-r%s/func/sub-%s_ses-r%s_task-orientation_rec-dico_run-*_bold.nii.gz',
                                        event='ds113c_R1.0.0/sub-%s/ses-r%s/func/sub-%s_ses-r%s_task-orientation_run-*_events.tsv',
                                        slice='ds113c_R1.0.0/ses-r%s_task-orientation_bold.json',
                                        struct='sub0%s/anatomy/highres001.nii.gz',
                                        lrstruct='sub0%s/anatomy/other/t2w001.nii.gz',
                                        swimag='sub0%s/anatomy/other/swi001_mag.nii.gz',
                                        swipha='sub0%s/anatomy/other/swi001_pha.nii.gz')
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True
datasource.inputs.subject_id=subject_list

# convert image to float
img2float = pe.MapNode(interface=fsl.ImageMaths(out_data_type='float', op_string='', suffix='_dtype'),
                       iterfield=['in_file'],
                       name='img2float')

# extract the middle volume of the first run
extract_ref = pe.Node(interface=fsl.ExtractROI(t_size=1), name='extractref')

# motion correct timecourse with mcflirt and save the mats and plots
mcflirter = pe.MapNode(interface=fsl.MCFLIRT(), name="mcflirter", iterfield=['in_file'])
mcflirter.inputs.save_mats = True
mcflirter.inputs.save_plots = True
mcflirter.inputs.save_rms = True

plot_motion = pe.MapNode(interface=fsl.PlotMotionParams(in_source='fsl'),
                         name='plot_motion',
                         iterfield=['in_file'])
plot_motion.iterables = ('plot_type', ['rotations', 'translations'])

#slice timing correction
st = pe.MapNode(interface=afni.TShift(), name='st', iterfield=['in_file'])
st.inputs.tr = '2.0s'
st.inputs.outputtype='NIFTI_GZ'
st.inputs.tpattern='seqplus'

# get the mean functional of run 1
meanfunc = pe.Node(interface=fsl.ImageMaths(op_string='-Tmean',
                                            suffix='_mean'),
                   name='meanfunc')

# extract brain with fsl and save the mask
extractor = pe.Node(interface=fsl.BET(), name="extractor")
extractor.inputs.mask = True
extractor.inputs.padding = True
extractor.inputs.radius = 56
extractor.inputs.frac = 0.1

# apply the mask to all runs
maskfunc = pe.MapNode(interface=fsl.ImageMaths(suffix='_bet',
                                               op_string='-mas'),
                      iterfield=['in_file'],
                      name='maskfunc')

# Get 2 and 98th percentiles
getthresh = pe.MapNode(interface=fsl.ImageStats(op_string='-p 2 -p 98'),
                       iterfield=['in_file'],
                       name='getthreshold')

# Threshold the first run of the functional data at 10% of the 98th percentile
threshold = pe.Node(interface=fsl.ImageMaths(out_data_type='char',
                                             suffix='_thresh'),
                    name='threshold')

# get median value using the mask
medianval = pe.MapNode(interface=fsl.ImageStats(op_string='-k %s -p 50'),
                       iterfield=['in_file'],
                       name='medianval')

# dilate the mask
dilatemask = pe.Node(interface=fsl.ImageMaths(suffix='_dil',
                                              op_string='-dilF'),
                     name='dilatemask')

# mask the data with dilated mask
maskfunc2 = pe.MapNode(interface=fsl.ImageMaths(suffix='_mask',
                                                op_string='-mas'),
                       iterfield=['in_file'],
                       name='maskfunc2')

# get the mean from each run
meanfunc2 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc2')

# merge the mean and median
mergenode = pe.Node(interface=ul.Merge(2, axis='hstack'),
                    name='merge')

# scale the run to have a median of 10000
intnorm = pe.MapNode(interface=fsl.ImageMaths(suffix='_intnorm'),
                     iterfield=['in_file', 'op_string'],
                     name='intnorm')

# get the mean from each run
meanfunc3 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc3')

# highpass the data
highpass = pe.MapNode(interface=fsl.ImageMaths(suffix='_tempfilt'),
                      iterfield=['in_file','in_file2'],
                      name='highpass')
hpcutoff = 100
TR = 2.
highpass.inputs.suffix = '_hpf'
highpass.inputs.op_string = '-bptf %d -1 -add ' % (hpcutoff / TR)

preproc = pe.Workflow(name='preproc')
preproc.connect(infosource, 'subject_id', datasource, 'subject_id')
preproc.connect(infosource, 'resolution', datasource, 'resolution')
preproc.connect(datasource, 'func', img2float, 'in_file')
preproc.connect(img2float, ('out_file', pickfirst), extract_ref, 'in_file')
preproc.connect(datasource, ('func', getmiddlevolume), extract_ref, 't_min')
preproc.connect(img2float, 'out_file', st, 'in_file')
preproc.connect(st, 'out_file', mcflirter, 'in_file')
preproc.connect(extract_ref, 'roi_file', mcflirter, 'ref_file')
preproc.connect(mcflirter, 'par_file', plot_motion, 'in_file')
preproc.connect(mcflirter, ('out_file', pickfirst), meanfunc, 'in_file')
preproc.connect(meanfunc, 'out_file', extractor, 'in_file')
preproc.connect(mcflirter, 'out_file', maskfunc, 'in_file')
preproc.connect(extractor, 'mask_file', maskfunc, 'in_file2')
preproc.connect(maskfunc, 'out_file', getthresh, 'in_file')
preproc.connect(maskfunc, ('out_file', pickfirst), threshold, 'in_file')
preproc.connect(mcflirter, 'out_file', medianval, 'in_file')
preproc.connect(threshold, 'out_file', medianval, 'mask_file')
preproc.connect(threshold, 'out_file', dilatemask, 'in_file')
preproc.connect(mcflirter, 'out_file', maskfunc2, 'in_file')
preproc.connect(dilatemask, 'out_file', maskfunc2, 'in_file2')
preproc.connect(maskfunc2, 'out_file', meanfunc2, 'in_file')
preproc.connect(meanfunc2, 'out_file', mergenode, 'in1')
preproc.connect(medianval, 'out_stat', mergenode, 'in2')
preproc.connect(maskfunc2, 'out_file', intnorm, 'in_file')
preproc.connect(medianval, ('out_stat', getinormscale), intnorm, 'op_string')
preproc.connect(intnorm, 'out_file', meanfunc3, 'in_file')
preproc.connect(intnorm, 'out_file', highpass, 'in_file')
preproc.connect(meanfunc3, 'out_file', highpass, 'in_file2')



# register to structural image

skullstriplr = pe.Node(interface=fsl.BET(frac=0.3, mask=True), name='stripstructlr')

cutoutneck = pe.Node(interface=fsl.ExtractROI(), name='cutoutneck')
cutoutneck.inputs.z_min=100
cutoutneck.inputs.z_size=-1
cutoutneck.inputs.x_min=0
cutoutneck.inputs.x_size=-1
cutoutneck.inputs.y_min=0
cutoutneck.inputs.y_size=-1

T1cut2T1 = pe.Node(interface=fsl.FLIRT(dof=3), name = 'T1cut2T1')

skullstrip = pe.Node(interface=fsl.BET(frac=0.3, vertical_gradient=-0.5, mask=True), name='stripstruct')

coregisterfunc2t2 = pe.Node(interface=fsl.FLIRT(dof=6), name='func2t2')

coregistert22struct = pe.Node(interface=fsl.EpiReg(),  name='t22struct')

concatxfm = pe.Node(interface=fsl.ConvertXFM(), name='concatxfm')
concatxfm.inputs.concat_xfm=True

concatxfm2 = pe.Node(interface=fsl.ConvertXFM(), name='concatxfm2')
concatxfm2.inputs.concat_xfm=True

invertxfm = pe.Node(interface=fsl.ConvertXFM(), name = 'invertxfm')
invertxfm.inputs.invert_xfm=True

avefunc2T1 = pe.MapNode(interface=fsl.preprocess.ApplyXfm(), name = 'avefunc2T1', iterfield = ['in_file'])
avefunc2T1.inputs.apply_xfm=True

func2T1 = pe.MapNode(interface=fsl.preprocess.FLIRT(), name = 'func2T1', iterfield = ['in_file'])

res2T1 = pe.MapNode(interface=fsl.preprocess.FLIRT(), name = 'res2T1', iterfield = ['in_file'])

pw2T1 = pe.MapNode(interface=fsl.preprocess.FLIRT(), name = 'pw2T1', iterfield = ['in_file'])

mask2T1 = pe.MapNode(interface=fsl.preprocess.FLIRT(), name = 'mask2T1', iterfield = ['in_file'])
mask2T1.inputs.in_matrix_file='/cfmm/apps/fsl-alt/fsl-5.0.9-centos6_64/etc/flirtsch/ident.mat'
mask2T1.inputs.uses_qform=True

register = pe.Workflow(name='register')

register.connect([(cutoutneck, skullstrip, [('roi_file','in_file')]),
                  (cutoutneck, T1cut2T1, [('roi_file','in_file')]),
                  (skullstriplr, coregisterfunc2t2, [('out_file', 'reference')]),
                  (cutoutneck, coregistert22struct, [('roi_file','t1_head')]),
                  (skullstrip, coregistert22struct, [('out_file','t1_brain')]),
                  (skullstriplr, coregistert22struct, [('out_file','epi')]),
                  (coregistert22struct, concatxfm, [('epi2str_mat','in_file2')]),
                  (coregisterfunc2t2, concatxfm, [('out_matrix_file', 'in_file')]),
                  (concatxfm, concatxfm2, [('out_file','in_file')]),
                  (T1cut2T1, concatxfm2, [('out_matrix_file', 'in_file2')]),
                  (concatxfm2, func2T1, [('out_file', 'in_matrix_file')]),
                  (concatxfm2, res2T1, [('out_file', 'in_matrix_file')]),
                  (concatxfm2, pw2T1, [('out_file', 'in_matrix_file')]),
                  (concatxfm2, invertxfm, [('out_file', 'in_file')]),
                  (concatxfm2, avefunc2T1, [('out_file', 'in_matrix_file')]),
                  ])


modelfit = pe.Workflow(name='modelfit')

modelspec = pe.Node(interface=model.SpecifyModel(), name="modelspec")
modelspec.inputs.high_pass_filter_cutoff=hpcutoff
modelspec.inputs.time_repetition=TR
modelspec.inputs.input_units='secs'

level1design = pe.Node(interface=fsl.Level1Design(), name="level1design")
level1design.inputs.interscan_interval=TR
level1design.inputs.bases = {'dgamma': {'derivs': True}}
level1design.inputs.model_serial_correlations = True
cont1 = ['Left:Task>Baseline', 'T', ['L0', 'L45', 'L90', 'L135'], [1, 1, 1, 1]]
cont2 = ['Right:Task>Baseline', 'T', ['Rnone', 'R0', 'R45', 'R90', 'R135'], [-4, 1, 1, 1, 1]]
level1design.inputs.contrasts = [cont1]

modelgen = pe.MapNode(interface=fsl.FEATModel(), name='modelgen',
                      iterfield=['fsf_file', 'ev_files'])

modelestimate = pe.MapNode(interface=fsl.FILMGLS(smooth_autocorr=False,
                                                 mask_size=5,
                                                 threshold=1000,
                                                 output_pwdata=True),
                           name='modelestimate',
                           iterfield=['design_file', 'in_file', 'tcon_file'])

conestimate = pe.MapNode(interface=fsl.ContrastMgr(), name='conestimate',
                         iterfield=['tcon_file', 'param_estimates',
                                    'sigmasquareds', 'dof_file',
                                    ])

modelfit.connect(datasource, ('event', parse_event), modelspec, 'subject_info')
modelfit.connect([
    (modelspec, level1design, [('session_info', 'session_info')]),
    (level1design, modelgen, [('fsf_files', 'fsf_file'),
                              ('ev_files', 'ev_files')]),
    (modelgen, modelestimate, [('design_file', 'design_file'),
                               ('con_file', 'tcon_file')]),
])

fixed_fx = pe.Workflow(name='fixedfx')
copemerge = pe.MapNode(interface=fsl.Merge(dimension='t'),
                       iterfield=['in_files'],
                       name="copemerge")

varcopemerge = pe.MapNode(interface=fsl.Merge(dimension='t'),
                          iterfield=['in_files'],
                          name="varcopemerge")
level2model = pe.Node(interface=fsl.L2Model(),
                      name='l2model')
flameo = pe.MapNode(interface=fsl.FLAMEO(run_mode='fe'), name="flameo",
                    iterfield=['cope_file', 'var_cope_file'])

fixed_fx.connect([(copemerge, flameo, [('merged_file', 'cope_file')]),
                  (varcopemerge, flameo, [('merged_file', 'var_cope_file')]),
                  (level2model, flameo, [('design_mat', 'design_file'),
                                         ('design_con', 't_con_file'),
                                         ('design_grp', 'cov_split_file')]),
                  ])

# freesurfer stuff
v1label = pe.MapNode(interface=freesurfer.model.Label2Vol(), name='v1label', iterfield=['label_file', 'hemi'])
v1label.reg_header = 'mri/T1.mgz'
v1label.proj = ('frac', 0, 1, 0.5)
v1label.subject_dir = freesurfer_subj_dir

dillabel = pe.MapNode(interface=fsl.ImageMaths(), name='dillabel', iterfield=['in_file'])
dillabel.inputs.op_string = '-dilF '

datasink = pe.Node(io.DataSink(base_directory=output_dir),name="datasink")


firstlevel = pe.Workflow(name='firstlevel')
firstlevel.base_dir = basedir
firstlevel.connect([(preproc, modelfit, [('highpass.out_file', 'modelspec.functional_runs'),
                                         ('highpass.out_file', 'modelestimate.in_file'),
                                         ('mcflirter.par_file', 'modelspec.realignment_parameters')]),
                    (datasource, register, [('struct', 'cutoutneck.in_file'),
                                            ('struct', 'T1cut2T1.reference'),
                                            ('lrstruct', 'stripstructlr.in_file')]),
                    (infosource, register, [(('resolution', get_scale), 'func2T1.apply_isoxfm'),
                                            (('resolution', get_scale), 'res2T1.apply_isoxfm'),
                                            (('resolution', get_scale), 'pw2T1.apply_isoxfm'),
                                            ]),
                    (infosource, mask2T1, [(('resolution', get_scale), 'apply_isoxfm')]),
                    (preproc, register, [(('meanfunc2.out_file', pickfirst), 'func2t2.in_file'),
                                         ('datasource.struct', 'avefunc2T1.reference'),
                                         ('datasource.struct', 'func2T1.reference'),
                                         ('datasource.struct', 'res2T1.reference'),
                                         ('datasource.struct', 'pw2T1.reference'),
                                         (('meanfunc2.out_file', pickfirst), 'avefunc2T1.in_file')]),
                    (preproc, mask2T1, [('datasource.struct', 'reference')]),
                    (preproc, fixed_fx, [(('maskfunc2.out_file', pickfirst), 'flameo.mask_file')]),
                    (modelfit, fixed_fx, [(('modelestimate.copes', sort_copes), 'copemerge.in_files'),
                                          (('modelestimate.varcopes', sort_copes), 'varcopemerge.in_files'),
                                          (('modelestimate.copes', num_copes), 'l2model.num_copes'),
                                          ]),
                    (modelfit, register, [(('modelestimate.param_estimates', list_params), 'func2T1.in_file'),
                                          ('modelestimate.residual4d', 'res2T1.in_file'),
                                          (('modelestimate.results_dir', appendpw), 'pw2T1.in_file')]),
                    (preproc, v1label, [('datasource.struct','template_file')]),
                    (infosource, v1label, [('subject_id','subject_id'),
                                           (('subject_id', make_labels), 'label_file'),
                                           (('subject_id', list_hemi), 'hemi'),
                                           (('subject_id', make_regheader), 'reg_header')]),
                    (v1label, dillabel, [('vol_label_file', 'in_file')]),
                    (dillabel, mask2T1, [('out_file', 'in_file')]),
                    ])

# firstlevel.run(updatehash=True)
firstlevel.write_graph(graph2use='colored', format='png', simple_form=False)
firstlevel.run(plugin='MultiProc', plugin_args={'n_procs' : 15})
# firstlevel.run(plugin='SGEGraph', plugin_args = {'dont_resubmit_completed_jobs': True,
#                                                               'qsub_args': '-q nipype_long'})
