# last working commit id 76b3608f88038280c853c02d008b844c465ce040
from nipype import config
config.enable_debug_mode()
from nipype_phase_regression.interfaces.core import *
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.prep_anat_wf import create_prep_anat_wf
from nipype_phase_regression.workflows.filtering_wf import create_filtering_wf
from nipype_phase_regression.workflows.plot_result_wf import create_plot_result_wf
import time

# set default output type
fsl.FSLCommand.set_default_output_type('NIFTI_GZ')

#a bunch of directories used in the workflows
data_base_dir="/cfmm/data/ostanle2/RAWDATA/"
dcm_dir=data_base_dir+'2016_05_17_HigherBit'

base_dir="/workspace/ostanle2/regression_wf_test"
processing_base_dir=base_dir+"processed/2016_05_17_HigherBit_processed/"
dcm_dir_sorted=base_dir+"data/2016_05_17_HigherBit_sorted/"
mask_dir=base_dir+"data/masks/"
nifti_dir=processing_base_dir+dcm_dir+"_nii/"

imageList = ['mbep2d_bold_mb2_p2_2iso_nmc','mprage_sag_750iso_p3_914']
ignoreSeries = []

#create a folder to store processed data
if not os.path.exists(processing_base_dir):
    os.makedirs(processing_base_dir)

# create a sorted directory of symbolic links to the original data
seriesName = ""
if not os.path.exists(dcm_dir_sorted):
    os.makedirs(dcm_dir_sorted)
    print 'creating sorted directory of symbolic links to original data'
    for path, subdirs, files in os.walk(dcm_dir):
        for name in files:  # this could be parallelized
            src_file = os.path.join(path, name)
            try:
                ds = dicom.read_file(src_file, stop_before_pixels=True)
            except dicom.filereader.InvalidDicomError:
                continue  # skip non-dicom file
            if ds.ProtocolName in imageList and ds.SeriesNumber not in ignoreSeries:
                if ds.StudyDescription == "Menon^Olivia":
                    seriesName = ds.ProtocolName + "ImageKind_" + str(ds.ImageType[2]) + '_' + "Conventional_Combination" + "_Series" + str(ds.SeriesNumber).zfill(4)
                elif ds.StudyDescription == "Gadgetron^SVD":
                    seriesName = ds.ProtocolName + "ImageKind_" + str(ds.ImageType[2]) + '_' + "SVD_Combination" + "_Series" + str(ds.SeriesNumber).zfill(4)
                dst_dir = os.path.join(dcm_dir_sorted, seriesName)
                if not os.path.exists(dst_dir):
                    os.makedirs(dst_dir)
                os.symlink(src_file, os.path.join(dst_dir, name))
    print 'symlinks made'
else:
    print 'using existing symlinks'

# PREPROCESS MAGNITUDE DATA
# Run preprocessing workflows separately (as recommended to avoid re-runs)
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=processing_base_dir

# Take grabbing initial data out of dicom worflows to allow for good templating
# grab data
grabber = pe.Node(interface=nio.DataGrabber(), name="grabber")
grabber.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_M_S*/*Image_SLC0_CON0_PHS0_REP0*.dcm'
grabber.inputs.sort_filelist = False
grabber.inputs.base_directory=dcm_dir_sorted

preproc_mag_wf.connect(grabber, 'outfiles', preproc_mag_wf.get_node('dataCleaner'), 'dcm')
preproc_mag_wf.connect(grabber, 'outfiles', preproc_mag_wf.get_node('converter'), 'in_file')

# Out of workflow for customization
preproc_mag_wf.inputs.ds.substitutions = [('data_2016_05_17_12_59_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run1SVDCOMBmag'),
    ('data_2016_05_17_13_02_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run2SVDCOMBmag'),
    ('data_2016_05_17_13_05_03_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run3SVDCOMBmag'),
    ('_brain_detrendmag', 'processed'),
    ('_maths', '_tSNR')]

preproc_mag_exec_graph = preproc_mag_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})

# PREPROCESS PHASE DATA
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=processing_base_dir

# grab data
grabber = pe.Node(interface=nio.DataGrabber(), name="grabber")
grabber.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_P_S*/*Image_SLC0_CON0_PHS0_REP0*.dcm'
grabber.inputs.sort_filelist = False
grabber.inputs.base_directory=dcm_dir_sorted

preproc_phase_wf.connect(grabber, 'outfiles', preproc_phase_wf.get_node('dataCleaner'), 'dcm')
preproc_phase_wf.connect(grabber, 'outfiles', preproc_phase_wf.get_node('converter'), 'in_file')

# Out of workflow for customization
preproc_phase_wf.inputs.prepPhase.bit_depth = 16
preproc_phase_wf.inputs.ds.substitutions = [('data_2016_05_17_12_59_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run1SVDCOMBphase'),
    ('data_2016_05_17_13_02_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run2SVDCOMBphase'),
    ('data_2016_05_17_13_05_03_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run3SVDCOMBphase'),
    ('_detrendphase', 'processed'),
    ('_maths', '_var')]

preproc_phase_exec_graph = preproc_phase_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})

# APPLY FILTERS
filtering_wf = create_filtering_wf()
filtering_wf.base_dir=processing_base_dir
filtering_wf.inputs.grabmag.base_directory = processing_base_dir
filtering_wf.inputs.grabphase.base_directory = processing_base_dir
filtering_exec_graph = filtering_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})

# PLOT RESULTS
plot_result_wf = create_plot_result_wf()
plot_result_wf.base_dir=processing_base_dir
plot_result_wf.inputs.grabmag.base_directory=processing_base_dir
plot_result_wf.inputs.grabphase.base_directory=processing_base_dir
plot_result_wf.inputs.grabsim.base_directory=processing_base_dir
plot_result_wf.inputs.grabfilt.base_directory=processing_base_dir
plot_result_wf.inputs.grabmask.base_directory=mask_dir
plot_result_exec_graph = plot_result_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})