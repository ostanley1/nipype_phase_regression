import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype.interfaces.freesurfer as fsurf
import nipype.interfaces.afni as afni
from nipype_phase_regression.interfaces.Zeropad import Zeropad

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

# scan1
inv1 = [12, 17, 22, 27, 32, 37]
average1 = [16, 21, 26, 28, 33, 41]
uniden = [14, 19, 24, 30, 35, 39]
t1map = [13, 18, 23, 29, 34, 38]
average1s=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190204/Jade%d.nii.gz' % (i) for i in average1]
inv1s=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190204/Jade%d.nii.gz' % (i) for i in inv1]
in_files=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190204/Jade%d.nii.gz' % (i) for i in uniden]
t1maps=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190204/Jade%d.nii.gz' % (i) for i in t1map]
base_dir='/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190204/'

# scan 2
inv1 =[10, 15, 20, 25, 30, 35]
average1 = [11, 19, 21, 29, 31, 36]
uniden = [13, 17, 23, 27, 33, 38]
t1map = [12, 16, 22, 26, 32, 37]
average1s=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190206/Jade%d.nii.gz' % (i) for i in average1]
inv1s=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190206/Jade%d.nii.gz' % (i) for i in inv1]
in_files=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190206/Jade%d.nii.gz' % (i) for i in uniden]
t1maps=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190206/Jade%d.nii.gz' % (i) for i in t1map]
base_dir='/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190206/'

# scan 3
inv1 =[10, 16, 25, 30, 35, 40]
average1 = [11, 17, 26, 31, 36, 44]
uniden = [13, 19, 28, 33, 38, 42]
t1map = [12, 18, 27, 32, 37, 41]
average1s=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190208/Jade%d.nii.gz' % (i) for i in average1]
inv1s=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190208/Jade%d.nii.gz' % (i) for i in inv1]
in_files=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190208/Jade%d.nii.gz' % (i) for i in uniden]
t1maps=['/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190208/Jade%d.nii.gz' % (i) for i in t1map]
base_dir='/cfmm/data/ostanle2/Ongoing_Work/NHP_HiRes/Jade/20190208/'

flirt=pe.MapNode(interface=fsl.FLIRT(), name='flirt', iterfield=['in_file'])
flirt.inputs.dof = 6
flirt.inputs.reference = average1s[0]
flirt.inputs.in_file = average1s

applyxfm = pe.MapNode(fsl.ApplyXFM(), name='applyxfm', iterfield=['in_file', 'in_matrix_file'])
applyxfm.inputs.reference = in_files[0]
applyxfm.inputs.in_file = in_files
applyxfm.inputs.interp='spline'

applyxfminv1 = pe.MapNode(fsl.ApplyXFM(), name='applyxfminv1', iterfield=['in_file', 'in_matrix_file'])
applyxfminv1.inputs.reference = inv1s[0]
applyxfminv1.inputs.in_file = inv1s
applyxfminv1.inputs.interp='spline'

applyxfmt1 = pe.MapNode(fsl.ApplyXFM(), name='applyxfmt1', iterfield=['in_file', 'in_matrix_file'])
applyxfmt1.inputs.reference = t1maps[0]
applyxfmt1.inputs.in_file = t1maps
applyxfmt1.inputs.interp='spline'

betn3 = pe.Node(interface=afni.SkullStrip(), name='betn3')
betn3.inputs.outputtype = 'NIFTI_GZ'
betn3.inputs.args = '-monkey'

n3 = pe.MapNode(interface=fsurf.MNIBiasCorrection(), name='n3', iterfield='in_file')
n3.inputs.iterations = 4
n3inv1 = n3.clone('n3inv1')
n3t1 = n3.clone('n3t1')
n3inv2 = n3.clone('n3inv2')

merge = pe.Node(interface=fsl.utils.Merge(dimension='t'), name='merge')
mergeinv1 = merge.clone('mergeinv1')
mergeinv2 = merge.clone('mergeinv2')
merget1 = merge.clone('merget1')

mean = pe.Node(interface=fsl.maths.MeanImage(), name='mean')
meaninv1 = mean.clone('meaninv1')
meaninv2 = mean.clone('meaninv2')
meant1 = mean.clone('meant1')

skullstripinv2 = pe.Node(interface=afni.SkullStrip(), name='skullstripinv2')
skullstripinv2.inputs.outputtype = 'NIFTI_GZ'
skullstripinv2.inputs.args = '-monkey'

zeropad = pe.Node(interface=Zeropad(padding_factor=2), name='zeropad')

metaflow = pe.Workflow(name='metaflow')
metaflow.config['execution'] = {'remove_unnecessary_outputs': 'False'}
metaflow.base_dir = base_dir
metaflow.connect([(flirt, applyxfm,[('out_matrix_file', 'in_matrix_file')]),
                  (flirt, applyxfminv1,[('out_matrix_file', 'in_matrix_file')]),
                  (flirt, applyxfmt1,[('out_matrix_file', 'in_matrix_file')]),
                  (flirt, betn3, [(('out_file', pickfirst), 'in_file')]),
                  (flirt, mergeinv2, [('out_file', 'in_files')]),
                  (mergeinv2, skullstripinv2, [('merged_file', 'in_file')]),
                  (skullstripinv2, n3inv2, [('out_file', 'in_file')]),
                  (mergeinv2, meaninv2, [('merged_file', 'in_file')]),
                  (applyxfm, n3, [('out_file', 'in_file')]),
                  (n3, merge, [('out_file', 'in_files')]),
                  (applyxfminv1, n3inv1, [('out_file', 'in_file')]),
                  (n3inv1, mergeinv1, [('out_file', 'in_files')]),
                  (merge, mean, [('merged_file', 'in_file')]),
                  (mergeinv1, meaninv1, [('merged_file', 'in_file')]),
                  (applyxfmt1, merget1, [('out_file', 'in_files')]),
                  # (applyxfmt1, n3t1, [('out_file', 'in_file')]),
                  # (n3t1, merget1, [('out_file', 'in_files')]),
                  (merget1, meant1, [('merged_file', 'in_file')]),
                  (mean, zeropad, [('out_file', 'in_file')])])
metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 8})

