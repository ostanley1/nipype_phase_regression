from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
from nipype_phase_regression.workflows.compcor import create_compcor_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
from numpy import linspace
from nipype.interfaces.base import Bunch
import nipype_phase_regression.interfaces as pr
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
from nipype_phase_regression.workflows.compcor import create_compcor_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr

basedir = '/cfmm/data/ostanle2/Plato_analysis/'
maglist = [basedir+'Platotest_poweroff.nii.gz',basedir+'Platotest_poweron_20volblocks.nii.gz']

# Initialize Magnitude Preprocessing
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

# Add inputs
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])
preproc_mag_wf.inputs.flipper.in_file=maglist

preproc_mag_wf.run()