import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype.interfaces.io as nio
import nipype_phase_regression.interfaces as pr
import nipype.interfaces.utility as ul
from nipype_phase_regression.workflows.preproc_phase_wf_SEvsGE import create_preproc_phasesevsge_wf

def trim_nifti(in_file):
    from nipype.utils.filemanip import split_filename
    _, base, _ = split_filename(in_file)
    return base

def run_pipeline(file_list):
    base_dir = './SNRcalcs'

    infosource = pe.Node(interface=ul.IdentityInterface(fields=['file']),
                         name="infosource")
    infosource.inputs.file=file_list

    preproc_phase_wf = create_preproc_phasesevsge_wf()
    preproc_phase_wf.base_dir='./RSCalcs'
    preproc_phase_wf.inputs.inputspec.siemensbool = True

    phasevarflow =  pe.Workflow(name='phasevarflow')
    phasevarflow.base_dir=base_dir

    phasevarflow.connect([(infosource, preproc_phase_wf, [('file', 'inputspec.input_phase')]),
                        ])

    phasevarflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 16})

if __name__=='__main__':
    from argparse import ArgumentParser, RawTextHelpFormatter
    parser = ArgumentParser(description=__doc__, formatter_class=RawTextHelpFormatter)
    parser.add_argument('file_list', default=['.*'], nargs='+', help='labels of sequences to analyze')
    args = parser.parse_args()

    file_list = args.file_list

    run_pipeline(file_list)
