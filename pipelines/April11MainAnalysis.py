# last working commit id 40912f734d20fd5fcc8f8c3a1500814eacba4257

from nipype import config
config.enable_debug_mode()
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.prep_anat_wf import create_prep_anat_wf
from nipype_phase_regression.workflows.apply_phase_wf import create_apply_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
# from nipype_phase_regression.workflows.plot_result_wf import create_plot_result_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
import nipype_phase_regression.interfaces as pr
import dicom
import os
import nipype.pipeline.engine as pe
import nipype.interfaces.io as nio
import nipype.interfaces.fsl as fsl
import nipype.interfaces.utility as ul
from numpy import linspace
from nipype.interfaces.base import Bunch
from nipype.utils.filemanip import split_filename

def makeresultsdir(filelist):
    from nipype.utils.filemanip import split_filename
    outlist = []
    for file in filelist:
        _, base, _ = split_filename(file)
        outlist.append(base + '_results')
    return outlist

def orderContrasts(zlist, mag):
    from nipype.utils.filemanip import split_filename
    from nipype_phase_regression.workflows.apply_phase_wf import matchFile
    outzlist = []
    maglist = []
    for con in zlist:
        outzlist.append(con[0])
        path,_,_=split_filename(con[0])
        match = matchFile(mag, path.split('/')[-1])
        maglist.append(match)
    print outzlist
    print maglist
    return outzlist, maglist

# set default output type
fsl.FSLCommand.set_default_output_type('NIFTI_GZ')

#a bunch of directories used in the workflows
data_base_dir="/cfmm/data/ostanle2/RAWDATA/"
dcm_folder_name="2016_04_11"
dcm_dir=data_base_dir+dcm_folder_name+"/"

base_dir="/workspace/ostanle2/regression_wf_test"
processing_base_dir=base_dir+"processed/"+dcm_folder_name+"compcor_processed/"
dcm_dir_sorted=base_dir+"data/"+dcm_folder_name+"_sorted/"
mask_dir=base_dir+"data/masks/"
nifti_dir=processing_base_dir+dcm_folder_name+"_nii/"

imageList = ['mbep2d_bold_mb2_p2_2iso_nmc','mprage_sag_750iso_p3_914']
ignoreSeries = [11,12, 15, 16, 17, 18]

#create a folder to store processed data
if not os.path.exists(processing_base_dir):
    os.makedirs(processing_base_dir)

# create a sorted directory of symbolic links to the original data
seriesName = ""
if not os.path.exists(dcm_dir_sorted):
    os.makedirs(dcm_dir_sorted)
    print 'creating sorted directory of symbolic links to original data'
    for path, subdirs, files in os.walk(dcm_dir):
        for name in files:  # this could be parallelized
            src_file = os.path.join(path, name)
            try:
                ds = dicom.read_file(src_file, stop_before_pixels=True)
            except dicom.filereader.InvalidDicomError:
                continue  # skip non-dicom file
            if ds.ProtocolName in imageList and ds.SeriesNumber not in ignoreSeries:
                if ds.StudyDescription == "Menon^Olivia":
                    seriesName = ds.ProtocolName + "ImageKind_" + str(ds.ImageType[2]) + '_' + "Conventional_Combination" + "_Series" + str(ds.SeriesNumber).zfill(4)
                elif ds.StudyDescription == "Gadgetron^SVD":
                    seriesName = ds.ProtocolName + "ImageKind_" + str(ds.ImageType[2]) + '_' + "SVD_Combination" + "_Series" + str(ds.SeriesNumber).zfill(4)
                dst_dir = os.path.join(dcm_dir_sorted, seriesName)
                if not os.path.exists(dst_dir):
                    os.makedirs(dst_dir)
                os.symlink(src_file, os.path.join(dst_dir, name))
    print 'symlinks made'
else:
    print 'using existing symlinks'

# PREPROCESS MAGNITUDE DATA
# Run preprocessing workflows separately (as recommended to avoid re-runs)
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=processing_base_dir

# Take grabbing initial data out of dicom worflows to allow for good templating
# grab data
grabber1 = pe.Node(interface=nio.DataGrabber(), name="grabber1")
grabber1.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_M_Conventional_Combination_Series0013/*.0001*.IMA'
grabber1.inputs.sort_filelist = False
grabber1.inputs.base_directory=dcm_dir_sorted

grabber2 = pe.Node(interface=nio.DataGrabber(), name="grabber2")
grabber2.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_M_SVD_Combination_Series0002/*Image_SLC0_CON0_PHS0_REP0*.dcm'
grabber2.inputs.sort_filelist = False
grabber2.inputs.base_directory=dcm_dir_sorted

mergemag = pe.Node(interface=ul.Merge(2), name="mergemag")

preproc_mag_wf.connect(grabber1, "outfiles", mergemag, "in1")
preproc_mag_wf.connect(grabber2, "outfiles", mergemag, "in2")
preproc_mag_wf.connect(mergemag, "out", preproc_mag_wf.get_node('dataCleaner'), 'dcm')
preproc_mag_wf.connect(mergemag, 'out', preproc_mag_wf.get_node('converter'), 'in_file')

# preproc_mag_exec_graph = preproc_mag_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4, 'n_procs' : 12})

# PREPROCESS PHASE DATA
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=processing_base_dir

# grab data
grabberp1 = pe.Node(interface=nio.DataGrabber(), name="grabberp1")
grabberp1.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_P_SVD_Combination_Series3002/*Image_SLC0_CON0_PHS0_REP0*.dcm'
grabberp1.inputs.sort_filelist = False
grabberp1.inputs.base_directory=dcm_dir_sorted

grabberp2 = pe.Node(interface=nio.DataGrabber(), name="grabberp2")
grabberp2.inputs.template = 'mbep2d_bold_mb2_p2_2iso_nmcImageKind_P_Conventional_Combination_Series0014/*.0001.*.IMA'
grabberp2.inputs.sort_filelist = False
grabberp2.inputs.base_directory=dcm_dir_sorted

mergephase = pe.Node(interface=ul.Merge(2), name="mergephase")

preproc_phase_wf.connect(grabberp1, "outfiles", mergephase, "in1")
preproc_phase_wf.connect(grabberp2, "outfiles", mergephase, "in2")
preproc_phase_wf.connect(mergephase, "out", preproc_phase_wf.get_node('dataCleaner'), 'dcm')
preproc_phase_wf.connect(mergephase, 'out', preproc_phase_wf.get_node('converter'), 'in_file')
preproc_phase_wf.inputs.prepPhase.siemens=[True, True]

# Out of workflow for customization
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# preproc_phase_exec_graph = preproc_phase_wf.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4, 'n_procs' : 12})

# PREPROCESS ANATOMICAL DATA
prep_anat_wf = create_prep_anat_wf()
prep_anat_wf.base_dir=processing_base_dir

# grab data
grabber = pe.Node(nio.DataGrabber(), name="grabber")
grabber.inputs.base_directory = dcm_dir_sorted
grabber.inputs.template = 'mprage*/*.0019.0001.*.IMA'
grabber.inputs.sort_filelist = True

prep_anat_wf.connect(grabber, 'outfiles', prep_anat_wf.get_node('converter'), 'in_file')
prep_anat_wf.connect(grabber, 'outfiles', prep_anat_wf.get_node('dataCleaner'), 'dcm')

# BET PHASE
betphase_wf = pe.MapNode(interface=fsl.ApplyMask(), name='betphase_wf', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=processing_base_dir

# APPLY FILTERS
apply_phase_wf = create_apply_phase_wf()
apply_phase_wf.base_dir=processing_base_dir

mergefilters = pe.Node(interface=ul.Merge(8),name="mergefilters")

# Perform GLM on data
level1_wf = create_first_level_analysis_wf(name='level1_wf', custom_results_dir=False)
level1_wf.base_dir=processing_base_dir

# set out experiment
onsets = list(linspace(25, 175, 3))
exp_info = [Bunch(conditions=["Checkerboard"], onsets=[onsets], durations=[[25]])]
level1_wf.inputs.makemodel.input_units = 'secs'
level1_wf.inputs.makemodel.time_repetition = 1.25
level1_wf.inputs.makemodel.high_pass_filter_cutoff = 0
level1_wf.inputs.makemodel.subject_info = exp_info * 2

level1_wf.inputs.modelfit.inputspec.interscan_interval = 1.25
level1_wf.inputs.modelfit.inputspec.bases = {'dgamma':{'derivs': False}}
level1_wf.inputs.modelfit.inputspec.model_serial_correlations = True
level1_wf.inputs.modelfit.inputspec.contrasts = [['Checkerboard>Baseline','T', ['Checkerboard'],[1]]]

# make sure the result dirs are dynamically named for data sink
resultdirs = pe.Node(interface=ul.Function(input_names=["filelist"], output_names=["outlist"],function=makeresultsdir), name="resultdirs")

# Create a level1_wf for results
level1_wfres = create_first_level_analysis_wf(name='level1_wfres')
level1_wfres.base_dir=processing_base_dir

level1_wfres.inputs.makemodel.input_units = 'secs'
level1_wfres.inputs.makemodel.time_repetition = 1.25
level1_wfres.inputs.makemodel.high_pass_filter_cutoff = 0
level1_wfres.inputs.makemodel.subject_info = exp_info * 164

level1_wfres.inputs.modelfit.inputspec.interscan_interval = 1.25
level1_wfres.inputs.modelfit.inputspec.bases = {'dgamma':{'derivs': False}}
level1_wfres.inputs.modelfit.inputspec.model_serial_correlations = True
level1_wfres.inputs.modelfit.inputspec.contrasts = [['Checkerboard>Baseline','T', ['Checkerboard'],[1]]]

# FILM_GLS uses a default of result so setting the results_dir to an iterfield is not enough
# we need to delete the default
del level1_wfres.inputs.modelfit.modelestimate.results_dir

# order background images
ordercontrasts = pe.Node(interface=ul.Function(input_names=["zlist", "mag"], output_names=["outzlist", "maglist"],function=orderContrasts), name="ordercontrasts")

# convert zstats to FWE corrected p-values
convertz2p = pe.MapNode(interface=fsl.maths.MathsCommand(), name='convertz2p', iterfield=['in_file'])
convertz2p.inputs.args = '-cpval'

# overlay stats
combine = pe.MapNode(interface=fsl.Overlay(), name="combine", iterfield=['background_image', 'stat_image'])
combine.inputs.auto_thresh_bg = True
combine.inputs.stat_thresh = (0, 0.05)
combine.inputs.show_negative_stats = True

# Calculate percent change maps for all nodes
percentchange = pe.MapNode(interface=pr.percentChange(), name="percentchange", iterfield=["func"])
percentchange.inputs.blockLength = 25
percentchange.inputs.TR = 1.25
percentchange.inputs.blockPattern = [0, 1, 0, 1, 0, 1, 0]

# Create data sink
# data sink to dump stuff in
ds = pe.Node(interface=nio.DataSink(), name="ds")
ds.inputs.regexp_substitutions = [('_[A-z]+[0-9]/', '/'),
                                  ('_[A-z]+[0-9][0-9]/', '/'),
                                  ('_[A-z]+[0-9][0-9][0-9]/', '/')]
ds.inputs.substitutions = [('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0013.0001.2016.04.11.10.13.53.671875.47514717_out_desript_reoriented',
    'Run1CCOMBmag'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0015.0001.2016.04.11.10.13.53.671875.47521889_out_desript_reoriented',
    'Run2CCOMBmag'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0017.0001.2016.04.11.10.13.53.671875.47529061_out_desript_reoriented',
    'Run3CCOMBmag'),
    ('data_2016_04_11_09_45_09_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run1SVDCOMBmag'),
    ('data_2016_04_11_09_49_58_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run2SVDCOMBmag'),
    ('data_2016_04_11_09_54_06_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_1_out_desript_reoriented', 'Run3SVDCOMBmag'),
    ('_brain_detrendmag','processed'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0014.0001.2016.04.11.10.13.53.671875.47515146_out_desript_reoriented', 'Run1CCOMBphase'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0016.0001.2016.04.11.10.13.53.671875.47522408_out_desript_reoriented', 'Run2CCOMBphase'),
    ('20160411_SVD_TEST_BOLD.MR.MENON_OLIVIA.0018.0001.2016.04.11.10.13.53.671875.47529634_out_desript_reoriented', 'Run3CCOMBphase'),
    ('data_2016_04_11_09_45_09_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run1SVDCOMBphase'),
    ('data_2016_04_11_09_49_58_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run2SVDCOMBphase'),
    ('data_2016_04_11_09_54_06_Image_SLC0_CON0_PHS0_REP0_SET0_AVE0_2_out_desript_reoriented', 'Run3SVDCOMBphase'),
    ('_detrendphase','processed')]

# CREATE META WF AND ATTACH AND RUN
metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = processing_base_dir
metaflow.connect([(preproc_phase_wf, betphase_wf, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase_wf, [('extractor.mask_file','mask_file')]),
                  (betphase_wf, mcflirtphase_wf, [('out_file','inputspec.input_phase')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file', 'inputspec.mat_dir'),
                                                     ('detrendmag.detrended_mag', 'inputspec.input_mag')]),
                  (preproc_mag_wf, level1_wf, [('detrendmag.detrended_mag', 'makemodel.functional_runs'),
                                             ('detrendmag.detrended_mag', 'modelfit.inputspec.functional_data')]),
                  (mcflirtphase_wf, apply_phase_wf, [('outputspec.out_file', 'bpftphase.in_file'),
                                                     ('outputspec.out_file', 'mergeprphase.in1'),
                                                     ('outputspec.out_file', 'windowphase.in_file'),
                                                     ('outputspec.out_file', 'savitzkygolay.phase'),
                                                     ('outputspec.out_file', 'orderfit.phase'),
                                                     ('outputspec.out_file', 'compcorphase.inputspec.func')
                                                     ]),
                  (preproc_mag_wf, apply_phase_wf, [('detrendmag.detrended_mag', 'bpftmag.in_file'),
                                                    ('detrendmag.detrended_mag', 'mergeprmag.in1'),
                                                    ('detrendmag.detrended_mag', 'windowmag.in_file'),
                                                    ('detrendmag.detrended_mag', 'savitzkygolay.mag'),
                                                    ('detrendmag.detrended_mag', 'orderfit.mag'),
                                                    ('detrendmag.detrended_mag', 'ordersim.mag'),
                                                    ('detrendmag.detrended_mag', 'compcormag.inputspec.func')
                                                    ]),
                  (level1_wf, apply_phase_wf, [('modelfit.outputspec.zfiles','compcormag.inputspec.zstat'),
                                               ('modelfit.outputspec.zfiles', 'compcorphase.inputspec.zstat')]),
                  (preproc_mag_wf, mergefilters, [('detrendmag.detrended_mag','in1')]),
                  (mcflirtphase_wf, mergefilters, [('outputspec.out_file','in2')]),
                  (apply_phase_wf, mergefilters, [('bpftmag.out_file','in3'),
                                                  ('windowmag.filt_tc', 'in4'),
                                                  ('mergeoutsim.out', 'in5'),
                                                  ('convolve.convolve_tc', 'in6'),
                                                  ('crosscorr.crosscorr_tc', 'in7'),
                                                  ('blur.smoothed_file', 'in8')]),
                  (mergefilters, percentchange, [('out', 'func')]),
                  (mergefilters, level1_wfres, [('out', 'makemodel.functional_runs'),
                                                ('out', 'modelfit.inputspec.functional_data')]),
                  (mergefilters, resultdirs, [('out', 'filelist')]),
                  (resultdirs, level1_wfres, [('outlist', 'modelfit.modelestimate.results_dir')]),
                  (level1_wfres, ordercontrasts, [('modelfit.outputspec.zfiles','zlist')]),
                  (preproc_mag_wf, ordercontrasts, [('meancalc.out_file','mag')]),
                  (ordercontrasts, convertz2p, [('outzlist', 'in_file')]),
                  (convertz2p, combine, [('out_file', 'stat_image')]),
                  (ordercontrasts, combine, [('maglist', 'background_image')]),
                  ])



# Output data
metaflow.connect([(preproc_mag_wf, ds, [('detrendmag.detrended_mag', 'mag'),
                                        ('SNRcalc.out_file', 'mag.@tSNR')]),
                  (preproc_phase_wf, ds, [('prepPhase.detrended_phase', 'phase'),
                                        ('varcalc.out_file', 'phase.@var')]),
                  (prep_anat_wf, ds, [('extractor.out_file', 'anat.@BET'),
                                      ('flipper.out_file', 'anat')]),
                  (mcflirtphase_wf, ds, [("outputspec.out_file", "mcflirt_phase")]),
                  (apply_phase_wf, ds, [('bpftmag.out_file', 'tc.@BPmag'),
                                        ('bpftphase.out_file', 'tc.@phase.BP'),
                                        ('windowmag.filt_tc', 'tc.@WINmag'),
                                        ('windowphase.filt_tc', 'tc.@phase.WIN'),
                                        ('compcormag.outputspec.filtered_file', 'tc.@COMPmag'),
                                        ('compcorphase.outputspec.filtered_file', 'tc.@phase.COMP'),
                                        ('applyfit.out_sim', 'tc.@applysim'),
                                        ('applyfit.out_filt', 'tc.@applyfilt'),
                                        ('savitzkygolay.sim', 'tc.@SGsim'),
                                        ('savitzkygolay.filt', 'tc.@SGfilt'),
                                        ('phaseregression.sim', 'tc.@PRsim'),
                                        ('phaseregression.filt', 'tc.@PRfilt'),
                                        ('convolve.convolve_tc', 'tc.@convove'),
                                        ('crosscorr.crosscorr_tc', 'tc.@crosscorr'),
                                        ('blur.smoothed_file', 'tc.@blur')]),
                  (percentchange, ds, [('pc_func','pcmaps')])
                  ])


metaflow.write_graph(graph2use='colored', format='png', simple_form=False)
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})