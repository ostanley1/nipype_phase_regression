import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr
from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                       create_fixed_effects_flow)
import nipype.algorithms.modelgen as model
from nipype.interfaces.base import Bunch
import nipype.interfaces.utility as ul
import nipype.interfaces.io as io
from nipype.interfaces import spm, fsl, afni
from nipype_phase_regression.workflows.preproc_mag_wf_SEvsGE import create_preproc_magsevsge_wf
from nipype_phase_regression.workflows.preproc_phase_wf_SEvsGE import create_preproc_phasesevsge_wf
from nipype_phase_regression.workflows.feat_wf import create_feat_wf
from nipype_phase_regression.workflows.make_surf_wf import create_make_surf_wf

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

# grab the data
basedir = '/workspace/ostanle2/SEvsGE2'
output_dir = '.cfmm.data.ostanle2.Ongoing_Work.SEvsGE2'
TR = 2.5
hpcutoff = 100

subject_list=['20171004','20171030']

info=dict(mag = [['subject_id']], phase=[['subject_id']], semag=[['subject_id']], mocophase=[['subject_id']],
          struct=[['subject_id']], normbrain=[['subject_id']])
infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id'],
                                               outfields=['mag', 'phase', 'semag', 'mocophase','struct','normbrain']),
                     name='datasource')
datasource.inputs.base_directory = basedir + '/orig/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(mag='%sGERun*Mag.nii.gz',
                                        phase='%sGERun*Phase.nii.gz',
                                        semag='%sSERun*Mag.nii.gz',
                                        mocophase='../moco/%sGERun*Phasemoco.nii.gz',
                                        struct='%sStruct.nii.gz',
                                        normbrain='../normbrain/%sStruct_normfs.mgz')
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True
datasource.inputs.subject_id=subject_list[0]

grabber = pe.Workflow(name='grabber')
grabber.connect(infosource, 'subject_id', datasource, 'subject_id')

# preprocess GE data
preproc_mag_wfge = create_preproc_magsevsge_wf()
preproc_mag_wfge.base_dir=basedir

# preprocess SE data
preproc_mag_wfse=preproc_mag_wfge.clone('preproc_mag_wfse')

# preprocess the phase sets
preproc_phase_wfge = create_preproc_phasesevsge_wf()
preproc_phase_wfge.base_dir = basedir
preproc_phase_wfge.inputs.inputspec.siemensbool = True

# bandpass the magnitude and phase ge sets
# Bandpass data
bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
bpftphase.inputs.highpass_sigma = 80
bpftphase.inputs.lowpass_sigma = 2

bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
bpftmag.inputs.highpass_sigma = 80
bpftmag.inputs.lowpass_sigma = 2

# Regress ge magnitude and phase
phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = TR
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb = 0.15

# Start structural pipeline
make_surfs_wf = create_make_surf_wf()
make_surfs_wf.basedir = basedir
make_surfs_wf.inputs.inputspec.midline = 120
make_surfs_wf.inputs.inputspec.x_min = 160
make_surfs_wf.inputs.inputspec.x_size = -1

# Set up experiment
onsets = [30,80,130,180,230]
exp_info = [Bunch(conditions=["Activation"], onsets=[onsets], durations=[[20] * 5])] * 3

cont1 = ['Activation>Baseline', 'T', ["Activation"], [1]]
contrasts = [cont1]

feat_wf = create_feat_wf()
feat_wf.inputs.inputspec.TR=2.5
feat_wf.inputs.inputspec.hpcutoff=100
feat_wf.inputs.inputspec.subject_info = exp_info
feat_wf.inputs.inputspec.contrasts = [cont1]

feat_wfse=feat_wf.clone('feat_wfse')

feat_wffilt=feat_wf.clone('feat_wffilt')

metaflow = pe.Workflow(name='metaflow3')
metaflow.base_dir = basedir
metaflow.connect([(grabber, preproc_mag_wfge, [('datasource.mag', 'inputspec.input_mag')]),
                  (grabber, preproc_mag_wfse, [('datasource.semag', 'inputspec.input_mag')]),
                  (grabber, preproc_phase_wfge, [('datasource.phase', 'inputspec.input_phase')]),
                  (grabber, preproc_phase_wfge, [('datasource.mocophase', 'inputspec.moco_phase'),
                                                 (('datasource.mag', pickfirst), 'inputspec.ref_phase')]),
                  # (grabber, make_surfs_wf, [('datasource.struct','inputspec.struct'),
                  #                           ('infosource.subject_id', 'inputspec.subject_id'),
                  #                           ('datasource.normbrain', 'inputspec.normbrain')]),
                  (preproc_mag_wfge, preproc_phase_wfge, [('outputspec.motion_par', 'inputspec.motion_par'),
                                                          ('outputspec.run_txfm', 'inputspec.run_txfm'),
                                                          ('outputspec.mask_file', 'inputspec.mask_file')]),
                  (preproc_mag_wfge, bpftmag, [('outputspec.proc_mag', 'in_file')]),
                  (preproc_phase_wfge, bpftphase, [('outputspec.proc_phase', 'in_file')]),
                  (preproc_phase_wfge, phaseregress, [('outputspec.proc_phase', 'phase')]),
                  (preproc_mag_wfge, phaseregress, [('outputspec.proc_mag', 'mag')]),
                  (preproc_mag_wfge, phaseregress, [('outputspec.proc_mag', 'bpmag')]),
                  (preproc_phase_wfge, phaseregress, [('outputspec.proc_phase', 'bpphase')]),
                  (preproc_mag_wfge, feat_wf, [('outputspec.proc_mag', 'inputspec.input')]),
                  (preproc_mag_wfse, feat_wfse, [('outputspec.proc_mag', 'inputspec.input')]),
                  (phaseregress, feat_wffilt, [('filt', 'inputspec.input')]),
                  ])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 10})
