import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype.interfaces.io as nio
import nipype_phase_regression.interfaces as pr
import nipype.interfaces.utility as ul


def trim_nifti(in_file):
    from nipype.utils.filemanip import split_filename
    _, base, _ = split_filename(in_file)
    return base

def run_pipeline(file_list):
    base_dir = './SNRcalcs'

    infosource = pe.Node(interface=ul.IdentityInterface(fields=['file']),
                         name="infosource")
    infosource.iterables = [('file', file_list)]

    # convert image to float
    img2float = pe.Node(interface=fsl.ImageMaths(out_data_type='float', op_string='', suffix='_dtype'),name='img2float')

    detrend = pe.Node(interface=pr.DetrendMag(), name='detrend')

    # get the mean functional of run 1
    meanfunc = pe.Node(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       name='meanfunc')

    stdfunc = pe.Node(interface=fsl.ImageMaths(op_string='-Tstd',
                                                   suffix='_std'),
                          name='stdfunc')

    snrfunc = pe.Node(interface=fsl.ImageMaths(op_string='-div',
                                                   suffix='_tsnr'),
                          name='snrfunc')

    snrsink = pe.Node(nio.DataSink(base_directory='/cfmm/data/ostanle2/Ongoing_Work/NHP_PhantomTest2019/sink'), name='snrsink')
    snrsink.inputs.regexp_substitutions=[('_file_*/','')]

    tSNRflow =  pe.Workflow(name='tSNRflow')
    tSNRflow.base_dir=base_dir

    tSNRflow.connect([(infosource, img2float, [('file', 'in_file')]),
                        (img2float, detrend, [('out_file', 'mag')]),
                        (detrend, meanfunc, [('detrended_mag', 'in_file')]),
                        (detrend, stdfunc, [('detrended_mag', 'in_file')]),
                        (meanfunc, snrfunc, [('out_file', 'in_file')]),
                        (stdfunc, snrfunc, [('out_file', 'in_file2')]),
                        (snrfunc, snrsink, [('out_file', 'snr.@snr')]),
                        (meanfunc, snrsink, [('out_file', 'snr.@mean')]),
                        (stdfunc, snrsink, [('out_file', 'snr.@std')]),
                        ])

    tSNRflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 16})

if __name__=='__main__':
    from argparse import ArgumentParser, RawTextHelpFormatter
    parser = ArgumentParser(description=__doc__, formatter_class=RawTextHelpFormatter)
    parser.add_argument('file_list', default=['.*'], nargs='+', help='labels of sequences to analyze')
    args = parser.parse_args()

    file_list = args.file_list

    run_pipeline(file_list)
