# last working commit id 270a7ae552697f29bcc8b8089688c013f55d4bc0
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
from nipype_phase_regression.workflows.compcor import create_compcor_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
from numpy import linspace
from nipype.interfaces.base import Bunch
import nipype_phase_regression.interfaces as pr
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype_phase_regression.workflows.first_level_analysis_wf import create_first_level_analysis_wf
from nipype_phase_regression.workflows.compcor import create_compcor_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr

basedir = '/cfmm/data/ostanle2/MortonAnalysis/'
maglist = [basedir+'MortonRSmag.nii.gz']
phaselist = [basedir+'MortonRSphase.nii.gz']

# Initialize Magnitude Preprocessing
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

# Add inputs
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])
preproc_mag_wf.inputs.flipper.in_file=maglist

# Specify BET threshold (used defaults so was not needed)

# Initialize Phase Preprocessing
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

# Add inputs
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])
preproc_phase_wf.inputs.flipper.in_file=phaselist

# Set conversion parameters
preproc_phase_wf.inputs.prepPhase.siemens=[True]*len(maglist)
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# BET PHASE
betphase = pe.MapNode(interface=fsl.ApplyMask(), name='betphase', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=basedir

# Bandpass data
bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
bpftphase.inputs.highpass_sigma = 80
bpftphase.inputs.lowpass_sigma = 2

bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
bpftmag.inputs.highpass_sigma = 80
bpftmag.inputs.lowpass_sigma = 2

# Regress magnitude and phase
phaseregress = pe.MapNode(interface=pr.phaseRegressionODR(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = 0.686
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb = 0.15

# Blur the images to the fit gaussians (from matlab: FitPSFSlices.m on local) FWHM=1.6523 voxels
blursim = pe.MapNode(interface=pr.Nilearnblur(), name='blursim', iterfield=['in_file'])
blursim.inputs.fwhm=2

submeanfrommag = pe.MapNode(interface=fsl.maths.MultiImageMaths(), name='submeanfrommag', iterfield=['in_file','operand_files'])
submeanfrommag.inputs.op_string='-sub %s'

# crosscorrelate the mag and sim timecourse across time
crosscorr = pe.MapNode(interface=pr.crosscorrMagPhase(), name="crosscorr", iterfield=["mag", "sim"])

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = basedir

# Preprocess and motion correct
metaflow.connect([(preproc_phase_wf, betphase, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase, [('extractor.mask_file','mask_file')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('extractor.out_file', 'inputspec.input_mag')]),
                  (betphase, mcflirtphase_wf, [('out_file', 'inputspec.input_phase')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file','inputspec.mat_dir')]),
                  (mcflirtphase_wf, bpftphase, [('outputspec.out_file', 'in_file')]),
                  (preproc_mag_wf, bpftmag, [('extractor.out_file','in_file')]),
                  ])

# Perform phase regression
metaflow.connect([(mcflirtphase_wf, phaseregress, [('outputspec.out_file', 'phase')]),
                  (preproc_mag_wf, phaseregress, [('extractor.out_file', 'mag')]),
                  (bpftmag, phaseregress, [('out_file', 'bpmag')]),
                  (bpftphase, phaseregress, [('out_file', 'bpphase')])])

# Cross correlate
metaflow.connect([(phaseregress, blursim, [('sim','in_file')]),
                  (blursim, crosscorr, [('out_file', 'sim')]),
                  (preproc_mag_wf, submeanfrommag, [('extractor.out_file', 'in_file'),
                                                    ('meancalc.out_file', 'operand_files')]),
                  (submeanfrommag, crosscorr, [('out_file','mag')])])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)

metaflow_exec_graph = metaflow.run(plugin='MultiProc')