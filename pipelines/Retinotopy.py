from __future__ import print_function
from __future__ import division
from builtins import str
from builtins import range

import os                                    # system functions

import nipype.interfaces.io as io           # Data i/o
from nipype.interfaces import fsl, freesurfer, afni          # fsl
import nipype.interfaces.utility as ul     # utility
import nipype.pipeline.engine as pe          # pypeline engine
import nipype.algorithms.modelgen as model   # model generation
import nipype_phase_regression.interfaces as pr

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def getmiddlevolume(func):
    from nibabel import load
    funcfile = func
    if isinstance(func, list):
        funcfile = func[0]
    _, _, _, timepoints = load(funcfile).shape
    return int(timepoints / 2) - 1

def getthreshop(thresh):
    return '-thr %.10f -Tmin -bin' % (0.1 * thresh[0][1])

def getinormscale(medianvals):
    return ['-mul %.10f' % (10000. / val) for val in medianvals]

subject_list = ['001']

infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

info=dict(mag = [['subject_id']],
          struct = [['subject_id']],
          fmapmag = [['subject_id']],
          fmapdphase = [['subject_id']],
          fmapdphasewh = [['subject_id']],
          fmapmagwh = [['subject_id']],
          epi800 = [['subject_id']]
          )

datasource = pe.Node(interface=io.DataGrabber(infields=['subject_id'],
                                              outfields=['mag',
                                                         'struct',
                                                         'fmapmag',
                                                         'fmapdphase',
                                                         'fmapmagwh',
                                                         'fmapdphasewh',
                                                         'epi800'
                                                         ]),
                     name='datasource')
datasource.inputs.base_directory = '/cfmm/data/ostanle2/Ongoing_Work/ODC/retinotopy/orig/'
datasource.inputs.template = '*'
datasource.inputs.field_template = dict(mag='Run*Retino%s*.nii.gz',
                                        struct='Retino%smp2rage.nii.gz',
                                        fmapmag='Retino%sfieldmap_mag.nii.gz',
                                        fmapdphase='Retino%sfieldmap_dphase.nii.gz',
                                        fmapmagwh='Retino%sfieldmapwh_mag.nii.gz',
                                        fmapdphasewh='Retino%sfieldmapwh_dphase.nii.gz',
                                        epi800='Retino%sepi800.nii.gz'
                                        )
datasource.inputs.template_args = info
datasource.inputs.sort_filelist = True

# convert image to float
img2float = pe.MapNode(interface=fsl.ImageMaths(out_data_type='float', op_string='', suffix='_dtype'),
                       iterfield=['in_file'],
                       name='img2float')

# motion correct each run
volreg = pe.MapNode(interface = afni.Volreg(), name='volreg', iterfield='in_file')
volreg.inputs.outputtype='NIFTI_GZ'

# register each run to first volume of first run

# extract the middle volume of the first run
extract_ref = pe.Node(interface=fsl.ExtractROI(t_size=1), name='extract_ref')

# registration
align2first = pe.MapNode(interface=fsl.FLIRT(dof=6), name='align2first', iterfield=['in_file'])

# apply first volume registration
applyalign = pe.MapNode(interface=fsl.ApplyXfm(), name='applyalign', iterfield=['in_file', 'in_matrix_file'])
applyalign.inputs.apply_xfm=True

# get the mean functional of run 1
meanfunc = pe.Node(interface=fsl.ImageMaths(op_string='-Tmean',
                                            suffix='_mean'),
                   name='meanfunc')

# extract brain with fsl and save the mask
extractor = pe.Node(interface=fsl.BET(), name="extractor")
extractor.inputs.mask = True
extractor.inputs.padding = True
extractor.inputs.frac = 0.3

# apply the mask to all runs
maskfunc = pe.MapNode(interface=fsl.ImageMaths(suffix='_bet',
                                               op_string='-mas'),
                      iterfield=['in_file'],
                      name='maskfunc')

# Get 2 and 98th percentiles
getthresh = pe.MapNode(interface=fsl.ImageStats(op_string='-p 2 -p 98'),
                       iterfield=['in_file'],
                       name='getthreshold')

# Threshold the first run of the functional data at 10% of the 98th percentile
threshold = pe.Node(interface=fsl.ImageMaths(out_data_type='char',
                                             suffix='_thresh'),
                    name='threshold')

# get median value using the mask
medianval = pe.MapNode(interface=fsl.ImageStats(op_string='-k %s -p 50'),
                       iterfield=['in_file'],
                       name='medianval')

# dilate the mask
dilatemask = pe.Node(interface=fsl.ImageMaths(suffix='_dil',
                                              op_string='-dilF'),
                     name='dilatemask')

# mask the data with dilated mask
maskfunc2 = pe.MapNode(interface=fsl.ImageMaths(suffix='_mask',
                                                op_string='-mas'),
                       iterfield=['in_file'],
                       name='maskfunc2')

# get the mean from each run
meanfunc2 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc2')

# merge the mean and median
mergenode = pe.Node(interface=ul.Merge(2, axis='hstack'),
                    name='merge')

# scale the run to have a median of 10000
intnorm = pe.MapNode(interface=fsl.ImageMaths(suffix='_intnorm'),
                     iterfield=['in_file', 'op_string'],
                     name='intnorm')

# get the mean from each run
meanfunc3 = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean',
                                                suffix='_mean'),
                       iterfield=['in_file'],
                       name='meanfunc3')

# highpass the data
highpass = pe.MapNode(interface=fsl.ImageMaths(suffix='_tempfilt'),
                      iterfield=['in_file','in_file2'],
                      name='highpass')
hpcutoff = 100
TR = 2.
highpass.inputs.suffix = '_hpf'
highpass.inputs.op_string = '-bptf %d -1 -add ' % (hpcutoff / TR)

outputnode = pe.Node(ul.IdentityInterface(fields=['highpass', 'meanfunc']), name='outputnode')

preproc = pe.Workflow(name='preproc')
preproc.connect(img2float, 'out_file', volreg, 'in_file')
preproc.connect(volreg, ('out_file', pickfirst), extract_ref, 'in_file')
preproc.connect(volreg, 'out_file', align2first, 'in_file')
preproc.connect(extract_ref, 'roi_file', align2first, 'reference')
preproc.connect(extract_ref, 'roi_file', applyalign, 'reference')
preproc.connect(volreg, 'out_file', applyalign, 'in_file')
preproc.connect(align2first, 'out_matrix_file', applyalign, 'in_matrix_file')
preproc.connect(align2first, ('out_file', pickfirst), meanfunc, 'in_file')
preproc.connect(meanfunc, 'out_file', extractor, 'in_file')
preproc.connect(applyalign, 'out_file', maskfunc, 'in_file')
preproc.connect(extractor, 'mask_file', maskfunc, 'in_file2')
preproc.connect(maskfunc, 'out_file', getthresh, 'in_file')
preproc.connect(maskfunc, ('out_file', pickfirst), threshold, 'in_file')
preproc.connect(applyalign, 'out_file', medianval, 'in_file')
preproc.connect(threshold, 'out_file', medianval, 'mask_file')
preproc.connect(threshold, 'out_file', dilatemask, 'in_file')
preproc.connect(applyalign, 'out_file', maskfunc2, 'in_file')
preproc.connect(dilatemask, 'out_file', maskfunc2, 'in_file2')
preproc.connect(maskfunc2, 'out_file', meanfunc2, 'in_file')
preproc.connect(meanfunc2, 'out_file', mergenode, 'in1')
preproc.connect(medianval, 'out_stat', mergenode, 'in2')
preproc.connect(maskfunc2, 'out_file', intnorm, 'in_file')
preproc.connect(medianval, ('out_stat', getinormscale), intnorm, 'op_string')
preproc.connect(intnorm, 'out_file', meanfunc3, 'in_file')
preproc.connect(intnorm, 'out_file', highpass, 'in_file')
preproc.connect(meanfunc3, 'out_file', highpass, 'in_file2')
preproc.connect(highpass, 'out_file', outputnode, 'highpass')
preproc.connect(meanfunc, 'out_file', outputnode, 'out_file')

inputnode = pe.Node(ul.IdentityInterface(fields=['struct',
                                                   'fmapmag',
                                                   'fmapdphase',
                                                   'fmapmagwh',
                                                   'fmapdphasewh',
                                                   'epi800',
                                                   'epi600']), name='inputnode')

reoristruct = pe.Node(interface=fsl.Reorient2Std(), name='reoristruct')
reorifmop = pe.Node(interface=fsl.Reorient2Std(), name='reorifmop')
reorifmdpop = pe.Node(interface=fsl.Reorient2Std(), name='reorifmdpop')
reorifmwh = pe.Node(interface=fsl.Reorient2Std(), name='reorifmwh')
reorifmdpwh = pe.Node(interface=fsl.Reorient2Std(), name='reorifmdpwh')
reoriepi600 = pe.Node(interface=fsl.Reorient2Std(), name='reoriepi600')
reoriepi800 = pe.Node(interface=fsl.Reorient2Std(), name='reoriepi800')

betstruct = pe.Node(interface=fsl.BET(frac=0.3, vertical_gradient=-0.3), name='betstruct')

betfmop = pe.Node(interface=fsl.BET(), name='betfmop')

prepfmop = pe.Node(interface=fsl.PrepareFieldmap(), name='prepfmop')
prepfmop.inputs.delta_TE = 1.02

betfmwh = pe.Node(interface=fsl.BET(), name='betfmwh')

prepfmwh = pe.Node(interface=fsl.PrepareFieldmap(), name='prepfmwh')
prepfmwh.inputs.delta_TE = 1.02

# epireg600 = pe.Node(interface=fsl.EpiReg(), name='epireg600')
# epireg600.inputs.echospacing = 1.04/1000
# epireg600.inputs.pedir = 'y'

epireg800 = pe.Node(interface=fsl.EpiReg(), name='epireg800')
# epireg800.inputs.echospacing = 0.76/1000
# epireg800.inputs.pedir = 'x'


register = pe.Workflow(name='register')
register.connect([(inputnode, reoristruct, [('struct', 'in_file')]),
                  (inputnode, reorifmop, [('fmapmag', 'in_file')]),
                  (inputnode, reorifmdpop, [('fmapdphase', 'in_file')]),
                  (inputnode, reorifmwh, [('fmapmagwh', 'in_file')]),
                  (inputnode, reorifmdpwh, [('fmapdphasewh', 'in_file')]),
                  (inputnode, reoriepi600, [('epi600', 'in_file')]),
                  (inputnode, reoriepi800, [('epi800', 'in_file')]),
                  (reorifmdpop, prepfmop, [('out_file', 'in_phase')]),
                  (reorifmdpwh, prepfmwh, [('out_file', 'in_phase')]),
                  # (reorifmop, epireg600, [('out_file', 'fmapmag')]),
                  # (reorifmwh, epireg800, [('out_file', 'fmapmag')]),
                  # (reoriepi600, epireg600, [('out_file', 'epi')]),
                  (reoriepi800, epireg800, [('out_file', 'epi')]),
                  (reoristruct, betstruct, [('out_file', 'in_file')]),
                  # (reoristruct, epireg600, [('out_file', 't1_head')]),
                  # (betstruct, epireg600, [('out_file', 't1_brain')]),
                  (reorifmop, betfmop, [('out_file', 'in_file')]),
                  (reoristruct, epireg800, [('out_file', 't1_head')]),
                  (betfmop, prepfmop, [('out_file', 'in_magnitude')]),
                  (reorifmwh, betfmwh, [('out_file', 'in_file')]),
                  (betfmwh, prepfmwh, [('out_file', 'in_magnitude')]),
                  # (prepfmop, epireg600, [('out_fieldmap', 'fmap')]),
                  # (betfmop, epireg600, [('out_file', 'fmapmagbrain')]),
                  (betstruct, epireg800, [('out_file', 't1_brain')]),
                  # (prepfmwh, epireg800, [('out_fieldmap', 'fmap')]),
                  # (betfmwh, epireg800, [('out_file', 'fmapmagbrain')])
                  ])

retphase = pe.MapNode(interface=pr.RetinoPhase(), name='retphase', iterfield = ['in_file', 'desc', 'tstim'])
retphase.inputs.prestim=0
retphase.inputs.tstim=[19.5, 21, 21, 21]
retphase.inputs.desc=['-ccw', '-exp', '-clw', '-con']

ret2nii = pe.MapNode(interface=freesurfer.MRIConvert(), name='ret2nii', iterfield=['in_file'])
ret2nii.inputs.out_type='niigz'

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = '/workspace/ostanle2/retinotopy'

metaflow.connect([(infosource, datasource, [('subject_id', 'subject_id')]),
                  (datasource, preproc, [('mag', 'img2float.in_file'),
                                         (('mag', getmiddlevolume), 'extract_ref.t_min')]),
                  (datasource, register, [('struct', 'inputnode.struct'),
                                          ('fmapmag', 'inputnode.fmapmag'),
                                          ('fmapdphase', 'inputnode.fmapdphase'),
                                          ('fmapmagwh', 'inputnode.fmapmagwh'),
                                          ('fmapdphasewh', 'inputnode.fmapdphasewh'),
                                          ('epi800', 'inputnode.epi800')
                                          ]),
                  (preproc, register, [('outputnode.meanfunc', 'inputnode.epi600')]),
                  (preproc, retphase, [('outputnode.highpass', 'in_file')]),
                  (retphase, ret2nii, [('out_file', 'in_file')])
                  ])

# metaflow.run(updatehash=True)
metaflow.write_graph(graph2use='colored', format='png', simple_form=False)
metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 10})