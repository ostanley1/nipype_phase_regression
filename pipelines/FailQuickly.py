import nipype_phase_regression.interfaces as pr
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl

peristim = pe.MapNode(interface=pr.PeristimPlot(), name='peristim', iterfield=['in_file'])

peristim.inputs.TR = 2.5
peristim.inputs.activation_time = 15.0
peristim.inputs.rest_time = 15.0
peristim.inputs.blocks = 8
peristim.inputs.in_file = ['/workspace/ostanle2/SEvsGEBIDStest/metaflow/featpre_wsim/_subject_id_01/_noise_lb_0.15/thresh/' \
                           'mapflow/_thresh0/sub-01_task-checkerboard_acq-ge_part-mag_run-01_bold_dtype_volreg_st_flirt_bet_sim_mask_intnorm_hpf_thresh.nii.gz',
                           '/workspace/ostanle2/SEvsGEBIDStest/metaflow/featpre_wsim/_subject_id_01/_noise_lb_0.15/thresh/' \
                           'mapflow/_thresh1/sub-01_task-checkerboard_acq-ge_part-mag_run-02_bold_dtype_volreg_st_flirt_bet_sim_mask_intnorm_hpf_thresh.nii.gz']

meansignal = pe.MapNode(interface=fsl.ImageMaths(op_string='-Tmean'), name='meansignal', iterfield='in_file')

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir='/workspace/ostanle2/SEvsGEBIDStest/metaflow/_subject_01/noise_lb_0.15/'
metaflow.connect([(peristim, meansignal, [('out_file', 'in_file')])])
metaflow.run()