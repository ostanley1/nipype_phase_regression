# last working commit id 270a7ae552697f29bcc8b8089688c013f55d4bc0

from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import nipype_phase_regression.interfaces as pr

basedir = '/workspace/ostanle2/RegressionConfirm'
phaselist = [basedir+'/Run1PhaseSVD.nii.gz', basedir+'/Run2PhaseSVD.nii.gz', basedir+'/Run3PhaseSVD.nii.gz']
maglist = [basedir+'/Run1MagSVD.nii.gz', basedir+'/Run2MagSVD.nii.gz', basedir+'/Run3MagSVD.nii.gz']

# Initialize Magnitude Preprocessing
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

# Add inputs
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])
preproc_mag_wf.inputs.flipper.in_file=maglist

# Specify BET threshold
preproc_mag_wf.inputs.extractor.frac=0.15

# Initialize Phase Preprocessing
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

# Add inputs
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])
preproc_phase_wf.inputs.flipper.in_file=phaselist

# Set conversion parameters
preproc_phase_wf.inputs.prepPhase.siemens=[False, True]
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# BET PHASE
betphase = pe.MapNode(interface=fsl.ApplyMask(), name='betphase', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=basedir

# Regress magnitude and phase
phaseregress = pe.MapNode(interface=pr.phaseRegression(), name='phaseregress', iterfield=['phase','mag'])

metaflow = pe.Workflow(name='metaflowdemean')
metaflow.base_dir  = basedir
metaflow.connect([(preproc_phase_wf, betphase, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase, [('extractor.mask_file','mask_file')]),
                  (betphase, mcflirtphase_wf, [('out_file', 'inputspec.input_phase')]),
                  (preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file', 'inputspec.mat_dir'),
                                                     ('detrendmag.detrended_mag', 'inputspec.input_mag')]),
                  (mcflirtphase_wf,phaseregress, [('outputspec.out_file', 'phase')]),
                  (preproc_mag_wf, phaseregress, [('detrendmag.detrended_mag', 'mag')])
                  ])

metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'memory_gb' : 4})