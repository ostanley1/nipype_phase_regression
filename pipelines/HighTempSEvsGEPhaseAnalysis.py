import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr
from nipype.workflows.fmri.fsl import (create_modelfit_workflow,
                                       create_fixed_effects_flow)
import nipype.algorithms.modelgen as model
from nipype.interfaces.base import Bunch
import nipype.interfaces.utility as ul
import nipype.interfaces.io as nio
from nipype.interfaces import spm, fsl, afni
from nipype_phase_regression.workflows.preproc_mag_wf_SEvsGE import create_preproc_magsevsge_wf
from nipype_phase_regression.workflows.preproc_phase_wf_SEvsGE import create_preproc_phasesevsge_wf
from nipype_phase_regression.workflows.feat_wf import create_feat_preproc_wf, create_feat_stats_wf
from nipype_phase_regression.workflows.make_surf_wf import create_make_surf_wf
from nipype_phase_regression.workflows.register_wf import create_reg_wf, create_apply_reg_wf, create_apply_reg_susc_wf
from nipype_phase_regression.workflows.vessel_map_wf import create_venous_map_wf
from nipype_phase_regression.workflows.visuotopy_wf import create_visuotopy_wf
from bids.grabbids import BIDSLayout
# from fmriprep.workflows.fieldmap.phdiff import init_phdiff_wf
# from fmriprep.workflows.fieldmap.unwarp import init_sdc_unwarp_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
from nipype.algorithms.confounds import ACompCor
import nipype.interfaces.freesurfer as fsurf
from nipype import config
import nipype.interfaces.ants as ants

crash_dir = './crashes'

config.update_config({'execution': {'crashdump_dir': crash_dir,
                                    'crashfile_format': 'txt'
                                   } })

def pickfirst(files):
    if isinstance(files, list):
        return files[0]
    else:
        return files

def picksecond(files):
    if isinstance(files, list):
        return files[1]
    else:
        return files

def trimvisuotopy(files):
    return files[-2:]

def get_niftis(subject_id, data_dir, type, modality, tags):
    #tags is a list with the bids tag in it
    from bids.grabbids import BIDSLayout

    layout = BIDSLayout(data_dir)

    if type==[]:
        files = [f.filename for f in layout.get(subject=subject_id, modality=modality,  extensions=['nii', 'nii.gz'])]
    else:
        files = [f.filename for f in layout.get(subject=subject_id, type=type, extensions=['nii', 'nii.gz'])]

    filtfiles = []
    for j in range(len(files)):
        inc=True
        for i in range(len(tags)):
            if tags[i] not in files[j]:
                inc = False
        if inc:
            filtfiles.append(files[j])
    if len(filtfiles)==1:
        return filtfiles[0]
    else:
        return filtfiles

def getBIDSmeta(in_file):
    from bids.grabbids import BIDSLayout
    from nipype.utils.filemanip import split_filename

    if isinstance(in_file, list):
        in_file=in_file[0]

    path,_,_=split_filename(in_file)

    layout = BIDSLayout(path)

    return layout.get_metadata(in_file)

def extendMotion(in_file):
    import numpy as np
    from nipype.utils.filemanip import split_filename
    moco = np.loadtxt(in_file)
    moco_ext=np.zeros([moco.shape[0], 24])
    moco_ext[:,0:6] = moco**2
    moco_ext[1:,6:12] = np.diff(moco, axis=0)
    moco_ext[:,12:18] = moco_ext[:,6:12]**2
    moco_ext[:, 18:24] = moco
    path, base,_=split_filename(in_file)
    out_file = path+'/'+base+'_ext.tsv'
    np.savetxt(out_file, moco_ext, delimiter='\t')
    return out_file

def get_exp_info(file, mocofile, physfile, physbool):
    from nipype.interfaces.base import Bunch
    import numpy as np
    with open(mocofile, 'r+') as f:
        regressors = np.loadtxt(f)
    regressor_list=[]
    for col in range(regressors.shape[1]):
        regressor_list.append(list(regressors[:,col]))
    regress_names=['roll2', 'pitch2', 'yaw2', 'dS2', 'dL2', 'dP2',
                   'droll', 'dpitch', 'dyaw', 'ddS', 'ddL', 'ddP',
                   'droll2', 'dpitch2', 'dyaw2', 'ddS2', 'ddL2', 'ddP2',
                   'roll', 'pitch', 'yaw', 'dS', 'dL', 'dP']
    if physbool==True:
        with open(physfile, 'r+') as f:
            regressors = np.loadtxt(f, skiprows=1)
        for col in range(regressors.shape[1]):
            regressor_list.append(list(regressors[:, col]))
            regress_names.append('compcor'+str(col))
        print(regress_names)
        print(regressor_list)
    if 'checkerboard' in file:
        onsets = [15, 45, 75, 105, 135, 165, 195, 225]
        exp_info=Bunch(conditions=["Activation"], onsets=[onsets], durations=[[15] * 8],
                       regressor_names=regress_names, regressors=regressor_list)
        return exp_info
    elif 'visuotopy' in file:
        onsets_pos = [10, 60, 110]
        onsets_neg = [35, 85, 135]
        exp_info=Bunch(conditions=["CondA","CondB"],
                       onsets=[onsets_pos, onsets_neg],
                       durations=[[15] * 3, [15] * 3],
                       regressor_names=regress_names,
                       regressors=regressor_list)
        return exp_info
    else:
        return None


def get_cont(file):
    print(file)
    if 'shortchecker' in file:
        return [['Activation>Baseline', 'T', ["Activation"], [1.0]]]


def freesurf_args(subject_id):
    recon_args = {'01': '-cc-crs 127 122 181 -pons-crs 127 125 184 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA  -no-wsgcaatlas -wsthresh 45',
                  '02': '-cc-crs 132 133 182 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA  -no-wsgcaatlas -wsthresh 45',
                  '03': '-cc-crs 127 114 181 -pons-crs 127 166 182 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '04': '-cc-crs 138 118 183 -pons-crs 138 166 183 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '05': '-cc-crs 127 117 181 -pons-crs 127 167 183 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '06': '-cc-crs 127 125 180 -pons-crs 127 169 186 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '07': '-cc-crs 127 119 175 -pons-crs 127 173 173 -notalairach -noaseg -noparcstats -noparcstats2 -noparcstats3 -no_label_BA '}
    return recon_args[subject_id]

def freesurf_argsdefault(subject_id):
    recon_args = {'01': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA -no-wsgcaatlas -wsthresh 45',
                  '02': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA  -no-wsgcaatlas -wsthresh 45',
                  '03': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '04': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '05': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '06': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA ',
                  '07': '-notalairach -noparcstats -noparcstats2 -noparcstats3 -no_label_BA '}
    return recon_args[subject_id]

def compcor_argsdefault(subject_id):
    recon_args = {'01': ['fake.txt']*4,
                  '02': ['fake.txt']*5,
                  '03': ['fake.txt']*5,
                  '04': ['fake.txt']*5,
                  '05': ['fake.txt']*5,
                  '06': ['fake.txt']*5,
                  '07': ['fake.txt']*5}
    return recon_args[subject_id]

def compcorse_argsdefault(subject_id):
    recon_args = {'01': ['fake.txt']*5,
                  '02': ['fake.txt']*5,
                  '03': ['fake.txt']*5,
                  '04': ['fake.txt']*5,
                  '05': ['fake.txt']*5,
                  '06': ['fake.txt']*5,
                  '07': ['fake.txt']*5}
    return recon_args[subject_id]

def matchMagPhase(maglist, phaselist):
    import re
    newmag = []
    newphase = []
    for f in maglist:
        run = re.search("run-0\\d_", f)
        task = re.search("task-(.*)_acq", f)
        for test in phaselist:
            print(test, run.group(0), task.group(0))
            if run.group(0) in test and task.group(0) in test and 'acq-ge' in test:
                newmag.append(f)
                newphase.append(test)
                phaselist.remove(test)
    print(newmag)
    print(newphase)
    return newmag, newphase

def get_dir(files):
    from nipype.utils.filemanip import split_filename
    if isinstance(files, list):
        if not isinstance(files[0], list):
            path, _, _ = split_filename(files[0])
            return path
        else:
            path=[]
            for i in range(len(files)):
                matdir, _, _ = split_filename(files[i][0])
                path.append(matdir)
            return path
    else:
        path, _, _ = split_filename(files)
        return path

# grab the data
basedir = '/workspace/ostanle2/SEvsGEBIDSfilterhightr'
output_dir = '/cfmm/data/ostanle2/Ongoing_Work/bids/GEvSE'

subject_list=['08']
TR=0.2


infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id','recon_cmd']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

BIDSFuncGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id",
                                       "data_dir","type","modality","tags"],
                                   output_names=["bold"]), name="BIDSFuncGrabber", iterfield=["tags"])
BIDSFuncGrabber.inputs.data_dir = output_dir
BIDSFuncGrabber.inputs.type='bold'
BIDSFuncGrabber.inputs.modality=[]
BIDSFuncGrabber.inputs.tags=[['acq-ge','part-mag'], ['acq-ge', 'part-phase'], ['acq-se']]

BIDSGREGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id","data_dir","type","modality","tags"],
                                           output_names=["gre"]), name="BIDSGREGrabber", iterfield=["tags"])
BIDSGREGrabber.inputs.data_dir = output_dir
BIDSGREGrabber.inputs.type='GRE'
BIDSGREGrabber.inputs.modality=[]
BIDSGREGrabber.inputs.tags=[['part-mag'],['part-phase']]

physioGrabber = pe.Node(interface=nio.DataGrabber(infields=['subject_id'], outfields=['physio']), name='physioGrabber')
physioGrabber.inputs.base_directory = '/cfmm/data/ostanle2/Ongoing_Work/bids/GEvSE/derivatives/physIO/'
physioGrabber.inputs.template = '*'
physioGrabber.inputs.sort_filelist=True
physioGrabber.inputs.field_template = dict(physio='sub-%s/func/run%d/multiple_regressors.txt')
physioGrabber.inputs.template_args = dict(physio=[['subject_id', [1,2,3,4,5]]])

selectGEmag=pe.Node(interface=ul.Select(index=0),name="selectGEmag")
selectGEphase=pe.Node(interface=ul.Select(index=1),name="selectGEphase")
selectgremag=pe.Node(interface=ul.Select(index=0),name="selectgremag")
selectgrephase=pe.Node(interface=ul.Select(index=1),name="selectgrephase")

grabber = pe.Workflow(name='grabber')
grabber.connect(infosource, 'subject_id', BIDSFuncGrabber, 'subject_id')
grabber.connect(infosource, 'subject_id', BIDSGREGrabber, 'subject_id')
grabber.connect(infosource, 'subject_id', physioGrabber, 'subject_id')
grabber.connect(BIDSFuncGrabber, 'bold', selectGEmag, 'inlist')
grabber.connect(BIDSFuncGrabber, 'bold', selectGEphase, 'inlist')
grabber.connect(BIDSGREGrabber, 'gre', selectgremag, 'inlist')
grabber.connect(BIDSGREGrabber, 'gre', selectgrephase, 'inlist')

# preprocess GE data
preproc_mag_wfge = create_preproc_magsevsge_wf()
preproc_mag_wfge.base_dir=basedir
preproc_mag_wfge.inputs.inputspec.TR = TR

volreg = preproc_mag_wfge.get_node('volreg')
img2float = preproc_mag_wfge.get_node('img2float')
extract_ref = preproc_mag_wfge.get_node('extract_ref')
align2first = preproc_mag_wfge.get_node('align2first')
stc = preproc_mag_wfge.get_node('stc')
calcrel = preproc_mag_wfge.get_node('calcrel')
plotmc = preproc_mag_wfge.get_node('plotmc')
outputspec = preproc_mag_wfge.get_node('outputspec')
applyalign = preproc_mag_wfge.get_node('applyalign')
detrend = preproc_mag_wfge.get_node('detrend')
maskfunc = preproc_mag_wfge.get_node('maskfunc')
extractor = preproc_mag_wfge.get_node('extractor')

# swap 3dvolreg out for mcflirt with 2d, sinc interp and 10.. of padding
preproc_mag_wfge.disconnect([(img2float, volreg, [('out_file', 'in_file')]),
                        (volreg, extract_ref, [(('out_file', pickfirst), 'in_file')]),
                        (volreg, align2first, [('out_file', 'in_file')]),
                        (volreg, stc, [('out_file', 'in_file')]),
                        (volreg, calcrel, [('md1d_file', 'in_file')]),
                        (volreg, plotmc, [('oned_file', 'in_file')]),
                        (volreg, outputspec, [('oned_matrix_save', 'motion_par')]),
                        (volreg, outputspec, [('oned_file', 'motion_data')]),
                        (volreg, outputspec, [('md1d_file', 'maxdisp_data')]),
                        (extract_ref, align2first, [('roi_file', 'reference')]),
                        (extract_ref, applyalign, [('roi_file', 'reference')]),
                        (stc, applyalign, [('slice_time_corrected_file', 'in_file')]),
                        (align2first, applyalign, [('out_matrix_file', 'in_matrix_file')]),
                        (applyalign, detrend, [('out_file', 'mag')]),
                        (applyalign, maskfunc, [('out_file', 'in_file')]),
                        (extractor, maskfunc, [('mask_file', 'in_file2')]),
                        (maskfunc, outputspec, [('out_file', 'proc_mag')]),
                        (align2first, outputspec, [('out_matrix_file', 'run_txfm')]),
                        (extractor, outputspec, [('mask_file', 'mask_file')]),
                        (extractor, outputspec, [('out_file', 'mean_file')]),

])

preproc_mag_wfge.remove_nodes([volreg, applyalign, align2first, maskfunc])

volreg2 = pe.MapNode(interface=fsl.MCFLIRT(args="-2d -sinc_final -fov 10", save_mats=True), name="volreg2", iterfield='in_file')


preproc_mag_wfge.connect([(img2float, volreg2, [('out_file', 'in_file')]),
                          (volreg2, extract_ref, [(('out_file', pickfirst), 'in_file')]),
                          (volreg2, align2first, [('out_file', 'in_file')]),
                          (volreg2, stc, [('out_file', 'in_file')]),
                          (volreg2, outputspec, [('mat_file', 'motion_par')]),
                          (stc, detrend, [('slice_time_corrected_file', 'mag')]),
                          (detrend, outputspec, [('detrended_mag', 'proc_mag')])
])

# preprocess the phase sets
preproc_phase_wfge = create_preproc_phasesevsge_wf()
preproc_phase_wfge.base_dir = basedir
preproc_phase_wfge.inputs.inputspec.siemensbool = True

# swap moco for applyxfm
moco = preproc_phase_wfge.get_node('moco')
prepphase = preproc_phase_wfge.get_node('prepphase')
inputspec = preproc_phase_wfge.get_node('inputspec')
outputspec = preproc_phase_wfge.get_node('outputspec')
stc = preproc_phase_wfge.get_node('stc')
maskfunc = preproc_phase_wfge.get_node('maskfunc')
applyalign = preproc_phase_wfge.get_node('applyalign')

preproc_phase_wfge.disconnect([(inputspec, moco, [('motion_par', 'in_matrix')]),
                               (prepphase, moco, [('detrended_phase', 'in_file')]),
                               (moco, stc, [('out_file', 'in_file')]),
                               (inputspec, applyalign, [('run_txfm','in_matrix_file')]),
                               (stc, applyalign, [('slice_time_corrected_file', 'in_file')]),
                               (applyalign, maskfunc, [('out_file', 'in_file')]),
                               (inputspec, applyalign, [('ref_phase','reference')]),
                               (inputspec, maskfunc, [('mask_file', 'in_file2')]),
                               (maskfunc, outputspec,[('out_file', 'proc_phase')]),
                            ])

preproc_phase_wfge.remove_nodes([moco, applyalign, maskfunc])

# moco2 = pe.MapNode(interface=pr.ApplyXfm4D(), name="moco2", iterfield=["in_file", "ref_vol", "trans_dir"])
# moco2.inputs.four_digit=True

moco2 = create_mcflirt_phase_wf()

preproc_phase_wfge.connect([(inputspec, moco2, [('motion_par', 'inputspec.mat_dir')]),
                            (prepphase, moco2, [('detrended_phase', 'inputspec.input_phase'),
                                                ('detrended_phase', 'inputspec.input_mag')]),
                            (moco2, stc, [('outputspec.out_file', 'in_file')]),
                            (stc, outputspec, [('slice_time_corrected_file', 'proc_phase')])
                            ])

# need to confirm all magnitude and phase files match
sortmagphase = pe.Node(interface=ul.Function(['maglist', 'phaselist'], ['maglist', 'phaselist'], matchMagPhase), name='sortmagphase')

# regress out physio data
filterphysiomag = pe.MapNode(interface=fsl.FilterRegressor(), name="filterphysiomag", iterfield=['in_file', 'design_file'])
filterphysiomag.inputs.filter_all=True

filterphysiophase = filterphysiomag.clone('filterphysiophase')

# bandpass the magnitude and phase ge sets
# Bandpass data
bpftphase = pe.MapNode(interface=afni.Bandpass(), name="bpftphase", iterfield=["in_file"])
bpftphase.inputs.highpass = 0.01
bpftphase.inputs.lowpass = 0.5
bpftphase.inputs.outputtype='NIFTI_GZ'

bpftmag = pe.MapNode(interface=afni.Bandpass(), name="bpftmag", iterfield=["in_file"])
bpftmag.inputs.highpass = 0.01
bpftmag.inputs.lowpass = 0.5
bpftmag.inputs.outputtype='NIFTI_GZ'

addmeange = pe.MapNode(interface=fsl.MultiImageMaths(op_string='-add %s'), name='addmeange', iterfield=['in_file', 'operand_files'])
addmeansim = addmeange.clone('addmeansim')
addmeanfilt = addmeange.clone('addmeanfilt')

# Regress ge magnitude and phase
phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregressodr', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = TR
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.25
phaseregress.iterables = [('noise_lb', [0.3])]

# Start venous mask pipeline
vessel_map_wf = create_venous_map_wf()

mocomag = vessel_map_wf.get_node('mocomag')
mocopre = vessel_map_wf.get_node('mocopre')
convphase = vessel_map_wf.get_node('convphase')
inputspec = vessel_map_wf.get_node('inputspec')
extractor = vessel_map_wf.get_node('extractor')
avemagpre = vessel_map_wf.get_node('avemagpre')
unwrappre = vessel_map_wf.get_node('unwrappre')

vessel_map_wf.disconnect([(inputspec, mocomag, [('gremag', 'in_file'),('moco_pars', 'in_matrix')]),
                        (mocomag, extractor, [('out_file', 'in_file')]),
                        (mocomag, avemagpre, [('out_file', 'func')]),
                        (convphase, mocopre, [('out_file', 'in_file')]),
                        (inputspec, mocopre, [('moco_pars', 'in_matrix')]),
                        (mocopre, unwrappre, [('out_file', 'in_file')]),
                        ])

vessel_map_wf.remove_nodes([mocomag, mocopre])

mocovessels_phase = moco2.clone('mocovessels_phase')
mocovessels_mag = moco2.clone('mocovessels_mag')

vessel_map_wf.connect([(inputspec, mocovessels_mag, [('gremag', 'inputspec.input_mag'),('gremag', 'inputspec.input_phase'),('moco_pars', 'inputspec.mat_dir')]),
                    (mocovessels_mag, extractor, [('outputspec.out_file', 'in_file')]),
                    (mocovessels_mag, avemagpre, [('outputspec.out_file', 'func')]),
                    (convphase, mocovessels_phase, [('out_file', 'inputspec.input_mag'), ('out_file', 'inputspec.input_phase')]),
                    (inputspec, mocovessels_phase, [('moco_pars', 'inputspec.mat_dir')]),
                    (mocovessels_phase, unwrappre, [('outputspec.out_file', 'in_file')]),
                    ])


# Create peristim plots
peristim_ge = pe.MapNode(interface=pr.PeristimPlot(TR=TR, blocks=15, activation_time=5, rest_time=25),
                         name='peristim_ge', iterfield=['in_file'])
peristim_filt=peristim_ge.clone('peristim_filt')
peristim_sim=peristim_ge.clone('peristim_sim')

# convert to percent change
convert_peristimge = pe.MapNode(interface=fsl.MultiImageMaths(op_string='-div %s -mul 100'), name='convert_peristimge', iterfield=['in_file', 'operand_files'])
convert_peristimsim = convert_peristimge.clone('convert_peristimsim')
convert_peristimfilt = convert_peristimge.clone('convert_peristimfilt')

# Set up GLMs
onsets = np.linspace(0,30*14,15)+25
exp_info = [Bunch(conditions=["Activation"], onsets=[onsets], durations=[[5] * 15])]*5

contsetup = pe.MapNode(interface=ul.Function(function=get_cont, input_names=["file"],
                                  output_names=["cont"]), name="contsetup", iterfield=["file"])

contsetupfilt = contsetup.clone('contsetupfilt')
contsetupsim = contsetup.clone('contsetupsim')

featpre_wfge = create_feat_preproc_wf()
featpre_wfge.inputs.inputspec.TR=TR
featpre_wfge.inputs.inputspec.highpass=100

featfit_wfge = create_feat_stats_wf()
featfit_wfge.inputs.inputspec.TR=TR
featfit_wfge.inputs.inputspec.hpcutoff=100
featfit_wfge.inputs.inputspec.subject_info=exp_info

featpre_wffilt = featpre_wfge.clone('featpre_wffilt')
featpre_wfsim = featpre_wfge.clone('featpre_wfsim')

featfit_wffilt = featfit_wfge.clone('featfit_wffilt')
featfit_wfsim = featfit_wfge.clone('featfit_wfsim')

metaflow = pe.Workflow(name='metaflow')
metaflow.config['execution'] = {'remove_unnecessary_outputs': 'False'}
metaflow.base_dir = basedir
metaflow.connect([(grabber, sortmagphase,[('selectGEmag.out', 'maglist')]),
                  (grabber, sortmagphase, [('selectGEphase.out', 'phaselist')]),
                  (grabber, filterphysiomag, [('physioGrabber.physio', 'design_file')]),
                  (grabber, filterphysiophase, [('physioGrabber.physio', 'design_file')]),
                  (sortmagphase, preproc_mag_wfge, [('maglist', 'inputspec.input_mag')]),
                  (sortmagphase, preproc_phase_wfge, [('phaselist', 'inputspec.input_phase')]),
                  (sortmagphase, preproc_phase_wfge, [(('maglist', pickfirst), 'inputspec.ref_phase')]),
                  (sortmagphase, vessel_map_wf, [('maglist', 'inputspec.gremag'),
                                            ('phaselist', 'inputspec.grephase')]),
                  (preproc_mag_wfge, vessel_map_wf, [('outputspec.motion_par', 'inputspec.moco_pars'),
                                                     ('outputspec.mask_file', 'inputspec.mask_file')]),
                  (preproc_mag_wfge, preproc_phase_wfge, [('outputspec.motion_par', 'inputspec.motion_par'),
                                                          ('outputspec.run_txfm', 'inputspec.run_txfm'),
                                                          ('outputspec.mask_file', 'inputspec.mask_file')]),
                  (preproc_mag_wfge, addmeange, [('meanfunc.out_file', 'operand_files')]),
                  (preproc_mag_wfge, addmeanfilt, [('meanfunc.out_file', 'operand_files')]),
                  (preproc_mag_wfge, addmeansim, [('meanfunc.out_file', 'operand_files')]),
                  (preproc_mag_wfge, filterphysiomag, [('outputspec.proc_mag', 'in_file')]),
                  (preproc_phase_wfge, filterphysiophase, [('outputspec.proc_phase', 'in_file')]),
                  (filterphysiomag, bpftmag, [('out_file', 'in_file')]),
                  (filterphysiophase, bpftphase, [('out_file', 'in_file')]),
                  (filterphysiophase, phaseregress, [('out_file', 'phase')]),
                  (filterphysiomag, phaseregress, [('out_file', 'mag')]),
                  (bpftmag, phaseregress, [('out_file', 'bpmag')]),
                  (bpftphase, phaseregress, [('out_file', 'bpphase')]),
                  (bpftmag, addmeange, [('out_file', 'in_file')]),
                  (bpftmag, peristim_ge, [('out_file', 'in_file')]),
                  (phaseregress, peristim_filt, [('filt', 'in_file')]),
                  (phaseregress, peristim_sim, [('sim', 'in_file')]),
                  (peristim_ge, convert_peristimge, [('out_file', 'in_file')]),
                  (peristim_filt, convert_peristimfilt, [('out_file', 'in_file')]),
                  (peristim_sim, convert_peristimsim, [('out_file', 'in_file')]),
                  (preproc_mag_wfge, convert_peristimge, [('meanfunc.out_file', 'operand_files')]),
                  (preproc_mag_wfge, convert_peristimfilt, [('meanfunc.out_file', 'operand_files')]),
                  (preproc_mag_wfge, convert_peristimsim, [('meanfunc.out_file', 'operand_files')]),
                  (addmeange, featpre_wfge, [('out_file', 'inputspec.input')]),
                  (phaseregress, addmeanfilt, [('filt', 'in_file')]),
                  (phaseregress, addmeansim, [('sim', 'in_file')]),
                  (addmeanfilt, featpre_wffilt, [('out_file', 'inputspec.input')]),
                  (addmeansim, featpre_wfsim, [('out_file', 'inputspec.input')]),
                  (featpre_wfge, contsetup, [('outputspec.filtered_functional_data', 'file')]),
                  (contsetup, featfit_wfge, [('cont','inputspec.contrasts')]),
                  (featpre_wfge, featfit_wfge, [('outputspec.filtered_functional_data', 'inputspec.input'),
                                            ('outputspec.mean_file', 'inputspec.mean'),
                                            ('outputspec.scalingfactor', 'inputspec.scalingfactor')]),
                  (featpre_wffilt, contsetupfilt, [('outputspec.filtered_functional_data', 'file')]),
                  (contsetupfilt, featfit_wffilt, [('cont', 'inputspec.contrasts')]),
                  (featpre_wffilt, featfit_wffilt, [('outputspec.filtered_functional_data', 'inputspec.input'),
                                            ('outputspec.mean_file', 'inputspec.mean'),
                                            ('outputspec.scalingfactor', 'inputspec.scalingfactor')]),
                  (featpre_wfsim, contsetupsim, [('outputspec.filtered_functional_data', 'file')]),
                  (contsetupsim, featfit_wfsim, [('cont', 'inputspec.contrasts')]),
                  (featpre_wfsim, featfit_wfsim, [('outputspec.filtered_functional_data', 'inputspec.input'),
                                            ('outputspec.mean_file', 'inputspec.mean'),
                                            ('outputspec.scalingfactor', 'inputspec.scalingfactor')]),
                  ])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)

metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 28})
