# not committed yet
from nipype_phase_regression.workflows.preproc_mag_wf import create_preproc_mag_wf
from nipype_phase_regression.workflows.preproc_phase_wf import create_preproc_phase_wf
from nipype_phase_regression.workflows.mcflirt_phase_wf import create_mcflirt_phase_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
import numpy as np
import nipype_phase_regression.interfaces as pr

basedir = '/cfmm/data/ostanle2/ODCanalysis'
rawdir = '/cfmm/data/ostanle2/RAWDATA/2016_09_16_ReconODC/'
maglist = [rawdir + '20160916_EPI1_Mag.nii.gz', rawdir + '20160916_EPI2_Mag.nii.gz', rawdir + '20160916_EPI3_Mag.nii.gz', '/cfmm/data/ostanle2/magODCtest.nii.gz']
phaselist = [rawdir + '20160916_EPI1_Phase.nii.gz', rawdir + '20160916_EPI2_Phase.nii.gz', rawdir + '20160916_EPI3_Phase.nii.gz', '/cfmm/data/ostanle2/phaseODCtest.nii.gz']

# Initialize Magnitude Preprocessing
preproc_mag_wf = create_preproc_mag_wf()
preproc_mag_wf.base_dir=basedir

# Add inputs
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('converter'), 'out_file', preproc_mag_wf.get_node('dataCleaner'), 'nii')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('dataCleaner'), 'fixednii', preproc_mag_wf.get_node('flipper'), 'in_file')
convert=preproc_mag_wf.get_node('converter')
preproc_mag_wf.remove_nodes([convert])
clean=preproc_mag_wf.get_node('dataCleaner')
preproc_mag_wf.remove_nodes([clean])
preproc_mag_wf.inputs.flipper.in_file=maglist

# Specify BET threshold
preproc_mag_wf.inputs.extractor.frac=0.2
preproc_mag_wf.inputs.extractor.vertical_gradient=0
preproc_mag_wf.inputs.extractor.radius=110

# Initialize Phase Preprocessing
preproc_phase_wf = create_preproc_phase_wf()
preproc_phase_wf.base_dir=basedir

# Add inputs
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('converter'), 'out_file', preproc_phase_wf.get_node('dataCleaner'), 'nii')
preproc_phase_wf.disconnect(preproc_phase_wf.get_node('dataCleaner'), 'fixednii', preproc_phase_wf.get_node('flipper'), 'in_file')
convert=preproc_phase_wf.get_node('converter')
preproc_phase_wf.remove_nodes([convert])
clean=preproc_phase_wf.get_node('dataCleaner')
preproc_phase_wf.remove_nodes([clean])
preproc_phase_wf.inputs.flipper.in_file=phaselist

preproc_mag_wf.disconnect(preproc_mag_wf.get_node('flipper'), 'out_file', preproc_mag_wf.get_node('mcflirter'), 'in_file')
preproc_mag_wf.disconnect(preproc_mag_wf.get_node('mcflirter'), 'out_file', preproc_mag_wf.get_node('detrendmag'), 'mag')
mcflirt=preproc_mag_wf.get_node('mcflirter')
preproc_mag_wf.remove_nodes([mcflirt])
preproc_mag_wf.connect(preproc_mag_wf.get_node('flipper'), 'out_file', preproc_mag_wf.get_node('detrendmag'), 'mag')

# Set conversion parameters
preproc_phase_wf.inputs.prepPhase.siemens=[True] * len(phaselist)
preproc_phase_wf.inputs.prepPhase.bit_depth = 12

# BET PHASE
betphase = pe.MapNode(interface=fsl.ApplyMask(), name='betphase', iterfield=['in_file','mask_file'])

# MCFLIRT PHASE
mcflirtphase_wf = create_mcflirt_phase_wf()
mcflirtphase_wf.base_dir=basedir

# Bandpass data
bpftphase = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftphase", iterfield=["in_file"])
bpftphase.inputs.highpass_sigma = 80
bpftphase.inputs.lowpass_sigma = 2

bpftmag = pe.MapNode(interface=fsl.maths.TemporalFilter(), name="bpftmag", iterfield=["in_file"])
bpftmag.inputs.highpass_sigma = 80
bpftmag.inputs.lowpass_sigma = 2

# Regress magnitude and phase
phaseregress = pe.MapNode(interface=pr.PhaseFitOdr(), name='phaseregress', iterfield=['phase','mag', 'bpphase', 'bpmag'])
phaseregress.inputs.TR = 1.25
phaseregress.inputs.sig_lb = 0.01
phaseregress.inputs.sig_ub = 0.1
phaseregress.inputs.noise_lb = 0.15

metaflow = pe.Workflow(name='metaflow')
metaflow.base_dir  = basedir

# Preprocess and motion correct
metaflow.connect([(preproc_phase_wf, betphase, [('prepPhase.detrended_phase', 'in_file')]),
                  (preproc_mag_wf, betphase, [('extractor.mask_file','mask_file')]),
                  #(preproc_mag_wf, mcflirtphase_wf, [('extractor.out_file', 'inputspec.input_mag')]),
                  #(betphase, mcflirtphase_wf, [('out_file', 'inputspec.input_phase')]),
                  #(preproc_mag_wf, mcflirtphase_wf, [('mcflirter.mat_file','inputspec.mat_dir')]),
                  (betphase, bpftphase, [('out_file', 'in_file')]),
                  # (preproc_mag_wf, level1_wf, [('extractor.out_file', 'makemodel.functional_runs'),
                  #                            ('extractor.out_file', 'modelfit.inputspec.functional_data')]),
                  (preproc_mag_wf, bpftmag, [('extractor.out_file','in_file')])])

# Perform phase regression
metaflow.connect([(preproc_phase_wf, phaseregress, [('prepPhase.detrended_phase', 'phase')]),
                  (preproc_mag_wf, phaseregress, [('extractor.out_file', 'mag')]),
                  (bpftmag, phaseregress, [('out_file', 'bpmag')]),
                  (bpftphase, phaseregress, [('out_file', 'bpphase')])])

metaflow.write_graph(graph2use='colored', format='png', simple_form=False)

metaflow_exec_graph = metaflow.run(plugin='MultiProc', plugin_args = {'n_procs' : 4})