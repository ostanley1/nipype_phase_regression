# this line is here to allow for fmriprep compatibility
# must be removed is niworkflows moves to a requirement
import niworkflows.nipype as nipype

from fmriprep.workflows.fieldmap.phdiff import init_phdiff_wf
import nipype.pipeline.engine as pe
import nipype.interfaces.utility as ul

def get_niftis(subject_id, data_dir, type, modality, tags):
    #tags is a list with the bids tag in it
    from bids.grabbids import BIDSLayout

    layout = BIDSLayout(data_dir)

    if type==[]:
        files = [f.filename for f in layout.get(subject=subject_id, modality=modality,  extensions=['nii', 'nii.gz'])]
    else:
        files = [f.filename for f in layout.get(subject=subject_id, type=type, extensions=['nii', 'nii.gz'])]

    filtfiles = []
    for j in range(len(files)):
        inc=True
        for i in range(len(tags)):
            if tags[i] not in files[j]:
                inc = False
        if inc:
            filtfiles.append(files[j])
    if len(filtfiles)==1:
        return filtfiles[0]
    else:
        return filtfiles


subject_list=['05']
basedir='.'
output_dir = '/cfmm/data/ostanle2/Ongoing_Work/bids/GEvSE'

infosource = pe.Node(interface=ul.IdentityInterface(fields=['subject_id','recon_cmd']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]

BIDSfmapGrabber = pe.MapNode(ul.Function(function=get_niftis, input_names=["subject_id","data_dir","type","modality","tags"],
                                           output_names=["fmap"]), name="BIDSfmapGrabber", iterfield=["tags"])
BIDSfmapGrabber.inputs.data_dir = output_dir
BIDSfmapGrabber.inputs.type=[]
BIDSfmapGrabber.inputs.modality='fmap'
BIDSfmapGrabber.inputs.tags=[['magnitude'],['phasediff']]

selectfmapmag=pe.Node(interface=ul.Select(index=0),name="selectfmapmag")
selectfmapphase=pe.Node(interface=ul.Select(index=1),name="selectfmapphase")

grabber = pe.Workflow(name='grabber')
grabber.connect(infosource, 'subject_id', BIDSfmapGrabber, 'subject_id')
grabber.connect(BIDSfmapGrabber, 'fmap', selectfmapmag, 'inlist')
grabber.connect(BIDSfmapGrabber, 'fmap', selectfmapphase, 'inlist')

# process the fieldmap
phasediff_wf = init_phdiff_wf(omp_nthreads=2, name="phasediff_wf")
phasediff_wf.base_dir = basedir
print(phasediff_wf.list_node_names())
print(grabber.list_node_names())

metaflow = pe.Workflow(name='metaflow')
metaflow.config['execution'] = {'remove_unnecessary_outputs': 'False'}
metaflow.base_dir = basedir
metaflow.connect([(grabber, phasediff_wf, [('selectfmapmag.out', 'inputnode.magnitude'),
                                       ('selectfmapphase.out', 'inputnode.phasediff')])])

metaflow.run()
