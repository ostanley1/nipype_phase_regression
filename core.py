import psutil


def calc_pool_size():
    cpustate = psutil.cpu_percent(percpu=True)
    return int((len(cpustate) - sum([x>50 for x in cpustate]))/ 6) # six should be changed to (number of map nodes + 5)
    # This number is owned by node not interface, need multiprocessing equipped node to use that

# Borrowed from Alan Kuurstra
def getoutimg(exec_graph, node, output_name, img_num=-1):
    #build the heirarchical name for the desired node
    hierarchyName=str(node)
    while type(node)==nipype.pipeline.engine.Workflow:
        node=node.get_node(output_name.split(".")[0])
        hierarchyName=hierarchyName+"."+node.name
        output_name=".".join(output_name.split(".")[1:])
    #get all nodes from the workflow
    nodelist=exec_graph.nodes()
    #get all nodes hierarchical string
    str_nodelist=np.array(nodelist,dtype=str)
    for i in range(len(str_nodelist)):
        if str_nodelist[i]==hierarchyName:
            break
    if not str_nodelist[i]==hierarchyName:
        print("getoutimg: Couldn't find node "+str(node))
        return
    else:
        imglocs=nodelist[i].get_output(output_name)
        if type(imglocs)==str:
            imglocs=[imglocs]
        if img_num>-1:
            imglocs=[imglocs[img_num]]
        imgs=[]
        for imgloc in imglocs:
            try:
                #load the nifti file and get the image as a numpy array
                imgs.append(nibabel.load(imgloc).get_data())
            except Exception as e:
                print("getoutimg: "+e.__str__())
                return
        if len(imgs)==1:
            imgs=imgs[0]
        return imgs

def getoutimglocs(exec_graph, node, output_name, img_num=-1):
    #build the heirarchical name for the desired node
    hierarchyName=str(node)
    while type(node)==nipype.pipeline.engine.Workflow:
        node=node.get_node(output_name.split(".")[0])
        hierarchyName=hierarchyName+"."+node.name
        output_name=".".join(output_name.split(".")[1:])
    #get all nodes from the workflow
    nodelist=exec_graph.nodes()
    #get all nodes hierarchical string
    str_nodelist=np.array(nodelist,dtype=str)
    for i in range(len(str_nodelist)):
        if str_nodelist[i]==hierarchyName:
            break
    if not str_nodelist[i]==hierarchyName:
        print("getoutimg: Couldn't find node "+str(node))
        return
    else:
        imglocs=nodelist[i].get_output(output_name)
        return imglocs

def fslview(file_location_list):
    command="fslview "+ ' '.join([n for n in file_location_list]) + " &"
    os.system(command)
def fsloverlay(file1,file2):
    command="fslview "+ file1+" "+file2+" -l Cool -t 0.6 &"
    os.system(command)